<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * list bugs.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');
require('Include/FuncImportOutport.php');
@ini_set('memory_limit', -1);

// the flag of using custom field
$hasCustomField = true;
$ProjectID = testGetCurrentProjectId();
if($_GET['ProjectID'] - 0 > 0)
{
    $_SESSION['BugQueryCondition'] = "ProjectID = '{$_GET['ProjectID']}'";
}


if($_GET['ModuleID'] - 0 > 0)
{
    $_SESSION['TestCurrentModuleID'] = $_GET['ModuleID'];
    $_SESSION['BugQueryCondition'] = "ModuleID IN ({$_GET['ChildModuleIDs']})";
}

if($_GET['ActionType']=='FromNotifyEmail')
{
      $TopModuleID = $_GET['TopModuleID'];
      $ChildModuleList = testGetProjectModuleList($_GET['ProjectID'], 'Bug');//获取所有Module，以及父子关
      $ChildModules = $ChildModuleList[$TopModuleID]['ChildIDs'];//获得当前Module的所有子Module
      if($TopModuleID>=0)
      {
         $_SESSION['BugQueryCondition'] = str_replace("\'", "'", $_GET['Condition'])." AND ProjectID={$_GET[ProjectID]} AND ModuleID in({$ChildModules})";
      }
      else
      {
         $_SESSION['BugQueryCondition'] = str_replace("\'", "'", $_GET['Condition'])." AND ProjectID={$_GET[ProjectID]}";
      }
      $_SESSION['TestMode'] ='Bug';
}

if($_POST['PostQuery'])
{
    $hasCustomField = false;
    $_SESSION['hasBugCustomField'] = false;
    $QueryStr = baseGetGroupQueryStr($_POST);
    $_SESSION['BugQueryCondition'] = $QueryStr;
    $_SESSION['BugQueryTitle'] = '';
    $_SESSION['BugAndOrListCondition'] = baseGetAndOrListStr($_POST);
    $_SESSION['BugFieldListCondition'] = baseGetFieldListStr($_POST);
    $_SESSION['BugOperatorListCondition'] = baseGetOperatorListStr($_POST);
    $_SESSION['BugValueListCondition'] = baseGetValueListStr($_POST);
    $_SESSION['BugLeftParenthesesListCondition'] = baseGetLeftParentheseListStr($_POST);
    $_SESSION['BugRightParenthesesListCondition'] = baseGetRightParentheseListStr($_POST);
    $arr = array_diff($_SESSION['BugFieldListCondition'], array_keys($_LANG['BugQueryField']));
    if(!empty($arr))
    {
        $hasCustomField = $_SESSION['hasBugCustomField']= true;
    }
    unset($arr);
}

if($_REQUEST['QueryID'])
{
    $hasCustomField = false;
    $_SESSION['hasBugCustomField'] = false;
    $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='Bug'");
    $_SESSION['BugQueryCondition'] = $QueryInfo['QueryString'];
    $_SESSION['BugQueryTitle'] = $QueryInfo['QueryTitle'];
    $_SESSION['BugAndOrListCondition'] = unserialize($QueryInfo['AndOrList']);
    $_SESSION['BugFieldListCondition'] = unserialize($QueryInfo['FieldList']);
    $_SESSION['BugOperatorListCondition'] = unserialize($QueryInfo['OperatorList']);
    $_SESSION['BugValueListCondition'] = unserialize($QueryInfo['ValueList']);
    $_SESSION['BugFieldsToShow'] = $QueryInfo['FieldsToShow'];
    $_SESSION['BugLeftParenthesesListCondition'] = unserialize($QueryInfo['LeftParentheses']);
    $_SESSION['BugRightParenthesesListCondition'] = unserialize($QueryInfo['RightParentheses']);
    $arr = array_diff($_SESSION['BugFieldListCondition'], array_keys($_LANG['BugQueryField']));
    if(!empty($arr))
    {
        $hasCustomField = $_SESSION['hasBugCustomField']= true;
    }
    unset($arr);
}
else
{
   $_REQUEST['QueryID'] = '-1';
}

$WHERE = array();
$URL = array();

$WHERE[] = $_SESSION['TestUserACLSQL'];

if($_SESSION['BugQueryCondition'] != '')
{
    $WHERE[] = $_SESSION['BugQueryCondition'];
}

//排序GET命令处理代码
if($_GET['OrderBy'])
{
    $OrderByList = explode('|', $_GET['OrderBy']);
    $OrderByColumn = $OrderByList[0];
    $OrderByType = $OrderByList[1];
    $OrderBy = join(' ', $OrderByList);
    $URL[] = 'OrderBy=' . $_GET['OrderBy'];
    $_SESSION['BugOrderBy']['OrderBy'] = $OrderBy;
    $_SESSION['BugOrderBy']['OrderByColumn'] = $OrderByColumn;
    $_SESSION['BugOrderBy']['OrderByType'] = $OrderByType;
}
else
{
    if(empty($_SESSION['BugOrderBy']))
    {
        $_SESSION['BugOrderBy']['OrderBy'] = ' BugID DESC';
        $_SESSION['BugOrderBy']['OrderByColumn'] = 'BugID';
        $_SESSION['BugOrderBy']['OrderByType'] = 'DESC';
    }
    $OrderBy = $_SESSION['BugOrderBy']['OrderBy'];
    $OrderByColumn = $_SESSION['BugOrderBy']['OrderByColumn'];
    $OrderByType = $_SESSION['BugOrderBy']['OrderByType'];
}

if($_GET['QueryMode'])
{
    $hasCustomField = false;
    $_SESSION['hasBugCustomField'] = false;
    $QueryModeList = explode('|', $_GET['QueryMode']);
    $QueryColumn = $QueryModeList[0];
    $QueryValue = $QueryModeList[1];
    $WHERE = array();
    $WHERE[] = $_SESSION['TestUserACLSQL'];
    $QueryCondition = "";
    if(preg_match('/date/i', $QueryColumn))
    {
        $QueryCondition =  $QueryColumn . ' ' . sysStrToDateSql($QueryValue);
    }
    else
    {
        $QueryCondition = "{$QueryColumn}='{$QueryValue}'";
    }
    $_SESSION['BugQueryCondition'] = $QueryCondition;
    $_SESSION['BugQueryColumn'] = $QueryColumn;
    $WHERE[] = $QueryCondition;
    $URL[] = 'QueryMode=' . sysStripSlash($_GET['QueryMode']);
    // check if is set custom field for special function
    if(!array_key_exists($QueryColumn, $_LANG['BugQueryField']))
    {
        $hasCustomField = $_SESSION['hasBugCustomField'] = true;
    }
}
$Url = '?' . join('&', $URL);
$WHERE[] = "IsDroped = '0'";
$Where = join(' AND ', $WHERE);

if(isset($_SESSION['hasBugCustomField']))
{
    $hasCustomField = $_SESSION['hasBugCustomField'];
}
$FieldsToShow = testSetCustomFields('Bug', $ProjectID, $hasCustomField);
if(!array_key_exists($OrderByColumn, $FieldsToShow))
{
    $OrderBy = 'BugID DESC';
    $OrderByColumn = 'BugID';
    $OrderByType = 'DESC';
}

/* Get pagination */
$Pagination = new Page('BugInfo', '', '', '', 'WHERE ' . $Where . ' ORDER BY ' . $OrderBy, $Url, $MyDB);
$LimitNum = $Pagination->LimitNum();
$TPL->assign('PaginationDetailInfo', $Pagination->getDetailInfo());
$ColumnArray = @array_keys($FieldsToShow);
$Columns = 'BugID,BugStatus,' . join(',',$ColumnArray);


$OrderColumnList = $ColumnArray;
$OrderByTypeList = array();
foreach($OrderColumnList as $OrderColumn)
{
    if($OrderColumn == $OrderByColumn)
    {
        $OrderByTypeList[$OrderColumn] = $OrderTypeReverseArray[$OrderByType];
    }
    else
    {
        $OrderByTypeList[$OrderColumn] = $OrderByType;
    }
}

//BugList的导入处理代码
if($_FILES['file']['error']==2)	//导入成功
{
      sysObFlushJs("alert('{$_LANG['ImportFileExceed']}');");
}
elseif($_FILES['file']['error']==0)
{
        if($_FILES['file']['tmp_name'])   // 如果文件存在则进行批量导入
        {
	  				$findtype=strtolower(strrchr($_FILES['file']['name'],"."));

          	if($findtype!='.xml')	//文件必须以.xml结尾
          	{
            		sysObFlushJs("alert('{$_LANG['ImportFileSuffixError']}');");
          	}
          	else 
          	{
          			//读取文件内容到FileContents
		  					$file=fopen($_FILES['file']['tmp_name'],"r");
		  					$FileContents = fread($file, filesize($_FILES['file']['tmp_name']));
		  					//Rainy_Debug($FileContents);
		  					$FileContents = preg_replace('/<Font.*>|<\/Font>/Us', '', $FileContents);
								//将文件内容解析成变量，并存储在Dom变量中	
		            $Dom =  XML_unserialize($FileContents);
    		        $DomRowArray = $Dom[Workbook][Worksheet][Table][Row];
								//解析DomRowArray 并导入至数据库中
            		ParseImportBugXML($DomRowArray,$_LANG);
        	}
    		}
}

if($_GET['Export'] == 'HtmlTable')
{
    $Columns = '';
    $LimitNum = '';
}

$BugListSql = dbGetListSql(dbGetPrefixTableNames('BugInfo'), '', $Where, '', $OrderBy, $LimitNum);
if($Where != $_SESSION['OldBugQueryWhereStr'])
{
    $_SESSION['OldBugQueryWhereStr'] = $Where;
    if(!($_GET['QueryMode']))
    {
        $_SESSION['BugQueryColumn'] = '';
    }
}
if(($ProjectID != null) && ($hasCustomField))
{
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="Bug"]/field');
        if($fields)
        {
            $tableName = testGetFieldTable('Bug', $ProjectID);
            $Pagination = new Page(dbGetPrefixTableNames('BugInfo') . ' LEFT JOIN ' . $tableName .' ON ' . dbGetPrefixTableNames('BugInfo') . '.BugID = ' . $tableName . '.FieldID', '', '', '', 'WHERE ' . $Where . ' ORDER BY ' . $OrderBy, $Url, $MyDB, false);
            $LimitNum = $Pagination->LimitNum();
            $TPL->assign('PaginationDetailInfo', $Pagination->getDetailInfo());
            $Where .= ' AND ProjectID = ' . $ProjectID;
            $sql =  dbGetListSql(dbGetPrefixTableNames('BugInfo') . ',' . $tableName, '', $Where, '', $OrderBy, $LimitNum);
            $BugListSql = str_replace(dbGetPrefixTableNames('BugInfo') . ',' . $tableName, dbGetPrefixTableNames('BugInfo') . ' LEFT JOIN ' . $tableName .' ON ' . dbGetPrefixTableNames('BugInfo') . '.BugID = ' . $tableName . '.FieldID', $sql);
        }
    }
}

$BugList = dbGetListBySql($BugListSql);
$UserNameList = testGetOneDimUserList();
$BugList = testSetBugListMultiInfo($BugList, $UserNameList);
//print_r($BugList);exit; 

$TPL->assign('BugList', $BugList);

$TPL->assign('OrderByColumn', $OrderByColumn);
$TPL->assign('OrderByType', $OrderByType);
$TPL->assign('QueryMode', $_GET['QueryMode']);
$TPL->assign('QueryColumn', $_SESSION['BugQueryColumn']);
$TPL->assign('OrderByTypeList', $OrderByTypeList);
$TPL->assign('BaseTarget', '_self');
$TPL->assign('TestMode', 'Bug');

if($_GET['Export'] == 'HtmlTable')
{
    $TPL->assign('DataList', $BugList);
    $TPL->assign('FieldsToShow', $_LANG["BugFields"]);
    $TPL->display('ExportList.tpl');
    exit;
}

//BugList导出命令处理代码
if($_GET['Export'] == 'XMLFile')  //批量导出Bug
{
   $BugExportList = dbGetList('BugInfo','', $Where, '', $OrderBy);
   $BugCount = sizeof($BugExportList);


   if($BugCount >5000 ){
     sysObFlushJs("alert('{$_LANG['ExportCountExceed']}');");
   }
   else{
       $BugExportColumnMust = array('BugID' , 'BugTitle', 'ProjectName', 'ModulePath', 'ReproSteps'); //必须包含的字段

      $BugExportColumnArray = $ColumnArray;
      foreach($BugExportColumnMust as $Item)
      {
         if(!in_array($Item, $BugExportColumnArray))
            $BugExportColumnArray[] = $Item;

      }

      $URL=$_CFG['File']['UploadDirectory'] . "/buglist.xml";
      $File = fopen($URL,"w");

      $Content = ExportXML($BugExportList,$BugExportColumnArray,$_LANG['BugFields']);

      file_put_contents($URL, $Content);
      header('Content-type: text/xml; charset=utf-8');
      header('Content-Disposition: attachment; filename=buglist.xml');
      readfile($URL);
      exit;
   }
}
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('QueryID', $_REQUEST['QueryID']);
$TPL->assign('QueryTitle', $_SESSION['BugQueryTitle'] );
$TPL->display('BugList.tpl');
?>