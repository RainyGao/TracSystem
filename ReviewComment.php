<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * view, new, edit, resolve, close, activate a review.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

/*** Local APIs ****/
function GetReviewCommentIdInfoList($ReviewID)
{
		//Set WhereStr 
    if(empty($_SESSION['ReviewCommentQueryCondition']))
    {
    		$WhereStr = "ReviewID={$ReviewID}";
		}
		else
		{
				$WhereStr = "{$_SESSION['TestUserACLSQL']} AND {$_SESSION['ReviewCommentQueryCondition']}";
		}
		
		//Set OrderByStr 
    if(empty($_SESSION['ReviewCommentOrderBy']))
    {
        $_SESSION['ReviewCommentOrderBy']['OrderBy'] = ' ReviewCommentID DESC';
        $_SESSION['ReviewCommentOrderBy']['OrderByColumn'] = 'ReviewCommentID';
        $_SESSION['ReviewCommentOrderBy']['OrderByType'] = 'DESC';
    }
    if('ReviewCommentID' == $_SESSION['ReviewCommentOrderBy']['OrderByColumn'])
    {
        $OrderByStr = "{$_SESSION['ReviewCommentOrderBy']['OrderByColumn']} {$_SESSION['ReviewCommentOrderBy']['OrderByType']}";
    }
    else
    {
        $OrderByStr = "{$_SESSION['ReviewCommentOrderBy']['OrderByColumn']} {$_SESSION['ReviewCommentOrderBy']['OrderByType']},ReviewCommentID ASC";
    }
    $ReviewCommentIdInfoList = dbGetList('ReviewCommentInfo','ReviewCommentID',$WhereStr,'',$OrderByStr,'');
    return $ReviewCommentIdInfoList;
}

function BuildHtmlReviewCommentTypeSelectList($ReviewCommentTypeList,$ReviewCommentInfo)
{
	$ReviewCommentTypeSelectList = htmlSelect($ReviewCommentTypeList, 'ReviewCommentType', '', $ReviewCommentInfo['ReviewCommentType'], 'class="MyInput RequiredField"');
	return $ReviewCommentTypeSelectList;
}

function BuildHtmlReviewCommentOwnerSelectList($ProjectUserList,$ReviewCommentInfo)
{
		$SelectedUser = $ReviewCommentInfo['ReviewCommentOwner'];
   	$UserSelectList = htmlSelect($ProjectUserList, 'ReviewCommentOwner', '',$SelectedUser, 'class="NormalSelect MyInput RequiredField"');
		return $UserSelectList;
}

sysXajaxRegister("xProjectSetSlaveModule,xSetModuleOwner,xCreateSelectDiv,xProjectSetAssignedUser,xDeleteTestFile,xFillCustomedField");
$DisplayValue = false;

//Set ModuleType
$ModuleType = 'Bug';	//ReviewComment will use the same module with Bug

//Get the ReviewCommentID and ActionType From Request Variables
$ReviewCommentID = $_REQUEST['ReviewCommentID'];
$ActionType = $_REQUEST['ActionType'];
Rainy_Debug($ReviewCommentID,__FUNCTION__,__LINE__,__FILE__);   

//Get ReviewCommentInfo From DataBase
$Columns = '*';
$ReviewCommentInfo = GetRawReviewCommentInfo($ReviewCommentID);
Rainy_Debug($ReviewCommentInfo,__FUNCTION__,__LINE__,__FILE__);

//Validate the ReviewCommentInfo
if(!$ReviewCommentInfo['ReviewCommentID'] && $ActionType != 'OpenReviewComment')
{
    sysErrorMsg();
}

//OpenReviewComment was triggered by Copy Action, We need to clear some information
if($ReviewCommentID > 0 && $ActionType == 'OpenReviewComment')
{
    $ProjectID = $ReviewCommentInfo['ProjectID'];
    $ModuleID = $ReviewCommentInfo['ModuleID'];
    unset($ReviewCommentInfo['LastEditedBy']);
    unset($ReviewCommentInfo['LastEditedDate']);
    unset($ReviewCommentInfo['ResolvedBy']);
    unset($ReviewCommentInfo['ResolvedBuild']);
    unset($ReviewCommentInfo['ResolvedDate']);
    unset($ReviewCommentInfo['ClosedBy']);
    unset($ReviewCommentInfo['ClosedDate']);
}

//Open ReviewComment Action
if($ActionType == 'OpenReviewComment')	
{
    /* Create PrjectList And ModuleList */
    $ProjectID = $ProjectID != '' ? $ProjectID : $_SESSION['TestCurrentProjectID'];
    $ModuleID = $ModuleID != '' ? $ModuleID : $_SESSION['TestCurrentModuleID'];
    $ModuleInfo = dbGetRow('TestModule','*',"ModuleID = '{$ModuleID}'");
    
    $ReviewCommentInfo['ReviewCommentStatus'] = 'Active';
    $ReviewCommentInfo['ReviewCommentStatusName'] = $_LANG['ReviewCommentStatus'][$ReviewCommentInfo['ReviewCommentStatus']];
    $ReviewCommentInfo['ReviewCommentType'] = 'Error';
  
    $PreReviewCommentID = 0;
    $NextReviewCommentID = 0;

    $ModuleSelected = $ModuleID;
}
else
{
    /* Create PrjectList And ModuleList */
    $ProjectID = $ReviewCommentInfo['ProjectID'];
    $ModuleID = $ReviewCommentInfo['ModuleID'];
    $ModuleSelected = $ModuleID;
		
    //Get original Change's Titlte
    if(!empty($ReviewCommentInfo['ChangeID']))
    {
        $ChangeInfo = dbGetRow('ChangeInfo',$Columns,"ChangeID = '{$ReviewCommentInfo['ChangeID']}' AND {$_SESSION[TestUserACLSQL]}");
        $ReviewCommentInfo['ChangeTitle'] = $ChangeInfo['ChangeTitle'];
    }
    
		//Get The ReviewCommentIDList with the ReviewCommentQueryCondition
    $ReviewCommentIdInfoList = GetReviewCommentIdInfoList($ReviewCommentInfo['ReviewID']);
    $PreNextIdArr = getPreNextId($ReviewCommentIdInfoList,'ReviewCommentID',$ReviewCommentID);
    $PreReviewCommentID = $PreNextIdArr[0];
    $NextReviewCommentID = $PreNextIdArr[1];

		//Get the UserList of Project       
    $UserList = testGetProjectUserNameList($ReviewCommentInfo['ProjectID']);
    
    $ReviewCommentInfo = testSetReviewCommentMultiInfo($ReviewCommentInfo, $UserList);

    $ReviewCommentActionList = testGetActionAndFileList('ReviewComment', $ReviewCommentInfo['ReviewCommentID']);
    $LastActionID = testGetLastActionID('ReviewComment',$ReviewCommentInfo['ReviewCommentID']);
    $ReviewCommentInfo['ActionList'] = $ReviewCommentActionList['ActionList'];
    $ReviewCommentInfo['FileList'] = $ReviewCommentActionList['FileList'];
    $ReviewCommentInfo['ReviewCommentIDList'] = getReviewCommentTitleArr($ReviewCommentInfo['ReviewCommentID']);
}

//Get Original ReviewInfo and Set ReviewCommentPosNames
$ReviewID = $_GET['ReviewID'] != ''? $_GET['ReviewID']:$ReviewCommentInfo['ReviewID'];
if($ReviewID != '')
{
		$ReviewInfo = GetRawReviewInfo($ReviewID);
    Rainy_Debug($ReviewInfo,__FUNCTION__,__LINE__,__FILE__);
    if(empty($ReviewInfo))
    {
     		Rainy_Debug("ReviewComment must be opened based on Review",__FUNCTION__,__LINE__,__FILE__);
      	sysErrorMsg();
    }
    else
    {
    		$ProjectID = $ReviewInfo['ProjectID'];
        $ModuleID = $ReviewInfo['ModuleID'];
        $ModuleInfo = dbGetRow('TestModule','ModuleName',"ModuleID = '{$ModuleID}'");
        $ModuleSelected = $ModuleInfo['ModuleName'];
        //Set ReviewID,ReviewType and ReviewCommentPos1Name...
        $ReviewCommentInfo['ReviewID'] = $ReviewInfo['ReviewID'];
        $ReviewType = $ReviewInfo['ReviewType'] != ''? $ReviewInfo['ReviewType']: 'Others';
        $ReviewCommentInfo['ReviewType'] = $ReviewInfo['ReviewType'];
				$ReviewCommentInfo['ReviewCommentPos1Name'] = $_LANG['ReviewCommentPos1Names'][$ReviewType];
				$ReviewCommentInfo['ReviewCommentPos2Name'] = $_LANG['ReviewCommentPos2Names'][$ReviewType];
				$ReviewCommentInfo['ReviewCommentPos3Name'] = $_LANG['ReviewCommentPos3Names'][$ReviewType];
        Rainy_Debug($ReviewCommentInfo,__FUNCTION__,__LINE__,__FILE__);
    }    
}

if($ActionType == 'Activated' && $ReviewCommentInfo['ReviewCommentStatus'] == 'Active')
{
    jsGoto('ReviewComment.php?ReviewCommentID=' . $ReviewCommentID);
}
if($ActionType == 'Resolved' && $ReviewCommentInfo['ReviewCommentStatus'] == 'Resolved')
{
    jsGoto('ReviewComment.php?ReviewCommentID=' . $ReviewCommentID);
}
if($ActionType == 'Closed' && $ReviewCommentInfo['ReviewCommentStatus'] == 'Closed')
{
    jsGoto('ReviewComment.php?ReviewCommentID=' . $ReviewCommentID);
}

if($ActionType == 'OpenReviewComment')
{
    $ReviewCommentID = -1;
}

$OnReviewCommentStr = 'onchange="';
$OnReviewCommentStr .= 'xajax_xProjectSetSlaveModule(this.value, \'SlaveModuleList\', \'ModuleID\', \'ReviewComment\');';
$OnReviewCommentStr .= 'xajax_xProjectSetAssignedUser(this.value);';
$OnReviewCommentStr .= 'xajax_xFillCustomedField(\'CustomedFieldSet\', this.value, \'ReviewComment\', ' . $ReviewCommentID . ');';
$OnReviewCommentStr .= '"';
$OnReviewCommentStr .= ' class="MyInput RequiredField"';
$ProjectListSelect = testGetValidProjectSelectList('ProjectID', $ProjectID, $OnReviewCommentStr);

$OnReviewCommentStr = '';
if($ActionType == 'OpenReviewComment')
{
    $OnReviewCommentStr = 'onchange="';
    $OnReviewCommentStr .= 'xajax_xSetModuleOwner(this.value);';
    $OnReviewCommentStr .= '"';
}
$OnReviewCommentStr .= ' class="MyInput RequiredField"';
$ModuleSelectList = testGetSelectModuleList($ProjectID, 'ModuleID', $ModuleSelected, $OnReviewCommentStr, $ModuleType);

$ProjectUserList = testGetProjectUserList($ProjectID, true);

if($ActionType == 'OpenReviewComment')
{
    $ActionTypeName = $_LANG['OpenReviewComment'];
    if(empty($ReviewCommentInfo['ReviewCommentOwner']))
    {
    	 $ReviewCommentInfo['ReviewCommentOwner'] = $_SESSION['TestUserName'];
    }
}
elseif($ActionType == '')
{
    $DisplayValue = true;
    $ActionTypeName = $_LANG['ViewReviewComment'];
}
elseif($ActionType == 'Edited')
{
    $ActionTypeName = $_LANG['EditReviewComment'];
}
elseif($ActionType == 'Activated')
{
    $ActionTypeName = $_LANG['ActiveReviewComment'];
}
elseif($ActionType == 'Resolved')
{
    $ActionTypeName = $_LANG['ResolveReviewComment'];
}
elseif($ActionType == 'Closed')
{
    $ActionTypeName = $_LANG['CloseReviewComment'];
}

$ReviewCommentTypeSelectList = BuildHtmlReviewCommentTypeSelectList($_LANG['ReviewCommentTypes'],$ReviewCommentInfo);
$ReviewCommentOwnerSelectList = BuildHtmlReviewCommentOwnerSelectList($ProjectUserList,$ReviewCommentInfo);

$CustomedFieldHtml = testGetCustomedFieldHtml($ProjectID, 'ReviewComment', $ReviewCommentInfo['ReviewCommentID'], $DisplayValue);

if($ActionType == 'OpenReviewComment')
{
    $TPL->assign('HeaderTitle', $_LANG['OpenReviewComment']);
}
else
{
    $TPL->assign('HeaderTitle', 'ReviewComment #' . $ReviewCommentInfo['ReviewCommentID'] . ' ' . $ReviewCommentInfo['ReviewCommentTitle']);
}
$ConfirmArray = array('ReplyNote'=>'ReplyNote') + $_LANG['ReviewCommentFields'];
$TPL->assign('ConfirmIds', jsArray($ConfirmArray, 'Key'));

$TPL->assign('ReviewType', $ReviewCommentInfo['ReviewType']);
$TPL->assign('ReviewCommentInfo', $ReviewCommentInfo);
$TPL->assign('ReviewInfo',$ReviewInfo);
$TPL->assign('PreReviewCommentID', $PreReviewCommentID);
$TPL->assign('NextReviewCommentID', $NextReviewCommentID);

$TPL->assign('ProjectID', $ProjectID);
$TPL->assign('ModuleID', $ModuleID);
$TPL->assign('ProjectList', $ProjectListSelect);
$TPL->assign('ModuleList', $ModuleSelectList);
$TPL->assign('ReviewCommentTypeList', $ReviewCommentTypeSelectList);
$TPL->assign('ProjectUserList', $SelectUserList);
$TPL->assign('OpenedByUserList', $SelectOpenUserList);
$TPL->assign('ReviewCommentOwnerList', $ReviewCommentOwnerSelectList);
$TPL->assign('ActionType', $ActionType);
$TPL->assign('ActionTypeName', $ActionTypeName);
$TPL->assign('ActionRealName', $_SESSION['TestRealName']);
$TPL->assign('ActionDate', date('Y-m-d'));
$TPL->assign('DefaultReviewCommentValue', $DefaultReviewCommentValue);
$TPL->assign('LastActionID',$LastActionID);
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('ReviewCommentFields', $_LANG['ReviewCommentFields']);
$TPL->assign('CustomedFieldHtml', $CustomedFieldHtml);

$TPL->display('ReviewComment.tpl');
?>
