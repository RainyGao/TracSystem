<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * mail notice.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require_once("Include/Init.inc.php");
@set_time_limit(0);
@ini_set('memory_limit', -1);

//Get All Open Bugs which was assigned
$Where = "AssignedTo != 'Closed' && AssignedTo != 'Active' AND IsDroped = '0' AND BugStatus != 'Closed'";
$OrderBy = 'LastEditedDate DESC';
$Columns = 'BugID,BugSeverity,BugPriority,BugTitle,BugStatus,OpenedBy,AssignedTo,ResolvedBy,Resolution,LastEditedDate';
$BugList = dbGetList('BugInfo',$Columns, $Where, '', $OrderBy);

//Get UserList
$UserList = testGetUserList();
foreach($UserList as $UserInfo)
{
    $UserNameList[$UserInfo['UserName']] = $UserInfo['RealName'];
}

//BugInfo Handler
$BugList = testSetBugListMultiInfo($BugList, $UserNameList);

//BugGroupedList1 is for AssignedTo, BugGroupedList2 is for OpenedBy
foreach($BugList as $BugInfo)
{
    $BugGroupedList1[$BugInfo["AssignedTo"]][] = $BugInfo;
    $BugGroupedList2[$BugInfo["OpenedBy"]][] = $BugInfo;
}
$UserNameList1 = @array_keys($BugGroupedList1);
$UserNameList2 = @array_keys($BugGroupedList2);

$InUserNames1 = dbCreateIN(sysAddSlash(join(',',$UserNameList1)));
$InUserNames2 = dbCreateIN(sysAddSlash(join(',',$UserNameList2)));

$UserList1 = testGetUserList("{$_CFG[UserTable][UserName]} {$InUserNames1}");
$UserList2 = testGetUserList("{$_CFG[UserTable][UserName]} {$InUserNames2}");
Rainy_Debug($UserList1,__FUNCTION__,__LINE__,__FILE__);
Rainy_Debug($UserList2,__FUNCTION__,__LINE__,__FILE__);

$CssStyle = join("",file($_CFG['RealRootPath'] . "/Css/Mail.css"));
$TPL->assign("CssStyle",$CssStyle);

if($_CFG['BrowserMode'])
{
    $BaseUrl = $_CFG["BaseURL"];
}
else
{
    $BaseUrl = $argv[1];
}
if($BaseUrl == '')
{
    die('Wrong Baseurl');
}
$TPL->assign("BaseUrl", $BaseUrl);

//Send the BugList for AssignedTo
foreach($BugGroupedList1 as $AssignedTo => $UserBugList)
{
    $TPL->assign("UserBugList",$UserBugList);

    // Get bug list message in html.
    $Message = $TPL->fetch("Notice.tpl");
    $Subject = $_LANG["NoticeBugSubject"] . " ". count($UserBugList);

    // Mail.
    if(!empty($UserList1[$AssignedTo]['Email']))
    {
        $To = $UserList1[$AssignedTo]['Email'];
				Rainy_Debug($To,__FUNCTION__,__LINE__,__FILE__);
				Rainy_Debug($Subject,__FUNCTION__,__LINE__,__FILE__);
				Rainy_Debug($Message,__FUNCTION__,__LINE__,__FILE__);
        @sysMail($To,'',$Subject,$Message);
    }
}

//Send the BugList for OpenedBy
foreach($BugGroupedList2 as $OpenedBy => $UserBugList)
{
    $TPL->assign("UserBugList",$UserBugList);

    // Get bug list message in html.
    $Message = $TPL->fetch("Notice.tpl");
    $Subject = $_LANG["NoticeBugSubject2"] . " ". count($UserBugList);

    // Mail.
    if(!empty($UserList2[$OpenedBy]['Email']))
    {
        $To = $UserList2[$OpenedBy]['Email'];
				Rainy_Debug($To,__FUNCTION__,__LINE__,__FILE__);
				Rainy_Debug($Subject,__FUNCTION__,__LINE__,__FILE__);
				Rainy_Debug($Message,__FUNCTION__,__LINE__,__FILE__);
        @sysMail($To,'',$Subject,$Message);
    }
}
?>
