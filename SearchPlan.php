<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * search Plan form.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

$FieldName = 'Field';
$OperatorName = 'Operator';
$ValueName = 'Value';
$AndOrName = 'AndOr';
$LeftParenthesesName = 'LeftParenthesesName';
$RightParenthesesName = 'RightParenthesesName';
$FieldList = array();
$OperatorList = array();
$ValueList = array();
$AndOrList = array();
$FieldCount = $_CFG['QueryFieldNumber'];
$Attrib = 'class="FullSelect"';


$FieldListSelectItem = array(0=>'ProjectName',1=>'OpenedBy',2=>'ModulePath',3=>'AssignedTo',4=>'PlanID',5=>'PlanTitle');
$OperatorListSelectItem = array(0=>'=', 2=>'LIKE', 5=>'LIKE');

if(!empty($_REQUEST['reset']))
{
    $_SESSION['PlanFieldListCondition']='';
    $_SESSION['PlanOperatorListCondition']='';
    $_SESSION['PlanAndOrListCondition']='';
    $_SESSION['PlanValueListCondition']='';
    $_SESSION['PlanQueryCondition'] = '';
    $_SESSION['PlanQueryTitle'] = '';
    $_SESSION['PlanFieldsToShow'] = '';
    $_SESSION['PlanLeftParenthesesListCondition']='';
    $_SESSION['PlanRightParenthesesListCondition']='';
}
/*获取用户列表*/
$UserList = testGetCurrentUserNameList('PreAppend');

/*获取当前Project的ID，如果当前Project已设置，则从数据库中获取信息*/
$ProjectID = testGetCurrentProjectId();
if($ProjectID != null)
{
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="Plan"]/field');
        if($fields)
        {
            $fields = sysFieldXmlToArr($fields);
            $fieldArr = array();
            $fieldNameArr = array();
            foreach($fields as $field)
            {
                $key = $field['name'];
                if($field['status'] != 'active')
                {
                    continue;
                }
                $_LANG['PlanQueryField']["$key"] = $field['text'];
                if($field['type'] == 'select')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'mulit')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'user')
                {
                    $fieldArr["$key"]['value']   = array_keys($UserList);
                    $fieldArr["$key"]['key'] = array_values($UserList);
                }
                $fieldNameArr[] = $key;
            }
            if(!empty($fieldArr))
            {
                $AutoTextValue['CustomField'] = json_encode($fieldArr);
                $AutoTextValue['CustomFieldName'] = jsArray($fieldNameArr);
            }
        }
    }
}

if($_SESSION['PlanFieldListCondition']!='')
  $FieldListSelectItem = $_SESSION['PlanFieldListCondition'];
if($_SESSION['PlanOperatorListCondition']!='')
  $OperatorListSelectItem = $_SESSION['PlanOperatorListCondition'];
if($_SESSION['PlanAndOrListCondition']!='')
  $SelectItem = $_SESSION['PlanAndOrListCondition'];
if($_SESSION['PlanValueListCondition']!='')
  $ValueListSelectItem = $_SESSION['PlanValueListCondition'];

if($_SESSION['PlanLeftParenthesesListCondition']!='')
  $LeftParenthesesSelectItem = $_SESSION['PlanLeftParenthesesListCondition'];
if($_SESSION['PlanRightParenthesesListCondition']!='')
  $RightParenthesesSelectItem = $_SESSION['PlanRightParenthesesListCondition'];

if($_GET['QueryID'])
{
   $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='Plan'");
   $AndOr = $QueryInfo['QueryString'];

   /*unserialize()函数可以将已序列化的字符串变回到php值*/
   if($QueryInfo['FieldList']!='')
       $FieldListSelectItem = unserialize($QueryInfo['FieldList']);
   if($QueryInfo['OperatorList']!='')
       $OperatorListSelectItem = unserialize($QueryInfo['OperatorList']);
   if($QueryInfo['AndOrList']!='')
       $SelectItem =  unserialize($QueryInfo['AndOrList']);
   if($QueryInfo['ValueList']!='')
       $ValueListSelectItem = unserialize($QueryInfo['ValueList']);
   if($QueryInfo['LeftParentheses']!='')
       $LeftParenthesesSelectItem = unserialize($QueryInfo['LeftParentheses']);
   if($QueryInfo['RightParentheses']!='')
       $RightParenthesesSelectItem = unserialize($QueryInfo['RightParentheses']);
}

if($_REQUEST['UpdateQueryID'])
{
	 /*addslashes() 在指定的预定义字符前添加反斜杠,这样可以将避免字符串在特殊字符情况下出错*/
   $QueryStr = addslashes($_SESSION['PlanQueryCondition']);
   $AndOrListCondition = serialize($_SESSION['PlanAndOrListCondition']);
   $OperatorListCondition = serialize($_SESSION['PlanOperatorListCondition']);
   $ValueListCondition = mysql_real_escape_string(serialize($_SESSION['PlanValueListCondition']));
   $FieldListCondition = serialize($_SESSION['PlanFieldListCondition']);
   $FieldsToShow = implode(",",array_keys(testSetCustomFields('Plan')));
   $LeftParenthesesCondition = serialize($_SESSION['PlanLeftParenthesesNameListCondition']);
   $RightParenthesesCondition = serialize($_SESSION['PlanRightParenthesesNameListCondition']);

   $ProjectID = testGetCurrentProjectId();
   dbUpdateRow('TestUserQuery', 'QueryString', "'{$QueryStr}'", 'AndOrList', "'{$AndOrListCondition}'", 'OperatorList',
                    "'$OperatorListCondition'", 'ValueList',"'$ValueListCondition'", 'FieldList',"'$FieldListCondition'",
           'FieldsToShow', "'$FieldsToShow'", 'ProjectID', "'$ProjectID'",
           'LeftParentheses',"'$LeftParenthesesCondition'",
           'RightParentheses',"'$RightParenthesesCondition'",
           "QueryID={$_REQUEST['UpdateQueryID']}");
   jsGoTo('PlanList.php',"parent.RightBottomFrame");

}

/*设置所有Search条件的FieldList[],ValueList[],OperatorList[],AndOrList[],LeftParentheses[]和RightParentheses[]*/
$QueryFieldCount = count($ValueListSelectItem);
if($QueryFieldCount == 0) $QueryFieldCount = 1;
$searchConditionTmp = getSearchCondition("Plan");
for($I=0; $I<$QueryFieldCount; $I ++)
{
		/*htmlSelect() 函数输出是html的下拉框的实现字符串*/
    $FieldListOnChange = ' onchange="setQueryForm('.$I.');"';
    $OperatorListOnChange = ' onchange="setQueryValue('.$I.');"';
    $FieldList[$I] = htmlSelect($_LANG['PlanQueryField'], $FieldName . $I, $Mode, $FieldListSelectItem[$I], $Attrib.$FieldListOnChange);
    $OperatorList[$I] = htmlSelect($_LANG['Operators'], $OperatorName . $I, $Mode, $OperatorListSelectItem[$I], $Attrib.$OperatorListOnChange);
    $ValueList[$I] = '<input id="'.$ValueName.$I.'" name="' . $ValueName.$I .'" type="text" size="5" style="width:95%;"/>';
    $AndOrList[$I] = htmlSelect($_LANG['AndOr'], $AndOrName . $I, $Mode, $SelectItem[$I], $Attrib);
    $LeftParentheses[$I] = htmlSelect($_LANG['LeftParentheses'], 
            $LeftParenthesesName . $I, $Mode, $LeftParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $RightParentheses[$I] = htmlSelect($_LANG['RightParentheses'], 
            $RightParenthesesName . $I, $Mode, $RightParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $AddRemoveLink[$I] = '<a href="javascript:;" onclick="addSearchField('.$I.');return false;" ><img src="Image/add_search.gif"/></a>&nbsp;&nbsp;<a href="javascript:;" onclick="removeSearchField('.$I.');return false;" ><img src="Image/cancel_search.gif"/></a>';
}

$SimpleProjectList = array(''=>'')+testGetValidSimpleProjectList();

$AutoTextValue['ProjectNameText']=jsArray($SimpleProjectList);
$AutoTextValue['ProjectNameValue']=jsArray($SimpleProjectList);
$AutoTextValue['SeverityText']=jsArray($_LANG['PlanSeveritys']);
$AutoTextValue['SeverityValue']=jsArray($_LANG['PlanSeveritys'], 'Key');
$AutoTextValue['PriorityText']=jsArray($_LANG['PlanPriorities']);
$AutoTextValue['PriorityValue']=jsArray($_LANG['PlanPriorities'], 'Key');
$AutoTextValue['TypeText']=jsArray($_LANG['PlanTypes']);
$AutoTextValue['TypeValue']=jsArray($_LANG['PlanTypes'], 'Key');
$AutoTextValue['StatusText']=jsArray($_LANG['PlanStatus']);
$AutoTextValue['StatusValue']=jsArray($_LANG['PlanStatus'], 'Key');
$AutoTextValue['OSText']=jsArray($_LANG['PlanOS']);
$AutoTextValue['OSValue']=jsArray($_LANG['PlanOS'], 'Key');
$AutoTextValue['BrowserText']=jsArray($_LANG['PlanBrowser']);
$AutoTextValue['BrowserValue']=jsArray($_LANG['PlanBrowser'], 'Key');
$AutoTextValue['MachineText']=jsArray($_LANG['PlanMachine']);
$AutoTextValue['MachineValue']=jsArray($_LANG['PlanMachine'], 'Key');
$AutoTextValue['ResolutionText']=jsArray($_LANG['PlanResolutions']);
$AutoTextValue['ResolutionValue']=jsArray($_LANG['PlanResolutions'], 'Key');
$AutoTextValue['HowFoundText']=jsArray($_LANG['PlanHowFound']);
$AutoTextValue['HowFoundValue']=jsArray($_LANG['PlanHowFound'], 'Key');
$AutoTextValue['SubStatusText']=jsArray($_LANG['PlanSubStatus']);
$AutoTextValue['SubStatusValue']=jsArray($_LANG['PlanSubStatus'], 'Key');

$AutoTextValue['FieldType'] = jsArray($_CFG['FieldType']);
$AutoTextValue['FieldOperationTypeValue'] = jsArray($_CFG['FieldTypeOperation']);
$AutoTextValue['FieldOperationTypeText'] = jsArray($_LANG['FieldTypeOperationName']);


$ACUserList = array(''=>'') + $UserList;
$UserList = array(''=>'') + $UserList;
$AutoTextValue['ACUserText']=jsArray($ACUserList);
$AutoTextValue['ACUserValue']=jsArray($ACUserList, 'Key');
$AutoTextValue['UserText']=jsArray($UserList);
$AutoTextValue['UserValue']=jsArray($UserList, 'Key');

$TPL->assign('OperatorListSelectItem', $OperatorListSelectItem);
$TPL->assign('ValueListSelectItem', $ValueListSelectItem);
$TPL->assign('QueryTitle', $_SESSION['PlanQueryTitle']);


$TPL->assign('AutoTextValue', $AutoTextValue);
$TPL->assign("FieldList", $FieldList);
$TPL->assign("OperatorList", $OperatorList);
$TPL->assign("ValueList", $ValueList);
$TPL->assign("AndOrList", $AndOrList);
$TPL->assign("FieldCount",$FieldCount);

$TPL->assign('UserLang',$_CFG['UserLang']);
$TPL->assign("QueryFieldCount",$QueryFieldCount);
$TPL->assign("LeftParentheses",$LeftParentheses);
$TPL->assign("RightParentheses",$RightParentheses);
$TPL->assign("AddRemoveLink",$AddRemoveLink);
$TPL->assign('searchConditionTmp',$searchConditionTmp);
$TPL->display('Search.tpl');
?>
