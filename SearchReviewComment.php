<?php
/**
 * ReviewCommentFree is free software under the terms of the FreeBSD License.
 *
 * search bug form.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

$FieldName = 'Field';
$OperatorName = 'Operator';
$ValueName = 'Value';
$AndOrName = 'AndOr';
$LeftParenthesesName = 'LeftParenthesesName';
$RightParenthesesName = 'RightParenthesesName';
$FieldList = array();
$OperatorList = array();
$ValueList = array();
$AndOrList = array();
$FieldCount = $_CFG['QueryFieldNumber'];
$Attrib = 'class="FullSelect"';


$FieldListSelectItem = array(0=>'ProjectName',1=>'OpenedBy',2=>'ModulePath',3=>'AssignedTo',4=>'ReviewCommentID',5=>'ReviewCommentTitle');
$OperatorListSelectItem = array(0=>'=', 2=>'LIKE', 5=>'LIKE');

if(!empty($_REQUEST['reset']))
{
    $_SESSION['ReviewCommentFieldListCondition']='';
    $_SESSION['ReviewCommentOperatorListCondition']='';
    $_SESSION['ReviewCommentAndOrListCondition']='';
    $_SESSION['ReviewCommentValueListCondition']='';
    $_SESSION['ReviewCommentQueryCondition'] = '';
    $_SESSION['ReviewCommentQueryTitle'] = '';
    $_SESSION['ReviewCommentFieldsToShow'] = '';
    $_SESSION['ReviewCommentLeftParenthesesListCondition']='';
    $_SESSION['ReviewCommentRightParenthesesListCondition']='';
}
/*获取用户列表*/
$UserList = testGetCurrentUserNameList('PreAppend');

/*获取当前Project的ID，如果当前Project已设置，则从数据库中获取信息*/
$ProjectID = testGetCurrentProjectId();
if($ProjectID != null)
{
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="ReviewComment"]/field');
        if($fields)
        {
            $fields = sysFieldXmlToArr($fields);
            $fieldArr = array();
            $fieldNameArr = array();
            foreach($fields as $field)
            {
                $key = $field['name'];
                if($field['status'] != 'active')
                {
                    continue;
                }
                $_LANG['ReviewCommentQueryField']["$key"] = $field['text'];
                if($field['type'] == 'select')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'mulit')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'user')
                {
                    $fieldArr["$key"]['value']   = array_keys($UserList);
                    $fieldArr["$key"]['key'] = array_values($UserList);
                }
                $fieldNameArr[] = $key;
            }
            if(!empty($fieldArr))
            {
                $AutoTextValue['CustomField'] = json_encode($fieldArr);
                $AutoTextValue['CustomFieldName'] = jsArray($fieldNameArr);
            }
        }
    }
}

if($_SESSION['ReviewCommentFieldListCondition']!='')
  $FieldListSelectItem = $_SESSION['ReviewCommentFieldListCondition'];
if($_SESSION['ReviewCommentOperatorListCondition']!='')
  $OperatorListSelectItem = $_SESSION['ReviewCommentOperatorListCondition'];
if($_SESSION['ReviewCommentAndOrListCondition']!='')
  $SelectItem = $_SESSION['ReviewCommentAndOrListCondition'];
if($_SESSION['ReviewCommentValueListCondition']!='')
  $ValueListSelectItem = $_SESSION['ReviewCommentValueListCondition'];

if($_SESSION['ReviewCommentLeftParenthesesListCondition']!='')
  $LeftParenthesesSelectItem = $_SESSION['ReviewCommentLeftParenthesesListCondition'];
if($_SESSION['ReviewCommentRightParenthesesListCondition']!='')
  $RightParenthesesSelectItem = $_SESSION['ReviewCommentRightParenthesesListCondition'];

if($_GET['QueryID'])
{
   $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='ReviewComment'");
   $AndOr = $QueryInfo['QueryString'];

   /*unserialize()函数可以将已序列化的字符串变回到php值*/
   if($QueryInfo['FieldList']!='')
       $FieldListSelectItem = unserialize($QueryInfo['FieldList']);
   if($QueryInfo['OperatorList']!='')
       $OperatorListSelectItem = unserialize($QueryInfo['OperatorList']);
   if($QueryInfo['AndOrList']!='')
       $SelectItem =  unserialize($QueryInfo['AndOrList']);
   if($QueryInfo['ValueList']!='')
       $ValueListSelectItem = unserialize($QueryInfo['ValueList']);
   if($QueryInfo['LeftParentheses']!='')
       $LeftParenthesesSelectItem = unserialize($QueryInfo['LeftParentheses']);
   if($QueryInfo['RightParentheses']!='')
       $RightParenthesesSelectItem = unserialize($QueryInfo['RightParentheses']);
}

if($_REQUEST['UpdateQueryID'])
{
	 /*addslashes() 在指定的预定义字符前添加反斜杠,这样可以将避免字符串在特殊字符情况下出错*/
   $QueryStr = addslashes($_SESSION['ReviewCommentQueryCondition']);
   $AndOrListCondition = serialize($_SESSION['ReviewCommentAndOrListCondition']);
   $OperatorListCondition = serialize($_SESSION['ReviewCommentOperatorListCondition']);
   $ValueListCondition = mysql_real_escape_string(serialize($_SESSION['ReviewCommentValueListCondition']));
   $FieldListCondition = serialize($_SESSION['ReviewCommentFieldListCondition']);
   $FieldsToShow = implode(",",array_keys(testSetCustomFields('ReviewComment')));
   $LeftParenthesesCondition = serialize($_SESSION['ReviewCommentLeftParenthesesNameListCondition']);
   $RightParenthesesCondition = serialize($_SESSION['ReviewCommentRightParenthesesNameListCondition']);

   $ProjectID = testGetCurrentProjectId();
   dbUpdateRow('TestUserQuery', 'QueryString', "'{$QueryStr}'", 'AndOrList', "'{$AndOrListCondition}'", 'OperatorList',
                    "'$OperatorListCondition'", 'ValueList',"'$ValueListCondition'", 'FieldList',"'$FieldListCondition'",
           'FieldsToShow', "'$FieldsToShow'", 'ProjectID', "'$ProjectID'",
           'LeftParentheses',"'$LeftParenthesesCondition'",
           'RightParentheses',"'$RightParenthesesCondition'",
           "QueryID={$_REQUEST['UpdateQueryID']}");
   jsGoTo('ReviewCommentList.php',"parent.RightBottomFrame");

}

/*设置所有Search条件的FieldList[],ValueList[],OperatorList[],AndOrList[],LeftParentheses[]和RightParentheses[]*/
$QueryFieldCount = count($ValueListSelectItem);
if($QueryFieldCount == 0) $QueryFieldCount = 1;
$searchConditionTmp = getSearchCondition("ReviewComment");
for($I=0; $I<$QueryFieldCount; $I ++)
{
		/*htmlSelect() 函数输出是html的下拉框的实现字符串*/
    $FieldListOnChange = ' onchange="setQueryForm('.$I.');"';
    $OperatorListOnChange = ' onchange="setQueryValue('.$I.');"';
    $FieldList[$I] = htmlSelect($_LANG['ReviewCommentQueryField'], $FieldName . $I, $Mode, $FieldListSelectItem[$I], $Attrib.$FieldListOnChange);
    $OperatorList[$I] = htmlSelect($_LANG['Operators'], $OperatorName . $I, $Mode, $OperatorListSelectItem[$I], $Attrib.$OperatorListOnChange);
    $ValueList[$I] = '<input id="'.$ValueName.$I.'" name="' . $ValueName.$I .'" type="text" size="5" style="width:95%;"/>';
    $AndOrList[$I] = htmlSelect($_LANG['AndOr'], $AndOrName . $I, $Mode, $SelectItem[$I], $Attrib);
    $LeftParentheses[$I] = htmlSelect($_LANG['LeftParentheses'], 
            $LeftParenthesesName . $I, $Mode, $LeftParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $RightParentheses[$I] = htmlSelect($_LANG['RightParentheses'], 
            $RightParenthesesName . $I, $Mode, $RightParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $AddRemoveLink[$I] = '<a href="javascript:;" onclick="addSearchField('.$I.');return false;" ><img src="Image/add_search.gif"/></a>&nbsp;&nbsp;<a href="javascript:;" onclick="removeSearchField('.$I.');return false;" ><img src="Image/cancel_search.gif"/></a>';
}

$SimpleProjectList = array(''=>'')+testGetValidSimpleProjectList();

$AutoTextValue['ProjectNameText']=jsArray($SimpleProjectList);
$AutoTextValue['ProjectNameValue']=jsArray($SimpleProjectList);
$AutoTextValue['TypeText']=jsArray($_LANG['ReviewCommentTypes']);
$AutoTextValue['TypeValue']=jsArray($_LANG['ReviewCommentTypes'], 'Key');
$AutoTextValue['StatusText']=jsArray($_LANG['ReviewCommentStatus']);
$AutoTextValue['StatusValue']=jsArray($_LANG['ReviewCommentStatus'], 'Key');
$AutoTextValue['FieldType'] = jsArray($_CFG['FieldType']);
$AutoTextValue['FieldOperationTypeValue'] = jsArray($_CFG['FieldTypeOperation']);
$AutoTextValue['FieldOperationTypeText'] = jsArray($_LANG['FieldTypeOperationName']);


$ACUserList = array(''=>'', 'Active' => 'Active') + $UserList+array('Closed'=>'Closed');
$UserList = array(''=>'') + $UserList;
$AutoTextValue['ACUserText']=jsArray($ACUserList);
$AutoTextValue['ACUserValue']=jsArray($ACUserList, 'Key');
$AutoTextValue['UserText']=jsArray($UserList);
$AutoTextValue['UserValue']=jsArray($UserList, 'Key');

$TPL->assign('OperatorListSelectItem', $OperatorListSelectItem);
$TPL->assign('ValueListSelectItem', $ValueListSelectItem);
$TPL->assign('QueryTitle', $_SESSION['ReviewCommentQueryTitle']);


$TPL->assign('AutoTextValue', $AutoTextValue);
$TPL->assign("FieldList", $FieldList);
$TPL->assign("OperatorList", $OperatorList);
$TPL->assign("ValueList", $ValueList);
$TPL->assign("AndOrList", $AndOrList);
$TPL->assign("FieldCount",$FieldCount);

$TPL->assign('UserLang',$_CFG['UserLang']);
$TPL->assign("QueryFieldCount",$QueryFieldCount);
$TPL->assign("LeftParentheses",$LeftParentheses);
$TPL->assign("RightParentheses",$RightParentheses);
$TPL->assign("AddRemoveLink",$AddRemoveLink);
$TPL->assign('searchConditionTmp',$searchConditionTmp);
$TPL->display('Search.tpl');
?>
