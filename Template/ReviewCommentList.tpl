{include file="XmlHeader.tpl"}
{include file="Header.tpl"}
{include file="UploadFile.tpl"}


{include file="ResizeList.tpl"}
<script>if(parent.window==window)window.location='index.php';</script>
<body class="{$TestMode}Mode RMode" onResize="resizeList();" onclick="hiddenDivCustomSetTable()" onload="initShowGotoBCR();resizeList();">
{include file="CustomField.tpl"}
  <div id="BlankCover">
    <table class="CommonTable TableMode">
      <tr>
        <td colspan="{$FieldToShowCount}" class="TdCaption">
          <table style="font-size:12px;width:98%;">
            <tr>
              <td style="text-align:left;width:50%">
                &nbsp;&nbsp;
                <a href="javascript:void(0);" id="CustomSetLink" onclick="x=530;y=event.clientY;showDivCustomSetTable(x,y);">
                    {$Lang.CustomDisplay}
                </a>&nbsp;|&nbsp;        
                <span id="VReport">
                  <a href="Report.php?ReportMode={$TestMode}" target="'{$TestMode}Report">
                    {$Lang.ReportForms}
                  </a>
                </span>&nbsp;|&nbsp;
                <a href="?Export=XMLFile">
                  {$Lang.ExportCases}
                </a>
                {if $TestIsAdmin}
                </span>&nbsp;|&nbsp;
                <a href="javascript:void(0);" onclick="x=event.clientX+document.body.scrollLeft;y=event.clientY;showUploadFile(x+420,y);">{$Lang.ImportCases}</a>
								{/if}
              </td>
              <td style="text-align:right;width:50%">
                {$Lang.Pagination.Result} 
                {$PaginationDetailInfo.FromNum}-{$PaginationDetailInfo.ToNum}/{$PaginationDetailInfo.RecTotal}
                &nbsp;
                {$PaginationDetailInfo.RecPerPageList}
                &nbsp;
                {$PaginationDetailInfo.PrePageLiteralLink}
                {$PaginationDetailInfo.NextPageLiteralLink}
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="{$FieldToShowCount}">
          <div id="ListSubTable" style="overflow-y:scroll;overflow-x:auto">
            <table class="CommonTable CommonSubTable TableSubMode">
              <tr class="TabLink">
                {foreach from=$FieldsToShow key=Field item=FieldName}
                <th align="{if $Field == 'ReviewCommentID' || $Field == 'ReviewCommentSeverity'}center{else}left{/if}">
                  <a href="?OrderBy={$Field}|{$OrderByTypeList[$Field]}&QueryMode={$QueryMode}">
                  {$FieldName}
                  </a>
                  {if $OrderByColumn == $Field}{$OrderTypeArrowArray[$OrderByType]}{/if}
                </th>
                {/foreach}
              </tr>
              {foreach from=$ReviewCommentList item=ReviewCommentInfo}
              <tr class="BugStatus{$ReviewCommentInfo.ReviewCommentStatus}">
                {foreach from=$FieldsToShow key=Field item=FieldName}
                <td align="{if $Field == 'ReviewCommentID' || $Field == 'ReviewCommentSeverity' || $Field == 'ReviewCommentPriority'}center{else}left{/if}"{if $QueryColumn eq $Field} class="SortActive"{/if}>
                  {assign var="FieldValue" value=$Field."Name"}
                    {if $Field == 'ReviewCommentTitle'}
                  <span class="Title">
                    <a href="ReviewComment.php?ReviewCommentID={$ReviewCommentInfo.ReviewCommentID}" title="{$ReviewCommentInfo.ReviewCommentTitle}" class="FullLink Title" target="_blank">
                      {$ReviewCommentInfo.ListTitle}
                    </a>
                  </span>
                  {elseif $Field == 'OpenedDate' || $Field == 'AssignedDate' || $Field == 'ResolvedDate' || $Field == 'ClosedDate' || $Field == 'LastEditedDate'}
                  {if $ReviewCommentInfo[$Field] != $CFG.ZeroTime}
                  <a href="?QueryMode={$Field}|{$ReviewCommentInfo[$Field]|date_format:"%Y-%m-%d"}">
                    {$ReviewCommentInfo[$Field]|date_format:"%Y-%m-%d"}
                  </a>
                  {/if}
                  {else}
                  <a href="?QueryMode={$Field}|{$ReviewCommentInfo[$Field]}">
                    {$ReviewCommentInfo[$FieldValue]|default:$ReviewCommentInfo[$Field]}
                  </a>
                  {/if}
                </td>
                {/foreach}
              </tr>
              {/foreach}
            </table>
          </div>
        </td>
      </tr>
    </table>
  </div>
<script>
    if({$QueryID}!='-1')
       parent.RightTopFrame.location = "SearchReviewComment.php?QueryID=" + {$QueryID} + "&QueryTitle=" + encodeURI('{$QueryTitle}');
    else if('{$QueryTitle}'!='')
       parent.RightTopFrame.location = "SearchReviewComment.php?QueryTitle=" + encodeURI('{$QueryTitle}');
</script>
</body>
</html>