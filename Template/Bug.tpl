{include file="Header.tpl"}
<body class="BugMode BugMain" onload="TestMode='Bug';initShowGotoBCR();" onscroll="setTopBarLeftPos()">
  {if $ActionType eq 'Edited' or $ActionType eq 'Resolved' or $ActionType eq 'Closed' or $ActionType eq 'Activated'}
  {assign var="EditMode" value= 'true'}
  {/if}

  {if $templatelite.session.TestUserName eq $BugInfo.OpenedBy}
  {assign var="testIsOpenedBy" value= 'true'}
  {/if}  
	{if $templatelite.session.TestUserName eq $BugInfo.AssignedTo}
	{assign var="testIsAssignedTo" value= 'true'}
	{/if}
  {if $testIsOpenedBy or $testIsAssignedTo or $TestIsAdmin}
  {assign var="testIsOwner" value= 'true'}
  {/if}
  {if $testIsOpenedBy or $TestIsAdmin}
  {assign var="testIsCloser" value= 'true'}
  {/if}

  {if $ActionType eq 'OpenBug'}
    <form id="BugForm" name="BugForm" action="PostAction.php?Action=OpenBug" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {elseif $EditMode eq 'true'}
    <form id="BugForm" name="BugForm" action="PostAction.php?Action=EditBug" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {else}
    <form id="BugForm" name="BugForm" action="Bug.php?BugID={$BugInfo.BugID}" method="post" target="_self">
  {/if}

<div id="TopNavMain" class="TopBar">
    <a id="TopNavLogo" href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    <!-- Set the Toolbar buttons status by Lichuan Liu -->
    {if $PreBugID eq 0 or $EditMode eq 'true'}
    	{assign var="PreButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $NextBugID eq 0 or $EditMode eq 'true'}
    	{assign var="NextButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ActionType eq 'OpenBug' or $EditMode eq 'true'}
    	{assign var="EditButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $BugInfo.BugStatus neq 'Active' or $ActionType eq 'OpenBug'or $EditMode eq 'true' or $testIsOwner neq 'true'}
    	{assign var="ResolveBugButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $BugInfo.BugStatus eq 'Closed' or $ActionType eq 'OpenBug'or $EditMode eq 'true' or $testIsCloser neq 'true'}
    	{assign var="CloseBugButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $BugInfo.BugStatus eq 'Active' or $EditMode eq 'true'}
    	{assign var="ActiveBugButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ActionType eq ''}
    	{assign var="SaveButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $BugInfo.BugStatus neq 'Active' or $ActionType eq 'OpenBug'or $EditMode eq 'true'}
    	{assign var="OpenChangeButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    
    <div id="ButtonList">
      <input type="button" class="ActionButton Btn" accesskey="P" value="{$Lang.PreButton}" onclick="location.href='Bug.php?BugID={$PreBugID}'" {$PreButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="N" value="{$Lang.NextButton}" onclick="location.href='Bug.php?BugID={$NextBugID}'" {$NextButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="E" value="{$Lang.EditBugButton}" onclick="xajax.$('ActionType').value='Edited';submitForm('BugForm')" {$EditButtonStatus}}/>
      <input type="button" class="ActionButton Btn" accesskey="C" value="{$Lang.CopyBugButton}" onclick="xajax.$('ActionType').value='OpenBug';submitForm('BugForm');" {$EditButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="R" value="{$Lang.ResolveBugButton}" onclick="xajax.$('ActionType').value='Resolved';submitForm('BugForm');" {$ResolveBugButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="L" value="{$Lang.CloseBugButton}" onclick="xajax.$('ActionType').value='Closed';submitForm('BugForm');" {$CloseBugButtonStatus} />
 			{if $TestIsAdmin}
      <input type="button" class="ActionButton Btn" accesskey="A" value="{$Lang.ActiveBugButton}" onclick="xajax.$('ActionType').value='Activated';submitForm('BugForm');" {$ActiveBugButtonStatus} />
 			{/if}
      <input type="button" class="ActionButton Btn" accesskey="S" value="{$Lang.SaveButton}" name="SubmitButton" id="SubmitButton" onclick="this.disabled='disabled';NeedToConfirm=false;document.BugForm.submit();" {$SaveButtonStatus}/>
    	<input type="button" class="ActionButton Btn" accesskey="B" value="{$Lang.OpenChangeButton}" {$OpenChangeButtonStatus}  onclick="openWindow('Change.php?ActionType=OpenChange&BugID={$BugInfo.BugID}','OpenChange');"/>
    </div>
</div>


<div id="BugMain" class="CommonForm BugMode">
	<span id="BugId">{if $ActionType eq 'OpenBug'}{$Lang.OpenBug}{else}{$BugInfo.BugIDName}{/if}</span>
  <div id="BugMainInfo">
  		<table style="width: 100%">
      		<tr>
          		<td style="width: 20%" valign="top">
          			<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.BugFields.BugID}</dt>
      									<dd>
      										{if $ActionType eq 'OpenBug'}{$Lang.OpenBug}{else}{$BugInfo.BugIDName}{/if}
      									</dd>
    								</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.BugFields.BugStatus}</dt>
                  			<dd>
                    			{if $ActionType eq 'Activated'}{$Lang.BugStatus.Active}
                    			{elseif $ActionType eq 'Closed'}{$Lang.BugStatus.Closed}
                    			{elseif $ActionType eq 'Resolved'}{$Lang.BugStatus.Resolved}
                    			{else}{$BugInfo.BugStatusName}{/if}
                  			</dd>
                		</dl>
										<dl style="line-height:17pt">
      									<dt>
      										<input type="checkbox" id="DuplicateCheckBox" name="DuplicateCheckBox" {if $BugInfo.IsDuplicated eq 'Duplicate'}checked="checked"{/if} onclick="DisplayDuplicateID();"   {if $EditMode neq 'true'}disabled="disabled"{/if}/>
      									  <input type="hidden" id="IsDuplicated" name="IsDuplicated" value="{$BugInfo.IsDuplicated}" />
      									</dt>
      									<dd>{$Lang.BugFields.IsDuplicated}</dd>
										</dl>
										<dl style="line-height:17pt">
      									<dt>&nbsp</dt>
      									<dd>
                    			{if $ActionType eq 'Activated'}
                      		{elseif $EditMode eq 'true'}
                         		<input type="text" name="DuplicateID" id="DuplicateID" size="20" class="MyInput RequiredField" value="{$BugInfo.DuplicateID}" {if $BugInfo.IsDuplicated neq "Duplicate"}style="display:none;"{/if} />
                        	{else}
                        		{if $BugInfo.IsDuplicated eq 'Duplicate'}
                        			{foreach from=$BugInfo.DuplicateIDList key=key value=value}
                        			<a href="Bug.php?BugID={$key}" title="{$value}" target="_blank">{$key}</a>
                        			{/foreach}
                        			<input type="hidden" name="DuplicateID" value="{$BugInfo.DuplicateID}" />
                        		{/if}
                        	{/if}
                    		</dd>
    								</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.OpenedBy}</dt>
                    		<dd>
                            {if $ActionType eq 'OpenBug'}
                            {$templatelite.session.TestRealName}
                        		{else}
                            {$BugInfo.OpenedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                	  <dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.ResolvedBy}</dt>
                    		<dd>
                        		{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Resolved'}
                          	{$ActionRealName}
                        		{else}
                          	{$BugInfo.ResolvedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                    <dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.ClosedBy}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Closed'}
                          	{$ActionRealName}
                        		{else}
                          	{$BugInfo.ClosedByName}
                      			{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.BugFields.LastEditedBy}</dt>
                  			<dd>{$BugInfo.LastEditedByName}</dd>
                		</dl>
                </fieldset>
              	<fieldset class="Normal FloatLeft" style="width: 94%">
										<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.OpenedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'OpenBug'}
                            {$templatelite.now|date_format:"%Y-%m-%d"}
                        		{else}
                            {$BugInfo.OpenedDate|date_format:"%Y-%m-%d"}
                        		{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.ResolvedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Resolved'}
                            {$ActionDate}
                						{else}
                  					{if $BugInfo.ResolvedDate neq $CFG.ZeroTime}{$BugInfo.ResolvedDate|date_format:"%Y-%m-%d"} {/if}
                						{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.ClosedDate}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                      			{elseif $ActionType eq 'Closed'}
                        		{$ActionDate}
                      			{else}
                        		{if $BugInfo.ClosedDate neq $CFG.ZeroTime}{$BugInfo.ClosedDate|date_format:"%Y-%m-%d"}{/if}{/if}
                    		</dd>
                  	</dl>
										<dl style="line-height:17pt">
                  			<dt>{$Lang.BugFields.LastEditedDate}</dt>
                  			<dd>{$BugInfo.LastEditedDate|date_format:"%Y-%m-%d"}</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  	 <dl style="line-height:17pt">
                    			<dt>{$Lang.BugFields.ResultID}</dt>
                    			<dd>
                      		<a href="Result.php?ResultID={$BugInfo.ResultID}" title="{$BugInfo.ResultTitle}" target="_top">{$BugInfo.ResultID}</a>
                      		<input type="hidden" name="ResultID" value="{$BugInfo.ResultID}" />
                    			</dd>
                  		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.BugFields.ChangeID}</dt>
                    			<dd>
                         	{foreach from=$BugInfo.ChangeIDList key=key value=value}
                          <a href="Change.php?ChangeID={$key}" title="{$value}" target="_top">{$Lang.ChangeIDPrefix}{$key}</a>
                         	{/foreach}
                    			</dd>
                  		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.BugFields.LinkID}</dt>
                    			<dd>
                       		{if $ActionType eq 'OpenBug' or $EditMode eq 'true'}
                         	<input type="text" name="LinkID" id="LinkID" size="20" class="MyInput" maxlength="80" value="{$BugInfo.LinkID}" />
                       		{else}
                         	{foreach from=$BugInfo.LinkIDList key=key value=value}
                          <a href="Bug.php?BugID={$key}" title="{$value}" target="_blank">{$key}</a>
                         	{/foreach}
                       		{/if}
                    			</dd>
                  		</dl>
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.BugFields.CaseID}</dt>
                    			<dd>
                       		{if $ActionType eq 'OpenBug' or $EditMode eq 'true'}
                         	<input type="text" name="CaseID" id="CaseID" size="20" class="MyInput" maxlength="80" value="{$BugInfo.CaseID}" />
                       		{else}
                         	{foreach from=$BugInfo.CaseIDList key=key value=value}
                          <a href="Case.php?CaseID={$key}" title="{$value}" target="_blank">{$key}</a>
                         	{/foreach}
                       		{/if}
                    			</dd>
                  		</dl>
                	</fieldset>
              </td>
              <td style="width: 49%" valign="top">
              		<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.BugFields.BugTitle}</dt>
      										<dd>
          										{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner) }
					            				<input type="text" id="BugTitle" name="BugTitle" class="MyInput RequiredField" value="{$BugInfo.BugTitle}" style="width:500px;"/>
          										{else}
            									<input type="text" id="BugTitle" name="BugTitle" readonly=readonly class="MyInput ReadOnlyField" title="{$BugInfo.BugTitle}" value="{$BugInfo.BugTitle}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
    									<dl style="line-height:17pt">
                    			<dt>{$Lang.BugFields.BugKeyword}</dt>
                    			<dd>
                        			{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                            	<input type="text" name="BugKeyword" id="BugKeyword" size="20" class="MyInput" maxlength="80" value="{$BugInfo.BugKeyword}" />
                        			{else}
                            	<input type="text" name="BugKeyword" id="BugKeyword" size="20" readonly=true  class="MyInput ReadOnlyField" maxlength="80" value="{$BugInfo.BugKeyword}" />
                        			{/if}
                    			</dd>
                  		</dl>
    							</fieldset>
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                			<dl style="line-height:17pt">
                  				<dt>{$Lang.BugFields.BugType}</dt>
                  				<dd>
                              {if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$BugTypeList}
                      				{else}
                          		{$BugInfo.BugTypeName}
                              {/if}
                  				</dd>
                			</dl>
                  </fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.BugFields.ProjectName}</dt>
         								{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
          							<dd>{$ProjectList}</dd>
        								{else}
          							<dd>{$BugInfo.ProjectName}</dd>
        								{/if}
    								</dl>
        						<dl style="line-height:17pt">
      									<dt>{$Lang.BugFields.ModulePath}</dt>
         								{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
            						<dd id="SlaveModuleList">{$ModuleList}</dd>
        								{else}
          							<dd>{$BugInfo.ModulePath}</dd>
        								{/if}
    								</dl>
          				</fieldset>
  	              <fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.OpenedBuild}</dt>
                    		<dd id="BuildContainer">
                      			{if $ActionType eq 'OpenBug' && $BugInfo.ResultID eq ''}
                        		<nobr>
                        			{if $OpenedBuildList eq ''}
                        			<input type=text name="OpenedBuildInput" id="OpenedBuildInput" size="35" maxlength="255" class="MyInput RequiredField" value="{$BugInfo.OpenedBuild}" />
                        			{else}
                        			<span id="Build">{$OpenedBuildList}</span>
                        			<img src="Image/edit.gif" align="middle" title="Edit"
                        				onclick="OpenedBuildTemp = document.BugForm.OpenedBuild.value;document.getElementById('Build').innerHTML = 
                        				'<input type=text name=OpenedBuild size=35 class=\'MyInput RequiredField\' value=' + OpenedBuildTemp + '>';this.style.display='none';">        
                        			{/if}
                        		</nobr>
                      			{elseif ($EditMode eq 'true' and $testIsOwner) || ($ActionType eq 'OpenBug' && $BugInfo.ResultID neq '')}
                        		<input type=text name="OpenedBuildInput" id="OpenedBuildInput" size="35" maxlength="255" class="MyInput RequiredField" value="{$BugInfo.OpenedBuild}" />
                      			{else}
                        		<input type=text name="OpenedBuildInput" id="OpenedBuildInput" size="35" readonly=true class="MyInput ReadOnlyField" value="{$BugInfo.OpenedBuild}" />
                      			{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.ResolvedBuild}</dt>
                  			<dd id="BuildContainer">
                    				{if $ActionType eq 'OpenBug'}
                        		<nobr>
                        			{if $ResolvedBuildList eq ''}
                        			<input type=text name="ResolvedBuildInput" id="ResolvedBuildInput" size="35" class="MyInput  RequiredField" maxlength="255" value="{$BugInfo.ResolvedBuild}" />
                        			{else}
                        			<span id="Build1">{$ResolvedBuildList}</span>
                        			<img src="Image/edit.gif" align="middle" title="Edit"
                        				onclick="ResolvedBuildTemp = document.BugForm.ResolvedBuild.value;document.getElementById('Build1').innerHTML = 
                        				'<input type=text name=ResolvedBuild size=35 class=\'MyInput RequiredField\' value=' + ResolvedBuildTemp + '>';this.style.display='none';">                        					       
                        			{/if}
                        		</nobr>
                      			{elseif $EditMode eq 'true'}
                        		<input type=text name="ResolvedBuildInput" id="ResolvedBuildInput" size="35" class="MyInput RequiredField" maxlength="255" value="{$BugInfo.ResolvedBuild}" />
                      			{else}
                        		<input type=text name="ResolvedBuildInput" id="ResolvedBuildInput" size="35" class="MyInput ReadOnlyField" readonly=true value="{$BugInfo.ResolvedBuild}" />
                        		{/if}
                  			</dd>
                  	</dl>
                	</fieldset>
                  <fieldset class="Normal FloatLeft" style="width: 94%">
                			<dl style="line-height:17pt">
                  				<dt>{$Lang.BugFields.HowFound}</dt>
                  				<dd>
                               {if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                          		 {$BugHowFoundList}
                      				 {else}
                          		 {$BugInfo.HowFoundName}
                               {/if}
                  				</dd>
                			</dl>
                  </fieldset>
                  <fieldset class="Normal FloatLeft" style="width: 94%">
                			<dl style="line-height:17pt">
                  				<dt>{$Lang.BugFields.BugSeverity}</dt>
                  				<dd>
                          		{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$BugSeverityList}
                      				{else}
                          		{$BugInfo.BugSeverityName}
                      				{/if}
                  				</dd>
                			</dl>
											<dl style="line-height:17pt">
                  				<dt>{$Lang.BugFields.BugPriority}</dt>
                  				<dd>
                          		{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$BugPriorityList}
                      				{else}
                              {$BugInfo.BugPriorityName}
                              {/if}
                  				</dd>
                			</dl>
                	</fieldset>
									{if $ActionType neq 'OpenBug'}
  	              <fieldset class="Normal FloatLeft" style="width: 94%">
                		<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.BugSubStatus}</dt>
                    		<dd>
                       			{if $ActionType eq 'OpenBug' or $EditMode eq 'true'}
                          	{$BugSubStatusList}      
                       			{else}
                          	{$BugInfo.BugSubStatusName}
                       			{/if}
                    		</dd>
                  	</dl>
                	</fieldset>
                	{/if}
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.BugFields.AssignedToDepartment}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenBug' or $EditMode eq 'true'}
                          	<span id="AssignedToDepartmentList">{$AssignedToDepartmentList}</span>
                      			{else}
                          	{$BugInfo.AssignedToDepartmentName}
                          	{/if}
                  			</dd>
                		</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.BugFields.OpenedByDepartment}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenBug' or $EditMode eq 'true'}
                          	<span id="OpenedByDepartmentList">{$OpenedByDepartmentList}</span>
                      			{else}
                          	{$BugInfo.OpenedByDepartmentName}
                          	{/if}
                  			</dd>
               	 		</dl>
	                </fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.BugFields.AssignedTo}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenBug' or $EditMode eq 'true'}
                          	<span id="AssignedToUserList">{$AssignedToUserList}</span>
                      			{else}
                          	{$BugInfo.AssignedToName}
                          	{/if}
                  			</dd>
                		</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.BugFields.MailTo}</dt>
                  			<dd>
                          {if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                          <input id="MailTo" name="MailTo" type="text" value="{$BugInfo.MailTo}" size="20"  maxlength="255" AUTOCOMPLETE="OFF" />
                      		{else}
                          <input id="MailToName" name="MailToName" type="text" value="{$BugInfo.MailToName}" size="20"  maxlength="255" readonly=true class="MyInput ReadOnlyField" AUTOCOMPLETE="OFF" />
                      		{/if}
                  			</dd>
               	 		</dl>
               	 		{if $EditMode eq 'true'}
               	 		<dl style="line-height:17pt">
      									<dt>
      										<input type="checkbox" id="SendNotifyEmail" name="SendNotifyEmail" checked="checked" value="SendNotifyEmail" onclick="SetNotifyEmail();"/>
      									</dt>
      									<dd>{$Lang.SendNotifyEmail}</dd>
										</dl>
										{/if}
	                </fieldset>
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl>
                    	<dt>{$Lang.BugFields.BugContent}</dt>
                    	<dd style="text-align:left;">
                      	{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                    			<textarea id="BugContent" name="BugContent" rows="11" cols="70" style="overflow-y:visible;">{$BugInfo.BugContent}</textarea>
                  			{else}
													<p style="overflow: auto">{$BugInfo.BugContent|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  	  </dd>
                   	</dl>
                	</fieldset>               	
                	<fieldset class="Normal FloatLeft" style="width: 94%">    									
    									<dl style="line-height:17pt">
                    			<dt>{$Lang.BugFields.ReproRate}</dt>
                    			<dd>
                        			{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                            	<input type="text" name="ReproRate" id="ReproRate" size="20" class="MyInput" maxlength="80" value="{$BugInfo.ReproRate}" />
                        			{else}
                            	<input type="text" name="ReproRate" id="ReproRate" size="20" readonly=true  class="MyInput ReadOnlyField" maxlength="80" value="{$BugInfo.ReproRate}" />
                        			{/if}
                    			</dd>
                  		</dl>
                  		<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.ReproSteps}</dt>
                    		<dd>
                  			{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                    			<textarea id="ReproSteps" name="ReproSteps" rows="11" cols="70" style="overflow-y:visible;">{$BugInfo.ReproSteps}</textarea>
                  			{else}
													<p style="overflow: auto">{$BugInfo.ReproSteps|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  			</dd>
                  		</dl>
                	</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                   	<legend>{$Lang.BugFiles}</legend>
                   	<dl>
                    	<dd style="text-align:left;">
                      		{assign var="FileList" value=$BugInfo.FileList}
                      		{include file="FileInfo.tpl"}
                  	  </dd>
                   	</dl>
                	</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl>
                    	<dt>{$Lang.BugFields.RootCause}</dt>
                    	<dd style="text-align:left;">
                      	{if $ActionType eq 'OpenBug' or $EditMode eq 'true'}
                    			<textarea id="RootCause" name="RootCause" rows="11" cols="70" style="overflow-y:visible;">{$BugInfo.RootCause}</textarea>
                  			{else}
													<p style="overflow: auto">{$BugInfo.RootCause|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  	  </dd>
                   	</dl>
                	</fieldset> 
                	<fieldset class="Normal FloatLeft" style="width: 94%">    									
    									<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.Resolution}</dt>
                    		<dd>
                    				{if $ActionType eq 'Activated'}
                      			{elseif $EditMode eq 'true'}
                            {$ResolutionList}
                            {else}
                            {$BugInfo.ResolutionName}
                            {if $BugInfo.BugStatus neq 'Active'}
                            <input type="hidden" name="Resolution" value="{$BugInfo.Resolution}" />
                            {/if}
                            {/if}
                  			</dd>
                  		</dl>
                  		<dl style="line-height:17pt">
                    		<dt>{$Lang.BugFields.ResolutionDetail}</dt>
                    		<dd>
                  			{if $ActionType eq 'OpenBug' or ($EditMode eq 'true' and $testIsOwner)}
                    			<textarea id="ResolutionDetail" name="ResolutionDetail" rows="11" cols="70" style="overflow-y:visible;">{$BugInfo.ResolutionDetail}</textarea>
                  			{else}
													<p style="overflow: auto">{$BugInfo.ResolutionDetail|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  			</dd>
                  		</dl>
                	</fieldset>
                	{if $EditMode eq 'true'}
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl>
                    	<dt>{$Lang.ReplyNote}</dt>
                    	<dd style="text-align:left;">
                      	<textarea id="ReplyNote" name="ReplyNote" rows="5" cols="70" style="overflow-y:visible;" ></textarea>
                  	  </dd>
                   	</dl>
                	</fieldset>
            	  	{/if}
            	</td>
          </tr>
          <tr>
              <td colspan="3">
                <fieldset id="CustomedFieldSet" class="Normal FloatLeft" style="width: 98%;{if $CustomedFieldHtml eq null}display:none{/if}">
                  {$CustomedFieldHtml}
                </fieldset>
              </td>
					</tr>
			</table>
   </div>
   <div id="BugHistory">
      <table style="width: 100%">
            <tr>
            	<td style="width: 20%" valign="top">
            	</td>
              <td style="width: 49%" valign="top">
                <fieldset class="Normal FloatLeft" id="BugHistoryInfo" style="width: 94%">
                  <legend>{$Lang.HistoryInfo}</legend>
                  {include file="BugHistory.tpl"}
                </fieldset>
              </td>
            </tr>
        </table>
  </div>
</div>

<input type="hidden" id="BugID" name="BugID" value="{$BugInfo.BugID}" />
<input type="hidden" id="DeleteFileIDs" name="DeleteFileIDs" value="" />
<input type="hidden" id="ActionType" name="ActionType" value="{$ActionType}" />
<input type="hidden" id="ActionObj" name="ActionObj" value="Bug" />
<input type="hidden" id="TestUserName" name="TestUserName" value="{$templatelite.session.TestUserName}" />
<input type="hidden" id="TestRealName" name="TestRealName" value="{$templatelite.session.TestRealName}" />
<input type="hidden" id="ToDisabledObj" name="ToDisabledObj" value="SubmitButton" />
<input type="hidden" id="CurrentProjectID" name="CurrentProjectID" value="{$ProjectID}" />
<input type="hidden" id="LastAcitonID" name="LastActionID" value="{$LastActionID}" />

</form>
{include file="PostActionFrame.tpl"}
{literal}
<script type="text/javascript">
//<![CDATA[
function superAddObjValue(objID,addValue)
{
    xajax.$(objID).value += ',' + addValue;
}
function DisplayDuplicateID()
{
	ItemCheckBox = document.getElementById('DuplicateCheckBox');
	ItemIsDuplicated = document.getElementById('IsDuplicated');
	ItemDuplicateID = document.getElementById('DuplicateID');
	
	//Revert the Element Value
	//alert(Element.checked);
	if(ItemCheckBox.checked==false)
	{
		ItemIsDuplicated.value = '';
		xajax.$('DuplicateID').value='';
		hiddenObj('DuplicateID');
	}
	else
	{
		ItemIsDuplicated.value = 'Duplicate';
		displayObj('DuplicateID');
		xajax.$('DuplicateID').focus();
	}
	//alert(ItemIsDuplicated.value);
}
function SetNotifyEmail()
{
	ItemCheckBox = document.getElementById('SendNotifyEmail');
	if(ItemCheckBox.checked==false)
	{
		ItemCheckBox.value = '';
	}
	else
	{
		ItemCheckBox.value = 'SendNotifyEmail';
	}
	//alert(ItemCheckBox.value);
}
{/literal}
{if $ActionType neq ''}
{literal}
  setConfirmExitArrays();
  initSelectDiv('MailTo','selectDivProjectUserList','getInputSearchValueByComma','setValueByComma', true);
  xajax.$('ReplyNote').focus();
{/literal}
{/if}
{if $ActionType eq 'OpenBug'}
   {literal}focusInputEndPos('BugTitle');{/literal}
{elseif $ActionType eq 'Resolved'}
   {literal}xajax.$('ResolvedBuild').focus();{/literal}
{/if}
{literal}
//]]>
</script>
{/literal}
</body>
</html>
