{include file="Header.tpl"}
<body class="BugMode BugMain" onload="TestMode='Change';initShowGotoBCR();" onscroll="setTopBarLeftPos()">
  {if $ActionType eq 'Edited' or $ActionType eq 'Resolved' or $ActionType eq 'Closed' or $ActionType eq 'Activated'}
  {assign var="EditMode" value= 'true'}
  {/if}

	{if $templatelite.session.TestUserName eq $ChangeInfo.OpenedBy}
	{assign var="testIsOpenedBy" value= 'true'}
	{/if}
	{if $templatelite.session.TestUserName eq $ChangeInfo.AssignedTo}
	{assign var="testIsAssignedTo" value= 'true'}
	{/if}
	{if $testIsOpenedBy or $testIsAssignedTo or $TestIsAdmin}
	{assign var="testIsOwner" value= 'true'}
	{/if}
	{if $testIsOpenedBy or $TestIsAdmin}
	{assign var="testIsCloser" value= 'true'}
	{/if}

  {if $ActionType eq 'OpenChange'}
    <form id="ChangeForm" name="ChangeForm" action="PostAction.php?Action=OpenChange" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {elseif $EditMode eq 'true'}
    <form id="ChangeForm" name="ChangeForm" action="PostAction.php?Action=EditChange" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {else}
    <form id="ChangeForm" name="ChangeForm" action="Change.php?ChangeID={$ChangeInfo.ChangeID}" method="post" target="_self">
  {/if}

<div id="TopNavMain" class="TopBar">
    <a id="TopNavLogo" href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    <!-- Set the Toolbar buttons status by Lichuan Liu -->
    {if $PreChangeID eq 0 or $EditMode eq 'true'}
    	{assign var="PreButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $NextChangeID eq 0 or $EditMode eq 'true'}
    	{assign var="NextButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ActionType eq 'OpenChange' or $EditMode eq 'true'}
    	{assign var="EditButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ChangeInfo.ChangeStatus neq 'Active' or $ActionType eq 'OpenChange'or $EditMode eq 'true'}
    	{assign var="ResolveChangeButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ChangeInfo.ChangeStatus eq 'Closed' or $ActionType eq 'OpenChange'or $EditMode eq 'true' or $testIsCloser neq 'true'}
    	{assign var="CloseChangeButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ChangeInfo.ChangeStatus eq 'Active' or $EditMode eq 'true'}
    	{assign var="ActiveChangeButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ActionType eq ''}
    	{assign var="SaveButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ChangeInfo.ChangeStatus neq 'Active' or $ActionType eq 'OpenChange'or $EditMode eq 'true' or $ChangeInfo.ReviewID neq ''}
    	{assign var="OpenReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}    
    
    <div id="ButtonList">
      <input type="button" class="ActionButton Btn" accesskey="P" value="{$Lang.PreButton}" onclick="location.href='Change.php?ChangeID={$PreChangeID}'" {$PreButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="N" value="{$Lang.NextButton}" onclick="location.href='Change.php?ChangeID={$NextChangeID}'" {$NextButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="E" value="{$Lang.EditChangeButton}" onclick="xajax.$('ActionType').value='Edited';submitForm('ChangeForm')" {$EditButtonStatus}}/>
      <!--input type="button" class="ActionButton Btn" accesskey="C" value="{$Lang.CopyChangeButton}" onclick="xajax.$('ActionType').value='OpenChange';submitForm('ChangeForm');" {$EditButtonStatus} /-->
      <input type="button" class="ActionButton Btn" accesskey="R" value="{$Lang.ResolveChangeButton}" onclick="xajax.$('ActionType').value='Resolved';submitForm('ChangeForm');" {$ResolveChangeButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="L" value="{$Lang.CloseChangeButton}" onclick="xajax.$('ActionType').value='Closed';submitForm('ChangeForm');" {$CloseChangeButtonStatus} />
      {if $TestIsAdmin}
      <input type="button" class="ActionButton Btn" accesskey="A" value="{$Lang.ActiveChangeButton}" onclick="xajax.$('ActionType').value='Activated';submitForm('ChangeForm');" {$ActiveChangeButtonStatus} />
      {/if}
      <input type="button" class="ActionButton Btn" accesskey="S" value="{$Lang.SaveButton}" name="SubmitButton" id="SubmitButton" onclick="this.disabled='disabled';NeedToConfirm=false;document.ChangeForm.submit();" {$SaveButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="B" value="{$Lang.OpenReviewButton}" {$OpenReviewButtonStatus}  onclick="openWindow('Review.php?ActionType=OpenReview&ChangeID={$ChangeInfo.ChangeID}','OpenReview');"/>
    </div>
</div>

<div id="BugMain" class="CommonForm BugMode">
	<span id="BugId">{if $ActionType eq 'OpenChange'}{$Lang.OpenChange}{else}{$ChangeInfo.ChangeIDName}{/if}</span>
  <div id="BugMainInfo">
  		<table style="width: 100%">
      		<tr>
          		<td style="width: 20%" valign="top">
          			<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.ChangeFields.ChangeID}</dt>
      									<dd>
      										{if $ActionType eq 'OpenChange'}{$Lang.OpenChange}{else}{$ChangeInfo.ChangeIDName}{/if}
      									</dd>
    								</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.ChangeFields.ChangeStatus}</dt>
                  			<dd>
                    			{if $ActionType eq 'Activated'}{$Lang.ChangeStatus.Active}
                    			{elseif $ActionType eq 'Closed'}{$Lang.ChangeStatus.Closed}
                    			{elseif $ActionType eq 'Resolved'}{$Lang.ChangeStatus.Resolved}
                    			{else}{$ChangeInfo.ChangeStatusName}{/if}
                  			</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl style="line-height:17pt">
                    		<dt>{$Lang.ChangeFields.OpenedBy}</dt>
                    		<dd>
                            {if $ActionType eq 'OpenChange'}
                            {$templatelite.session.TestRealName}
                        		{else}
                            {$ChangeInfo.OpenedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                	  <dl style="line-height:17pt">
                    		<dt>{$Lang.ChangeFields.ResolvedBy}</dt>
                    		<dd>
                        		{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Resolved'}
                          	{$ActionRealName}
                        		{else}
                          	{$ChangeInfo.ResolvedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                    <dl style="line-height:17pt">
                    		<dt>{$Lang.ChangeFields.ClosedBy}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Closed'}
                          	{$ActionRealName}
                        		{else}
                          	{$ChangeInfo.ClosedByName}
                      			{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.ChangeFields.LastEditedBy}</dt>
                  			<dd>{$ChangeInfo.LastEditedByName}</dd>
                		</dl>
                </fieldset>
              	<fieldset class="Normal FloatLeft" style="width: 94%">
										<dl style="line-height:17pt">
                    		<dt>{$Lang.ChangeFields.OpenedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'OpenChange'}
                            {$templatelite.now|date_format:"%Y-%m-%d"}
                        		{else}
                            {$ChangeInfo.OpenedDate|date_format:"%Y-%m-%d"}
                        		{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.ChangeFields.ResolvedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Resolved'}
                            {$ActionDate}
                						{else}
                  					{if $ChangeInfo.ResolvedDate neq $CFG.ZeroTime}{$ChangeInfo.ResolvedDate|date_format:"%Y-%m-%d"} {/if}
                						{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.ChangeFields.ClosedDate}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                      			{elseif $ActionType eq 'Closed'}
                        		{$ActionDate}
                      			{else}
                        		{if $ChangeInfo.ClosedDate neq $CFG.ZeroTime}{$ChangeInfo.ClosedDate|date_format:"%Y-%m-%d"}{/if}{/if}
                    		</dd>
                  	</dl>
										<dl style="line-height:17pt">
                  			<dt>{$Lang.ChangeFields.LastEditedDate}</dt>
                  			<dd>{$ChangeInfo.LastEditedDate|date_format:"%Y-%m-%d"}</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.ChangeFields.BugID}</dt>
                    			<dd>
                      		<a href="Bug.php?BugID={$ChangeInfo.BugID}" title="{$ChangeInfo.BugTitle}" target="_top">{$Lang.BugIDPrefix}{$ChangeInfo.BugID}</a>
                      		<input type="hidden" name="BugID" value="{$ChangeInfo.BugID}" />
                    			</dd>
                  		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.ChangeFields.ReviewID}</dt>
                    			<dd>
                         	{foreach from=$ChangeInfo.ReviewIDList key=key value=value}
                          <a href="Review.php?ReviewID={$key}" title="{$value}" target="_top">{$Lang.ReviewIDPrefix}{$key}</a>
                         	{/foreach}
                    			</dd>
                  		</dl>
                </fieldset>
              </td>
              <td style="width: 49%" valign="top">
              		<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.ChangeFields.ChangeTitle}</dt>
      										<dd>
          										{if $ActionType eq 'OpenChange' or ($EditMode eq 'true' and $testIsOwner)}
					            				<input type="text" id="ChangeTitle" name="ChangeTitle" class="MyInput RequiredField" value="{$ChangeInfo.ChangeTitle}" style="width:500px;"/>
          										{else}
            									<input type="text" id="ChangeTitle" name="ChangeTitle" readonly=readonly class="MyInput ReadOnlyField" title="{$ChangeInfo.ChangeTitle}" value="{$ChangeInfo.ChangeTitle}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
    							</fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
                			<dl style="line-height:17pt">
                  				<dt>{$Lang.ChangeFields.ChangeType}</dt>
                  				<dd>
                              {if $ActionType eq 'OpenChange' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$ChangeTypeList}
                      				{else}
                          		{$ChangeInfo.ChangeTypeName}
                              {/if}
                  				</dd>
                			</dl>
                	</fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.ChangeFields.ProjectName}</dt>
         								{if $ActionType eq 'OpenChange' or ($EditMode eq 'true' and $testIsOwner)}
          							<dd>{$ProjectList}</dd>
        								{else}
          							<dd>{$ChangeInfo.ProjectName}</dd>
        								{/if}
    								</dl>
        						<dl style="line-height:17pt">
      									<dt>{$Lang.ChangeFields.ModulePath}</dt>
         								{if $ActionType eq 'OpenChange' or ($EditMode eq 'true' and $testIsOwner)}
            						<dd id="SlaveModuleList">{$ModuleList}</dd>
        								{else}
          							<dd>{$ChangeInfo.ModulePath}</dd>
        								{/if}
    								</dl>
          				</fieldset>
  	              <fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.ChangeFields.OpenedBuild}</dt>
                    		<dd id="BuildContainer">
                      			{if $ActionType eq 'OpenChange'}
                        		<nobr>
                        			{if $OpenedBuildList eq ''}
                        			<input type=text name="OpenedBuildInput" id="OpenedBuildInput" size="35" maxlength="255" class="MyInput RequiredField" value="{$ChangeInfo.OpenedBuild}" />
                        			{else}
                        			<span id="Build">{$OpenedBuildList}</span>
                        			<img src="Image/edit.gif" align="middle" title="Edit"
                        				onclick="OpenedBuildTemp = document.ChangeForm.OpenedBuild.value;document.getElementById('Build').innerHTML = 
                        				'<input type=text name=OpenedBuild size=35 class=\'MyInput RequiredField\' value=' + OpenedBuildTemp + '>';this.style.display='none';">        
                        			{/if}
                        		</nobr>
                      			{elseif ($EditMode eq 'true' and $testIsOwner)}
                        		<input type=text name="OpenedBuildInput" id="OpenedBuildInput" size="35" maxlength="255" class="MyInput RequiredField" value="{$ChangeInfo.OpenedBuild}" />
                      			{else}
                        		<input type=text name="OpenedBuildInput" id="OpenedBuildInput" size="35" readonly=true class="MyInput ReadOnlyField" value="{$ChangeInfo.OpenedBuild}" />
                      			{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.ChangeFields.ResolvedBuild}</dt>
                  			<dd id="BuildContainer">
                    				{if $ActionType eq 'OpenChange'}
                        		<nobr>
                        			{if $ResolvedBuildList eq ''}
                        			<input type=text name="ResolvedBuildInput" id="ResolvedBuildInput" size="35" class="MyInput RequiredField" maxlength="255" value="{$ChangeInfo.ResolvedBuild}" />
                        			{else}
                        			<span id="Build1">{$ResolvedBuildList}</span>
                        			<img src="Image/edit.gif" align="middle" title="Edit"
                        				onclick="ResolvedBuildTemp = document.ChangeForm.ResolvedBuild.value;document.getElementById('Build1').innerHTML = 
                        				'<input type=text name=ResolvedBuild size=35 class=\'MyInput RequiredField\' value=' + ResolvedBuildTemp + '>';this.style.display='none';">                        					       
                        			{/if}
                        		</nobr>
                      			{elseif $EditMode eq 'true'}
                        		<input type=text name="ResolvedBuildInput" id="ResolvedBuildInput" size="35" class="MyInput RequiredField" maxlength="255" value="{$ChangeInfo.ResolvedBuild}" />
                      			{else}
                        		<input type=text name="ResolvedBuildInput" id="ResolvedBuildInput" size="35" class="MyInput ReadOnlyField" readonly=true value="{$ChangeInfo.ResolvedBuild}" />
                        		{/if}
                  			</dd>
                  	</dl>
                	</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.ChangeFields.AssignedTo}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenChange' or $EditMode eq 'true'}
                          	<span id="AssignedToUserList">{$AssignedToUserList}</span>
                      			{else}
                          	{$ChangeInfo.AssignedToName}
                          	{/if}
                  			</dd>
                		</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.ChangeFields.MailTo}</dt>
                  			<dd>
                          {if $ActionType eq 'OpenChange' or $EditMode eq 'true'}
                          <input id="MailTo" name="MailTo" type="text" value="{$ChangeInfo.MailTo}" size="20"  maxlength="255" AUTOCOMPLETE="OFF" />
                      		{else}
                          <input id="MailTo" name="MailTo" type="text" value="{$ChangeInfo.MailToName}" size="20"  maxlength="255" readonly=true class="MyInput ReadOnlyField" AUTOCOMPLETE="OFF" />
                      		{/if}
                  			</dd>
               	 		</dl>
               	 		{if $EditMode eq 'true'}
               	 		<dl style="line-height:17pt">
      									<dt>
      										<input type="checkbox" id="SendNotifyEmail" name="SendNotifyEmail" checked="checked" value="SendNotifyEmail" onclick="SetNotifyEmail();"/>
      									</dt>
      									<dd>{$Lang.SendNotifyEmail}</dd>
										</dl>
										{/if}
	                </fieldset>
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl>
                    	<dt>{$Lang.ChangeFields.RootCause}</dt>
                    	<dd style="text-align:left;">
                      	{if $ActionType eq 'OpenChange' or $EditMode eq 'true'}
                    			<textarea id="RootCause" name="RootCause" rows="11" cols="70" style="overflow-y:visible;">{$ChangeInfo.RootCause}</textarea>
                  			{else}
													<p style="overflow: auto">{$ChangeInfo.RootCause|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  	  </dd>
                   	</dl>
                	</fieldset>    
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl>
                    	<dt>{$Lang.ChangeFields.Resolution}</dt>
                    	<dd style="text-align:left;">
                      	{if $ActionType eq 'OpenChange' or $EditMode eq 'true'}
                    			<textarea id="Resolution" name="Resolution" rows="11" cols="70" style="overflow-y:visible;">{$ChangeInfo.Resolution}</textarea>
                  			{else}
													<p style="overflow: auto">{$ChangeInfo.Resolution|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  	  </dd>
                   	</dl>
                	</fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
                   	<legend>{$Lang.ChangeFiles}</legend>
                   	<dl>
                    	<dd style="text-align:left;">
                      		{assign var="FileList" value=$ChangeInfo.FileList}
                      		{include file="FileInfo.tpl"}
                  	  </dd>
                   	</dl>
                	</fieldset>
                	{if $ActionType eq 'OpenChange' or $EditMode eq 'true'}
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl>
                    	<dt>{$Lang.ReplyNote}</dt>
                    	<dd style="text-align:left;">
                      	<textarea id="ReplyNote" name="ReplyNote" rows="5" cols="70" style="overflow-y:visible;" ></textarea>
                  	  </dd>
                   	</dl>
                	</fieldset>
            	  	{/if}
            	</td>
          </tr>
          <tr>
              <td colspan="3">
                <fieldset id="CustomedFieldSet" class="Normal FloatLeft" style="width: 98%;{if $CustomedFieldHtml eq null}display:none{/if}">
                  {$CustomedFieldHtml}
                </fieldset>
              </td>
					</tr>
			</table>
   </div>
   <div id="BugHistory">
      <table style="width: 100%">
          <tr>
            	<td style="width: 20%" valign="top">
            	</td>
              <td style="width: 49%" valign="top">
                <fieldset class="Normal FloatLeft" id="ChangeHistoryInfo" style="width: 94%">
                  <legend>{$Lang.HistoryInfo}</legend>
                  {include file="ChangeHistory.tpl"}
                </fieldset>
              </td>
          </tr>
      </table>
  	</div>
</div>

<input type="hidden" id="ChangeID" name="ChangeID" value="{$ChangeInfo.ChangeID}" />
<input type="hidden" id="DeleteFileIDs" name="DeleteFileIDs" value="" />
<input type="hidden" id="ActionType" name="ActionType" value="{$ActionType}" />
<input type="hidden" id="ActionObj" name="ActionObj" value="Change" />
<input type="hidden" id="TestUserName" name="TestUserName" value="{$templatelite.session.TestUserName}" />
<input type="hidden" id="TestRealName" name="TestRealName" value="{$templatelite.session.TestRealName}" />
<input type="hidden" id="ToDisabledObj" name="ToDisabledObj" value="SubmitButton" />
<input type="hidden" id="CurrentProjectID" name="CurrentProjectID" value="{$ProjectID}" />
<input type="hidden" id="LastAcitonID" name="LastActionID" value="{$LastActionID}" />

</form>
{include file="PostActionFrame.tpl"}
{literal}
<script type="text/javascript">
//<![CDATA[
function superAddObjValue(objID,addValue)
{
    xajax.$(objID).value += ',' + addValue;
}
function SetNotifyEmail()
{
	ItemCheckBox = document.getElementById('SendNotifyEmail');
	if(ItemCheckBox.checked==false)
	{
		ItemCheckBox.value = '';
	}
	else
	{
		ItemCheckBox.value = 'SendNotifyEmail';
	}
	//alert(ItemCheckBox.value);
}
{/literal}
{if $ActionType neq ''}
{literal}
  setConfirmExitArrays();
  initSelectDiv('MailTo','selectDivProjectUserList','getInputSearchValueByComma','setValueByComma', true);
  xajax.$('ReplyNote').focus();
{/literal}
{/if}
{if $ActionType eq 'OpenChange'}
   {literal}focusInputEndPos('ChangeTitle');{/literal}
{elseif $ActionType eq 'Resolved'}
   {literal}xajax.$('ResolvedBuild').focus();{/literal}
{/if}
{literal}
//]]>
</script>
{/literal}
</body>
</html>
