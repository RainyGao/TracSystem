{include file="Header.tpl"}
<body class="BugMode BugMain" onload="TestMode='ReviewComment';initShowGotoBCR();" onscroll="setTopBarLeftPos()">
  {if $ActionType eq 'Edited' or $ActionType eq 'Resolved' or $ActionType eq 'Closed' or $ActionType eq 'Activated'}
  {assign var="EditMode" value= 'true'}
  {/if}

  {if ($templatelite.session.TestUserName eq $ReviewInfo.Moderator)}
	{assign var="testIsModerator" value= 'true'}
	{/if}
	{if $testIsModerator or $TestIsAdmin}
	{assign var="testIsCloser" value= 'true'}
	{/if}

  {if $ActionType eq 'OpenReviewComment'}
    <form id="ReviewCommentForm" name="ReviewCommentForm" action="PostAction.php?Action=OpenReviewComment" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {elseif $EditMode eq 'true'}
    <form id="ReviewCommentForm" name="ReviewCommentForm" action="PostAction.php?Action=EditReviewComment" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {else}
    <form id="ReviewCommentForm" name="ReviewCommentForm" action="ReviewComment.php?ReviewCommentID={$ReviewCommentInfo.ReviewCommentID}" method="post" target="_self">
  {/if}

<div id="TopNavMain" class="TopBar">
    <a id="TopNavLogo" href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    <!-- Set the Toolbar buttons status by Lichuan Liu -->
    {if $PreReviewCommentID eq 0 or $EditMode eq 'true'}
    		{assign var="PreButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $NextReviewCommentID eq 0 or $EditMode eq 'true'}
    		{assign var="NextButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
    		{assign var="EditButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ReviewCommentInfo.ReviewCommentStatus neq 'Active' or $ActionType eq 'OpenReviewComment'or $EditMode eq 'true'}
    		{assign var="ResolveReviewCommentButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ReviewCommentInfo.ReviewCommentStatus eq 'Closed' or $ActionType eq 'OpenReviewComment'or $EditMode eq 'true' or $testIsCloser neq 'true'}
    		{assign var="CloseReviewCommentButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ReviewCommentInfo.ReviewCommentStatus eq 'Active' or $EditMode eq 'true'}
    		{assign var="ActiveReviewCommentButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ActionType eq ''}
    		{assign var="SaveButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    
    <div id="ButtonList">
      <input type="button" class="ActionButton Btn" accesskey="P" value="{$Lang.PreButton}" onclick="location.href='ReviewComment.php?ReviewCommentID={$PreReviewCommentID}'" {$PreButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="N" value="{$Lang.NextButton}" onclick="location.href='ReviewComment.php?ReviewCommentID={$NextReviewCommentID}'" {$NextButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="E" value="{$Lang.EditReviewCommentButton}" onclick="xajax.$('ActionType').value='Edited';submitForm('ReviewCommentForm')" {$EditButtonStatus}}/>
      <input type="button" class="ActionButton Btn" accesskey="C" value="{$Lang.CopyReviewCommentButton}" onclick="xajax.$('ActionType').value='OpenReviewComment';submitForm('ReviewCommentForm');" {$EditButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="R" value="{$Lang.ResolveReviewCommentButton}" onclick="xajax.$('ActionType').value='Resolved';submitForm('ReviewCommentForm');" {$ResolveReviewCommentButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="L" value="{$Lang.CloseReviewCommentButton}" onclick="xajax.$('ActionType').value='Closed';submitForm('ReviewCommentForm');" {$CloseReviewCommentButtonStatus} />
      <!--input type="button" class="ActionButton Btn" accesskey="A" value="{$Lang.ActiveReviewCommentButton}" onclick="xajax.$('ActionType').value='Activated';submitForm('ReviewCommentForm');" {$ActiveReviewCommentButtonStatus} /-->
      <input type="button" class="ActionButton Btn" accesskey="S" value="{$Lang.SaveButton}" name="SubmitButton" id="SubmitButton" onclick="this.disabled='disabled';NeedToConfirm=false;document.ReviewCommentForm.submit();" {$SaveButtonStatus}/>
    </div>
</div>

<div id="BugMain" class="CommonForm BugMode">
	<span id="BugId">{if $ActionType eq 'OpenReviewComment'}{$Lang.OpenReviewComment}{else}{$ReviewCommentInfo.ReviewCommentID}{/if}</span>
  <div id="BugMainInfo">
  		<table style="width: 100%">
      		<tr>
          		<td style="width: 20%" valign="top">
          			<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.ReviewCommentFields.ReviewCommentID}</dt>
      									<dd>
      										{if $ActionType eq 'OpenReviewComment'}{$Lang.OpenReviewComment}{else}{$ReviewCommentInfo.ReviewCommentID}{/if}
      									</dd>
    								</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewCommentFields.ReviewCommentStatus}</dt>
                  			<dd>
                    			{if $ActionType eq 'Activated'}{$Lang.ReviewCommentStatus.Active}
                    			{elseif $ActionType eq 'Closed'}{$Lang.ReviewCommentStatus.Closed}
                    			{elseif $ActionType eq 'Resolved'}{$Lang.ReviewCommentStatus.Resolved}
                    			{else}{$ReviewCommentInfo.ReviewCommentStatusName}{/if}
                  			</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewCommentFields.OpenedBy}</dt>
                    		<dd>
                            {if $ActionType eq 'OpenReviewComment'}
                            {$templatelite.session.TestRealName}
                        		{else}
                            {$ReviewCommentInfo.OpenedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                	  <dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewCommentFields.ResolvedBy}</dt>
                    		<dd>
                        		{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Resolved'}
                          	{$ActionRealName}
                        		{else}
                          	{$ReviewCommentInfo.ResolvedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                    <dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewCommentFields.ClosedBy}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Closed'}
                          	{$ActionRealName}
                        		{else}
                          	{$ReviewCommentInfo.ClosedByName}
                      			{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewCommentFields.LastEditedBy}</dt>
                  			<dd>{$ReviewCommentInfo.LastEditedByName}</dd>
                		</dl>
                </fieldset>
              	<fieldset class="Normal FloatLeft" style="width: 94%">
										<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewCommentFields.OpenedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'OpenReviewComment'}
                            {$templatelite.now|date_format:"%Y-%m-%d"}
                        		{else}
                            {$ReviewCommentInfo.OpenedDate|date_format:"%Y-%m-%d"}
                        		{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewCommentFields.ResolvedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Resolved'}
                            {$ActionDate}
                						{else}
                  					{if $ReviewCommentInfo.ResolvedDate neq $CFG.ZeroTime}{$ReviewCommentInfo.ResolvedDate|date_format:"%Y-%m-%d"} {/if}
                						{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewCommentFields.ClosedDate}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                      			{elseif $ActionType eq 'Closed'}
                        		{$ActionDate}
                      			{else}
                        		{if $ReviewCommentInfo.ClosedDate neq $CFG.ZeroTime}{$ReviewCommentInfo.ClosedDate|date_format:"%Y-%m-%d"}{/if}{/if}
                    		</dd>
                  	</dl>
										<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewCommentFields.LastEditedDate}</dt>
                  			<dd>{$ReviewCommentInfo.LastEditedDate|date_format:"%Y-%m-%d"}</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.ReviewCommentFields.ReviewID}</dt>
                    			<dd>
                      		<a href="Review.php?ReviewID={$ReviewCommentInfo.ReviewID}" title="{$ReviewCommentInfo.ReviewTitle}" target="_top">{$Lang.ReviewIDPrefix}{$ReviewCommentInfo.ReviewID}</a>
                      		<input type="hidden" name="ReviewID" value="{$ReviewCommentInfo.ReviewID}" />
                    			</dd>
                  		</dl>
                  		<dl style="line-height:17pt">&nbsp;</dl>
                	</fieldset>
              </td>
              <td style="width: 49%" valign="top">
              		<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.ReviewCommentFields.ReviewCommentTitle}</dt>
      										<dd>
          										{if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
					            				<input type="text" id="ReviewCommentTitle" name="ReviewCommentTitle" class="MyInput RequiredField" value="{$ReviewCommentInfo.ReviewCommentTitle}" style="width:500px;"/>
          										{else}
            									<input type="text" id="ReviewCommentTitle" name="ReviewCommentTitle" readonly=readonly class="MyInput ReadOnlyField" title="{$ReviewCommentInfo.ReviewCommentTitle}" value="{$ReviewCommentInfo.ReviewCommentTitle}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
    							</fieldset>
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                			<dl style="line-height:17pt">
                  				<dt>{$Lang.ReviewCommentFields.ReviewCommentType}</dt>
                  				<dd>
                              {if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
                          		{$ReviewCommentTypeList}
                      				{else}
                          		{$ReviewCommentInfo.ReviewCommentTypeName}
                              {/if}
                  				</dd>
                			</dl>
                	</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$ReviewCommentInfo.ReviewCommentPos1Name}</dt>
      										<dd>
          										{if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
					            				<input type="text" id="ReviewCommentPos1" name="ReviewCommentPos1" class="MyInput RequiredField" value="{$ReviewCommentInfo.ReviewCommentPos1}" style="width:500px;"/>
          										{else}
            									<input type="text" id="ReviewCommentPos1" name="ReviewCommentPos1" readonly=readonly class="MyInput ReadOnlyField" title="{$ReviewCommentInfo.ReviewCommentPos1}" value="{$ReviewCommentInfo.ReviewCommentPos1}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
    							</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$ReviewCommentInfo.ReviewCommentPos2Name}</dt>
      										<dd>
          										{if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
					            				<input type="text" id="ReviewCommentPos2" name="ReviewCommentPos2" class="MyInput RequiredField" value="{$ReviewCommentInfo.ReviewCommentPos2}" style="width:500px;"/>
          										{else}
            									<input type="text" id="ReviewCommentPos2" name="ReviewCommentPos2" readonly=readonly class="MyInput ReadOnlyField" title="{$ReviewCommentInfo.ReviewCommentPos2}" value="{$ReviewCommentInfo.ReviewCommentPos2}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
    							</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$ReviewCommentInfo.ReviewCommentPos3Name}</dt>
      										<dd>
          										{if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
					            				<input type="text" id="ReviewCommentPos3" name="ReviewCommentPos3" class="MyInput RequiredField" value="{$ReviewCommentInfo.ReviewCommentPos3}" style="width:500px;"/>
          										{else}
            									<input type="text" id="ReviewCommentPos3" name="ReviewCommentPos3" readonly=readonly class="MyInput ReadOnlyField" title="{$ReviewCommentInfo.ReviewCommentPos3}" value="{$ReviewCommentInfo.ReviewCommentPos3}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
    							</fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
 					       		<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewCommentFields.ReviewCommentContent}</dt>
                    		<dd>
                    			{if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
														<textarea id="ReviewCommentContent" name="ReviewCommentContent" rows="6" cols="70" style="overflow-y:visible;">{$ReviewCommentInfo.ReviewCommentContent}</textarea>
													{else}
													<p style="overflow: auto">{$ReviewCommentInfo.ReviewCommentContent|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                    		</dd>
                  	</dl>
    							</fieldset>
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl>
                    	<dt>{$Lang.ReviewCommentFields.Resolution}</dt>
                    	<dd style="text-align:left;">
                      	{if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
                    			<textarea id="Resolution" name="Resolution" rows="11" cols="70" style="overflow-y:visible;">{$ReviewCommentInfo.Resolution}</textarea>
                  			{else}
													<p style="overflow: auto">{$ReviewCommentInfo.Resolution|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  	  </dd>
                   	</dl>
                	</fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
 					       	   <dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewCommentFields.ReviewCommentOwner}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenReviewComment' or $EditMode eq 'true'}
                          	<span id="AssignedToUserList">{$ReviewCommentOwnerList}</span>
                      			{else}
                          	{$ReviewCommentInfo.ReviewCommentOwnerName}
                          	{/if}
                  			</dd>
               	 		 </dl>
               	 	</fieldset>
            	</td>
          </tr>
          <tr>
              <td colspan="3">
                <fieldset id="CustomedFieldSet" class="Normal FloatLeft" style="width: 98%;{if $CustomedFieldHtml eq null}display:none{/if}">
                  {$CustomedFieldHtml}
                </fieldset>
              </td>
					</tr>
			</table>
   </div>
</div>

<input type="hidden" id="ReviewCommentID" name="ReviewCommentID" value="{$ReviewCommentInfo.ReviewCommentID}" />
<input type="hidden" id="DeleteFileIDs" name="DeleteFileIDs" value="" />
<input type="hidden" id="ActionType" name="ActionType" value="{$ActionType}" />
<input type="hidden" id="ActionObj" name="ActionObj" value="ReviewComment" />
<input type="hidden" id="TestUserName" name="TestUserName" value="{$templatelite.session.TestUserName}" />
<input type="hidden" id="TestRealName" name="TestRealName" value="{$templatelite.session.TestRealName}" />
<input type="hidden" id="ToDisabledObj" name="ToDisabledObj" value="SubmitButton" />
<input type="hidden" id="CurrentProjectID" name="CurrentProjectID" value="{$ProjectID}" />
<input type="hidden" id="LastAcitonID" name="LastActionID" value="{$LastActionID}" />

</form>
{include file="PostActionFrame.tpl"}
{literal}
<script type="text/javascript">
//<![CDATA[
function superAddObjValue(objID,addValue)
{
    xajax.$(objID).value += ',' + addValue;
}
{/literal}
{if $ActionType neq ''}
{literal}
  setConfirmExitArrays();
  initSelectDiv('MailTo','selectDivProjectUserList','getInputSearchValueByComma','setValueByComma', true);
  xajax.$('ReplyNote').focus();
{/literal}
{/if}
{if $ActionType eq 'OpenReviewComment'}
   {literal}focusInputEndPos('ReviewCommentTitle');{/literal}
{elseif $ActionType eq 'Resolved'}
   {literal}xajax.$('ResolvedBuild').focus();{/literal}
{/if}
{literal}
//]]>
</script>
{/literal}
</body>
</html>
