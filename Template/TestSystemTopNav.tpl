{include file="Header.tpl"}
<body id="BGColor" onload="this.focus();initShowGotoBCR();">
  <div id="TopNavMain">
    <div id="TopNavLogo">
      <a href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    </div>
    <div id="TopNavAbout">
      <span>{$Lang.Welcome},</span>
      <span id="UserName" class="UserName">{$TestRealName}</span>
      <a id="EditMyInfo" href="EditMyInfo.php" target="_blank">{$Lang.EditPer}</a>
      <a id="TarcSystem" href="index.php" target="_blank">{$Lang.TracSystem}</a>
      {if $TestIsAdmin || $TestIsProjectAdmin}
      <a href="Admin/" target="_blank">{$Lang.Admin}</a>
      {/if} 
      <a href="Logout.php?Logout=Yes">{$Lang.Logout}</a>
    </div>
    <div id="ProjectList">{$TopNavProjectList}</div>
    <div id="TopNavButton">
    <ul>
      <li id="TopNavPlanLi"   class="Cram{if $TestMode eq 'Plan'} Active{/if}">  <a href="TestSystem.php?TestMode=Plan" id="TopNavPlan" target="_top"><span class="BugMenuButton">{$Lang.TopNavPlan}</span></a></li>
      <li id="TopNavCaseLi"   class="Cram{if $TestMode eq 'Case'} Active{/if}">  <a href="TestSystem.php?TestMode=Case" id="TopNavCase" target="_top"><span class="BugMenuButton">{$Lang.TopNavCase}</span></a></li>
      <li id="TopNavResultLi" class="Cram{if $TestMode eq 'Result'} Active{/if}"><a href="TestSystem.php?TestMode=Result" id="TopNavResult" target="_top"><span class="BugMenuButton">{$Lang.TopNavResult}</span></a></li>
    </ul>
    </div>
    <div id="Open">
      <a href="Plan.php?ActionType=OpenPlan" id="OpenPlan" class="BigButton OpenPlan" target="_blank"{if $TestMode neq 'Plan'} style="display:none;"{/if}>
        {$Lang.OpenPlan}
      </a>
      <a href="Case.php?ActionType=OpenCase" id="OpenCase" class="BigButton OpenCase" target="_blank"{if $TestMode neq 'Case'} style="display:none;"{/if}>
        {$Lang.OpenCase}
      </a>
    </div>
  </div>
</body>
</html>