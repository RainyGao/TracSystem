{include file="Header.tpl"}
<!--script>if(parent.window==window)window.location='ReviewIndex.php';</script-->
<body class="BugMode BugMain" onload="TestMode='Review';initShowGotoBCR();" onscroll="setTopBarLeftPos()">
  {if $ActionType eq 'Edited' or $ActionType eq 'Resolved' or $ActionType eq 'Closed' or $ActionType eq 'Activated'}
  {assign var="EditMode" value= 'true'}
  {/if}
  
  <!--Rainy Added Start: Set the CurrentUser's properity-->
	{if ($templatelite.session.TestUserName eq $ReviewInfo.OpenedBy)}
	{assign var="testIsOpenedBy" value= 'true'}
	{/if}
  {if ($templatelite.session.TestUserName eq $ReviewInfo.Author)}
	{assign var="testIsAuthor" value= 'true'}
	{/if}
  {if ($templatelite.session.TestUserName eq $ReviewInfo.Moderator)}
	{assign var="testIsModerator" value= 'true'}
	{/if}
	{if $testIsOpenedBy or $testIsAuthor or $testIsModerator or $TestIsAdmin}
	{assign var="testIsOwner" value= 'true'}
	{/if}
	{if $testIsModerator or $TestIsAdmin}
	{assign var="testIsCloser" value= 'true'}
	{/if}
	<!--Rainy Added End: Set the CurrentUser's properity-->
	
  {if $ActionType eq 'OpenReview'}
    <form id="ReviewForm" name="ReviewForm" action="PostAction.php?Action=OpenReview" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {elseif $EditMode eq 'true'}
    <form id="ReviewForm" name="ReviewForm" action="PostAction.php?Action=EditReview" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {else}
    <form id="ReviewForm" name="ReviewForm" action="Review.php?ReviewID={$ReviewInfo.ReviewID}" method="post" target="_self">
  {/if}

<div id="TopNavMain" class="TopBar">
    <a id="TopNavLogo" href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    <!-- Set the Toolbar buttons status by Lichuan Liu -->
    {if $PreReviewID eq 0 or $EditMode eq 'true'}
    	{assign var="PreButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $NextReviewID eq 0 or $EditMode eq 'true'}
    	{assign var="NextButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ActionType eq 'OpenReview' or $EditMode eq 'true'}
    	{assign var="EditButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ReviewInfo.ReviewStatus neq 'Active' or $ActionType eq 'OpenReview'or $EditMode eq 'true' or $testIsOwner neq 'true'}
    	{assign var="ResolveReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ReviewInfo.ReviewStatus eq 'Closed' or $ActionType eq 'OpenReview' or $EditMode eq 'true' or $testIsCloser neq 'true'}
    	{assign var="CloseReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ReviewInfo.ReviewStatus eq 'Active' or $ActionType eq 'OpenReview' or $EditMode eq 'true'}
    	{assign var="ActiveReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ActionType eq ''}
    	{assign var="SaveButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ReviewInfo.ReviewStatus neq 'Active' or $EditMode eq 'true'}
    	{assign var="OpenReviewCommentButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    
    <div id="ButtonList">
      <input type="button" class="ActionButton Btn" accesskey="P" value="{$Lang.PreButton}" onclick="location.href='Review.php?ReviewID={$PreReviewID}'" {$PreButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="N" value="{$Lang.NextButton}" onclick="location.href='Review.php?ReviewID={$NextReviewID}'" {$NextButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="E" value="{$Lang.EditReviewButton}" onclick="xajax.$('ActionType').value='Edited';submitForm('ReviewForm')" {$EditButtonStatus}}/>
      <!--input type="button" class="ActionButton Btn" accesskey="C" value="{$Lang.CopyReviewButton}" onclick="xajax.$('ActionType').value='OpenReview';submitForm('ReviewForm');" {$EditButtonStatus} /-->
      <!--input type="button" class="ActionButton Btn" accesskey="R" value="{$Lang.ResolveReviewButton}" onclick="xajax.$('ActionType').value='Resolved';submitForm('ReviewForm');" {$ResolveReviewButtonStatus} /-->
      <input type="button" class="ActionButton Btn" accesskey="L" value="{$Lang.CloseReviewButton}" onclick="xajax.$('ActionType').value='Closed';submitForm('ReviewForm');" {$CloseReviewButtonStatus} />
      {if $TestIsAdmin}
      <input type="button" class="ActionButton Btn" accesskey="A" value="{$Lang.ActiveReviewButton}" onclick="xajax.$('ActionType').value='Activated';submitForm('ReviewForm');" {$ActiveReviewButtonStatus}>
      {/if}
      <input type="button" class="ActionButton Btn" accesskey="S" value="{$Lang.SaveButton}" name="SubmitButton" id="SubmitButton" onclick="this.disabled='disabled';NeedToConfirm=false;document.ReviewForm.submit();" {$SaveButtonStatus}/>
			<input type="button" class="ActionButton Btn" accesskey="B" value="{$Lang.OpenReviewCommentButton}" {$OpenReviewCommentButtonStatus}  onclick="openWindow('ReviewComment.php?ActionType=OpenReviewComment&ReviewID={$ReviewInfo.ReviewID}','OpenReviewComment');"/>
    </div>
</div>

<div id="BugMain" class="CommonForm BugMode">
  <a id="CommentList" href="./ReviewCommentList1.php?ReviewID={$ReviewInfo.ReviewID}" target="_top">{$Lang.GoToReviewCommentList}&gt;&gt;</a>
	<span id="ReviewId">
		{if $ActionType eq 'OpenReview'}{$Lang.OpenReview}{else}{$ReviewInfo.ReviewIDName}{/if}
	</span>
  <div id="BugMainInfo">
  		<table style="width: 100%">
      		<tr>
          		<td style="width: 20%" valign="top">
          			<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.ReviewFields.ReviewID}</dt>
      									<dd>
      										{if $ActionType eq 'OpenReview'}{$Lang.OpenReview}{else}{$ReviewInfo.ReviewIDName}{/if}
      									</dd>
    								</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewFields.ReviewStatus}</dt>
                  			<dd>
                    			{if $ActionType eq 'Activated'}{$Lang.ReviewStatus.Active}
                    			{elseif $ActionType eq 'Closed'}{$Lang.ReviewStatus.Closed}
                    			{elseif $ActionType eq 'Resolved'}{$Lang.ReviewStatus.Resolved}
                    			{else}{$ReviewInfo.ReviewStatusName}{/if}
                  			</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewFields.OpenedBy}</dt>
                    		<dd>
                            {if $ActionType eq 'OpenReview'}
                            {$templatelite.session.TestRealName}
                        		{else}
                            {$ReviewInfo.OpenedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                	  <dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewFields.ResolvedBy}</dt>
                    		<dd>
                        		{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Resolved'}
                          	{$ActionRealName}
                        		{else}
                          	{$ReviewInfo.ResolvedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                    <dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewFields.ClosedBy}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Closed'}
                          	{$ActionRealName}
                        		{else}
                          	{$ReviewInfo.ClosedByName}
                      			{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewFields.LastEditedBy}</dt>
                  			<dd>{$ReviewInfo.LastEditedByName}</dd>
                		</dl>
                </fieldset>
              	<fieldset class="Normal FloatLeft" style="width: 94%">
										<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewFields.OpenedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'OpenReview'}
                            {$templatelite.now|date_format:"%Y-%m-%d"}
                        		{else}
                            {$ReviewInfo.OpenedDate|date_format:"%Y-%m-%d"}
                        		{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewFields.ResolvedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Resolved'}
                            {$ActionDate}
                						{else}
                  					{if $ReviewInfo.ResolvedDate neq $CFG.ZeroTime}{$ReviewInfo.ResolvedDate|date_format:"%Y-%m-%d"} {/if}
                						{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewFields.ClosedDate}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                      			{elseif $ActionType eq 'Closed'}
                        		{$ActionDate}
                      			{else}
                        		{if $ReviewInfo.ClosedDate neq $CFG.ZeroTime}{$ReviewInfo.ClosedDate|date_format:"%Y-%m-%d"}{/if}{/if}
                    		</dd>
                  	</dl>
										<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewFields.LastEditedDate}</dt>
                  			<dd>{$ReviewInfo.LastEditedDate|date_format:"%Y-%m-%d"}</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.ReviewFields.ChangeID}</dt>
                    			<dd>
                      		<a href="Change.php?ChangeID={$ReviewInfo.ChangeID}" title="{$ReviewInfo.ChangeTitle}" target="_top">{$Lang.ChangeIDPrefix}{$ReviewInfo.ChangeID}</a>
                      		<input type="hidden" name="ChangeID" value="{$ReviewInfo.ChangeID}" />
                    			</dd>
                  		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.ReviewFields.ReviewCommentID}</dt>
                    			<dd>
                         	{foreach from=$ReviewInfo.ReviewCommentIDList key=key value=value}
                          <a href="ReviewComment.php?ReviewCommentID={$key}" title="{$value}" target="_top">{$key}</a>
                         	{/foreach}
                    			</dd>
                  		</dl>
                </fieldset>
              </td>
              <td style="width: 49%" valign="top">
              		<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.ReviewFields.ReviewTitle}</dt>
      										<dd>
          										{if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
					            				<input type="text" id="ReviewTitle" name="ReviewTitle" class="MyInput RequiredField" value="{$ReviewInfo.ReviewTitle}" style="width:500px;"/>
          										{else}
            									<input type="text" id="ReviewTitle" name="ReviewTitle" readonly=readonly class="MyInput ReadOnlyField" title="{$ReviewInfo.ReviewTitle}" value="{$ReviewInfo.ReviewTitle}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
    							</fieldset>
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                			<dl style="line-height:17pt">
                  				<dt>{$Lang.ReviewFields.ReviewType}</dt>
                  				<dd>
                              {if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$ReviewTypeList}
                      				{else}
                          		{$ReviewInfo.ReviewTypeName}
                              {/if}
                  				</dd>
                			</dl>
                	</fieldset>    							
    							<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.ReviewFields.ProjectName}</dt>
         								{if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
          							<dd>{$ProjectList}</dd>
        								{else}
          							<dd>{$ReviewInfo.ProjectName}</dd>
        								{/if}
    								</dl>
        						<dl style="line-height:17pt">
      									<dt>{$Lang.ReviewFields.ModulePath}</dt>
         								{if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
            						<dd id="SlaveModuleList">{$ModuleList}</dd>
        								{else}
          							<dd>{$ReviewInfo.ModulePath}</dd>
        								{/if}
    								</dl>
          				</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewFields.Author}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                          	<span id="AssignedToUserList">{$AuthorSelectList}</span>
                      			{else}
                          	{$ReviewInfo.AuthorName}
                          	{/if}
                  			</dd>
               	 		</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewFields.Moderator}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                          	<span id="AssignedToUserList">{$ModeratorSelectList}</span>
                      			{else}
                          	{$ReviewInfo.ModeratorName}
                          	{/if}
                  			</dd>
               	 		</dl>
               	 		<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewFields.Recorder}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                          	<span id="AssignedToUserList">{$RecorderSelectList}</span>
                      			{else}
                          	{$ReviewInfo.RecorderName}
                          	{/if}
                  			</dd>
               	 		</dl>
               	 		<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewFields.Inspectors}</dt>
                  			<dd>
                          {if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                          <input id="Inspectors" name="Inspectors" type="text" value="{$ReviewInfo.Inspectors}" size="80"  maxlength="255" class="MyInput RequiredField" AUTOCOMPLETE="OFF" />
                      		{else}
                          <input id="Inspectors" name="Inspectors" type="text" value="{$ReviewInfo.InspectorsName}" size="80"  maxlength="255" readonly=true class="MyInput ReadOnlyField" AUTOCOMPLETE="OFF" />
                      		{/if}
                  			</dd>
               	 		</dl>
               	 		<dl style="line-height:17pt">
                  			<dt>{$Lang.ReviewFields.MailTo}</dt>
                  			<dd>
                          {if $ActionType eq 'OpenReview' or $EditMode eq 'true'}
                          <input id="MailTo" name="MailTo" type="text" value="{$ReviewInfo.MailTo}" size="20"  maxlength="255" AUTOCOMPLETE="OFF" />
                      		{else}
                          <input id="MailTo" name="MailTo" type="text" value="{$ReviewInfo.MailToName}" size="20"  maxlength="255" readonly=true class="MyInput ReadOnlyField" AUTOCOMPLETE="OFF" />
                      		{/if}
                  			</dd>
               	 		</dl>
               	 		{if $EditMode eq 'true'}
               	 		<dl style="line-height:17pt">
      									<dt>
      										<input type="checkbox" id="SendNotifyEmail" name="SendNotifyEmail" checked="checked" value="SendNotifyEmail" onclick="SetNotifyEmail();"/>
      									</dt>
      									<dd>{$Lang.SendNotifyEmail}</dd>
										</dl>
										{/if}
	                </fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.ReviewFields.MeetingDate}</dt>
                    			<dd>
                       				{if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                        			<input type=text name="MeetingDate" id="MeetingDate" size="12" class="MyInput" value="{$ReviewInfo.MeetingDate}" readonly="readonly" onclick="SetCalendar('MeetingDate');"/>
                        		{else}
                  					{$ReviewInfo.MeetingDate}
                						{/if}
                    			</dd>
                    			<dd>
                              {if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$StartTimeSelectList}
                      				{else}
                          		{$ReviewInfo.StartTimeName}
                              {/if}
                    			</dd>                    			
                    			<dd>
                              {if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$EndTimeSelectList}
                      				{else}
                          		{$ReviewInfo.EndTimeName}
                              {/if}
                    			</dd>                    			
    									</dl>
    									<dl style="line-height:17pt">
      										<dt>{$Lang.ReviewFields.MeetingLocation}</dt>
      										<dd>
          										{if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
					            				<input type="text" id="MeetingLocation" name="MeetingLocation" class="MyInput RequiredField" value="{$ReviewInfo.MeetingLocation}" style="width:500px;"/>
          										{else}
            									<input type="text" id="MeetingLocation" name="MeetingLocation" readonly=readonly class="MyInput ReadOnlyField" title="{$ReviewInfo.MeetingLocation}" value="{$ReviewInfo.MeetingLocation}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
											{if $ActionType eq 'OpenReview' or $EditMode eq 'true'}
               	 			<dl style="line-height:17pt">
      									<dt>
      										<input type="checkbox" id="SendMeetingRequest" name="SendMeetingRequest" checked="checked" value="SendMeetingRequest" onclick="SetMeetingRequest();"/>
      									</dt>
      									<dd>{$Lang.SendMeetingRequest}</dd>
											</dl>
											{/if}
    							</fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
 					       		<dl style="line-height:17pt">
                    		<dt>{$Lang.ReviewFields.ReviewContent}</dt>
                    		<dd>
                      	{if $ActionType eq 'OpenReview' or $EditMode eq 'true'}
                      		{if $ReviewInfo.ReviewContent neq ''}
													<textarea id="ReviewContent" name="ReviewContent" rows="6" cols="70"  class="MyInput RequiredField" style="overflow-y:visible;">{$ReviewInfo.ReviewContent}</textarea>
													{else}
													<textarea id="ReviewContent" name="ReviewContent" rows="6" cols="70"  class="MyInput RequiredField" style="overflow-y:visible;" onclick="AutoClearContent('ReviewContent');">{$Lang.ReviewContentReminder}</textarea>
													{/if}
                  			{else}
													<p style="overflow: auto">{$ReviewInfo.ReviewContent|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                    		</dd>
                  	</dl>
    							</fieldset>
									{if $ActionType neq 'OpenReview'}
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                			<dl style="line-height:17pt">
                  				<dt>{$Lang.ReviewFields.ReviewConclusion}</dt>
                  				<dd>
                              {if $ActionType eq 'OpenReview' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$ReviewConclusionList}
                      				{else}
                          		{$ReviewInfo.ReviewConclusionName}
                              {/if}
                  				</dd>
                			</dl>
                	</fieldset>    
                  {/if}
    							<fieldset class="Normal FloatLeft" style="width: 94%">
                   	<legend>{$Lang.ReviewFiles}</legend>
                   	<dl>
                    	<dd style="text-align:left;">
                      		{assign var="FileList" value=$ReviewInfo.FileList}
                      		{include file="FileInfo.tpl"}
                  	  </dd>
                   	</dl>
                	</fieldset>
                	{if $EditMode eq 'true'}
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl>
                    	<dt>{$Lang.ReplyNote}</dt>
                    	<dd style="text-align:left;">
                      	<textarea id="ReplyNote" name="ReplyNote" rows="5" cols="70" style="overflow-y:visible;" ></textarea>
                  	  </dd>
                   	</dl>
                	</fieldset>
            	  	{/if}    							
            	</td>
          </tr>
          <tr>
              <td colspan="3">
                <fieldset id="CustomedFieldSet" class="Normal FloatLeft" style="width: 98%;{if $CustomedFieldHtml eq null}display:none{/if}">
                  {$CustomedFieldHtml}
                </fieldset>
              </td>
					</tr>
			</table>
   </div>
   
   <div id="BugHistory">
      <table style="width: 100%">
          <tr>
            	<td style="width: 20%" valign="top">
            	</td>
              <td style="width: 49%" valign="top">
                <fieldset class="Normal FloatLeft" id="ReviewHistoryInfo" style="width: 94%">
                  <legend>{$Lang.HistoryInfo}</legend>
                  {include file="ReviewHistory.tpl"}
                </fieldset>
              </td>
          </tr>
      </table>
  	</div>
</div>

<input type="hidden" id="ReviewID" name="ReviewID" value="{$ReviewInfo.ReviewID}" />
<input type="hidden" id="DeleteFileIDs" name="DeleteFileIDs" value="" />
<input type="hidden" id="ActionType" name="ActionType" value="{$ActionType}" />
<input type="hidden" id="ActionObj" name="ActionObj" value="Review" />
<input type="hidden" id="TestUserName" name="TestUserName" value="{$templatelite.session.TestUserName}" />
<input type="hidden" id="TestRealName" name="TestRealName" value="{$templatelite.session.TestRealName}" />
<input type="hidden" id="ToDisabledObj" name="ToDisabledObj" value="SubmitButton" />
<input type="hidden" id="CurrentProjectID" name="CurrentProjectID" value="{$ProjectID}" />
<input type="hidden" id="LastAcitonID" name="LastActionID" value="{$LastActionID}" />
<input type="hidden" id="ShowPageFlag" name="ShowPageFlag" value="{$ShowPageFlag}" />

</form>
{include file="PostActionFrame.tpl"}
{literal}
<script type="text/javascript">
//<![CDATA[
function superAddObjValue(objID,addValue)
{
    xajax.$(objID).value += ',' + addValue;
}
function SetNotifyEmail()
{
	ItemCheckBox = document.getElementById('SendNotifyEmail');
	if(ItemCheckBox.checked==false)
	{
		ItemCheckBox.value = '';
	}
	else
	{
		ItemCheckBox.value = 'SendNotifyEmail';
	}
	//alert(ItemCheckBox.value);
}
function SetCalendar(ItemName)
{
  Calendar.setup(
    {
      inputField  : ItemName,
      ifFormat    : "%Y-%m-%d",
      button      : ItemName
    }
  );
}
function SetMeetingRequest()
{
	ItemCheckBox = document.getElementById('SendMeetingRequest');
	if(ItemCheckBox.checked==false)
	{
		ItemCheckBox.value = '';
	}
	else
	{
		ItemCheckBox.value = 'SendMeetingRequest';
	}
	//alert(ItemCheckBox.value);
}

function AutoClearContent(ItemId)
{
	Item = document.getElementById(ItemId);
	//alert(Item.value);
	//alert(ReviewContentReminder);
	if(Item.value == ReviewContentReminder)
	{
		Item.value = '';
	}
}
{/literal}
{if $ActionType neq ''}
{literal}
  setConfirmExitArrays();
  initSelectDiv('Inspectors','selectDivProjectUserList','getInputSearchValueByComma','setValueByComma', true);
  initSelectDiv('MailTo','selectDivProjectUserList','getInputSearchValueByComma','setValueByComma', true);
  xajax.$('ReplyNote').focus();
{/literal}
{/if}
{if $ActionType eq 'OpenReview'}
   {literal}focusInputEndPos('ReviewTitle');{/literal}
{elseif $ActionType eq 'Resolved'}
   {literal}xajax.$('ResolvedBuild').focus();{/literal}
{/if}
{literal}
//]]>
</script>
{/literal}
</body>
</html>
