{include file="Header.tpl"}
<body class="{$TestMode}Mode RMode" onload="initShowGotoBCR();">
{literal}
<script type="text/javascript">
//<![CDATA[
{/literal}
var FieldCount = {$FieldCount};
var QueryFieldCount = {$QueryFieldCount};
var searchConditionRowNum = QueryFieldCount;
var searchConditionTmp = "{$searchConditionTmp}";
var searchDeleteErrorMsg = "{$Lang.SearchDeleteErrorMsg}";
var searchAddErrorMsg = "{$Lang.SearchAddErrorMsg}";
{foreach from=$AutoTextValue key=key item=item}
var {$key} = {$item}
{/foreach}

var OperatorListSelectItem = new Array();

{foreach from=$OperatorListSelectItem key=key item=item}
   OperatorListSelectItem[{$key}] = '{$item}';
{/foreach}

var ValueListSelectItem = new Array();

{foreach from=$ValueListSelectItem key=key item=item}
   ValueListSelectItem[{$key}] = '{$item}';
{/foreach}

{literal}
function getOffset(evt)
{
  if(navigator.userAgent.indexOf("MSIE")>0) {
    return evt;
  }
  var target = evt.target;
  if (target.offsetLeft == undefined)
  {
    target = target.parentNode;
  }
  var pageCoord = getPageCoord(target);
  var eventCoord =
  {
    x: window.pageXOffset + evt.clientX,
    y: window.pageYOffset + evt.clientY
  };
  var offset =
  {
    offsetX: eventCoord.x - pageCoord.x,
    offsetY: eventCoord.y - pageCoord.y
  };
  return offset;
}

function getPageCoord(element)
{
  var coord = {x: 0, y: 0};
  while (element)
  {
    coord.x += element.offsetLeft;
    coord.y += element.offsetTop;
    element = element.offsetParent;
  }
  return coord;
}

function getEventOffset(evt)
{
  var msg = "";
  if (evt.offsetX == undefined)
  {
    var evtOffsets = getOffset(evt);
    msg += "offsetX: " + evtOffsets.offsetX + "; ";
    msg += "offsetY: " + evtOffsets.offsetY + "; ";
  }
  else
  {
    msg += "offsetX: " + evt.offsetX + "; ";
    msg += "offsetY: " + evt.offsetY + "; ";
  }

  return msg;
}

//]]>
</script>
{/literal}
<form id="Search{$TestMode}" name="Search{$TestMode}" action="{$TestMode}List.php" method='post' target='RightBottomFrame'>
{include file = "SaveQuery.tpl"}
   <div id="SearchBlankCover">
   <table id="SearchTable" class="CommonTable SearchTable {$TestMode}Mode TabelReset" onclick="hiddenDivSaveQuerySetTable()" style="margin-left:auto;margin-right:auto;width:90%;">
    <caption style="padding-bottom:1px;padding-top:3px;">
        {$Lang.QueryCondition}{if $QueryTitle != ''} - {$QueryTitle}{/if}
        <input type="checkbox" name="AutoComplete" id="AutoComplete" checked="checked" onclick="resetQueryForm();" class="NoBorder" style="display:none"/>
        <!--<label for="AutoComplete">Auto</label>-->
    </caption>
    <colgroup>
      {if $UserLang != 'EN_UTF-8' }
          <col span="1" width="6%">
          <col span="1" width="20%">
          <col span="1" width="10%">
          <col span="1" width="40%">
          <col span="1" width="6%">
          <col span="1" width="8%">
          <col span="1" width="8%">
      { else }
          <col span="1" width="6%">
          <col span="1" width="18%">
          <col span="1" width="24%">
          <col span="1" width="31%">
          <col span="1" width="6%">
          <col span="1" width="8%">
          <col span="1" width="8%">
      {/if}
    </colgroup>

    {foreach from=$AndOrList key=Key value=Value}
    <tr id="SearchConditionRow{$Key}">
      <td>{$LeftParentheses[$Key]}</td>
      <td>{$FieldList[$Key]}</td>
      <td>{$OperatorList[$Key]}</td>
      <td id="ValueTd{$Key}">{$ValueList[$Key]}</td>
      <td>{$RightParentheses[$Key]}</td>
      <td>{$AndOrList[$Key]}</td>
      <td>{$AddRemoveLink[$Key]}</td>
    </tr>
    {/foreach}
    <tr >
        <td colspan="7" style="padding-bottom:6px;">
        <center>
            <input type="submit" class="Btn" name="PostQuery" id="PostQuery" value="{$Lang.PostQuery}" onclick="setSearchConditionOrder();document.Search{$TestMode}.action='{$TestMode}List.php';" />
            <input type="button" class="Btn" name="SaveQuery" id="SaveQuery" value="{$Lang.SaveQuery}" onclick="setSearchConditionOrder();document.Search{$TestMode}.action='SaveQuery.php';x=event.clientX-getOffset(event).offsetX;y=event.clientY-getOffset(event).offsetY;  showSaveQuery(x,y); xajax.$('QueryTitle').value = '{$QueryTitle}'; " />
            <input type="reset"  class="Btn" value="{$Lang.ResetQuery}" onclick="window.location.href= 'Search{$TestMode}.php?reset=1'" />
        </center>
        </td>
    </tr>
  </table>
  <input type="hidden" name="QueryType" value="{$TestMode}" />
  <input type="hidden" id="QueryRowOrder" name="QueryRowOrder" value="" />
  <input type="hidden" id="QueryFieldCount" name="QueryFieldCount" value="{$QueryFieldCount}" />
  </div>
</form>
{literal}
<script type="text/javascript">
//<![CDATA[
function resetQueryForm(OperatorListSelectItem,ValueListSelectItem)
{
    for(var i=0;i<QueryFieldCount;i++)
    {
        setQueryForm(i,OperatorListSelectItem[i],ValueListSelectItem[i]);
    }
}
resetQueryForm(OperatorListSelectItem,ValueListSelectItem);
$(function(){  
    resizeframe();
});
function validateParentheses()
{
    var stack = new Array();
    var $parenthesesArr = $("select[id*=ParenthesesName]");
    $parenthesesArr.css('color','black');
    $parenthesesArr.each(function(){
        var $selectedValue = $(this).find("option:selected").text();
        $selectedValue = jQuery.trim($selectedValue);
        if('' != $selectedValue)
        {
            if(stack.length == 0)
            {
                stack.push($(this));
            }
            else
            {
                if('(' == $selectedValue)
                {
                    stack.push($(this));
                }
                else
                {
                    var $preObj = stack.pop();
                    if('(' != $preObj.find("option:selected").text())
                    {
                        stack.push($preObj);
                        stack.push($(this));
                    }
                }
            }

        }
    });
    if(stack.length>0)
    {
        $("#SaveQuery").attr("disabled","disabled");
        $("#SaveQuery").css('color','grey');
        $("#SaveQuery").css('cursor','default');

        $("#PostQuery").attr("disabled","disabled");
        $("#PostQuery").css('color','grey');
        $("#PostQuery").css('cursor','default');
    }
    else
    {
        $("#PostQuery").removeAttr("disabled");
        $("#PostQuery").css('color','#000000');
        $("#PostQuery").css('cursor','pointer');
        $("#SaveQuery").removeAttr("disabled");
        $("#SaveQuery").css('color','#000000');
        $("#SaveQuery").css('cursor','pointer');
    }
    for(var i=0;i<stack.length;i++)
    {
        stack[i].css('color','red');
    }

}

function setSearchConditionOrder()
{
    var rowOrder = "";
    var $searchRows = $("tr[id^=SearchConditionRow]");
    $searchRows.each(function(){
        rowOrder += $(this).attr("id")+",";
    });
    $("#QueryRowOrder").attr("value",rowOrder);
}

function addSearchField(fieldRowNum)
{
    var $rowNum = $("#SearchTable tr").length;
    if($rowNum >FieldCount )
    {
        alert(searchAddErrorMsg);
        return;
    }else{
        addFieldCount();
        $("#SearchConditionRow"+fieldRowNum).after($(getOneSearchRow(searchConditionRowNum)));
        setQueryForm(searchConditionRowNum);
        resizeframe();
    }
}

function getOneSearchRow(fieldRowNum)
{
    raRegExp = new RegExp("NUM_TEMPLATE","g");
    var newSearchCondition = searchConditionTmp.replace(raRegExp,fieldRowNum);
    return newSearchCondition;
}

function removeSearchField(fieldRowNum)
{
    var $rowNum = $("#SearchTable tr").length;
    if($rowNum <=2 )
    {
        alert(searchDeleteErrorMsg);
        return;
    }else{
        $("#SearchConditionRow"+fieldRowNum).remove();
        validateParentheses();
        resizeframe();
    }
}

function addFieldCount(){
    searchConditionRowNum++;
    document.getElementById("QueryFieldCount").value = searchConditionRowNum;
}

function resizeframe(type){
    var $rightFrame = $(window.parent.document.getElementById( "RightFrame"));
    topHeight = $("#SearchBlankCover").height()+",*";
    $rightFrame.attr("rows",topHeight);
}
//]]
</script>
{/literal}
</body>
</html>
