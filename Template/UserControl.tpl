{include file="Header.tpl"}
<body class="LMode" onload="initShowGotoBCR();">
{literal}
<script type="text/javascript">
//<![CDATA[
function dispayUserControlDiv(I)
{
    var display=false;
    for(j=0;j<4;j++)
    {
        if(xajax.$('UCTitle'+j))
        {
            if(j == I)
            {
                if(j != 3)
                {
                    xajax.$('UCTitle'+j).className = 'TabSelect';
                }
                displayObj('UCDiv'+j);
                display=true;
            }
            else
            {
                if(j != 3)
                {
                    xajax.$('UCTitle'+j).className = 'Tab';
                }
                hiddenObj('UCDiv'+j);
            }
        }
    }
    if(!display)
    {
        xajax.$('UCTitle'+1).className = 'TabSelect';
        displayObj('UCDiv'+1);
    }
}
/*==============================loading function==============================*/

//]]
</script>
{/literal}
  <div id="LeftUserControl" class="TabContainer">
    <h2 id="UCTitle0" class="Tab"><a href="javascript:void(0);" onclick="currentBugTabNum=0;dispayUserControlDiv(0);xajax_xSetCurrentTabNum(0);">{$Lang.UCAssignedToMe}</a></h2>
    <h2 id="UCTitle1" class="TabSelect"><a href="javascript:void(0);" onclick="currentBugTabNum=1;dispayUserControlDiv(1);xajax_xSetCurrentTabNum(1);">{$Lang.UCOpenedByMe}</a></h2>
    <h2 id="UCTitle2" class="Tab"><a href="javascript:void(0);" onclick="currentBugTabNum=2;dispayUserControlDiv(2);xajax_xSetCurrentTabNum(2);">{$Lang.UCMyQuery}</a></h2>
    <h2 id="UCTitle3" class="Tab {$TestMode}Mode" style="border:0;padding:0 2px;background:none;"><a href="javascript:void(0);" onclick="xajax_xUpdateUserControl('{$TestMode}');return false;"><img src="Image/icon_refresh.png" /></a></h2>
   {if $TestMode == 'Bug'}
   <div id="UCDiv0" class="TabPage" style="display:none;">
    <table>
    {foreach from=$AssignedList item=Item}
        <tr><td>{$Item.BugIDName}</td><td><a href="Bug.php?BugID={$Item.BugID}" title="{$Item.BugTitle}" target="_blank" class="LeftTabBug{$Item.BugStatus}">{$Item.UCTitle}</a></td></tr>
    {/foreach}
    </table>
    </div>
    <div id="UCDiv1" class="TabPage" style="display:block;">
    <table>
    {foreach from=$OpenedList item=Item}
        <tr><td>{$Item.BugIDName}</td><td><a href="Bug.php?BugID={$Item.BugID}" title="{$Item.BugTitle}" target="_blank" class="LeftTabBug{$Item.BugStatus}">{$Item.UCTitle}</a></td></tr>
    {/foreach}
    </table>
    </div>
    <div id="UCDiv2" class="TabPage" style="display:none;">
    {foreach from=$QueryList item=Item}
      <div>
        <a href="?DelQueryID={$Item.QueryID}" title="{$Lang.Delete}" onclick="return confirm('{$Lang.ConfirmDelQuery}');" target="_self" style="color:#CC0000;text-decoration:none;font-size:11px;font-weight:bold;margin-right:0px;padding:0 5px;"><img src="Image/delete.gif"/></a>
        <a href="BugList.php?QueryID={$Item.QueryID}&ProjectID={$Item.ProjectID}" title="{$Item.QueryTitle}">{$Item.QueryTitle}</a>
      </div>
    {/foreach}
    </div>
    {elseif $TestMode == 'Change'}
    <div id="UCDiv0" class="TabPage" style="display:none;">
    {foreach from=$AssignedList item=Item}
      {$Item.ChangeIDName}&nbsp;<a href="Change.php?ChangeID={$Item.ChangeID}" title="{$Item.ChangeTitle}" target="_blank" class="LeftTabBug{$Item.ChangeStatus}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv1" class="TabPage" style="display:block;">
    {foreach from=$OpenedList item=Item}
      {$Item.ChangeIDName}&nbsp;<a href="Change.php?ChangeID={$Item.ChangeID}" title="{$Item.ChangeTitle}" target="_blank" class="LeftTabBug{$Item.ChangeStatus}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv2" class="TabPage" style="display:none;">
    {foreach from=$QueryList item=Item}
      <div>
        <a href="?DelQueryID={$Item.QueryID}" title="{$Lang.Delete}" onclick="return confirm('{$Lang.ConfirmDelQuery}');" target="_self" style="color:#CC0000;text-decoration:none;font-size:11px;font-weight:bold;padding:0 5px;"><img src="Image/delete.gif" /></a>
        <a href="ChangeList.php?QueryID={$Item.QueryID}&ProjectID={$Item.ProjectID}" title="{$Item.QueryTitle}">{$Item.QueryTitle}</a>
      </div>
    {/foreach}
    </div>
    {elseif $TestMode == 'Review'}
    <div id="UCDiv0" class="TabPage" style="display:none;">
    {foreach from=$AssignedList item=Item}
      {$Item.ReviewIDName}&nbsp;<a href="Review.php?ReviewID={$Item.ReviewID}" title="{$Item.ReviewTitle}" target="_blank" class="LeftTabBug{$Item.ReviewStatus}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv1" class="TabPage" style="display:block;">
    {foreach from=$OpenedList item=Item}
      {$Item.ReviewIDName}&nbsp;<a href="Review.php?ReviewID={$Item.ReviewID}" title="{$Item.ReviewTitle}" target="_blank" class="LeftTabBug{$Item.ReviewStatus}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv2" class="TabPage" style="display:none;">
    {foreach from=$QueryList item=Item}
      <div>
        <a href="?DelQueryID={$Item.QueryID}" title="{$Lang.Delete}" onclick="return confirm('{$Lang.ConfirmDelQuery}');" target="_self" style="color:#CC0000;text-decoration:none;font-size:11px;font-weight:bold;padding:0 5px;"><img src="Image/delete.gif" /></a>
        <a href="ReviewList.php?QueryID={$Item.QueryID}&ProjectID={$Item.ProjectID}" title="{$Item.QueryTitle}">{$Item.QueryTitle}</a>
      </div>
    {/foreach}
    </div>
    {elseif $TestMode == 'ReviewComment'}
    <div id="UCDiv0" class="TabPage" style="display:none;">
    {foreach from=$AssignedList item=Item}
      {$Item.ReviewCommentIDName}&nbsp;<a href="ReviewComment.php?ReviewCommentID={$Item.ReviewCommentID}" title="{$Item.ReviewCommentTitle}" target="_blank" class="LeftTabBug{$Item.ReviewCommentStatus}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv1" class="TabPage" style="display:block;">
    {foreach from=$OpenedList item=Item}
      {$Item.ReviewCommentIDName}&nbsp;<a href="ReviewComment.php?ReviewCommentID={$Item.ReviewCommentID}" title="{$Item.ReviewCommentTitle}" target="_blank" class="LeftTabBug{$Item.ReviewCommentStatus}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv2" class="TabPage" style="display:none;">
    {foreach from=$QueryList item=Item}
      <div>
        <a href="?DelQueryID={$Item.QueryID}" title="{$Lang.Delete}" onclick="return confirm('{$Lang.ConfirmDelQuery}');" target="_self" style="color:#CC0000;text-decoration:none;font-size:11px;font-weight:bold;padding:0 5px;"><img src="Image/delete.gif" /></a>
        <a href="ReviewCommentList.php?QueryID={$Item.QueryID}&ProjectID={$Item.ProjectID}" title="{$Item.QueryTitle}">{$Item.QueryTitle}</a>
      </div>
    {/foreach}
    </div>
    {elseif $TestMode == 'Plan'}
    <div id="UCDiv0" class="TabPage" style="display:none;">
    {foreach from=$AssignedList item=Item}
      {$Item.PlanIDName}&nbsp;<a href="Plan.php?PlanID={$Item.PlanID}" title="{$Item.PlanTitle}" target="_blank" class="LeftTabBug{$Item.PlanStatus}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv1" class="TabPage" style="display:block;">
    {foreach from=$OpenedList item=Item}
      {$Item.PlanIDName}&nbsp;<a href="Plan.php?PlanID={$Item.PlanID}" title="{$Item.PlanTitle}" target="_blank" class="LeftTabBug{$Item.PlanStatus}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv2" class="TabPage" style="display:none;">
    {foreach from=$QueryList item=Item}
      <div>
        <a href="?DelQueryID={$Item.QueryID}" title="{$Lang.Delete}" onclick="return confirm('{$Lang.ConfirmDelQuery}');" target="_self" style="color:#CC0000;text-decoration:none;font-size:11px;font-weight:bold;padding:0 5px;"><img src="Image/delete.gif" /></a>
        <a href="PlanList.php?QueryID={$Item.QueryID}&ProjectID={$Item.ProjectID}" title="{$Item.QueryTitle}">{$Item.QueryTitle}</a>
      </div>
    {/foreach}
    </div>
    {elseif $TestMode == 'Case'}
    <div id="UCDiv0" class="TabPage" style="display:none;">
    {foreach from=$AssignedList item=Item}
      {$Item.CaseIDName}&nbsp;<a href="Case.php?CaseID={$Item.CaseID}" title="{$Item.CaseTitle}" target="_blank">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv1" class="TabPage" style="display:block;">
    {foreach from=$OpenedList item=Item}
      {$Item.CaseIDName}&nbsp;<a href="Case.php?CaseID={$Item.CaseID}" title="{$Item.CaseTitle}" target="_blank">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv2" class="TabPage" style="display:none;">
    {foreach from=$QueryList item=Item}
      <div>
        <a href="?DelQueryID={$Item.QueryID}" title="{$Lang.Delete}" onclick="return confirm('{$Lang.ConfirmDelQuery}');" target="_self" style="color:#CC0000;text-decoration:none;font-size:11px;font-weight:bold;padding:0 5px;"><img src="Image/delete.gif" /></a>
        <a href="CaseList.php?QueryID={$Item.QueryID}&ProjectID={$Item.ProjectID}" title="{$Item.QueryTitle}">{$Item.QueryTitle}</a>
      </div>
    {/foreach}
    </div>
    {elseif $TestMode == 'Result'}
    <div id="UCDiv0" class="TabPage" style="display:none;">
    {foreach from=$AssignedList item=Item}
      {$Item.ResultIDName}&nbsp;<a href="Result.php?ResultID={$Item.ResultID}" title="{$Item.ResultTitle}" target="_blank" style="color:{$Item.ResultValueColor}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv1" class="TabPage" style="display:block;">
    {foreach from=$OpenedList item=Item}
      {$Item.ResultIDName}&nbsp;<a href="Result.php?ResultID={$Item.ResultID}" title="{$Item.ResultTitle}" target="_blank" style="color:{$Item.ResultValueColor}">{$Item.UCTitle}</a><br />
    {/foreach}
    </div>
    <div id="UCDiv2" class="TabPage" style="display:none;">
    {foreach from=$QueryList item=Item}
      <div>
        <a href="?DelQueryID={$Item.QueryID}" title="{$Lang.Delete}" onclick="return confirm('{$Lang.ConfirmDelQuery}');" target="_self" style="color:#CC0000;text-decoration:none;font-size:11px;font-weight:bold;margin-right:0px;padding:0 5px;"><img src="Image/delete.gif" /></a>
        <a href="ResultList.php?QueryID={$Item.QueryID}&ProjectID={$Item.ProjectID}" title="{$Item.QueryTitle}">{$Item.QueryTitle}</a>
      </div>
    {/foreach}
    </div>
    {/if}
<div id="UCDiv3" class="TabPage" style="display:none;">
<strong>Loading...</strong><img src="Image/Loading.gif" />
</div>
</div>
<div id="ModuleListBottom"></div>
{literal}
<script type="text/javascript">
//<![CDATA[
{/literal}
var currentBugTabNum = {$CurrentBugTabNum};
{literal}
dispayUserControlDiv(currentBugTabNum);
if(typeof(xajax) != "undefined")
{
    var LoadingMessage = xajax.$('UCDiv3');
    if(LoadingMessage)
    {
        xajax.loadingFunction = function(){dispayUserControlDiv(3);};
        function hideLoadingMessage()
        {
          dispayUserControlDiv(currentBugTabNum);
        }
        xajax.doneLoadingFunction = hideLoadingMessage;
    }
}
setInterval("xajax_xUpdateUserControl('{/literal}{$TestMode}{literal}')",300000);
//]]
</script>
{/literal}
<body>
</html>