{include file="Header.tpl"}
<body id="BGColor" onload="this.focus();initShowGotoBCR();">
  <div id="TopNavMain">
    <div id="TopNavLogo">
      <a href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    </div>
    <div id="TopNavAbout">
      <span>{$Lang.Welcome},</span>
      <span id="UserName" class="UserName">{$TestRealName}</span>
      <a id="EditMyInfo" href="EditMyInfo.php" target="_blank">{$Lang.EditPer}</a>
      <a id="TestSystem" href="TestSystem.php" target="_blank">{$Lang.TestSystem}</a>
      {if $TestIsAdmin || $TestIsProjectAdmin}
      <a href="Admin/" target="_blank">{$Lang.Admin}</a>
      {/if} 
      <a href="Logout.php?Logout=Yes">{$Lang.Logout}</a>
    </div>
    <div id="ProjectList">{$TopNavProjectList}</div>
    <div id="TopNavButton">
    <ul>
      <li id="TopNavBugLi"    class="Cram{if $TestMode eq 'Bug'} Active{/if}">   <a href="index.php?TestMode=Bug" id="TopNavBug" target="_top"><span class="BugMenuButton">{$Lang.TopNavBug}</span></a></li>
      <li id="TopNavChangeLi" class="Cram{if $TestMode eq 'Change'} Active{/if}"><a href="index.php?TestMode=Change" id="TopNavChange" target="_top"><span class="BugMenuButton">{$Lang.TopNavChange}</span></a></li>
      <li id="TopNavReviewLi" class="Cram{if $TestMode eq 'Review'} Active{/if}"><a href="index.php?TestMode=Review" id="TopNavReview" target="_top"><span class="BugMenuButton">{$Lang.TopNavReview}</span></a></li>
      <li id="TopNavReviewCommentLi" class="Cram{if $TestMode eq 'ReviewComment'} Active{/if}"><a href="index.php?TestMode=ReviewComment" id="TopNavReviewComment" target="_top"><span class="BugMenuButton">{$Lang.TopNavReviewComment}</span></a></li>
    </ul>
    </div>
    <div id="Open">
      <a href="Bug.php?ActionType=OpenBug" id="OpenBug" class="BigButton OpenBug" target="_blank" {if $TestMode neq 'Bug'}style="display:none;"{/if}>
        {$Lang.OpenBug}
      </a>
      {if $TestIsAdmin}
      <!--a href="Change.php?ActionType=OpenChange" id="OpenChange" class="BigButton OpenBug" target="_blank" {if $TestMode neq 'Change'}style="display:none;"{/if}>
        {$Lang.OpenChange}
      </a-->
      <!--a href="Review.php?ActionType=OpenReview" id="OpenReview" class="BigButton OpenBug" target="_blank"{if $TestMode neq 'Review'} style="display:none;"{/if}>
        {$Lang.OpenReview}
      </a-->
      <!--a href="ReviewComment.php?ActionType=OpenReviewComment" id="OpenReviewComment" class="BigButton OpenBug" target="_blank"{if $TestMode neq 'ReviewComment'} style="display:none;"{/if}>
        {$Lang.OpenReviewComment}
      </a-->
      {/if}
    </div>
  </div>
</body>
</html>