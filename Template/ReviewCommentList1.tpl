{include file="XmlHeader.tpl"}
{include file="Header.tpl"}
{include file="UploadFile.tpl"}

{include file="ResizeList.tpl"}
<body class="BugMode BugMain" onload="TestMode='Review';initShowGotoBCR();" onscroll="setTopBarLeftPos()">
  {if $ActionType eq 'Edited' or $ActionType eq 'Resolved' or $ActionType eq 'Closed' or $ActionType eq 'Activated'}
  {assign var="EditMode" value= 'true'}
  {/if}

  {if $ActionType eq 'OpenReview'}
    <form id="ReviewForm" name="ReviewForm" action="PostAction.php?Action=OpenReview" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {elseif $EditMode eq 'true'}
    <form id="ReviewForm" name="ReviewForm" action="PostAction.php?Action=EditReview" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {else}
    <form id="ReviewForm" name="ReviewForm" action="Review.php?ReviewID={$ReviewInfo.ReviewID}" method="post" target="_self">
  {/if}

<div id="TopNavMain" class="TopBar">
    <a id="TopNavLogo" href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    <!-- Set the Toolbar buttons status by Rainy Gao-->
    {assign var="PreButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {assign var="NextButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {assign var="EditButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {assign var="ResolveReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {assign var="CloseReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {assign var="ActiveReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {assign var="SaveButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {if $ReviewInfo.ReviewStatus neq 'Active' or $EditMode eq 'true'}
    	{assign var="OpenReviewCommentButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    
    <div id="ButtonList">
      <input type="button" class="ActionButton Btn" accesskey="P" value="{$Lang.PreButton}" onclick="location.href='Review.php?ReviewID={$PreReviewID}'" {$PreButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="N" value="{$Lang.NextButton}" onclick="location.href='Review.php?ReviewID={$NextReviewID}'" {$NextButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="E" value="{$Lang.EditReviewButton}" onclick="xajax.$('ActionType').value='Edited';submitForm('ReviewForm')" {$EditButtonStatus}}/>
      <!--input type="button" class="ActionButton Btn" accesskey="C" value="{$Lang.CopyReviewButton}" onclick="xajax.$('ActionType').value='OpenReview';submitForm('ReviewForm');" {$EditButtonStatus} /-->
      <!--input type="button" class="ActionButton Btn" accesskey="R" value="{$Lang.ResolveReviewButton}" onclick="xajax.$('ActionType').value='Resolved';submitForm('ReviewForm');" {$ResolveReviewButtonStatus} /-->
      <input type="button" class="ActionButton Btn" accesskey="L" value="{$Lang.CloseReviewButton}" onclick="xajax.$('ActionType').value='Closed';submitForm('ReviewForm');" {$CloseReviewButtonStatus} />
			{if $TestIsAdmin}
      <input type="button" class="ActionButton Btn" accesskey="A" value="{$Lang.ActiveReviewButton}" onclick="xajax.$('ActionType').value='Activated';submitForm('ReviewForm');" {$ActiveReviewButtonStatus} />
      {/if}
      <input type="button" class="ActionButton Btn" accesskey="S" value="{$Lang.SaveButton}" name="SubmitButton" id="SubmitButton" onclick="this.disabled='disabled';NeedToConfirm=false;document.ReviewForm.submit();" {$SaveButtonStatus}/>
			<input type="button" class="ActionButton Btn" accesskey="B" value="{$Lang.OpenReviewCommentButton}" {$OpenReviewCommentButtonStatus}  onclick="openWindow('ReviewComment.php?ActionType=OpenReviewComment&ReviewID={$ReviewInfo.ReviewID}','OpenReviewComment');"/>
			<!--input type="button" class="Action1Button Btn" value="{$Lang.GoToReviewCommentPageButton}" onclick="xajax.$('ActionType').value='GoToReviewCommentPage';submitForm('ReviewForm');" {$GoToReviewCommentPageButtonStatus} /-->
    </div>
</div>
  
  <span id="ReviewId1">
  	{if $ActionType eq 'OpenReview'}{$Lang.OpenReview}{else}{$ReviewInfo.ReviewIDName}{/if}
  </span>
	{include file="CustomField.tpl"}
	<div id="BlankCover">
  	<table class="CommonTable TableMode">
      <tr>
        <td colspan="{$FieldToShowCount}" class="TdCaption">
          <table style="font-size:12px;width:98%;">
            <tr>
              <td style="text-align:left;width:50%">
                &nbsp;&nbsp;
                &lt;<a href="Review.php?ReviewID={$ReviewInfo.ReviewID}">{$Lang.BackToReview}</a>&#124;
                <a href="javascript:void(0);" id="CustomSetLink" onclick="x=530;y=event.clientY;showDivCustomSetTable(x,y);">
                    {$Lang.CustomDisplay}
                </a>&nbsp;
                {if $TestIsAdmin}|&nbsp;
                <span id="VReport">
                  <a href="Report.php?ReportMode={$TestMode}" target="'{$TestMode}Report">
                    {$Lang.ReportForms}
                  </a>
                &nbsp;|&nbsp;
                <a href="?Export=XMLFile">
                  {$Lang.ExportCases}
                </a>
                </span>&nbsp;|&nbsp;
                <a href="javascript:void(0);" onclick="x=event.clientX+document.body.scrollLeft;y=event.clientY;showUploadFile(x+420,y);">{$Lang.ImportCases}</a>
								{/if}       
              </td>
              <td style="text-align:right;width:50%">
                {$Lang.Pagination.Result} 
                {$PaginationDetailInfo.FromNum}-{$PaginationDetailInfo.ToNum}/{$PaginationDetailInfo.RecTotal}
                &nbsp;
                {$PaginationDetailInfo.RecPerPageList}
                &nbsp;
                {$PaginationDetailInfo.PrePageLiteralLink}
                {$PaginationDetailInfo.NextPageLiteralLink}
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="{$FieldToShowCount}">
          <div id="ListSubTable" style="overflow-y:hidden;overflow-x:hidden">
            <table class="CommonTable CommonSubTable TableSubMode">
              <tr class="TabLink">
                {foreach from=$FieldsToShow key=Field item=FieldName}
                <th align="{if $Field == 'ReviewCommentID' || $Field == 'ReviewCommentSeverity'}center{else}left{/if}">
                  <a href="?OrderBy={$Field}|{$OrderByTypeList[$Field]}&QueryMode={$QueryMode}">
                  {$FieldName}
                  </a>
                  {if $OrderByColumn == $Field}{$OrderTypeArrowArray[$OrderByType]}{/if}
                </th>
                {/foreach}
              </tr>
              {foreach from=$ReviewCommentList item=ReviewCommentInfo}
              <tr class="BugStatus{$ReviewCommentInfo.ReviewCommentStatus}">
                {foreach from=$FieldsToShow key=Field item=FieldName}
                <td align="{if $Field == 'ReviewCommentID' || $Field == 'ReviewCommentSeverity' || $Field == 'ReviewCommentPriority'}center{else}left{/if}"{if $QueryColumn eq $Field} class="SortActive"{/if}>
                  {assign var="FieldValue" value=$Field."Name"}
                    {if $Field == 'ReviewCommentTitle'}
                  <span class="Title">
                    <a href="ReviewComment.php?ReviewCommentID={$ReviewCommentInfo.ReviewCommentID}" title="{$ReviewCommentInfo.ReviewCommentTitle}" class="FullLink Title" target="_blank">
                      {$ReviewCommentInfo.ListTitle}
                    </a>
                  </span>
                  {elseif $Field == 'OpenedDate' || $Field == 'AssignedDate' || $Field == 'ResolvedDate' || $Field == 'ClosedDate' || $Field == 'LastEditedDate'}
                  {if $ReviewCommentInfo[$Field] != $CFG.ZeroTime}
                  <a href="?QueryMode={$Field}|{$ReviewCommentInfo[$Field]|date_format:"%Y-%m-%d"}">
                    {$ReviewCommentInfo[$Field]|date_format:"%Y-%m-%d"}
                  </a>
                  {/if}
                  {else}
                  <a href="?QueryMode={$Field}|{$ReviewCommentInfo[$Field]}">
                    {$ReviewCommentInfo[$FieldValue]|default:$ReviewCommentInfo[$Field]}
                  </a>
                  {/if}
                </td>
                {/foreach}
              </tr>
              {/foreach}
            </table>
          </div>
        </td>
      </tr>
    </table>
  </div>
<script>
    if({$QueryID}!='-1')
       parent.RightTopFrame.location = "SearchReviewComment.php?QueryID=" + {$QueryID} + "&QueryTitle=" + encodeURI('{$QueryTitle}');
    else if('{$QueryTitle}'!='')
       parent.RightTopFrame.location = "SearchReviewComment.php?QueryTitle=" + encodeURI('{$QueryTitle}');
</script>
</body>
</html>