{include file="Header.tpl"}
<body class="BugMode BugMain" onload="TestMode='Plan';initShowGotoBCR();" onscroll="setTopBarLeftPos()">
  {if $ActionType eq 'Edited' or $ActionType eq 'Resolved' or $ActionType eq 'Closed' or $ActionType eq 'Activated'}
  {assign var="EditMode" value= 'true'}
  {/if}

  {if $templatelite.session.TestUserName eq $PlanInfo.OpenedBy}
  {assign var="testIsOpenedBy" value= 'true'}
  {/if}  
	{if $templatelite.session.TestUserName eq $PlanInfo.AssignedTo}
	{assign var="testIsAssignedTo" value= 'true'}
	{/if}
  {if $testIsOpenedBy or $testIsAssignedTo or $TestIsAdmin}
  {assign var="testIsOwner" value= 'true'}
  {/if}
  {if $testIsOpenedBy or $TestIsAdmin}
  {assign var="testIsCloser" value= 'true'}
  {/if}

  {if $ActionType eq 'OpenPlan'}
    <form id="PlanForm" name="PlanForm" action="PostAction.php?Action=OpenPlan" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {elseif $EditMode eq 'true'}
    <form id="PlanForm" name="PlanForm" action="PostAction.php?Action=EditPlan" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {else}
    <form id="PlanForm" name="PlanForm" action="Plan.php?PlanID={$PlanInfo.PlanID}" method="post" target="_self">
  {/if}

<div id="TopNavMain" class="TopBar">
    <a id="TopNavLogo" href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    <!-- Set the Toolbar buttons status by Lichuan Liu -->
    {if $PrePlanID eq 0 or $EditMode eq 'true'}
    	{assign var="PreButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $NextPlanID eq 0 or $EditMode eq 'true'}
    	{assign var="NextButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ActionType eq 'OpenPlan' or $EditMode eq 'true'}
    	{assign var="EditButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $PlanInfo.PlanStatus neq 'Active' or $ActionType eq 'OpenPlan'or $EditMode eq 'true' or $testIsOwner neq 'true'}
    	{assign var="ResolvePlanButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $PlanInfo.PlanStatus eq 'Closed' or $ActionType eq 'OpenPlan'or $EditMode eq 'true' or $testIsCloser neq 'true'}
    	{assign var="ClosePlanButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $PlanInfo.PlanStatus eq 'Active' or $EditMode eq 'true'}
    	{assign var="ActivePlanButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $ActionType eq ''}
    	{assign var="SaveButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    {if $PlanInfo.PlanStatus neq 'Active' or $ActionType eq 'OpenPlan'or $EditMode eq 'true'}
    	{assign var="OpenChangeButtonStatus" value = 'disabled="disabled" style="cursor:default"'}
    {/if}
    
    <div id="ButtonList">
      <input type="button" class="ActionButton Btn" accesskey="P" value="{$Lang.PreButton}" onclick="location.href='Plan.php?PlanID={$PrePlanID}'" {$PreButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="N" value="{$Lang.NextButton}" onclick="location.href='Plan.php?PlanID={$NextPlanID}'" {$NextButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="E" value="{$Lang.EditPlanButton}" onclick="xajax.$('ActionType').value='Edited';submitForm('PlanForm')" {$EditButtonStatus}}/>
      <input type="button" class="ActionButton Btn" accesskey="C" value="{$Lang.CopyPlanButton}" onclick="xajax.$('ActionType').value='OpenPlan';submitForm('PlanForm');" {$EditButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="R" value="{$Lang.ResolvePlanButton}" onclick="xajax.$('ActionType').value='Resolved';submitForm('PlanForm');" {$ResolvePlanButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="L" value="{$Lang.ClosePlanButton}" onclick="xajax.$('ActionType').value='Closed';submitForm('PlanForm');" {$ClosePlanButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="A" value="{$Lang.ActivePlanButton}" onclick="xajax.$('ActionType').value='Activated';submitForm('PlanForm');" {$ActivePlanButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="S" value="{$Lang.SaveButton}" name="SubmitButton" id="SubmitButton" onclick="this.disabled='disabled';NeedToConfirm=false;document.PlanForm.submit();" {$SaveButtonStatus}/>
    	<input type="button" class="ActionButton Btn" accesskey="B" value="{$Lang.OpenChangeButton}" {$OpenChangeButtonStatus}  onclick="openWindow('Change.php?ActionType=OpenChange&PlanID={$PlanInfo.PlanID}','OpenChange');"/>
    </div>
</div>


<div id="BugMain" class="CommonForm BugMode">
	<span id="BugId">{if $ActionType eq 'OpenPlan'}{$Lang.OpenPlan}{else}{$PlanInfo.PlanIDName}{/if}</span>
  <div id="BugMainInfo">
  		<table style="width: 100%">
      		<tr>
          		<td style="width: 20%" valign="top">
          			<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.PlanFields.PlanID}</dt>
      									<dd>
      										{if $ActionType eq 'OpenPlan'}{$Lang.OpenPlan}{else}{$PlanInfo.PlanIDName}{/if}
      									</dd>
    								</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.PlanFields.PlanStatus}</dt>
                  			<dd>
                    			{if $ActionType eq 'Activated'}{$Lang.PlanStatus.Active}
                    			{elseif $ActionType eq 'Closed'}{$Lang.PlanStatus.Closed}
                    			{elseif $ActionType eq 'Resolved'}{$Lang.PlanStatus.Resolved}
                    			{else}{$PlanInfo.PlanStatusName}{/if}
                  			</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl style="line-height:17pt">
                    		<dt>{$Lang.PlanFields.OpenedBy}</dt>
                    		<dd>
                            {if $ActionType eq 'OpenPlan'}
                            {$templatelite.session.TestRealName}
                        		{else}
                            {$PlanInfo.OpenedByName}
                        		{/if}
                    		</dd>
                  	</dl>
                    <dl style="line-height:17pt">
                    		<dt>{$Lang.PlanFields.ClosedBy}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                        		{elseif $ActionType eq 'Closed'}
                          	{$ActionRealName}
                        		{else}
                          	{$PlanInfo.ClosedByName}
                      			{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.PlanFields.LastEditedBy}</dt>
                  			<dd>{$PlanInfo.LastEditedByName}</dd>
                		</dl>
                </fieldset>
              	<fieldset class="Normal FloatLeft" style="width: 94%">
										<dl style="line-height:17pt">
                    		<dt>{$Lang.PlanFields.OpenedDate}</dt>
                    		<dd>
                        		{if $ActionType eq 'OpenPlan'}
                            {$templatelite.now|date_format:"%Y-%m-%d"}
                        		{else}
                            {$PlanInfo.OpenedDate|date_format:"%Y-%m-%d"}
                        		{/if}
                    		</dd>
                  	</dl>
                  	<dl style="line-height:17pt">
                    		<dt>{$Lang.PlanFields.ClosedDate}</dt>
                    		<dd>
                      			{if $ActionType eq 'Activated'}
                      			{elseif $ActionType eq 'Closed'}
                        		{$ActionDate}
                      			{else}
                        		{if $PlanInfo.ClosedDate neq $CFG.ZeroTime}{$PlanInfo.ClosedDate|date_format:"%Y-%m-%d"}{/if}{/if}
                    		</dd>
                  	</dl>
										<dl style="line-height:17pt">
                  			<dt>{$Lang.PlanFields.LastEditedDate}</dt>
                  			<dd>{$PlanInfo.LastEditedDate|date_format:"%Y-%m-%d"}</dd>
                		</dl>
                </fieldset>
                <fieldset class="Normal FloatLeft" style="width: 94%">
                  		<dl style="line-height:17pt">
                    			<dt>{$Lang.PlanFields.PlanCaseID}</dt>
                    			<dd>
                         	{foreach from=$PlanInfo.CaseIDList key=key value=value}
                          <a href="Case.php?CaseID={$key}" title="{$value}" target="_blank">{$Lang.CaseIDPrefix}{$key}</a>
                         	{/foreach}
                    			</dd>
                  		</dl>
                </fieldset>
              </td>
              <td style="width: 49%" valign="top">
              		<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.PlanFields.PlanTitle}</dt>
      										<dd>
          										{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner) }
					            				<input type="text" id="PlanTitle" name="PlanTitle" class="MyInput RequiredField" value="{$PlanInfo.PlanTitle}" style="width:500px;"/>
          										{else}
            									<input type="text" id="PlanTitle" name="PlanTitle" readonly=readonly class="MyInput ReadOnlyField" title="{$PlanInfo.PlanTitle}" value="{$PlanInfo.PlanTitle}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
    									<dl style="line-height:17pt">
                    			<dt>{$Lang.PlanFields.PlanKeyword}</dt>
                    			<dd>
                        			{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
                            	<input type="text" name="PlanKeyword" id="PlanKeyword" size="20" class="MyInput" maxlength="80" value="{$PlanInfo.PlanKeyword}" />
                        			{else}
                            	<input type="text" name="PlanKeyword" id="PlanKeyword" size="20" readonly=true  class="MyInput ReadOnlyField" maxlength="80" value="{$PlanInfo.PlanKeyword}" />
                        			{/if}
                    			</dd>
                  		</dl>
    							</fieldset>
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                			<dl style="line-height:17pt">
                  				<dt>{$Lang.PlanFields.PlanType}</dt>
                  				<dd>
                              {if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
                          		{$PlanTypeList}
                      				{else}
                          		{$PlanInfo.PlanTypeName}
                              {/if}
                  				</dd>
                			</dl>
                  </fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
          					<dl style="line-height:17pt">
      									<dt>{$Lang.PlanFields.ProjectName}</dt>
         								{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
          							<dd>{$ProjectList}</dd>
        								{else}
          							<dd>{$PlanInfo.ProjectName}</dd>
        								{/if}
    								</dl>
        						<dl style="line-height:17pt">
      									<dt>{$Lang.PlanFields.ModulePath}</dt>
         								{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
            						<dd id="SlaveModuleList">{$ModuleList}</dd>
        								{else}
          							<dd>{$PlanInfo.ModulePath}</dd>
        								{/if}
    								</dl>
          				</fieldset>
              		<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.PlanFields.PlanBuild}</dt>
      										<dd>
          										{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner) }
					            				<input type="text" id="PlanBuild" name="PlanBuild" class="MyInput RequiredField" value="{$PlanInfo.PlanBuild}" style="width:200px;"/>
          										{else}
            									<input type="text" id="PlanBuild" name="PlanBuild" readonly=readonly class="MyInput ReadOnlyField" title="{$PlanInfo.PlanBuild}" value="{$PlanInfo.PlanBuild}" style="width:535px;"/>
        											{/if}
      										</dd>
    									</dl>
                	</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl style="line-height:17pt">
                  			<dt>{$Lang.PlanFields.AssignedTo}</dt>
                  			<dd>
                    				{if $ActionType eq 'OpenPlan' or $EditMode eq 'true'}
                          	<span id="AssignedToUserList">{$AssignedToUserList}</span>
                      			{else}
                          	{$PlanInfo.AssignedToName}
                          	{/if}
                  			</dd>
                		</dl>
                		<dl style="line-height:17pt">
                  			<dt>{$Lang.PlanFields.MailTo}</dt>
                  			<dd>
                          {if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
                          <input id="MailTo" name="MailTo" type="text" value="{$PlanInfo.MailTo}" size="50"  maxlength="255" AUTOCOMPLETE="OFF" />
                      		{else}
                          <input id="MailTo" name="MailTo" type="text" value="{$PlanInfo.MailToName}" size="50"  maxlength="255" readonly=true class="MyInput ReadOnlyField" AUTOCOMPLETE="OFF" />
                      		{/if}
                  			</dd>
               	 		</dl>
               	 		{if $EditMode eq 'true'}
               	 		<dl style="line-height:17pt">
      									<dt>
      										<input type="checkbox" id="SendNotifyEmail" name="SendNotifyEmail" checked="checked" value="SendNotifyEmail" onclick="SetNotifyEmail();"/>
      									</dt>
      									<dd>{$Lang.SendNotifyEmail}</dd>
										</dl>
										{/if}
	                </fieldset>
    							<fieldset class="Normal FloatLeft" style="width: 94%">
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.PlanFields.PlanStartDate}</dt>
                    			<dd>
                       				{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
                        			<input type=text name="PlanStartDate" id="PlanStartDate" size="20" class="MyInput RequiredField" value="{$PlanInfo.PlanStartDate}" readonly="readonly" onclick="SetCalendar('PlanStartDate');"/>
                        		{else}
                  					{$PlanInfo.PlanStartDate}
                						{/if}
                    			</dd>            			
    									</dl>
 					       			<dl style="line-height:17pt">
      										<dt>{$Lang.PlanFields.PlanEndDate}</dt>
                    			<dd>
                       				{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
                        			<input type=text name="PlanEndDate" id="PlanEndDate" size="20" class="MyInput RequiredField" value="{$PlanInfo.PlanEndDate}" readonly="readonly" onclick="SetCalendar('PlanEndDate');"/>
                        		{else}
                  					{$PlanInfo.PlanEndDate}
                						{/if}
                    			</dd>            			
    									</dl>    									
                	</fieldset>      									
	                <fieldset class="Normal FloatLeft" style="width: 94%">
                   	<dl>
                    	<dt>{$Lang.PlanFields.PlanContent}</dt>
                    	<dd style="text-align:left;">
                      	{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
                    			<textarea id="PlanContent" name="PlanContent" rows="11" cols="70" style="overflow-y:visible;">{$PlanInfo.PlanContent}</textarea>
                  			{else}
													<p style="overflow: auto">{$PlanInfo.PlanContent|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  	  </dd>
                   	</dl>
                	</fieldset>               	
                	<fieldset class="Normal FloatLeft" style="width: 94%">    									
    									<dl style="line-height:17pt">
                    			<dt>{$Lang.PlanFields.PlanPassRate}</dt>
                    			<dd>
                        			{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
                            	<input type="text" name="PlanPassRate" id="PlanPassRate" size="20" class="MyInput" maxlength="80" value="{$PlanInfo.PlanPassRate}" />
                        			{else}
                            	<input type="text" name="PlanPassRate" id="PlanPassRate" size="20" readonly=true  class="MyInput ReadOnlyField" maxlength="80" value="{$PlanInfo.PlanPassRate}" />
                        			{/if}
                    			</dd>
                  		</dl>
                  		<dl style="line-height:17pt">
                    		<dt>{$Lang.PlanFields.PlanConclusion}</dt>
                    		<dd>
                  			{if $ActionType eq 'OpenPlan' or ($EditMode eq 'true' and $testIsOwner)}
                    			<textarea id="PlanConclusion" name="PlanConclusion" rows="11" cols="70" style="overflow-y:visible;">{$PlanInfo.PlanConclusion}</textarea>
                  			{else}
													<p style="overflow: auto">{$PlanInfo.PlanConclusion|replace:" ":"&nbsp;"|bbcode2html}</p>
                    		{/if}
                  			</dd>
                  		</dl>
                	</fieldset>
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                   	<legend>{$Lang.BugFiles}</legend>
                   	<dl>
                    	<dd style="text-align:left;">
                      		{assign var="FileList" value=$PlanInfo.FileList}
                      		{include file="FileInfo.tpl"}
                  	  </dd>
                   	</dl>
                	</fieldset>
                	{if $EditMode eq 'true'}
                	<fieldset class="Normal FloatLeft" style="width: 94%">
                  	<dl>
                    	<dt>{$Lang.ReplyNote}</dt>
                    	<dd style="text-align:left;">
                      	<textarea id="ReplyNote" name="ReplyNote" rows="5" cols="70" style="overflow-y:visible;" ></textarea>
                  	  </dd>
                   	</dl>
                	</fieldset>
            	  	{/if}
            	</td>
          </tr>
          <tr>
              <td colspan="3">
                <fieldset id="CustomedFieldSet" class="Normal FloatLeft" style="width: 98%;{if $CustomedFieldHtml eq null}display:none{/if}">
                  {$CustomedFieldHtml}
                </fieldset>
              </td>
					</tr>
			</table>
   </div>
   <div id="PlanHistory">
      <table style="width: 100%">
            <tr>
            	<td style="width: 20%" valign="top">
            	</td>
              <td style="width: 49%" valign="top">
                <fieldset class="Normal FloatLeft" id="PlanHistoryInfo" style="width: 94%">
                  <legend>{$Lang.HistoryInfo}</legend>
                  {include file="PlanHistory.tpl"}
                </fieldset>
              </td>
            </tr>
        </table>
  </div>
</div>

<input type="hidden" id="PlanID" name="PlanID" value="{$PlanInfo.PlanID}" />
<input type="hidden" id="DeleteFileIDs" name="DeleteFileIDs" value="" />
<input type="hidden" id="ActionType" name="ActionType" value="{$ActionType}" />
<input type="hidden" id="ActionObj" name="ActionObj" value="Plan" />
<input type="hidden" id="TestUserName" name="TestUserName" value="{$templatelite.session.TestUserName}" />
<input type="hidden" id="TestRealName" name="TestRealName" value="{$templatelite.session.TestRealName}" />
<input type="hidden" id="ToDisabledObj" name="ToDisabledObj" value="SubmitButton" />
<input type="hidden" id="CurrentProjectID" name="CurrentProjectID" value="{$ProjectID}" />
<input type="hidden" id="LastAcitonID" name="LastActionID" value="{$LastActionID}" />

</form>
{include file="PostActionFrame.tpl"}
{literal}
<script type="text/javascript">
//<![CDATA[
function superAddObjValue(objID,addValue)
{
    xajax.$(objID).value += ',' + addValue;
}
function SetNotifyEmail()
{
	ItemCheckBox = document.getElementById('SendNotifyEmail');
	if(ItemCheckBox.checked==false)
	{
		ItemCheckBox.value = '';
	}
	else
	{
		ItemCheckBox.value = 'SendNotifyEmail';
	}
	//alert(ItemCheckBox.value);
}
function SetCalendar(ItemName)
{
  Calendar.setup(
    {
      inputField  : ItemName,
      ifFormat    : "%Y-%m-%d",
      button      : ItemName
    }
  );
}
{/literal}
{if $ActionType neq ''}
{literal}
  setConfirmExitArrays();
  initSelectDiv('MailTo','selectDivProjectUserList','getInputSearchValueByComma','setValueByComma', true);
  xajax.$('ReplyNote').focus();
{/literal}
{/if}
{if $ActionType eq 'OpenPlan'}
   {literal}focusInputEndPos('PlanTitle');{/literal}
{elseif $ActionType eq 'Resolved'}
   {literal}xajax.$('ResolvedBuild').focus();{/literal}
{/if}
{literal}
//]]>
</script>
{/literal}
</body>
</html>
