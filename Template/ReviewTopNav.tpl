{include file="Header.tpl"}
<body class="BugMode BugMain" onload="TestMode='Review';initShowGotoBCR();" onscroll="setTopBarLeftPos()">
  {if $ActionType eq 'Edited' or $ActionType eq 'Resolved' or $ActionType eq 'Closed' or $ActionType eq 'Activated'}
  {assign var="EditMode" value= 'true'}
  {/if}

  {if $ActionType eq 'OpenReview'}
    <form id="ReviewForm" name="ReviewForm" action="PostAction.php?Action=OpenReview" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {elseif $EditMode eq 'true'}
    <form id="ReviewForm" name="ReviewForm" action="PostAction.php?Action=EditReview" target="PostActionFrame" enctype="multipart/form-data" method="post">
  {else}
    <form id="ReviewForm" name="ReviewForm" action="Review.php?ReviewID={$ReviewInfo.ReviewID}" method="post" target="_self">
  {/if}

<div id="TopNavMain" class="TopBar">
    <a id="TopNavLogo" href="./" target="_top"><img src="Image/logo.png" title={$Lang.ProductName} /></a>
    <!-- Set the Toolbar buttons status by Lichuan Liu -->
    {if $PreReviewID eq 0 or $EditMode eq 'true'}{assign var="PreButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $NextReviewID eq 0 or $EditMode eq 'true'}{assign var="NextButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ActionType eq 'OpenReview' or $EditMode eq 'true'}{assign var="EditButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ReviewInfo.ReviewStatus neq 'Active' or $ActionType eq 'OpenReview'or $EditMode eq 'true'}{assign var="ResolveReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ReviewInfo.ReviewStatus neq 'Resolved' or $ActionType eq 'OpenReview'or $EditMode eq 'true'}{assign var="CloseReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ReviewInfo.ReviewStatus eq 'Active' or $EditMode eq 'true'}{assign var="ActiveReviewButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    {if $ActionType eq ''}{assign var="SaveButtonStatus" value = 'disabled="disabled" style="cursor:default"'}{/if}
    
    <div id="ButtonList">
      <input type="button" class="ActionButton Btn" accesskey="P" value="{$Lang.PreButton}" onclick="location.href='Review.php?ReviewID={$PreReviewID}'" {$PreButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="N" value="{$Lang.NextButton}" onclick="location.href='Review.php?ReviewID={$NextReviewID}'" {$NextButtonStatus}/>
      <input type="button" class="ActionButton Btn" accesskey="E" value="{$Lang.EditReviewButton}" onclick="xajax.$('ActionType').value='Edited';submitForm('ReviewForm')" {$EditButtonStatus}}/>
      <!--input type="button" class="ActionButton Btn" accesskey="C" value="{$Lang.CopyReviewButton}" onclick="xajax.$('ActionType').value='OpenReview';submitForm('ReviewForm');" {$EditButtonStatus} /-->
      <input type="button" class="ActionButton Btn" accesskey="R" value="{$Lang.ResolveReviewButton}" onclick="xajax.$('ActionType').value='Resolved';submitForm('ReviewForm');" {$ResolveReviewButtonStatus} />
      <input type="button" class="ActionButton Btn" accesskey="L" value="{$Lang.CloseReviewButton}" onclick="xajax.$('ActionType').value='Closed';submitForm('ReviewForm');" {$CloseReviewButtonStatus} />
      <!--input type="button" class="ActionButton Btn" accesskey="A" value="{$Lang.ActiveReviewButton}" onclick="xajax.$('ActionType').value='Activated';submitForm('ReviewForm');" {$ActiveReviewButtonStatus} /-->
      <input type="button" class="ActionButton Btn" accesskey="S" value="{$Lang.SaveButton}" name="SubmitButton" id="SubmitButton" onclick="this.disabled='disabled';NeedToConfirm=false;document.ReviewForm.submit();" {$SaveButtonStatus}/>
			<input type="button" class="Action1Button Btn" value="{$Lang.GoToReviewCommentPageButton}" onclick="xajax.$('ActionType').value='GoToReviewCommentPage';submitForm('ReviewForm');" {$GoToReviewCommentPageButtonStatus} />
    </div>
</div>

<input type="hidden" id="ReviewID" name="ReviewID" value="{$ReviewInfo.ReviewID}" />
<input type="hidden" id="DeleteFileIDs" name="DeleteFileIDs" value="" />
<input type="hidden" id="ActionType" name="ActionType" value="{$ActionType}" />
<input type="hidden" id="ActionObj" name="ActionObj" value="Review" />
<input type="hidden" id="TestUserName" name="TestUserName" value="{$templatelite.session.TestUserName}" />
<input type="hidden" id="TestRealName" name="TestRealName" value="{$templatelite.session.TestRealName}" />
<input type="hidden" id="ToDisabledObj" name="ToDisabledObj" value="SubmitButton" />
<input type="hidden" id="CurrentProjectID" name="CurrentProjectID" value="{$ProjectID}" />
<input type="hidden" id="LastAcitonID" name="LastActionID" value="{$LastActionID}" />
<input type="hidden" id="ShowPageFlag" name="ShowPageFlag" value="{$ShowPageFlag}" />

</form>
{include file="PostActionFrame.tpl"}
{literal}
<script type="text/javascript">
//<![CDATA[
function superAddObjValue(objID,addValue)
{
    xajax.$(objID).value += ',' + addValue;
}
{/literal}
{if $ActionType neq ''}
{literal}
  setConfirmExitArrays();
  initSelectDiv('MailTo','selectDivProjectUserList','getInputSearchValueByComma','setValueByComma', true);
  xajax.$('ReplyNote').focus();
{/literal}
{/if}
{if $ActionType eq 'OpenReview'}
   {literal}focusInputEndPos('ReviewTitle');{/literal}
{elseif $ActionType eq 'Resolved'}
   {literal}xajax.$('ResolvedBuild').focus();{/literal}
{/if}
{literal}
//]]>
</script>
{/literal}
</body>
</html>
