<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * list reviews.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');
require('Include/FuncImportOutport.php');
@ini_set('memory_limit', -1);

// the flag of using custom field
$hasCustomField = true;
$ProjectID = testGetCurrentProjectId();
if($_GET['ProjectID'] - 0 > 0)
{
    $_SESSION['ReviewCommentQueryCondition'] = "ProjectID = '{$_GET['ProjectID']}'";
}


if($_GET['ModuleID'] - 0 > 0)
{
    $_SESSION['TestCurrentModuleID'] = $_GET['ModuleID'];
    $_SESSION['ReviewCommentQueryCondition'] = "ModuleID IN ({$_GET['ChildModuleIDs']})";
}

if($_GET['ActionType']=='FromNotifyEmail')
{
      $TopModuleID = $_GET['TopModuleID'];
      $ChildModuleList = testGetProjectModuleList($_GET['ProjectID'], 'Bug');//获取所有Module，以及父子关
      $ChildModules = $ChildModuleList[$TopModuleID]['ChildIDs'];//获得当前Module的所有子Module
      if($TopModuleID>=0)
      {
         $_SESSION['ReviewCommentQueryCondition'] = str_replace("\'", "'", $_GET['Condition'])." AND ProjectID={$_GET[ProjectID]} AND ModuleID in({$ChildModules})";
      }
      else
      {
         $_SESSION['ReviewCommentQueryCondition'] = str_replace("\'", "'", $_GET['Condition'])." AND ProjectID={$_GET[ProjectID]}";
      }
      $_SESSION['TestMode'] ='ReviewComment';
}

if($_POST['PostQuery'])
{
    $hasCustomField = false;
    $_SESSION['hasReviewCommentCustomField'] = false;
    $QueryStr = baseGetGroupQueryStr($_POST);
    $_SESSION['ReviewCommentQueryCondition'] = $QueryStr;
    $_SESSION['ReviewCommentQueryTitle'] = '';
    $_SESSION['ReviewCommentAndOrListCondition'] = baseGetAndOrListStr($_POST);
    $_SESSION['ReviewCommentFieldListCondition'] = baseGetFieldListStr($_POST);
    $_SESSION['ReviewCommentOperatorListCondition'] = baseGetOperatorListStr($_POST);
    $_SESSION['ReviewCommentValueListCondition'] = baseGetValueListStr($_POST);
    $_SESSION['ReviewCommentLeftParenthesesListCondition'] = baseGetLeftParentheseListStr($_POST);
    $_SESSION['ReviewCommentRightParenthesesListCondition'] = baseGetRightParentheseListStr($_POST);
    $arr = array_diff($_SESSION['ReviewCommentFieldListCondition'], array_keys($_LANG['ReviewCommentQueryField']));
    if(!empty($arr))
    {
        $hasCustomField = $_SESSION['hasReviewCommentCustomField']= true;
    }
    unset($arr);
}

if($_REQUEST['QueryID'])
{
    $hasCustomField = false;
    $_SESSION['hasReviewCommentCustomField'] = false;
    $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='ReviewComment'");
    $_SESSION['ReviewCommentQueryCondition'] = $QueryInfo['QueryString'];
    $_SESSION['ReviewCommentQueryTitle'] = $QueryInfo['QueryTitle'];
    $_SESSION['ReviewCommentAndOrListCondition'] = unserialize($QueryInfo['AndOrList']);
    $_SESSION['ReviewCommentFieldListCondition'] = unserialize($QueryInfo['FieldList']);
    $_SESSION['ReviewCommentOperatorListCondition'] = unserialize($QueryInfo['OperatorList']);
    $_SESSION['ReviewCommentValueListCondition'] = unserialize($QueryInfo['ValueList']);
    $_SESSION['ReviewCommentFieldsToShow'] = $QueryInfo['FieldsToShow'];
    $_SESSION['ReviewCommentLeftParenthesesListCondition'] = unserialize($QueryInfo['LeftParentheses']);
    $_SESSION['ReviewCommentRightParenthesesListCondition'] = unserialize($QueryInfo['RightParentheses']);
    $arr = array_diff($_SESSION['ReviewCommentFieldListCondition'], array_keys($_LANG['ReviewCommentQueryField']));
    if(!empty($arr))
    {
        $hasCustomField = $_SESSION['hasReviewCommentCustomField']= true;
    }
    unset($arr);
}
else
{
   $_REQUEST['QueryID'] = '-1';
}

$WHERE = array();
$URL = array();

$WHERE[] = $_SESSION['TestUserACLSQL'];

if($_SESSION['ReviewCommentQueryCondition'] != '')
{
    $WHERE[] = $_SESSION['ReviewCommentQueryCondition'];
}

if($_GET['OrderBy'])
{
    $OrderByList = explode('|', $_GET['OrderBy']);
    $OrderByColumn = $OrderByList[0];
    $OrderByType = $OrderByList[1];
    $OrderBy = join(' ', $OrderByList);
    $URL[] = 'OrderBy=' . $_GET['OrderBy'];
    $_SESSION['ReviewCommentOrderBy']['OrderBy'] = $OrderBy;
    $_SESSION['ReviewCommentOrderBy']['OrderByColumn'] = $OrderByColumn;
    $_SESSION['ReviewCommentOrderBy']['OrderByType'] = $OrderByType;
}
else
{
    if(empty($_SESSION['ReviewCommentOrderBy']))
    {
        $_SESSION['ReviewCommentOrderBy']['OrderBy'] = ' ReviewCommentID DESC';
        $_SESSION['ReviewCommentOrderBy']['OrderByColumn'] = 'ReviewCommentID';
        $_SESSION['ReviewCommentOrderBy']['OrderByType'] = 'DESC';
    }
    $OrderBy = $_SESSION['ReviewCommentOrderBy']['OrderBy'];
    $OrderByColumn = $_SESSION['ReviewCommentOrderBy']['OrderByColumn'];
    $OrderByType = $_SESSION['ReviewCommentOrderBy']['OrderByType'];
}

if($_GET['QueryMode'])
{
    $hasCustomField = false;
    $_SESSION['hasReviewCommentCustomField'] = false;
    $QueryModeList = explode('|', $_GET['QueryMode']);
    $QueryColumn = $QueryModeList[0];
    $QueryValue = $QueryModeList[1];
    $WHERE = array();
    $WHERE[] = $_SESSION['TestUserACLSQL'];
    $QueryCondition = "";
    if(preg_match('/date/i', $QueryColumn))
    {
        $QueryCondition =  $QueryColumn . ' ' . sysStrToDateSql($QueryValue);
    }
    else
    {
        $QueryCondition = "{$QueryColumn}='{$QueryValue}'";
    }
    $_SESSION['ReviewCommentQueryCondition'] = $QueryCondition;
    $_SESSION['ReviewCommentQueryColumn'] = $QueryColumn;
    $WHERE[] = $QueryCondition;
    $URL[] = 'QueryMode=' . sysStripSlash($_GET['QueryMode']);
    // check if is set custom field for special function
    if(!array_key_exists($QueryColumn, $_LANG['ReviewCommentQueryField']))
    {
        $hasCustomField = $_SESSION['hasReviewCommentCustomField'] = true;
    }
}
$Url = '?' . join('&', $URL);
$WHERE[] = "IsDroped = '0'";
$Where = join(' AND ', $WHERE);

if(isset($_SESSION['hasReviewCommentCustomField']))
{
    $hasCustomField = $_SESSION['hasReviewCommentCustomField'];
}
$FieldsToShow = testSetCustomFields('ReviewComment', $ProjectID, $hasCustomField);
if(!array_key_exists($OrderByColumn, $FieldsToShow))
{
    $OrderBy = 'ReviewCommentID DESC';
    $OrderByColumn = 'ReviewCommentID';
    $OrderByType = 'DESC';
}

/* Get pagination */
$Pagination = new Page('ReviewCommentInfo', '', '', '', 'WHERE ' . $Where . ' ORDER BY ' . $OrderBy, $Url, $MyDB);
$LimitNum = $Pagination->LimitNum();
$TPL->assign('PaginationDetailInfo', $Pagination->getDetailInfo());
$ColumnArray = @array_keys($FieldsToShow);
$Columns = 'ReviewCommentID,ReviewCommentStatus,' . join(',',$ColumnArray);


$OrderColumnList = $ColumnArray;
$OrderByTypeList = array();
foreach($OrderColumnList as $OrderColumn)
{
    if($OrderColumn == $OrderByColumn)
    {
        $OrderByTypeList[$OrderColumn] = $OrderTypeReverseArray[$OrderByType];
    }
    else
    {
        $OrderByTypeList[$OrderColumn] = $OrderByType;
    }
}

if($_GET['Export'] == 'HtmlTable')
{
    $Columns = '';
    $LimitNum = '';
}

$ReviewCommentListSql = dbGetListSql(dbGetPrefixTableNames('ReviewCommentInfo'), '', $Where, '', $OrderBy, $LimitNum);
if($Where != $_SESSION['OldReviewCommentQueryWhereStr'])
{
    $_SESSION['OldReviewCommentQueryWhereStr'] = $Where;
    if(!($_GET['QueryMode']))
    {
        $_SESSION['ReviewCommentQueryColumn'] = '';
    }
}
if(($ProjectID != null) && ($hasCustomField))
{
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="ReviewComment"]/field');
        if($fields)
        {
            $tableName = testGetFieldTable('ReviewComment', $ProjectID);
            $Pagination = new Page(dbGetPrefixTableNames('ReviewCommentInfo') . ' LEFT JOIN ' . $tableName .' ON ' . dbGetPrefixTableNames('ReviewCommentInfo') . '.ReviewCommentID = ' . $tableName . '.FieldID', '', '', '', 'WHERE ' . $Where . ' ORDER BY ' . $OrderBy, $Url, $MyDB, false);
            $LimitNum = $Pagination->LimitNum();
            $TPL->assign('PaginationDetailInfo', $Pagination->getDetailInfo());
            $Where .= ' AND ProjectID = ' . $ProjectID;
            $sql =  dbGetListSql(dbGetPrefixTableNames('ReviewCommentInfo') . ',' . $tableName, '', $Where, '', $OrderBy, $LimitNum);
            $ReviewCommentListSql = str_replace(dbGetPrefixTableNames('ReviewCommentInfo') . ',' . $tableName, dbGetPrefixTableNames('ReviewCommentInfo') . ' LEFT JOIN ' . $tableName .' ON ' . dbGetPrefixTableNames('ReviewCommentInfo') . '.ReviewCommentID = ' . $tableName . '.FieldID', $sql);
        }
    }
}

$ReviewCommentList = dbGetListBySql($ReviewCommentListSql);
$UserNameList = testGetOneDimUserList();
$ReviewCommentList = testSetReviewCommentListMultiInfo($ReviewCommentList, $UserNameList);
$TPL->assign('ReviewCommentList', $ReviewCommentList);

$TPL->assign('OrderByColumn', $OrderByColumn);
$TPL->assign('OrderByType', $OrderByType);
$TPL->assign('QueryMode', $_GET['QueryMode']);
$TPL->assign('QueryColumn', $_SESSION['ReviewCommentQueryColumn']);
$TPL->assign('OrderByTypeList', $OrderByTypeList);
$TPL->assign('BaseTarget', '_self');
$TPL->assign('TestMode', 'ReviewComment');

//ReviewCommentList的导入处理代码
if($_FILES['file']['error']==2)	//导入成功
{
      sysObFlushJs("alert('{$_LANG['ImportFileExceed']}');");
}
elseif($_FILES['file']['error']==0)
{
        if($_FILES['file']['tmp_name'])   // 如果文件存在则进行批量导入
        {
	  				$findtype=strtolower(strrchr($_FILES['file']['name'],"."));

          	if($findtype!='.xml')	//文件必须以.xml结尾
          	{
            		sysObFlushJs("alert('{$_LANG['ImportFileSuffixError']}');");
          	}
          	else 
          	{
          			//读取文件内容到FileContents
		  					$file=fopen($_FILES['file']['tmp_name'],"r");
		  					$FileContents = fread($file, filesize($_FILES['file']['tmp_name']));
		  					//Rainy_Debug($FileContents);
		  					$FileContents = preg_replace('/<Font.*>|<\/Font>/Us', '', $FileContents);
								//将文件内容解析成变量，并存储在Dom变量中	
		            $Dom =  XML_unserialize($FileContents);
    		        $DomRowArray = $Dom[Workbook][Worksheet][Table][Row];
								//解析DomRowArray 并导入至数据库中
            		ParseImportReviewCommentXML($DomRowArray,$_LANG);
        	}
    		}
}

if($_GET['Export'] == 'HtmlTable')
{
    $TPL->assign('DataList', $ReviewCommentList);
    $TPL->assign('FieldsToShow', $_LANG["ReviewCommentFields"]);
    $TPL->display('ExportList.tpl');
    exit;
}

if($_GET['Export'] == 'XMLFile')  //批量导出ReviewComment
{
   $ReviewCommentExportList = dbGetList('ReviewCommentInfo','', $Where, '', $OrderBy);
   $ReviewCommentCount = sizeof($ReviewCommentExportList);


   if($ReviewCommentCount >5000 ){
     sysObFlushJs("alert('{$_LANG['ExportCountExceed']}');");
   }
   else{
       $ReviewCommentExportColumnMust = array('ReviewCommentID' , 'ReviewCommentTitle', 'ProjectName', 'ModulePath', 'ReproSteps'); //必须包含的字段

      $ReviewCommentExportColumnArray = $ColumnArray;
      foreach($ReviewCommentExportColumnMust as $Item)
      {
         if(!in_array($Item, $ReviewCommentExportColumnArray))
            $ReviewCommentExportColumnArray[] = $Item;

      }

      $URL=$_CFG['File']['UploadDirectory'] . "/ReviewCommentlist.xml";
      $File = fopen($URL,"w");

      $Content = ExportXML($ReviewCommentExportList,$ReviewCommentExportColumnArray,$_LANG['ReviewCommentFields']);

      file_put_contents($URL, $Content);
      header('Content-type: text/xml; charset=utf-8');
      header('Content-Disposition: attachment; filename=ReviewCommentlist.xml');
      readfile($URL);
      exit;
   }
}
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('QueryID', $_REQUEST['QueryID']);
$TPL->assign('QueryTitle', $_SESSION['ReviewCommentQueryTitle'] );
$TPL->display('ReviewCommentList.tpl');
?>