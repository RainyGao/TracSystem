<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * search Change form.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

$FieldName = 'Field';
$OperatorName = 'Operator';
$ValueName = 'Value';
$AndOrName = 'AndOr';
$LeftParenthesesName = 'LeftParenthesesName';
$RightParenthesesName = 'RightParenthesesName';
$FieldList = array();
$OperatorList = array();
$ValueList = array();
$AndOrList = array();
$FieldCount = $_CFG['QueryFieldNumber'];
$Attrib = 'class="FullSelect"';


$FieldListSelectItem = array(0=>'ProjectName',1=>'OpenedBy',2=>'ModulePath',3=>'AssignedTo',4=>'ChangeID',5=>'ChangeTitle');
$OperatorListSelectItem = array(0=>'=', 2=>'LIKE', 5=>'LIKE');

if(!empty($_REQUEST['reset']))
{
    $_SESSION['ChangeFieldListCondition']='';
    $_SESSION['ChangeOperatorListCondition']='';
    $_SESSION['ChangeAndOrListCondition']='';
    $_SESSION['ChangeValueListCondition']='';
    $_SESSION['ChangeQueryCondition'] = '';
    $_SESSION['ChangeQueryTitle'] = '';
    $_SESSION['ChangeFieldsToShow'] = '';
    $_SESSION['ChangeLeftParenthesesListCondition']='';
    $_SESSION['ChangeRightParenthesesListCondition']='';
}
$UserList = testGetCurrentUserNameList('PreAppend');

$ProjectID = testGetCurrentProjectId();
if($ProjectID != null)
{
		/*从默认数据库的TestProject表中指定行读取信息*/
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="Change"]/field');
        if($fields)
        {
            $fields = sysFieldXmlToArr($fields);
            $fieldArr = array();
            $fieldNameArr = array();
            foreach($fields as $field)
            {
                $key = $field['name'];
                if($field['status'] != 'active')
                {
                    continue;
                }
                $_LANG['ChangeQueryField']["$key"] = $field['text'];
                if($field['type'] == 'select')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'mulit')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'user')
                {
                    $fieldArr["$key"]['value']   = array_keys($UserList);
                    $fieldArr["$key"]['key'] = array_values($UserList);
                }
                $fieldNameArr[] = $key;
            }
            if(!empty($fieldArr))
            {
                $AutoTextValue['CustomField'] = json_encode($fieldArr);
                $AutoTextValue['CustomFieldName'] = jsArray($fieldNameArr);
            }
        }
    }
}

if($_SESSION['ChangeFieldListCondition']!='')
  $FieldListSelectItem = $_SESSION['ChangeFieldListCondition'];
if($_SESSION['ChangeOperatorListCondition']!='')
  $OperatorListSelectItem = $_SESSION['ChangeOperatorListCondition'];
if($_SESSION['ChangeAndOrListCondition']!='')
  $SelectItem = $_SESSION['ChangeAndOrListCondition'];
if($_SESSION['ChangeValueListCondition']!='')
  $ValueListSelectItem = $_SESSION['ChangeValueListCondition'];

if($_SESSION['ChangeLeftParenthesesListCondition']!='')
  $LeftParenthesesSelectItem = $_SESSION['ChangeLeftParenthesesListCondition'];
if($_SESSION['ChangeRightParenthesesListCondition']!='')
  $RightParenthesesSelectItem = $_SESSION['ChangeRightParenthesesListCondition'];

if($_GET['QueryID'])
{
   $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='Change'");
   $AndOr = $QueryInfo['QueryString'];


   if($QueryInfo['FieldList']!='')
       $FieldListSelectItem = unserialize($QueryInfo['FieldList']);
   if($QueryInfo['OperatorList']!='')
       $OperatorListSelectItem = unserialize($QueryInfo['OperatorList']);
   if($QueryInfo['AndOrList']!='')
       $SelectItem =  unserialize($QueryInfo['AndOrList']);
   if($QueryInfo['ValueList']!='')
       $ValueListSelectItem = unserialize($QueryInfo['ValueList']);
   if($QueryInfo['LeftParentheses']!='')
       $LeftParenthesesSelectItem = unserialize($QueryInfo['LeftParentheses']);
   if($QueryInfo['RightParentheses']!='')
       $RightParenthesesSelectItem = unserialize($QueryInfo['RightParentheses']);
}

if($_REQUEST['UpdateQueryID'])
{
   $QueryStr = addslashes($_SESSION['ChangeQueryCondition']);
   $AndOrListCondition = serialize($_SESSION['ChangeAndOrListCondition']);
   $OperatorListCondition = serialize($_SESSION['ChangeOperatorListCondition']);
   $ValueListCondition = mysql_real_escape_string(serialize($_SESSION['ChangeValueListCondition']));
   $FieldListCondition = serialize($_SESSION['ChangeFieldListCondition']);
   $FieldsToShow = implode(",",array_keys(testSetCustomFields('Change')));
   $LeftParenthesesCondition = serialize($_SESSION['ChangeLeftParenthesesNameListCondition']);
   $RightParenthesesCondition = serialize($_SESSION['ChangeRightParenthesesNameListCondition']);

   $ProjectID = testGetCurrentProjectId();
   dbUpdateRow('TestUserQuery', 'QueryString', "'{$QueryStr}'", 'AndOrList', "'{$AndOrListCondition}'", 'OperatorList',
                    "'$OperatorListCondition'", 'ValueList',"'$ValueListCondition'", 'FieldList',"'$FieldListCondition'",
           'FieldsToShow', "'$FieldsToShow'", 'ProjectID', "'$ProjectID'",
           'LeftParentheses',"'$LeftParenthesesCondition'",
           'RightParentheses',"'$RightParenthesesCondition'",
           "QueryID={$_REQUEST['UpdateQueryID']}");
   jsGoTo('ChangeList.php',"parent.RightBottomFrame");

}

$QueryFieldCount = count($ValueListSelectItem);
if($QueryFieldCount == 0) $QueryFieldCount = 1;
$searchConditionTmp = getSearchCondition("Change");
for($I=0; $I<$QueryFieldCount; $I ++)
{
    $FieldListOnChange = ' onchange="setQueryForm('.$I.');"';
    $OperatorListOnChange = ' onchange="setQueryValue('.$I.');"';
    $FieldList[$I] = htmlSelect($_LANG['ChangeQueryField'], $FieldName . $I, $Mode, $FieldListSelectItem[$I], $Attrib.$FieldListOnChange);
    $OperatorList[$I] = htmlSelect($_LANG['Operators'], $OperatorName . $I, $Mode, $OperatorListSelectItem[$I], $Attrib.$OperatorListOnChange);
    $ValueList[$I] = '<input id="'.$ValueName.$I.'" name="' . $ValueName.$I .'" type="text" size="5" style="width:95%;"/>';
    $AndOrList[$I] = htmlSelect($_LANG['AndOr'], $AndOrName . $I, $Mode, $SelectItem[$I], $Attrib);
    $LeftParentheses[$I] = htmlSelect($_LANG['LeftParentheses'], 
            $LeftParenthesesName . $I, $Mode, $LeftParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $RightParentheses[$I] = htmlSelect($_LANG['RightParentheses'], 
            $RightParenthesesName . $I, $Mode, $RightParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $AddRemoveLink[$I] = '<a href="javascript:;" onclick="addSearchField('.$I.');return false;" ><img src="Image/add_search.gif"/></a>&nbsp;&nbsp;<a href="javascript:;" onclick="removeSearchField('.$I.');return false;" ><img src="Image/cancel_search.gif"/></a>';
}

$SimpleProjectList = array(''=>'')+testGetValidSimpleProjectList();

$AutoTextValue['ProjectNameText']=jsArray($SimpleProjectList);
$AutoTextValue['ProjectNameValue']=jsArray($SimpleProjectList);
$AutoTextValue['SeverityText']=jsArray($_LANG['ChangeSeveritys']);
$AutoTextValue['SeverityValue']=jsArray($_LANG['ChangeSeveritys'], 'Key');
$AutoTextValue['PriorityText']=jsArray($_LANG['ChangePriorities']);
$AutoTextValue['PriorityValue']=jsArray($_LANG['ChangePriorities'], 'Key');
$AutoTextValue['TypeText']=jsArray($_LANG['ChangeTypes']);
$AutoTextValue['TypeValue']=jsArray($_LANG['ChangeTypes'], 'Key');
$AutoTextValue['StatusText']=jsArray($_LANG['ChangeStatus']);
$AutoTextValue['StatusValue']=jsArray($_LANG['ChangeStatus'], 'Key');
$AutoTextValue['OSText']=jsArray($_LANG['ChangeOS']);
$AutoTextValue['OSValue']=jsArray($_LANG['ChangeOS'], 'Key');
$AutoTextValue['BrowserText']=jsArray($_LANG['ChangeBrowser']);
$AutoTextValue['BrowserValue']=jsArray($_LANG['ChangeBrowser'], 'Key');
$AutoTextValue['MachineText']=jsArray($_LANG['ChangeMachine']);
$AutoTextValue['MachineValue']=jsArray($_LANG['ChangeMachine'], 'Key');
$AutoTextValue['ResolutionText']=jsArray($_LANG['ChangeResolutions']);
$AutoTextValue['ResolutionValue']=jsArray($_LANG['ChangeResolutions'], 'Key');
$AutoTextValue['HowFoundText']=jsArray($_LANG['ChangeHowFound']);
$AutoTextValue['HowFoundValue']=jsArray($_LANG['ChangeHowFound'], 'Key');
$AutoTextValue['SubStatusText']=jsArray($_LANG['ChangeSubStatus']);
$AutoTextValue['SubStatusValue']=jsArray($_LANG['ChangeSubStatus'], 'Key');

$AutoTextValue['FieldType'] = jsArray($_CFG['FieldType']);
$AutoTextValue['FieldOperationTypeValue'] = jsArray($_CFG['FieldTypeOperation']);
$AutoTextValue['FieldOperationTypeText'] = jsArray($_LANG['FieldTypeOperationName']);


$ACUserList = array(''=>'') + $UserList;
$UserList = array(''=>'') + $UserList;
$AutoTextValue['ACUserText']=jsArray($ACUserList);
$AutoTextValue['ACUserValue']=jsArray($ACUserList, 'Key');
$AutoTextValue['UserText']=jsArray($UserList);
$AutoTextValue['UserValue']=jsArray($UserList, 'Key');

$TPL->assign('OperatorListSelectItem', $OperatorListSelectItem);
$TPL->assign('ValueListSelectItem', $ValueListSelectItem);
$TPL->assign('QueryTitle', $_SESSION['ChangeQueryTitle']);


$TPL->assign('AutoTextValue', $AutoTextValue);
$TPL->assign("FieldList", $FieldList);
$TPL->assign("OperatorList", $OperatorList);
$TPL->assign("ValueList", $ValueList);
$TPL->assign("AndOrList", $AndOrList);
$TPL->assign("FieldCount",$FieldCount);

$TPL->assign('UserLang',$_CFG['UserLang']);
$TPL->assign("QueryFieldCount",$QueryFieldCount);
$TPL->assign("LeftParentheses",$LeftParentheses);
$TPL->assign("RightParentheses",$RightParentheses);
$TPL->assign("AddRemoveLink",$AddRemoveLink);
$TPL->assign('searchConditionTmp',$searchConditionTmp);
$TPL->display('Search.tpl');
?>
