<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * search case form.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

$FieldName = 'Field';
$OperatorName = 'Operator';
$ValueName = 'Value';
$AndOrName = 'AndOr';
$LeftParenthesesName = 'LeftParenthesesName';
$RightParenthesesName = 'RightParenthesesName';

$FieldList = array();
$OperatorList = array();
$ValueList = array();
$AndOrList = array();
$FieldCount = $_CFG['QueryFieldNumber'];
$Attrib = 'class="FullSelect"';


$FieldListSelectItem = array(0=>'ProjectName',1=>'OpenedBy',2=>'ModulePath',3=>'AssignedTo',4=>'CaseID',5=>'CaseTitle');
$OperatorListSelectItem = array(0=>'=', 2=>'LIKE', 5=>'LIKE');

if($_REQUEST['reset'])
{
    $_SESSION['CaseFieldListCondition']='';
    $_SESSION['CaseOperatorListCondition']='';
    $_SESSION['CaseAndOrListCondition']='';
    $_SESSION['CaseValueListCondition']='';
    $_SESSION['CaseQueryCondition'] = '';
    $_SESSION['CaseQueryTitle'] = '';
    $_SESSION['CaseFieldsToShow'] = '';
    $_SESSION['CaseLeftParenthesesListCondition']='';
    $_SESSION['CaseRightParenthesesListCondition']='';
}

$UserList = testGetCurrentUserNameList('PreAppend');
$ProjectID = testGetCurrentProjectId();
if($ProjectID != null)
{
    $ProjectID = ($_SESSION['TestCurrentProjectID'] > 0) ? $_SESSION['TestCurrentProjectID'] : $_GET['ProjectID'];
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="Case"]/field');
        if($fields)
        {
            $fields = sysFieldXmlToArr($fields);
            $fieldArr = array();
            $fieldNameArr = array();
            foreach($fields as $field)
            {
                $key = $field['name'];
                $_LANG['CaseQueryField']["$key"] = $field['text'];
                if($field['status'] != 'active')
                {
                    continue;
                }
                if($field['type'] == 'select')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'mulit')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'user')
                {
                    $fieldArr["$key"]['value']   = array_keys($UserList);
                    $fieldArr["$key"]['key'] = array_values($UserList);
                }
                $fieldNameArr[] = $key;
            }
            if(!empty($fieldArr))
            {
                $AutoTextValue['CustomField'] = json_encode($fieldArr);
                $AutoTextValue['CustomFieldName'] = jsArray($fieldNameArr);
            }
        }
    }
}

if($_SESSION['CaseFieldListCondition']!='')
  $FieldListSelectItem = $_SESSION['CaseFieldListCondition'];
if($_SESSION['CaseOperatorListCondition']!='')
  $OperatorListSelectItem = $_SESSION['CaseOperatorListCondition'];
if($_SESSION['CaseAndOrListCondition']!='')
  $SelectItem = $_SESSION['CaseAndOrListCondition'];
if($_SESSION['CaseValueListCondition']!='')
  $ValueListSelectItem = $_SESSION['CaseValueListCondition'];
if($_SESSION['CaseLeftParenthesesListCondition']!='')
  $LeftParenthesesSelectItem = $_SESSION['CaseLeftParenthesesListCondition'];
if($_SESSION['CaseRightParenthesesListCondition']!='')
  $RightParenthesesSelectItem = $_SESSION['CaseRightParenthesesListCondition'];



if($_GET['QueryID'])
{
   $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='Case'");
   $AndOr = $QueryInfo['QueryString'];
 

  if($QueryInfo['FieldList']!='')
       $FieldListSelectItem = unserialize($QueryInfo['FieldList']);
   if($QueryInfo['OperatorList']!='')
       $OperatorListSelectItem = unserialize($QueryInfo['OperatorList']); 
   if($QueryInfo['AndOrList']!='')
       $SelectItem =  unserialize($QueryInfo['AndOrList']);  
   if($QueryInfo['ValueList']!='')
       $ValueListSelectItem = unserialize($QueryInfo['ValueList']);
   if($QueryInfo['LeftParentheses']!='')
       $LeftParenthesesSelectItem = unserialize($QueryInfo['LeftParentheses']);
   if($QueryInfo['RightParentheses']!='')
       $RightParenthesesSelectItem = unserialize($QueryInfo['RightParentheses']);
}

if($_REQUEST['UpdateQueryID'])
{
   $QueryStr = addslashes($_SESSION['CaseQueryCondition']);
   $AndOrListCondition = serialize($_SESSION['CaseAndOrListCondition']);
   $OperatorListCondition = serialize($_SESSION['CaseOperatorListCondition']);
   $ValueListCondition = mysql_real_escape_string(serialize($_SESSION['CaseValueListCondition']));
   $FieldListCondition = serialize($_SESSION['CaseFieldListCondition']);
   $FieldsToShow = implode(",",array_keys(testSetCustomFields('Case')));
   $LeftParenthesesCondition = serialize($_SESSION['CaseLeftParenthesesNameListCondition']);
   $RightParenthesesCondition = serialize($_SESSION['CaseRightParenthesesNameListCondition']);

   
   dbUpdateRow('TestUserQuery', 'QueryString', "'{$QueryStr}'", 'AndOrList', "'{$AndOrListCondition}'", 'OperatorList', 
                    "'$OperatorListCondition'", 'ValueList',"'$ValueListCondition'",
           'FieldList', "'$FieldListCondition'",
           'LeftParentheses',"'$LeftParenthesesCondition'",
           'RightParentheses',"'$RightParenthesesCondition'",
           'FieldsToShow', "'$FieldsToShow'","QueryID={$_REQUEST['UpdateQueryID']}");
   
   jsGoTo('CaseList.php',"parent.RightBottomFrame");
  
}

$QueryFieldCount = count($ValueListSelectItem);

if($QueryFieldCount == 0) $QueryFieldCount = 1;
$searchConditionTmp = getSearchCondition("Case");

for($I=0; $I<$QueryFieldCount; $I ++)
{
    $FieldListOnChange = ' onchange="setQueryForm('.$I.');"';
    $OperatorListOnChange = ' onchange="setQueryValue('.$I.');"';
    $FieldList[$I] = htmlSelect($_LANG['CaseQueryField'], $FieldName . $I, $Mode, $FieldListSelectItem[$I], $Attrib.$FieldListOnChange);
    $OperatorList[$I] = htmlSelect($_LANG['Operators'], $OperatorName . $I, $Mode, $OperatorListSelectItem[$I], $Attrib.$OperatorListOnChange);
    $ValueList[$I] = '<input id="'.$ValueName.$I.'" name="' . $ValueName.$I .'" type="text" size="5" style="width:95%;"/>';
    $AndOrList[$I] = htmlSelect($_LANG['AndOr'], $AndOrName . $I, $Mode, $SelectItem[$I], $Attrib);
    $LeftParentheses[$I] = htmlSelect($_LANG['LeftParentheses'],
            $LeftParenthesesName . $I, $Mode, $LeftParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $RightParentheses[$I] = htmlSelect($_LANG['RightParentheses'],
            $RightParenthesesName . $I, $Mode, $RightParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $AddRemoveLink[$I] = '<a href="javascript:;" onclick="addSearchField('.$I.');return false;" ><img src="Image/add_search.gif"/></a>&nbsp;&nbsp;<a href="javascript:;" onclick="removeSearchField('.$I.');return false;" ><img src="Image/cancel_search.gif"/></a>';
}

$AutoTextValue['PriorityText']=jsArray($_LANG['CasePriorities']);
$AutoTextValue['PriorityValue']=jsArray($_LANG['CasePriorities'], 'Key');
$AutoTextValue['TypeText']=jsArray($_LANG['CaseTypes']);
$AutoTextValue['TypeValue']=jsArray($_LANG['CaseTypes'], 'Key');
$AutoTextValue['StatusText']=jsArray($_LANG['CaseStatuses']);
$AutoTextValue['StatusValue']=jsArray($_LANG['CaseStatuses'], 'Key');

$AutoTextValue['MethodText']=jsArray($_LANG['CaseMethods']);
$AutoTextValue['MethodValue']=jsArray($_LANG['CaseMethods'], 'Key');
$AutoTextValue['PlanText']=jsArray($_LANG['CasePlans']);
$AutoTextValue['PlanValue']=jsArray($_LANG['CasePlans'], 'Key');
$AutoTextValue['MarkForDeletionText']=jsArray($_LANG['MarkForDeletions']);
$AutoTextValue['MarkForDeletionValue']=jsArray($_LANG['MarkForDeletions'], 'Key');
$AutoTextValue['ScriptStatusText']=jsArray($_LANG['ScriptStatuses']);
$AutoTextValue['ScriptStatusValue']=jsArray($_LANG['ScriptStatuses'], 'Key');


$SimpleProjectList = array(''=>'')+testGetValidSimpleProjectList();

$AutoTextValue['ProjectNameText']=jsArray($SimpleProjectList);
$AutoTextValue['ProjectNameValue']=jsArray($SimpleProjectList);

$ACUserList = array(''=>'', 'Active' => 'Active') + $UserList;
$UserList = array(''=>'') + $UserList;
$AutoTextValue['ACUserText']=jsArray($ACUserList);
$AutoTextValue['ACUserValue']=jsArray($ACUserList, 'Key');
$AutoTextValue['UserText']=jsArray($UserList);
$AutoTextValue['UserValue']=jsArray($UserList, 'Key');
$AutoTextValue['ScriptedByText']=jsArray($UserList);
$AutoTextValue['ScriptedByValue']=jsArray($UserList, 'Key');

$AutoTextValue['FieldType'] = jsArray($_CFG['FieldType']);
$AutoTextValue['FieldOperationTypeValue'] = jsArray($_CFG['FieldTypeOperation']);
$AutoTextValue['FieldOperationTypeText'] = jsArray($_LANG['FieldTypeOperationName']);

$TPL->assign('OperatorListSelectItem', $OperatorListSelectItem);
$TPL->assign('ValueListSelectItem', $ValueListSelectItem);

$TPL->assign('QueryTitle', $_REQUEST['QueryTitle']);
$TPL->assign('AutoTextValue', $AutoTextValue);
$TPL->assign("FieldList", $FieldList);
$TPL->assign("OperatorList", $OperatorList);
$TPL->assign("ValueList", $ValueList);
$TPL->assign("AndOrList", $AndOrList);
$TPL->assign("FieldCount",$FieldCount);

$TPL->assign('UserLang',$_CFG['UserLang']);
$TPL->assign("QueryFieldCount",$QueryFieldCount);
$TPL->assign("LeftParentheses",$LeftParentheses);
$TPL->assign("RightParentheses",$RightParentheses);
$TPL->assign("AddRemoveLink",$AddRemoveLink);
$TPL->assign('searchConditionTmp',$searchConditionTmp);
$TPL->display('Search.tpl');

?>
