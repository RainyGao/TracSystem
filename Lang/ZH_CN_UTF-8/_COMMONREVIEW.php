<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*-BASIC SETTING FOR Review RELATED FEATURES -*/
/* The fields of ReviewInfo table. */
$_LANG['ReviewFields'] = array
(
    'ReviewID'       => 'IR ID',
    'ProjectID'      => '产品ID',
    'ProjectName'    => '产品',
    'ModuleID'       => '模块ID',
    'ModulePath'     => '模块路径',
    'ReviewTitle'    => '标题',
    'ReviewKeyword'  => '关键字',
    'ReviewType'     => '评审类型',
    'ReviewStatus'   => '状态',
		'MeetingDate'		 => '评审会议时间',
		'StartTime'		 	 => '开始时间',
		'EndTime'		 		 => '结束时间',
		'MeetingLocation'=> '评审会议地点',
    'ReviewContent'  => '评审内容',
    'ReviewConclusion'=> '评审结论',
		'Author'         => '作者',
    'Moderator'      => '验证人',
    'Recorder'       => '记录人',
    'Inspectors'	 	 =>	'评审人员',
    'ChangeID'       => '对应的修改记录',
    'ReviewCommentID'=> '关联的评审意见',
    'MailTo'         => '抄送',
    'OpenedBy'       => '提交人',
    'OpenedDate'     => '提交日期',
    'ResolvedBy'     => '解决人',
    'ResolvedDate'   => '解决日期',
    'ClosedBy'       => '关闭人',
    'ClosedDate'     => '关闭日期',
    'LastEditedBy'   => '最后修改者',
    'LastEditedDate' => '最后修改日期',
    'ModifiedBy'     => '曾经修改者',
);

//Change Import Related Configuration
$_LANG['ReviewMustImportFields'] = array
(
    'ReviewID',        // 'CR ID',
    'ProjectName',  // '产品',
    'ModulePath',   // '模块路径',
    'ReviewTitle',     // '标题',
    'ReviewType',      // '类型',
    'ReviewStatus',    // '状态',
);

$_LANG['ReviewImportFileSuffixError'] = '导入文件格式错误,请选择XML文件！请确认导入文件包含如下字段：IR ID，产品，模块路径，标题，类型，状态。';
$_LANG['ReviewImportColumnNotNull'] = '不能为空, 操作失败!';
$_LANG['ReviewImportFinished'] = '导入完毕！一共导入{param0}个IR,其中{param1}个导入成功，{param2}个导入失败!({param3}个新增,{param4}个更新)';
$_LANG['ReviewImportNotes'] = ' <br><div  style="text-align:left;color:red"><b>（1）导入必须包含以下字段：</b></div>
                          	 IR ID，标题、产品、模块路径、类型，状态
                          	 <br><div  style="text-align:left;color:red"><b>（2）所有导入的字段都不能为空</b></div>';

/* The fields used to query in QueryReview.php.(Note: the field will be displayed in the order you defined here). */
$_LANG['ReviewQueryField'] = array
(
    'ReviewID'       => 'IR ID',
    'ProjectName'    => '产品',
    'ModulePath'     => '模块路径',
    'ReviewTitle'    => '标题',
    'ReviewType'     => '评审类型',
    'ReviewStatus'   => '状态',
		'MeetingDate'		 => '评审会议时间',
		'StartTime'		 	 => '开始时间',
		'EndTime'		 		 => '结束时间',
		'MeetingLocation'=> '评审会议地点',
    'ReviewContent'  => '评审内容',
    'ReviewConclusion'=> '评审结论',
		'Author'         => '作者',
    'Moderator'      => '验证人',
    'Recorder'       => '记录人',
    'Inspectors'	 	 =>	'评审人员',
    'ChangeID'       => '对应的修改记录',
    'ReviewCommentID'=> '关联的评审意见',
    'OpenedBy'       => '提交人',
    'OpenedDate'     => '提交日期',
    'ResolvedBy'     => '解决人',
    'ResolvedDate'   => '解决日期',
    'ClosedBy'       => '关闭人',
    'ClosedDate'     => '关闭日期',
    'LastEditedBy'   => '最后修改者',
    'LastEditedDate' => '最后修改日期',
    'ModifiedBy'     => '曾经修改者',
);

$_LANG["DefaultReviewQueryFields"] = array
(
    'ReviewID'       => 'IR ID',
    'ReviewTitle'    => '标题',
		'Author'         => '作者',
    'Moderator'      => '验证人',
    'Recorder'       => '记录人',
    'Inspectors'	 	 =>	'评审人员',
    'OpenedBy'       => '提交人',
    'ClosedBy'       => '关闭人',
    'OpenedDate'     => '提交日期',
    'ClosedDate'     => '关闭日期',
    'ReviewStatus'   => '状态',
);

/* Define the types. */
$_LANG['ReviewTypes'] = array
(
  ''             => '',
  'Software'     => '软件',
  'Hardware'     => '硬件',
  'Document'     => '文档',
  'Structure'    => '结构',
  'Others'       => '其他'
);

/* Define the status. */
$_LANG['ReviewStatus'] = array
(
    ''         => '',
    'Active'   => 'Open',
    'Resolved' => 'Performed',
    'Closed'   => 'Closed'
);

$_LANG['ReviewConclusions'] = array
(
    ''         			=> '',
    'Passed'   			=> '通过，无需修改',
    'PassedNeedFix'	=> '通过，需要做次要修改，由验证人验证',
    'Failed'   			=> '不通过，需要重新评审',
    'ToBeContinue'  => '评审未完成，需要重新安排时间',
);

/* Define the TimeList. */
$_LANG['TimeList'] = array
(
	'',
  '08:30',
  '08:45',
  '09:00',
  '09:15',
  '09:30',
  '09:45',  
  '10:00',
  '10:15',
  '10:30',
  '10:45',  
  '11:00',
  '11:15',
  '11:30',
  '11:45',  
  '12:00',
  '12:15',
  '12:30',
  '12:45',  
  '13:00',
  '13:15',
  '13:30',
  '13:45',  
  '14:00',
  '14:15',
  '14:30',
  '14:45',
  '15:00',
  '15:15',
  '15:30',
  '15:45',
  '16:00',
  '16:15',
  '16:30',
  '16:45',
  '17:00',
  '17:15',
  '17:30',
);

$_LANG['ReviewInvitation'] = "评审邀请";
$_LANG['PleaseSubmitReviewCommentHere'] = "请提交评审意见至如下评审记录";
$_LANG['ReviewContentReminder'] = "请填写文档或代码所在服务器地址!!!!";
?>
