<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*-BASIC SETTING FOR BUG RELATED FEATURES -*/
/* The fields of BugInfo table. */
$_LANG['CaseFields'] = array
(
  'CaseID'            => 'Case ID',
  'ProjectID'         => '项目 ID',
  'ProjectName'       => '产品',
  'ModuleID'          => '模块 ID',
  'ModulePath'        => '模块路径',
  'CaseIndex'         => '用例编号',
  'CaseTitle'         => '标题',
  'CasePriority'      => '优先级',
  'CaseType'          => '类型',
  'CaseStatus'        => '状态',
  'CaseMethod'        => '测试方法',
  'CasePlan'          => '测试计划',
  'CaseDescription'   => '测试目的',
  'CaseSetup'         => '前置条件',
  'CaseSteps'         => '测试步骤',
  'CaseCriteria'      => '期望输出',
  'DisplayOrder'      => '显示顺序',
  'OpenedBy'          => '创建人',
  'OpenedDate'        => '创建日期',
  'AssignedTo'        => '负责人',
  'AssignedDate'      => '指派日期',
  'LastEditedBy'      => '最后修改者',
  'LastEditedDate'    => '最后修改日期',
  'ModifiedBy'        => '曾经修改者',
  'ScriptStatus'      => '脚本状态',
  'ScriptedBy'        => '编写人',
  'ScriptedDate'      => '编写日期',
  'ScriptLocation'    => '脚本地址',
  'MailTo'            => '抄送给',
  'CaseKeyword'       => '关键词',
  'LinkID'            => '相关测试用例',
  'BugID'             => '相关问题记录',
  'ResultID'          => '关联的测试结果',
  'DisplayOrder'      => '显示顺序',
  'MarkForDeletion'   => '标记删除',
);

$_LANG['CaseMustImportFields'] = array
(
	'CaseID',
	'CaseIndex',
	'CaseTitle',
	'ProjectName',
	'ModulePath',
	'CaseDescription',
	'CaseSteps',
	'CaseCriteria',	
);
$_LANG['ImportFileSuffixError'] = '导入文件格式错误,请选择XML文件！请确认导入文件包含如下字段：Case ID，项目名称，模块路径，Case标题，步骤。';
$_LANG['ImportColumnNotNull'] = '不能为空, 操作失败!';
$_LANG['ImportFinished'] = '导入完毕！一共导入{param0}个CASE,其中{param1}个导入成功，{param2}个导入失败!';
$_LANG['ImportNotes'] = ' <br><div  style="text-align:left;color:red"><b>所有导入的字段都不能为空</b></div>
                          <br><div  style="text-align:left;"><b>导入必须包含以下字段：</b></div><br> 
                          1. Case ID:    为0则新增Case，非0则更新Case<br>
                          2. Case 标题、项目名称、模块路径、步骤<br>
                          <br> 
                          <div  style="text-align:left;"><b>以下字段可选择导入：</b></div><br> 
                          指派给、抄送给、优先级、Case 类型、测试方法<br>
                          编写日期、脚本地址、标记删除、关键词、编写者<br>
                          Case 状态、测试计划、脚本状态、显示顺序';
                          
/* The fields used to query in QueryCase.php.(Note: the field will be displayed in the order you defined here). */
$_LANG['CaseQueryField'] = array
(
  'CaseID'            => 'Case ID',
  'ProjectName'       => '产品',
  'ModulePath'        => '模块路径',
  'CaseIndex'         => '用例编号',
  'CaseTitle'         => '标题',
  'CasePriority'      => '优先级',
  'CaseType'          => '类型',
  'CaseStatus'        => '状态',
  'CaseMethod'        => '测试方法',
  'CasePlan'          => '测试计划',
  'CaseDescription'   => '测试目的',
  'CaseSetup'         => '前置条件',
  'CaseSteps'         => '测试步骤',
  'CaseCriteria'      => '期望输出',
  'DisplayOrder'      => '显示顺序',
  'OpenedBy'          => '创建人',
  'OpenedDate'        => '创建日期',
  'AssignedTo'        => '负责人',
  'AssignedDate'      => '指派日期',
  'LastEditedBy'      => '最后修改者',
  'LastEditedDate'    => '最后修改日期',
  'ModifiedBy'        => '曾经修改者',
  'ScriptStatus'      => '脚本状态',
  'ScriptedBy'        => '编写人',
  'ScriptedDate'      => '编写日期',
  'ScriptLocation'    => '脚本地址',
  'MailTo'            => '抄送给',
  'CaseKeyword'       => '关键词',
  'LinkID'            => '相关测试用例',
  'BugID'             => '相关问题记录',
  'ResultID'          => '关联的测试结果',
  'DisplayOrder'      => '显示顺序',
  'MarkForDeletion'   => '标记删除',
);


$_LANG['DefaultCaseQueryFields'] = array
(
   'CaseID'          => $_LANG['CaseFields']['CaseID'],
   'CasePriority'    => $_LANG['CaseFields']['CasePriority'],
   'CaseTitle'       => $_LANG['CaseFields']['CaseTitle'],
   'CaseStatus'      => $_LANG['CaseFields']['CaseStatus'],
   'OpenedBy'        => $_LANG['CaseFields']['OpenedBy'],
   'AssignedTo'      => $_LANG['CaseFields']['AssignedTo'],
   'LastEditedDate'  => $_LANG['CaseFields']['LastEditedDate'],
   'DisplayOrder'    => $_LANG['CaseFields']['DisplayOrder'],
);


/* Define the CasePriority */
$_LANG['CaseStatuses'] = array
(
 'Active'      => 'Active',
 'Blocked'     => 'Blocked',
 'Investigate' => 'Investigating',
 'Reviewed'    => 'Reviewed',
);

$_LANG['CasePriorities'] = array
(
 ''=>'',
 '1'=>'1',
 '2'=>'2',
 '3'=>'3',
 '4'=>'4',
);

$_LANG['CaseTypes'] = array
(
 ''              => '',
 'Functional'    => '功能',
 'Configuration' => '配置相关',
 'Setup'         => '安装部署',
 'Security'      => '安全相关',
 'Performance'   => '性能压力',
 'Other'         => '其他',
);

$_LANG['CaseMethods'] = array
(
 ''           => '',
 'Manual'     => '手动执行',
 'Automation' => '自动化脚本'
);

$_LANG['CasePlans'] = array
(
 ''           => '',
 'Function'   => '功能测试',
 'UnitTest'   => '单元测试',
 'BVT'        => '版本验证测试',
 'Intergrate' => '集成测试',
 'System'     => '系统测试',
 'Smoke'      => '冒烟测试',
 'Acceptance' => '验收测试',
);

$_LANG['ScriptStatuses'] = array
(
 ''              => '',
 'NotPlanned'    => '未计划',
 'Planning'      => '计划',
 'Blocked'       => '被阻止',
 'Coding'        => '正在编写',
 'CodingDone'    => '已完成',
 'Reviewed'      => '已评审',
);


$_LANG['MarkForDeletions'] = array
(
 '0' => '否',
 '1' => '是'
);

?>
