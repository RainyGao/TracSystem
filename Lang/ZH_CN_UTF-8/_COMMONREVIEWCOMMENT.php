<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*-BASIC SETTING FOR ReviewComment RELATED FEATURES -*/
/* The fields of ReviewCommentInfo table. */
$_LANG['ReviewCommentFields'] = array
(
    'ReviewCommentID'=> '评审意见ID',
    'ProjectID'      => '产品ID',
    'ProjectName'    => '产品',
    'ModuleID'       => '模块ID',
    'ModulePath'     => '模块路径',
    'ReviewCommentTitle'    => '描述',
    'ReviewCommentType'     => '类型',
    'ReviewCommentStatus'   => '状态',
    'ReviewCommentPos1'  		=> '位置1',
    'ReviewCommentPos2'  		=> '位置2',
    'ReviewCommentPos3'  		=> '位置3',
    'ReviewCommentContent'  => '详细描述',
    'Resolution'  					=> '解决方案',
    'ReviewCommentOwner'  	=> '意见人',
		'ReviewID'       => '对应的评审记录',
		'ReviewType'     => '评审类型',
    'OpenedBy'       => '提交人',
    'OpenedDate'     => '提交日期',
    'ResolvedBy'     => '解决人',
    'ResolvedDate'   => '解决日期',
    'ClosedBy'       => '关闭人',
    'ClosedDate'     => '关闭日期',
    'LastEditedBy'   => '最后修改者',
    'LastEditedDate' => '最后修改日期',
    'ModifiedBy'     => '曾经修改者',
);

/* The fields used to query in QueryReviewComment.php.(Note: the field will be displayed in the order you defined here). */
$_LANG['ReviewCommentQueryField'] = array
(
   'ReviewCommentID'       => $_LANG['ReviewCommentFields']['ReviewCommentID'],
   'ReviewID'        			 => $_LANG['ReviewCommentFields']['ReviewID'],
   'ReviewCommentTitle'    => $_LANG['ReviewCommentFields']['ReviewCommentTitle'],
   'ReviewCommentType'     => $_LANG['ReviewCommentFields']['ReviewCommentType'],
   'ReviewCommentStatus'   => $_LANG['ReviewCommentFields']['ReviewCommentStatus'],
   'ReviewCommentOwner'   => $_LANG['ReviewCommentFields']['ReviewCommentOwner'],
   'OpenedBy'       => $_LANG['ReviewCommentFields']['OpenedBy'],
   'ResolvedBy'     => $_LANG['ReviewCommentFields']['ResolvedBy'],
   'ClosedBy'       => $_LANG['ReviewCommentFields']['ClosedBy'],
   'OpenedDate'     => $_LANG['ReviewCommentFields']['OpenedDate'],
   'ResolvedDate'   => $_LANG['ReviewCommentFields']['ResolvedDate'],
   'ClosedDate'     => $_LANG['ReviewCommentFields']['ClosedDate'],
   'ProjectName'    => $_LANG['ReviewCommentFields']['ProjectName'],
   'ModulePath'     => $_LANG['ReviewCommentFields']['ModulePath'],   
   'LastEditedBy'   => $_LANG['ReviewCommentFields']['LastEditedBy'],
   'LastEditedDate' => $_LANG['ReviewCommentFields']['LastEditedDate'],   
   'ModifiedBy'     => $_LANG['ReviewCommentFields']['ModifiedBy'],
);

$_LANG["DefaultReviewCommentQueryFields"] = array
(
	 'ReviewCommentID'       => $_LANG['ReviewCommentFields']['ReviewCommentID'],
   'ReviewID'        			 => $_LANG['ReviewCommentFields']['ReviewID'],
   'ReviewCommentTitle'    => $_LANG['ReviewCommentFields']['ReviewCommentTitle'],
   'ReviewCommentType'     => $_LANG['ReviewCommentFields']['ReviewCommentType'],
   'ReviewCommentStatus'   => $_LANG['ReviewCommentFields']['ReviewCommentStatus'],
   'ReviewCommentOwner'   => $_LANG['ReviewCommentFields']['ReviewCommentOwner'],
);

/* Define the types. */
$_LANG['ReviewCommentTypes'] = array
(
  ''            => '',
  'Error'     	=> '错误',
  'Suggestion'  => '建议',
  'Others'      => '其他'
);

/* Define the status. */
$_LANG['ReviewCommentStatus'] = array
(
    ''         => '',
    'Active'   => 'Open',
    'Resolved' => 'Performed',
    'Closed'   => 'Closed'
);

$_LANG['ReviewCommentResolutions'] = array
(
    ''             		=> '',
    'FixedAsComment'  => '按评审意见修改',
    'WillNoFix' 			=> "不修改",
);

$_LANG['ReviewCommentPos1Names'] = array
(
    'Software'   		=> '文件名',
    'Document'			=> '章节',
    'Hardware'   		=> 'Sheet名',
    'Structure'  		=> 'Sheet名',
    'Others'  			=> '位置1',
);
$_LANG['ReviewCommentPos2Names'] = array
(
    'Software'   		=> '函数名',
    'Document'			=> '页码',
    'Hardware'   		=> '模块',
    'Structure'  		=> '模块',
    'Others'  			=> '位置2',
);
$_LANG['ReviewCommentPos3Names'] = array
(
    'Software'   		=> '行号',
    'Document'			=> '行号',
    'Hardware'   		=> '网络标号',
    'Structure'  		=> '结构件名',
    'Others'  			=> '位置3',
);

?>
