<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*-BASIC SETTING FOR Project RELATED FEATURES -*/
/* The fields of ProjectInfo table. */
$_LANG['ProjectFields'] = array
(
    'ProjectID'  				=> '产品ID',
    'ProjectName'      	=> '产品名称',
    'ProjectManagers'  	=> '管理员',
    'ProjectGroupIDs'  	=> '产品用户组',
    'ProjectDoc'     		=> 'ProjectDoc',
    'ProjectPlan'    		=> 'ProjectPlan',
    'DisplayOrder'  		=> '显示次序',
    'AddedBy'       		=> '添加者',
    'AddDate'     			=> '添加日期',
    'LastEditedBy'  		=> '修改者',
    'LastDate'    			=> '修改时间',
    'NotifyEmail'    		=> '通知邮箱',
);

//But Import Related Configuration
$_LANG['ProjectMustImportFields'] = array
(
    'ProjectID',
    'ProjectName',
);

$_LANG['ProjectImportFileSuffixError'] = '导入文件格式错误,请选择XML文件！请确认导入文件包含如下字段：SR ID，产品，模块路径，标题，问题类型，状态。';
$_LANG['ProjectImportColumnNotNull'] = '不能为空, 操作失败!';
$_LANG['ProjectImportFinished'] = '导入完毕！一共导入{param0}个SR,其中{param1}个导入成功，{param2}个导入失败!({param3}个新增,{param4}个更新)';
$_LANG['ProjectImportNotes'] = ' <br><div  style="text-align:left;color:red"><b>（1）导入必须包含以下字段：</b></div>
                          	 ProjectID，产品名称
                          	 <br><div  style="text-align:left;color:red"><b>（2）所有导入的字段都不能为空</b></div>';

?>
