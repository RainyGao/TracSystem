<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*-BASIC SETTING FOR CHANGE RELATED FEATURES -*/
/* The fields of ChangeInfo table. */
$_LANG['ChangeFields'] = array
(
    'ChangeID'       => 'CR ID',
    'ProjectID'      => '产品ID',
    'ProjectName'    => '产品',
    'ModuleID'       => '模块ID',
    'ModulePath'     => '模块路径',
    'ChangeTitle'    => '标题',
    'ChangeKeyword'  => '关键词',
    'ChangeType'     => '修改类型',
    'ChangeStatus'   => '状态',
    'OpenedBuild'    => '问题版本',
    'ResolvedBuild'  => '解决版本',
    'BugID'          => '对应的问题记录',
    'ReviewID'       => '关联的评审记录',
    'RootCause'      => '根本原因',
    'Resolution'     => '解决方案',
    'MailTo'         => '抄送',
    'OpenedBy'       => '提交人',
    'OpenedDate'     => '提交日期',
    'AssignedTo'     => '负责人',
    'AssignedDate'   => '分配日期',
    'ResolvedBy'     => '解决人',
    'ResolvedDate'   => '解决日期',
    'ClosedBy'       => '关闭人',
    'ClosedDate'     => '关闭日期',
    'LastEditedBy'   => '最后修改者',
    'LastEditedDate' => '最后修改日期',
    'ModifiedBy'     => '曾经修改者',
    'OpenedByDepartment' 		=> '提交部门',	
    'AssignedToDepartment' 	=> '负责部门',	
);

//Change Import Related Configuration
$_LANG['ChangeMustImportFields'] = array
(
    'ChangeID',        // 'CR ID',
    'ProjectName',  // '产品',
    'ModulePath',   // '模块路径',
    'ChangeTitle',     // '标题',
    'ChangeType',      // '类型',
    'ChangeStatus',    // '状态',
);

$_LANG['ChangeImportFileSuffixError'] = '导入文件格式错误,请选择XML文件！请确认导入文件包含如下字段：CR ID，产品，模块路径，标题，类型，状态。';
$_LANG['ChangeImportColumnNotNull'] = '不能为空, 操作失败!';
$_LANG['ChangeImportFinished'] = '导入完毕！一共导入{param0}个CR,其中{param1}个导入成功，{param2}个导入失败!({param3}个新增,{param4}个更新)';
$_LANG['ChangeImportNotes'] = ' <br><div  style="text-align:left;color:red"><b>（1）导入必须包含以下字段：</b></div>
                          	 CR ID，标题、产品、模块路径、类型，状态
                          	 <br><div  style="text-align:left;color:red"><b>（2）所有导入的字段都不能为空</b></div>';

/* The fields used to query in QueryChange.php.(Note: the field will be displayed in the order you defined here). */
$_LANG['ChangeQueryField'] = array
(
    'ChangeID'       => 'CR ID',
    'ProjectName'    => '产品',
    'ModulePath'     => '模块路径',
    'ChangeTitle'    => '标题',
    'ChangeKeyword'  => '关键词',
    'ChangeType'     => '修改类型',
    'ChangeStatus'   => '状态',
    'OpenedBuild'    => '问题版本',
    'ResolvedBuild'  => '解决版本',
    'BugID'          => '对应的问题记录',
    'ReviewID'       => '关联的评审记录',
    'RootCause'      => '根本原因',
    'Resolution'     => '解决方案',
    'MailTo'         => '抄送',
    'OpenedBy'       => '提交人',
    'OpenedDate'     => '提交日期',
    'AssignedTo'     => '负责人',
    'AssignedDate'   => '分配日期',
    'ResolvedBy'     => '解决人',
    'ResolvedDate'   => '解决日期',
    'ClosedBy'       => '关闭人',
    'ClosedDate'     => '关闭日期',
    'LastEditedBy'   => '最后修改者',
    'LastEditedDate' => '最后修改日期',
    'ModifiedBy'     => '曾经修改者',
    'OpenedByDepartment' 		=> '提交部门',	
    'AssignedToDepartment' 	=> '负责部门',	
);

$_LANG["DefaultChangeQueryFields"] = array
(
    'ChangeID'       => 'CR ID',
    'ProjectName'    => '产品',
    'ModulePath'     => '模块路径',
    'ChangeTitle'    => '标题',
    'ChangeType'     => '修改类型',
    'OpenedBy'       => '提交人',
    'AssignedTo'     => '负责人',
    'ClosedBy'       => '关闭人',
    'OpenedDate'     => '提交日期',
    'ClosedDate'     => '关闭日期',
    'ChangeStatus'   => '状态',
);

/* Define the types. */
$_LANG['ChangeTypes'] = array
(
  ''             => '',
  'Software'     => '软件',
  'Hardware'     => '硬件',
  'Document'     => '文档',
  'Structure'    => '结构',
  'Others'       => '其他'
);

/* Define the status. */
$_LANG['ChangeStatus'] = array
(
    ''         => '',
    'Active'   => 'Open',
    'Resolved' => 'Performed',
    'Closed'   => 'Closed'
);

/*-BASIC SETTING FOR Review RELATED FEATURES -*/
/* The fields of ReviewInfo table. */
$_LANG['ReviewFields'] = array
(
    'ReviewID'       => 'IR ID',
    'ProjectID'      => '产品ID',
    'ProjectName'    => '产品',
    'ModuleID'       => '模块ID',
    'ModulePath'     => '模块路径',
    'ReviewTitle'    => '标题',
    'ReviewType'     => '评审类型',
    'ReviewStatus'   => '状态',
    'MailTo'         => '抄送',
    'OpenedBy'       => '提交人',
    'OpenedDate'     => '提交日期',
    'OpenedBuild'    => '问题版本',
    'AssignedTo'     => '负责人',
    'AssignedDate'   => '分配日期',
    'ResolvedBy'     => '解决人',
    'ResolvedBuild'  => '解决版本',
    'ResolvedDate'   => '解决日期',
    'ClosedBy'       => '关闭人',
    'ClosedDate'     => '关闭日期',
    'LastEditedBy'   => '最后修改者',
    'LastEditedDate' => '最后修改日期',
    'ModifiedBy'     => '曾经修改者',
    'ChangeID'          => '相关修改记录',
);

?>
