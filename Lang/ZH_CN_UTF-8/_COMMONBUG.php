<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*-BASIC SETTING FOR BUG RELATED FEATURES -*/
/* The fields of BugInfo table. */
$_LANG['BugFields'] = array
(
    'BugID'          => 'SR ID',
    'ProjectID'      => '产品ID',
    'ProjectName'    => '产品',
    'ModuleID'       => '模块ID',
    'ModulePath'     => '模块路径',
    'OpenedBuild'    => '问题版本',
    'ResolvedBuild'  => '解决版本',
    'BugTitle'       => '标题',
    'BugKeyword'     => '关键词',
    'BugType'        => '问题类型',
    'BugSeverity'    => '严重程度',
    'BugPriority'    => '优先级',
    'BugOS'          => '操作系统',
    'BugBrowser'     => '浏览器',
    'BugMachine'     => '机器配置',
    'HowFound'       => '如何发现',
    'BugContent'     => '详细描述',
    'ReproSteps'     => '复现步骤',
    'ReproRate'     => 	'复现率',
    'RootCause'     => 	'问题根本原因',
    'BugStatus'      => '状态',
    'BugSubStatus'   => '处理状态',
    'Resolution'     => '解决方案',   
    'ResolutionDetail'=> '解决方案描述',   
    'IsDuplicated'   => '与其他问题记录重复',
    'DuplicateID'    => '重复的问题记录',
		'ResultID'       => '对应的测试结果',
    'ChangeID'			 =>	'关联的修改记录',
    'LinkID'         => '相关问题记录',
    'CaseID'         => '相关测试用例',
    'MailTo'         => '抄送',
    'OpenedBy'       => '提交人',
    'OpenedDate'     => '提交日期',
    'AssignedTo'     => '负责人',
    'AssignedDate'   => '分配日期',
    'ResolvedBy'     => '解决人',
    'ResolvedDate'   => '解决日期',
    'ClosedBy'       => '关闭人',
    'ClosedDate'     => '关闭日期',
    'LastEditedBy'   => '最后修改者',
    'LastEditedDate' => '最后修改日期',
    'ModifiedBy'     => '曾经修改者',
    'OpenedByDepartment' 		=> '提交部门',	
    'AssignedToDepartment' 	=> '负责部门',	
);

//But Import Related Configuration
$_LANG['BugMustImportFields'] = array
(
    'BugID',        // 'SR ID',
    'ProjectName',  // '产品',
    'ModulePath',   // '模块路径',
    'BugTitle',     // '标题',
    'BugType',      // '问题类型',
    'BugStatus',    // '状态',
);

$_LANG['BugImportFileSuffixError'] = '导入文件格式错误,请选择XML文件！请确认导入文件包含如下字段：SR ID，产品，模块路径，标题，问题类型，状态。';
$_LANG['BugImportColumnNotNull'] = '不能为空, 操作失败!';
$_LANG['BugImportFinished'] = '导入完毕！一共导入{param0}个SR,其中{param1}个导入成功，{param2}个导入失败!({param3}个新增,{param4}个更新)';
$_LANG['BugImportNotes'] = ' <br><div  style="text-align:left;color:red"><b>（1）导入必须包含以下字段：</b></div>
                          	 SR ID，标题、产品、模块路径、问题类型，状态
                          	 <br><div  style="text-align:left;color:red"><b>（2）所有导入的字段都不能为空</b></div>';

/* The fields used to query in QueryBug.php.(Note: the field will be displayed in the order you defined here). */
$_LANG['BugQueryField'] = array
(
    'BugID'          => 'SR ID',
    'ProjectName'    => '产品',
    'ModulePath'     => '模块路径',
    'OpenedBuild'    => '问题版本',
    'ResolvedBuild'  => '解决版本',
    'BugTitle'       => '标题',
    'BugKeyword'     => '关键词',
    'BugStatus'      => '状态',
    'BugSubStatus'   => '处理状态',
    'BugType'        => '问题类型',
    'BugSeverity'    => '严重程度',
    'BugPriority'    => '优先级',
    'HowFound'       => '如何发现',
    'BugContent'     => '详细描述',
    'ReproSteps'     => '复现步骤',
    'ReproRate'     => 	'复现率',
    'RootCause'     => 	'根本原因',
    'Resolution'     => '解决方案',   
    'ResolutionDetail'=> '解决方案描述',   
    'DuplicateID'    => '重复的问题记录',
		'ResultID'       => '对应的测试结果',
    'ChangeID'			 =>	'关联的修改记录',
    'LinkID'         => '相关问题记录',
    'CaseID'         => '相关测试用例',
    'MailTo'         => '抄送',
    'OpenedBy'       => '提交人',
    'OpenedDate'     => '提交日期',
    'AssignedTo'     => '负责人',
    'AssignedDate'   => '分配日期',
    'ResolvedBy'     => '解决人',
    'ResolvedDate'   => '解决日期',
    'ClosedBy'       => '关闭人',
    'ClosedDate'     => '关闭日期',
    'LastEditedBy'   => '最后修改者',
    'LastEditedDate' => '最后修改日期',
    'ModifiedBy'     => '曾经修改者',
    'OpenedByDepartment' 		=> '提交部门',	
    'AssignedToDepartment' 	=> '负责部门',	
);

$_LANG["DefaultBugQueryFields"] = array
(
   'BugID'        => $_LANG["BugFields"]["BugID"],
   'BugSeverity'  => $_LANG["BugFields"]["BugSeverity"],
   'BugPriority'  => $_LANG["BugFields"]["BugPriority"],
   'ProjectName'    => $_LANG['BugFields']['ProjectName'],
   'BugTitle'     => $_LANG["BugFields"]["BugTitle"],
   'BugStatus'    => $_LANG['BugFields']['BugStatus'],
   'OpenedBy'     => $_LANG["BugFields"]["OpenedBy"],
   'AssignedTo'   => $_LANG["BugFields"]["AssignedTo"],
   'OpenedDate'   => $_LANG['BugFields']['OpenedDate'],
   'ClosedDate'   => $_LANG['BugFields']['ClosedDate'],
);

/* Bug Severity. */
$_LANG['BugSeveritys'] = array
(
   '' => '',
   1 => '致命',
   2 => '严重',
   3 => '一般',
   4 => '轻微',
);

/* Bug Priority. */
$_LANG['BugPriorities'] = array
(
   '' => '',
   1 => '紧急',
   2 => '高',
   3 => '中',
   4 => '低',
);

/* Define the OS list. */
$_LANG['BugOS'] = array
(
    ''        => '',
    'All'     => '全部',
    'Win7'    => 'Windows 7',
    'WinVista'=> 'Windows Vista',
    'WinXP'   => 'Windows XP',
    'Win2000' => 'Windows 2000',
    'Linux'   => 'Linux',
    'FreeBSD' => 'FreeBSD',
    'Unix'    => 'Unix',
    'MacOS'   => 'Mac OS',
    'Others'  => '其他',
);

/* Define the Browser list. */
$_LANG['BugBrowser'] = array
(
 ''           => '',
 'All'        => '全部',
 'IE8'        => 'IE 8.0',
 'IE7'        => 'IE 7.0',
 'IE6'        => 'IE 6.0',
 'FireFox3.0' => 'FireFox 3.0',
 'FireFox2.0' => 'FireFox 2.0',
 'Chrome'     => 'Chrome',
 'Safari'     => 'Safari',
 'Opera'      => 'Opera',
 'Others'     => '其他',
);

/* Define the types. */
$_LANG['BugTypes'] = array
(
  ''            => '',
  'Defect'			=> '产品缺陷',
  'NewFeature'  => '新需求',
  'Enhancement'	=> '性能改进',
  'Software'    => '软件错误',
  'Hardware' 		=> '硬件错误',
  'Structure'		=> '工艺结构错误',  
  'Document' 		=> '文档错误',
  'Config'   		=> '配置错误',
  'DesignChange'=> '需求变更',
  'Suggestion'	=> '改进建议',
  'Others'      => '其他'
);

/* Define the status. */
$_LANG['BugStatus'] = array
(
    ''         => '',
    'Active'   => 'Open',
    'Resolved' => 'Performed',
		'Rejected' => 'Rejected',		
    'Closed'   => 'Closed',
);

/* Define the substatus. */
$_LANG['BugSubStatus'] = array
(
    ''              => '',
    'Hold'          => '未处理',
    'LocalFix'      => '处理中',
    'CheckedIn'     => '已处理',
    'CannotRegress' => '无法重现'
);

/* Define the Resolution. */
$_LANG['BugResolutions'] = array
(
    ''             	=> '',
    'FixCode'    	 	=> '修改代码',
    'FixDocument'  	=> '修改文档',
    'FixSchematic'  => '修改原理图',
    'FixPCB'  			=> '修改PCB',
    'FixStrueture' 	=> '修改工艺结构设计',
    'FixConfig' 		=> '修改配置',
    'ReDesign'    	=> '重新设计',
    'NewDesign'    	=> '全新设计',
    'Reject'    		=> '拒绝',
);

/* Define the HowFound. */
$_LANG['BugHowFound'] = array
(
    ''             => '',
    'FuncTest'     => '功能测试',
    'UnitTest'     => '单元测试',
    'BVT'          => '版本验证测试',
    'Integrate'    => '集成测试',
    'System'       => '系统测试',
    'SpecReview'   => '需求检查',
    'CodeReview'   => '代码检查',
    'PostRTW'      => '上线测试',
    'Customer'     => '客户反馈',
    'Other'        => '其他',
);
?>
