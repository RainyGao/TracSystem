<?php
/**
 * UserFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*-BASIC SETTING FOR User RELATED FEATURES -*/
/* The fields of UserInfo table. */
$_LANG['UserFields'] = array
(
    'UserID'  			=> 'UserID',
    'UserName'      => '用户名',
    'UserPassword'  => '密码',
    'RealName'      => '真实姓名',
    'Email'     		=> '电子邮件',
    'Wangwang'    	=> 'Wangwang',
    'NoticeFlag'  	=> '通知标志',
    'AddedBy'       => '添加者',
    'AddDate'     	=> '添加日期',
    'LastEditedBy'  => '修改者',
    'LastDate'    	=> '修改时间',
    'AuthMode'    	=> 'AuthMode',
);

//But Import Related Configuration
$_LANG['UserMustImportFields'] = array
(
    'UserID',
    'UserName',
    'Email',
);

$_LANG['UserImportFileSuffixError'] = '导入文件格式错误,请选择XML文件！请确认导入文件包含如下字段：SR ID，产品，模块路径，标题，问题类型，状态。';
$_LANG['UserImportColumnNotNull'] = '不能为空, 操作失败!';
$_LANG['UserImportFinished'] = '导入完毕！一共导入{param0}个SR,其中{param1}个导入成功，{param2}个导入失败!({param3}个新增,{param4}个更新)';
$_LANG['UserImportNotes'] = ' <br><div  style="text-align:left;color:red"><b>（1）导入必须包含以下字段：</b></div>
                          	 UserID，用户名、电子邮件
                          	 <br><div  style="text-align:left;color:red"><b>（2）所有导入的字段都不能为空</b></div>';

?>
