<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
$_LANG['Charset'] = 'UTF-8';

/* The error message to display when a user not logged in. */
$_LANG['Message'] =  array
(
    'NotLogin'     => '您还没有登录到系统。\\n请首先登录 BugFree!',
    'ErrorLogin'   => '您的用户名或密码不正确。请重新输入!',
    'NoPriv'       => "您现在没有访问任何项目的权限，<br />请联系系统管理员为您分配权限。",
    "SucceedLogin" => "登录成功"
);

$_LANG['SysErrorMsg'] = '请求的URL没有找到或者无权限访问。';
$_LANG['SearchDeleteErrorMsg'] = '唯一一条查询条件，不能删除!';
$_LANG['SearchAddErrorMsg'] = '达到最大查询数目，不能再添加!';

/* Settting for DB */
$_LANG['DBErrorConnection']    = '数据库连接失败！';
$_LANG['DBSureExists']         = '请确认是否存在数据库';
$_LANG['DBSureUserNameAndPwd'] = '请确认数据库的用户名和密码是否正确。';
$_LANG['DBSureHost']           = '请确认数据库的服务器地址是否正确。';
$_LANG['DBSureRunning']        = '请确认数据库是否在运行。';

/* BugFree team info. Don't change. */
$_LANG['AboutBF']    = '关于';
$_LANG['BFHome']     = '官方网站';
$_LANG['BFHomePage'] = 'http://www.bugfree.org.cn';
$_LANG['ProductName']= 'TracSystem';
$_LANG['Version']    = '1.0.1';

/* Define css style list. */
$_LANG['StyleList'] = array
(
  'Default' => '默认风格',
);

$_LANG['DefaultStyle'] = 'Default';

/*TracSystem and TestSystem Name Definiton*/
$_LANG['TracSystem']		= "问题跟踪系统";
$_LANG['TestSystem']		=	"测试系统";

/*Definition of Tag's Name for TopNav of Main Page*/
$_LANG['TopNavBug']     = '问题记录';
$_LANG['TopNavChange']  = '修改记录';
$_LANG['TopNavReview']  = '评审记录';
$_LANG['TopNavReviewComment']  = '评审意见';
$_LANG['TopNavPlan']    = '测试计划';
$_LANG['TopNavCase']    = '测试用例';
$_LANG['TopNavResult']  = '测试结果';

$_LANG['UCAssignedToMe'] = '指派给我';
$_LANG['UCOpenedByMe']   = '由我创建';
$_LANG['UCMyQuery']      = '我的查询';

/* Define the length of the title to show in result list window and user control window. */
$_LANG['QueryTitleLength']   = 75;    // Used in QueryBug.php
$_LANG['ControlTitleLength'] = 36;    // Used in UserControl.php

/* Define overlib's value */
$_LANG['OLWIDTH']   = '300';
$_LANG['OLBGCOLOR'] = '#75736E';
$_LANG['OLFGCOLOR'] = '#F6F6F6';

/* And Or list. */
$_LANG['AndOr'] = array('And' => '并且', 'Or'  => '或者');
$_LANG['LeftParentheses'] = array(' ' => ' ','('  => '(',);
$_LANG['RightParentheses'] = array(' ' => ' ',')'  => ')',);

$_LANG['FieldTypeOperationName'] = array
(
    'Project'=> '等于,不等于,包含,不包含',
    'Number' => '等于,不等于,大于,小于,大于等于,小于等于,包含',
    'Date'   => '等于,不等于,大于,小于,大于等于,小于等于,',
    'String' => '包含,不包含',
    'Group'	 => '等于,不等于,包含',
    'People' => '等于,不等于,包含',
    'MutilPeople' => '等于',
    'Option' => '等于,不等于',
    'Path'   => '包含,不包含,在某路径下,'
);

$_LANG['QueryCondition'] = '查询条件';
$_LANG['QueryTitle']     = '查询标题';
$_LANG['PostQuery']      = '提交查询';
$_LANG['SaveQuery']      = '保存查询';
$_LANG['ResetQuery']     = '重置查询';
$_LANG['NoQueryTitle']   = '没有输入查询标题';
$_LANG['DuplicateQueryTitle'] = '确定修改查询条件';
$_LANG['AddBuild'] = '新增 Build';

$_LANG['ConfirmDeleteTestFile']  = '确认删除该附件吗？';
$_LANG['ErrorExceedSize']        = '超过了附件的限制尺寸';
$_LANG['ErrorDangerousFileType'] = '是危险的文件类型';

/* Definiton For Bug, Change, Review, Case and Result */
/*BugID Prefix String*/
$_LANG['BugIDPrefix'] = 'SR';
$_LANG['ChangeIDPrefix'] = 'CR';
$_LANG['ReviewIDPrefix'] = 'IR';
$_LANG['ReviewCommentIDPrefix'] = 'Comment';
require('_COMMONBUG.php');
require('_COMMONCHANGE.php');
require('_COMMONREVIEW.php');
require('_COMMONREVIEWCOMMENT.php');
require('_COMMONPLAN.php');
require('_COMMONCASE.php');
require('_COMMONRESULT.php');
require('_COMMONUSER.php');
require('_COMMONPROJECT.php');

/* nomal value. */
$_LANG['Confirm'] = '确定';
$_LANG['Cancle']  = '取消';
$_LANG['NotNull'] = '不能为空';
$_LANG['MaxLength'] = '最大长度是';

$_LANG['Edit']    = '编辑';
$_LANG['Save']    = '保存';
$_LANG['Reset']   = '重置';
$_LANG['Delete']  = '删除';

$_LANG['AddTime']  = '添加时间';
$_LANG['AddedBy']  = '添加者';
$_LANG['LastTime'] = '修改时间';
$_LANG['LastModifiedBy'] = '修改者';

$_LANG['SendNotifyEmail'] = '发送邮件通知';
$_LANG['SendMeetingRequest'] = '发送会议邀请';
/* Define the Case Action  */
$_LANG['OpenCase'] = '新建 测试用例';

/* Define the language about admin management */
$_LANG['ManageProject']    = '产品管理';
$_LANG['ManageModule']     = '模块管理';
$_LANG['ManageBugModule']  = '跟踪系统模块';
$_LANG['ManageCaseModule'] = '测试系统模块';
$_LANG['ManageBasicConfig']= '基本设置';
$_LANG['ManageField']      = '字段管理';
$_LANG['AddField']         = '增加新字段';
$_LANG['UpdateField']      = '修改';
$_LANG['ActiveField']      = '激活';
$_LANG['DisableField']     = '禁用';
$_LANG['ManageUser']       = '用户管理';
$_LANG['ManageGroup']      = '用户组管理';
$_LANG['UserLog']          = '用户日志';

/* Field */
$_LANG['FieldText']        = '字段提示文字';
$_LANG['FieldName']        = '数据字段名';
$_LANG['FieldType']        = '字段类型';
$_LANG['FieldModify']      = '维护';
$_LANG['FieldValue']       = '字段默认值';
$_LANG['FieldStatus']      = '字段状态';
$_LANG['FieldOption']      = '字段选项';
$_LANG['FieldActive']      = '正常';
$_LANG['FieldDisable']     = '禁用';
$_LANG['FieldNull']        = '可选';
$_LANG['FieldNotNull']     = '必填';
$_LANG['FieldTextTip']     = '发布内容时显示的提示文字';
$_LANG['FieldNameTip']     = '只能用英文字母或数字，数据表的真实字段名。<br/>注意：该字段不可修改。';
$_LANG['FieldTypeTip']     = '';
$_LANG['FieldValueTip']    = '如果定义字段类型类型为下拉框时，此处填写被选择的项目(用“,”分开，如“,是,否”)。';
$_LANG['FieldStatusTip']   = '禁用状态字段不会被显示';
$_LANG['FieldOptionTip']   = '必填字段不能为空，可选字段可以为空';
$_LANG['Field']            = array(
    'Text' => '文本框',
    'TextArea' => '文本域',
    'CheckBox' => '多选框',
    'Radio' => '单选框',
    'MulitSelect' => '多选列表',
    'Select' => '下拉框',
    'Date' => '日期',
    'User' => '用户列表',
);
$_LANG['CustomedField']       = '自定义信息';
$_LANG['SucceedAddField']     = '增加字段成功。';
$_LANG['SucceedUpdateField']  = '修改字段成功。';
$_LANG['BackToProject']       = '返回该项目';
$_LANG['NoProject']           = '不存在该项目';
$_LANG['NoType']              = '不存在该类型';
$_LANG['NoFieldText']         = '没有输入字段提示文字';
$_LANG['NoFieldName']         = '没有输入数据字段名';
$_LANG['FieldNameNotValid']   = '数据字段名只能包含字母,并且长度范围为4-16个字节';
$_LANG['FieldTextNotValid']   = '字段提示文字长度范围为4-32个字节,汉字占3个字节';
$_LANG['FieldNameRepeat']     = '数据字段名已存在';
$_LANG['FieldNameRepeatWithBasic']     = '数据字段名不能与基本字段名相同';
$_LANG['MulitSelectUniqueValue']       = '多选列表默认值不能重复';

/* Project */
$_LANG['ProjectList']      = '项目列表';
$_LANG['BackToProjectList']= '返回项目列表';
$_LANG['ProjectID']        = '项目 ID';
$_LANG['ProjectName']      = '项目名称';
$_LANG['AllProject']       = '所有项目';
$_LANG['AddProject']       = '添加项目';
$_LANG['GoOnAddPro']       = '继续添加';
$_LANG['SaveProject']      = '保存项目';
$_LANG['EditProject']      = '编辑项目';
$_LANG['DropProject']      = '禁用';
$_LANG['ConfirmDropProject'] = '确认禁用该项目？';
$_LANG['ActiveProject']    = '激活';
$_LANG['ConfirmActiveProject'] = '确认激活该项目？';
$_LANG['ProjectExist']     = '项目名称已被占用,请选择其它的名称';
$_LANG['NoProjectName']    = '没有输入项目名称';
$_LANG['NoEditPro']        = '没有任何改动';
$_LANG['SucceedEditPro']   = '编辑项目成功。' . htmlLink($_LANG['BackToProjectList'], 'AdminProjectList.php');
$_LANG['SucceedAddPro']    = '添加成功。';
$_LANG['ProjectGroup']     = '项目用户组';
$_LANG['ProjectManager']   = '项目管理员';
$_LANG['MergeProject']     = '合并项目';
$_LANG['MergeTo']          = '将项目合并至';
$_LANG['MergeName']        = '合并后的名称';
$_LANG['MergeSelfDenied']  = '项目不能与自己合并';
$_LANG['SucceedMergeProject'] = '合并项目成功';

/* Module */
$_LANG['ModuleList']       = '模块列表';
$_LANG['AddModule']        = '增加模块';
$_LANG['EditModule']       = '编辑模块';
$_LANG['DeleteModule']     = '删除模块';
$_LANG['ConfirmDeleteModule'] = '确认删除该模块？';
$_LANG['ModuleName']       = '模块名称';
$_LANG['OwnProject']       = '所属项目';
$_LANG['ParentModule']     = '父模块';
$_LANG['DisplayOrder']     = '显示顺序';
$_LANG['DisplayOrderNote'] = '请输入0~255之间的一个整数';
$_LANG['ModuleOwner']      = '模块负责人';
$_LANG['SaveModule']       = '保存模块';
$_LANG['NoModuleName']     = '没有输入模块名称';
$_LANG['ModuleExist']      = '该模块名称已经被占用，请选择其他的名称';
$_LANG['ModuleNotBeRecursive'] = '模块不能成为自己的子模块或自己子模块的子模块';
$_LANG['ModuleHasChildModule'] = '该模块下有子模块，不能删除';
$_LANG['SucceedAddModule'] = '添加模块成功';
$_LANG['SucceedEditModule'] = '编辑模块成功';
$_LANG['SucceedDeleteModule'] = '删除模块成功';


/* User */
$_LANG['UserList']         = '用户列表';
$_LANG['BackToUserList']   = '返回用户列表';
$_LANG['UserID']           = '用户 ID';
$_LANG['UserName']         = '用户名';
$_LANG['RealName']         = '真实姓名';
$_LANG['Email']            = 'Email';
$_LANG['RawUserPassword']  = '原密码';
$_LANG['UserPassword']     = '密码';
$_LANG['RepeatUserPassword'] = '重复密码';
$_LANG['ReceiveEmail']     = '是否接受邮件通知';
$_LANG['RealNameNote']     = '请输入指派用户显示的名称';
$_LANG['RawUserPasswordNote'] = '请输入用户首次登陆的默认密码';
$_LANG['EditUserPasswordNote'] = '密码为空，保持不变';
$_LANG['EmailNote']        = '请输入邮件地址用于接受邮件通知';
$_LANG['WrongRawUserPassword'] = '原始密码错误';
$_LANG['PasswordNotEqual'] = '两次输入的密码不一致';
$_LANG['AddUser']          = '添加用户';
$_LANG['GoOnAddUser']      = '继续添加用户';
$_LANG['SaveUser']         = '保存用户';
$_LANG['EditUser']         = '编辑用户';
$_LANG['DropUser']         = '禁用';
$_LANG['AlreadyDropedUser'] = '该用户已禁用';
$_LANG['ActiveUser']       = '激活';
$_LANG['UserExist']        = '用户名已存在！';
$_LANG['NoUserName']       = '没有输入用户名';
$_LANG['NoRealName']       = '没有输入用户真实姓名';
$_LANG['UserNameExist']        = '该用户名已存在！';
$_LANG['RealNameExist']        = '该真实姓名已存在！';
$_LANG['NoEmail']          = '没有输入Email';
$_LANG['InvalidEmail']     = '无效的Email地址';
$_LANG['InvalidUserName']  = '用户名中不能含有引号和&';
$_LANG['NoPassword']       = '没有输入密码';
$_LANG['NoEditUser']       = '没有任何改动';
$_LANG['SucceedEditUser']  = '编辑用户成功。' . htmlLink($_LANG['BackToUserList'], 'AdminUserList.php');
$_LANG['SuccessEditMyInfo'] = '您的用户信息已经更新';
$_LANG['SucceedAddUser']   = '添加用户成功。';
$_LANG['ConfirmDelUser']   = '确认禁用该用户？';
$_LANG['ConfirmActiveUser'] = '确认激活该用户？';
$_LANG['AuthModeName']      = '登录验证';
$_LANG['AuthMode']['DB']    = '内部帐号';
$_LANG['AuthMode']['LDAP']  = '域帐号';
$_LANG['InvalidLDAPUserNameFormat'] = '域帐号格式不正确，请输入类似<strong>domain\user</strong>格式的帐号';
$_LANG['LDAPUserNotFound']  = '没有发现该域帐号';

/* Group */
$_LANG['GroupList']        = '用户组列表';
$_LANG['BackToGroupList']  = '返回用户组列表';
$_LANG['GroupID']          = '用户组 ID';
$_LANG['GroupName']        = '用户组名';
$_LANG['GroupUser']        = '用户组用户';
$_LANG['GroupManager']     = '用户组管理员';
$_LANG['GroupACL']         = '用户组权限';
$_LANG['AddGroup']         = '添加用户组';
$_LANG['GoOnAddGroup']     = '继续添加用户组';
$_LANG['SaveGroup']        = '保存用户组';
$_LANG['EditGroup']        = '编辑用户组';
$_LANG['DropGroup']        = '删除用户组';
$_LANG['GroupNameExist']   = '用户组名已存在！';
$_LANG['GroupExist']       = '相同的用户组已存在！';
$_LANG['NoGroupName']      = '没有输入用户组名';
$_LANG['NoEditGroup']      = '没有任何改动';
$_LANG['SucceedEditGroup'] = '编辑用户组成功。' . htmlLink($_LANG['BackToGroupList'], 'AdminGroupList.php');
$_LANG['SucceedAddGroup']  = '添加用户组成功。';
$_LANG['ConfirmDelGroup']  = '确认删除该用户组？';
$_LANG['AllUserGroupName'] = '[All Users]';

/* User log */
$_LANG['UserLogList']      = '用户登录日志列表';
$_LANG['LogID']            = '日志 ID';
$_LANG['LoginIP']          = '登录 IP';
$_LANG['LoginTime']        = '登录时间';

/* Setting the header menu */
$_LANG['Welcome'] = '欢迎';
$_LANG['EditPer'] = '编辑我的信息';
$_LANG['Admin']   = '后台管理';
$_LANG['Logout']  = '退出';
$_LANG['Manual']  = '帮助';

/* Setting the upload file */
$_LANG['Accessories']  = '附件';
$_LANG['AttachFile']   = '上传附件';
$_LANG['FileShowName'] = '显示名称';


/*========================================Setting for single page=========================*/
/* Setting of Login.php */
$_LANG['LoginTitle']     = '欢迎使用 BugFree';
$_LANG['TestUserName']   = '用户名:';
$_LANG['TestUserPWD']    = '密&nbsp;&nbsp;&nbsp;码:';
$_LANG['ButtonLogin']    = '登录(L)';
$_LANG['SelectLanguage'] = '选择语言:';
$_LANG['SelectStyle']    = '选择风格:';
$_LANG['RememberMe']     = '记住密码';

/* Setting of UserControl.php */
$_LANG['ConfirmDelQuery'] = '确认删除这个查询？';


/*====================================Setting for bug, change, review, case, result=========================*/
$_LANG['PreButton']  = '上一个(P)';
$_LANG['NextButton'] = '下一个(N)';
$_LANG['SaveButton'] = '保存(S)';
$_LANG['Comments']   = '注释';
$_LANG['ReplyNote'] = '备注';
$_LANG['HistoryInfo'] = '历史信息';

/*========= ErrorMsg Setting for bug, change, review, case, result=========================*/
$_LANG['NoBugTitle']        = '标题不能为空';
$_LANG['NoBugContent']      = '详细描述不能为空';
$_LANG['NoBugRepro']        = '复现步骤不能为空';
$_LANG['NoOpenBuild']       = '问题版本不能为空';
$_LANG['NoBugSeverity']     = '严重程度不能为空';
$_LANG['NoBugType']         = '问题类型不能为空';
$_LANG['NoHowFound']        = '如何发现不能为空';
$_LANG['NoDuplicateID']     = '请输入重复 问题记录 的编号';
$_LANG['NoResolvedBuild']   = '解决版本不能为空';
$_LANG['NoResolution']      = '解决方案不能为空';
$_LANG['AlreadyResolved']   = '问题记录 已经处于解决的状态';
$_LANG['AlreadyClosed']     = '问题记录 已经处于关闭的状态';
$_LANG['AlreadyActive']     = '问题记录 已经处于激活的状态';
$_LANG['BugAlreadyChanged'] = '该 问题记录 已经发生变更，请重新打开编辑。';

$_LANG['NoChangeTitle']        = '标题不能为空';
$_LANG['NoChangeRootCause']    = '根本原因不能为空';
$_LANG['NoChangeResolution']    = '解决方案不能为空';
$_LANG['NoChangeType']         = '修改类型不能为空';
$_LANG['ChangeAlreadyResolved']   = '修改记录 已经处于解决的状态';
$_LANG['ChangeAlreadyClosed']     = '修改记录 已经处于关闭的状态';
$_LANG['ChangeAlreadyActive']     = '修改记录 已经处于激活的状态';
$_LANG['ChangeAlreadyChanged'] = '该 修改记录 已经发生变更，请重新打开编辑。';

$_LANG['ModeratorSameWithAuthor'] = '验证人不能是作者本人';
$_LANG['RecorderSameWithAuthor'] = '记录人不能是作者本人';
$_LANG['StartTimeLaterThanEndTime'] = '会议开始时间晚于结束时间';
$_LANG['ReviewAlreadyResolved']   = '评审记录 已经处于解决的状态';
$_LANG['ReviewAlreadyClosed']     = '评审记录 已经处于关闭的状态';
$_LANG['ReviewAlreadyActive']     = '评审记录 已经处于激活的状态';
$_LANG['ReviewAlreadyChanged'] = '该 评审记录 已经发生变更，请重新打开编辑。';
$_LANG['NotPassedReviewCanNotBeClosed'] = '评审未通过，无法关闭。';
$_LANG['NotCompletedReviewCanNotBeClosed'] = '评审未完成，无法关闭。';

$_LANG['NoReviewCommentTitle']        = '标题不能为空';
$_LANG['NoReviewCommentType']         = '意见类型不能为空';
$_LANG['ReviewCommentAlreadyResolved']   = '评审意见 已经处于解决的状态';
$_LANG['ReviewCommentAlreadyClosed']     = '评审意见 已经处于关闭的状态';
$_LANG['ReviewCommentAlreadyActive']     = '评审意见 已经处于激活的状态';
$_LANG['ReviewCommentAlreadyChanged'] = '该 评审意见 已经发生变更，请重新打开编辑。';

$_LANG['ConnectedChangesNotClosed'] = '关联的修改记录未关闭，请先关闭';
$_LANG['ConnectedReviewsNotClosed'] = '关联的评审记录未关闭，请先关闭';
$_LANG['ConnectedReviewCommentsNotSet'] = '评审意见未提交，无法关闭';
$_LANG['ConnectedReviewCommentsNotClosed'] = '关联的评审意见未关闭，无法关闭';

$_LANG['EmptyErrorMsg'] = array
(
		//Bug Fields
    'ProjectID'      => '产品ID 不能为空',
    'ProjectName'    => '产品 不能为空',
    'ModuleID'       => '模块ID 不能为空',
    'ModulePath'     => '模块路径 不能为空',
    'OpenedBuild'    => '问题版本 不能为空',
    'ResolvedBuild'  => '解决版本 不能为空',
    'BugTitle'       => '标题 不能为空',
    'BugKeyword'     => '关键词 不能为空',
    'BugType'        => '问题类型 不能为空',
    'BugSeverity'    => '严重程度 不能为空',
    'BugPriority'    => '优先级 不能为空',
    'BugOS'          => '操作系统 不能为空',
    'BugBrowser'     => '浏览器 不能为空',
    'BugMachine'     => '机器配置 不能为空',
    'HowFound'       => '如何发现 不能为空',
    'BugContent'     => '详细描述 不能为空',
    'ReproSteps'     => '复现步骤 不能为空',
    'ReproRate'     => 	'复现率 不能为空',
    'RootCause'     => 	'根本原因 不能为空',
    'BugStatus'      => '状态 不能为空',
    'BugSubStatus'   => '处理状态 不能为空',
    'Resolution'     => '解决方案 不能为空',   
    'DuplicateID'    => '重复的问题记录 不能为空',
    'MailTo'         => '抄送 不能为空',
    'OpenedBy'       => '提交人 不能为空',
    'OpenedDate'     => '提交日期 不能为空',
    'AssignedTo'     => '负责人 不能为空',
    'AssignedDate'   => '分配日期 不能为空',
    'ResolvedBy'     => '解决人 不能为空',
    'ResolvedDate'   => '解决日期 不能为空',
    'ClosedBy'       => '关闭人 不能为空',
    'ClosedDate'     => '关闭日期 不能为空',
    'LastEditedBy'   => '最后修改者 不能为空',
    'LastEditedDate' => '最后修改日期 不能为空',
    'ModifiedBy'     => '曾经修改者 不能为空',
    'OpenedByDepartment' 		=> '提交部门 不能为空',	
    'AssignedToDepartment' 	=> '负责部门 不能为空',			
		//Change Fields
		'ChangeID'       => 'CR ID 不能为空',
    'ChangeTitle'    => '标题  不能为空',
    'ChangeKeyword'  => '关键词  不能为空',
    'ChangeType'     => '修改类型  不能为空',
    'ChangeStatus'   => '状态  不能为空',
    //Review Fields
    'ReviewID'       => 'IR ID 不能为空',
    'ReviewTitle'    => '标题  不能为空',
    'ReviewKeyword'  => '关键字  不能为空',
    'ReviewType'     => '评审类型  不能为空',
    'ReviewStatus'   => '状态  不能为空',
		'MeetingDate'		 => '评审会议时间  不能为空',
		'StartTime'		 	 => '开始时间  不能为空',
		'EndTime'		 		 => '结束时间  不能为空',
		'MeetingLocation'=> '评审会议地点  不能为空',
    'ReviewContent'  => '评审内容  不能为空',
    'ReviewConclusion'=> '评审结论  不能为空',
		'Author'         => '作者  不能为空',
    'Moderator'      => '验证人  不能为空',
    'Recorder'       => '记录人  不能为空',
    'Inspectors'	 	 =>	'评审人员  不能为空',
		//ReviewComment Fields
    'ReviewCommentTitle'    => '描述 不能为空',
    'ReviewCommentType'     => '类型  不能为空',
    'ReviewCommentStatus'   => '状态  不能为空',
    'ReviewCommentContent'  => '详细描述 不能为空',
    'Resolution'  					=> '解决方案 不能为空',
    'ReviewCommentOwner'  	=> '意见人 不能为空',    
    //User Fields
    'UserID'    => 'UserID 不能为空',
    'UserName'     => '用户名  不能为空',
    'UserPassword'   => '密码  不能为空',
    'RealName'  => '真实名字 不能为空',
    'Email'  					=> '电子邮件 不能为空',
    'AuthMode'  	=> 'AuthMode 不能为空',
    //Project Fields
    'ProjectManagers'    => '管理员 不能为空',
    'ProjectGroupIDs'     => '产品用户组 不能为空',
    'ProjectDoc'   => 'ProjectDoc  不能为空',
    'ProjectPlan'  => 'ProjectPlan 不能为空',
    'DisplayOrder' => '显示次序 不能为空',
    'NotifyEmail'  	=> '通知邮箱 不能为空',
    //Plan Fields
   	'PlanTitle'					=> '计划名称 不能为空',
	  'PlanType'					=> '计划类型 不能为空',
	  'PlanBuild'					=> '测试版本 不能为空',
	  'PlanContent'				=> '测试内容 不能为空',
	  'PlanStartDate'			=> '开始时间 不能为空',
	  'PlanEndDate'				=> '结束时间 不能为空',
    //Case Fields
   	'CaseIndex'					=> '用例编号 不能为空',
	  'CaseTitle'					=> '用例名称 不能为空',
	  'CaseType'					=> '用例类型 不能为空',
	  'CaseDescription'		=> '测试目的 不能为空',
	  'CaseSteps'					=> '测试步骤 不能为空',
	  'CaseCriteria'			=> '期望输出 不能为空',
	  'CasePriority'			=> '优先级 不能为空',
	  'CaseMethod'				=> '测试方法 不能为空',
);

$_LANG['Position1ErrorMsg'] = array
(
	'Software' => '文件名 不能为空', 
	'Document' => '章节 不能为空',
	'Hardware' => 'Sheet名 不能为空', 
	'Structure' => 'Sheet名 不能为空', 
	'Others' => '位置1 不能为空',
);

$_LANG['Position2ErrorMsg'] = array
(
	'Software' => '函数名 不能为空', 
	'Document' => '页码 不能为空',
	'Hardware' => '模块 不能为空', 
	'Structure' => '模块 不能为空', 
	'Others' => '位置2 不能为空',
);

$_LANG['Position3ErrorMsg'] = array
(
	'Software' => '行号 不能为空', 
	'Document' => '行号 不能为空', 
	'Hardware' => '网络标号 不能为空', 
	'Structure' => '结构件名 不能为空', 
	'Others' => '位置3 不能为空',
);
/*=====================================Setting for open, edit, fix bug====================*/
$_LANG['OpenBug']    = '新建 问题记录';
$_LANG['ViewBug']    = '问题记录';
$_LANG['EditBug']    = '编辑 问题记录';
$_LANG['ResolveBug'] = '解决 问题记录';
$_LANG['CloseBug']   = '关闭 问题记录';
$_LANG['ActiveBug']  = '激活 问题记录';
$_LANG['Project']    = '产品';
$_LANG['Module']     = '模块';
$_LANG['BugTitle']   = '标题';
$_LANG['BugContent'] = '详细描述';
$_LANG['ReproSteps'] = '复现步骤';
$_LANG['DefaultReproSteps']['Step'] = '[复现步骤]';
$_LANG['DefaultReproSteps']['StepInfo'] = <<<EOT
1.
2.

EOT;
$_LANG['DefaultReproSteps']['Result']       = '[结果]';
$_LANG['DefaultReproSteps']['ExpectResult'] = '[期望]';
$_LANG['DefaultReproSteps']['Note']         = '[备注]';
$_LANG['BugID'] = 'Bug ID';

$_LANG['OpenBugButton']     = '新建(N)';
$_LANG['EditBugButton']     = '编辑(E)';
$_LANG['CopyBugButton']     = '复制(C)';
$_LANG['ResolveBugButton']  = '解决(R)';
$_LANG['CloseBugButton']    = '关闭(L)';
$_LANG['ActiveBugButton']   = '激活(A)';

//Bug Page's Fieldset Names
$_LANG['BugStatusInfo']     = '相关信息';
$_LANG['BugOpenedInfo']     = '新建';
$_LANG['BugResolvedInfo']   = '解决方案';
$_LANG['BugClosedInfo']     = '关闭';
$_LANG['BugConditionInfo']  = '相关';
$_LANG['BugOtherInfo']      = '其他信息';
$_LANG['BugReproInfo']      = '详细描述';
$_LANG['BugFiles']          = '附件';
$_LANG['MoreInfo']          = '更多信息';

/*=====================================Setting for open, edit, fix change====================*/
$_LANG['OpenChange']    = '新建 修改记录';
$_LANG['ViewChange']    = '修改记录';
$_LANG['EditChange']    = '编辑 修改记录';
$_LANG['ResolveChange'] = '解决 修改记录';
$_LANG['CloseChange']   = '关闭 修改记录';
$_LANG['ActiveChange']  = '激活 修改记录';
$_LANG['Project']    = '产品';
$_LANG['Module']     = '模块';
$_LANG['ChangeTitle']   = '标题';
$_LANG['RootCause'] = '根本原因';
$_LANG['Resolution'] = '解决方案';
$_LANG['ChangeID'] = 'CR ID';

$_LANG['OpenChangeButton']     = '新建 CR (B)';
$_LANG['EditChangeButton']     = '编辑(E)';
$_LANG['CopyChangeButton']     = '复制(C)';
$_LANG['ResolveChangeButton']  = '解决(R)';
$_LANG['CloseChangeButton']    = '关闭(L)';
$_LANG['ActiveChangeButton']   = '激活(A)';
$_LANG['ChangeStatusInfo']     = '相关信息';
$_LANG['ChangeOpenedInfo']     = '新建';
$_LANG['ChangeResolvedInfo']   = '解决方案';
$_LANG['ChangeClosedInfo']     = '关闭';
$_LANG['ChangeConditionInfo']  = '相关';
$_LANG['ChangeOtherInfo']      = '其他信息';
$_LANG['ChangeHistoryInfo']    = '历史';
$_LANG['ChangeDetailInfo']      = '详细描述';
$_LANG['ChangeFiles']          = '附件';
$_LANG['ChangeMoreInfo']          = '更多信息';

/*=====================================Setting for open, edit, fix review====================*/
$_LANG['OpenReview']    = '新建 评审记录';
$_LANG['ViewReview']    = '评审记录';
$_LANG['EditReview']    = '编辑 评审记录';
$_LANG['ResolveReview'] = '解决 评审记录';
$_LANG['CloseReview']   = '关闭 评审记录';
$_LANG['ActiveReview']  = '激活 评审记录';
$_LANG['Project']    = '产品';
$_LANG['Module']     = '模块';
$_LANG['ReviewTitle']   = '标题';
$_LANG['ReviewID'] = 'IR ID';

$_LANG['OpenReviewButton']     = '新建 IR (B)';
$_LANG['EditReviewButton']     = '编辑(E)';
$_LANG['CopyReviewButton']     = '复制(C)';
$_LANG['ResolveReviewButton']  = '解决(R)';
$_LANG['CloseReviewButton']    = '关闭(L)';
$_LANG['ActiveReviewButton']   = '激活(A)';
$_LANG['GoToReviewPageButton']     = '<< 设置';
$_LANG['GoToReviewCommentPageButton']     = '评审 >>';

$_LANG['ReviewStatusInfo']     = '相关信息';
$_LANG['ReviewOpenedInfo']     = '新建';
$_LANG['ReviewResolvedInfo']   = '解决方案';
$_LANG['ReviewClosedInfo']     = '关闭';
$_LANG['ReviewConditionInfo']  = '相关';
$_LANG['ReviewOtherInfo']      = '其他信息';
$_LANG['ReviewHistoryInfo']    = '历史';
$_LANG['ReviewDetailInfo']      = '详细描述';
$_LANG['ReviewFiles']          = '附件';
$_LANG['ReviewMoreInfo']          = '更多信息';

$_LANG['GoToReviewCommentList']    = '查看评审意见';
/*=====================================Setting for open, edit, fix reviewComment====================*/
$_LANG['OpenReviewComment']    = '新建 评审意见';
$_LANG['ViewReviewComment']    = '评审意见';
$_LANG['EditReviewComment']    = '编辑 评审意见';
$_LANG['ResolveReviewComment'] = '解决 评审意见';
$_LANG['CloseReviewComment']   = '关闭 评审意见';
$_LANG['ActiveReviewComment']  = '激活 评审意见';
$_LANG['Project']    = '产品';
$_LANG['Module']     = '模块';
$_LANG['ReviewCommentTitle']   = '标题';
$_LANG['ReviewCommentID'] = 'Comment ID';

$_LANG['EditReviewCommentButton']     = '编辑(E)';
$_LANG['CopyReviewCommentButton']     = '复制(C)';
$_LANG['ResolveReviewCommentButton']  = '解决(R)';
$_LANG['CloseReviewCommentButton']    = '关闭(L)';
$_LANG['ActiveReviewCommentButton']   = '激活(A)';
$_LANG['OpenReviewCommentButton']     = '提交评审意见';

$_LANG['ReviewCommentDetailInfo']      = '详细描述';
$_LANG['ReviewCommentFiles']          = '附件';

$_LANG['BackToReview']= '返回评审邀请页面';
/*=====================================Setting for open, edit, fix review====================*/
$_LANG['OpenPlan']    = '新建 测试计划';
$_LANG['ViewPlan']    = '测试计划';
$_LANG['EditPlan']    = '编辑 测试计划';
$_LANG['ResolvePlan'] = '解决 测试计划';
$_LANG['ClosePlan']   = '关闭 测试计划';
$_LANG['ActivePlan']  = '激活 测试计划';
$_LANG['PlanTitle']   = '标题';
$_LANG['PlanID'] = 'Plan ID';

$_LANG['OpenPlanButton']     = '新建 Plan (B)';
$_LANG['EditPlanButton']     = '编辑(E)';
$_LANG['CopyPlanButton']     = '复制(C)';
$_LANG['ResolvePlanButton']  = '解决(R)';
$_LANG['ClosePlanButton']    = '关闭(L)';
$_LANG['ActivePlanButton']   = '激活(A)';
/*=====================================Setting for add, edit case====================*/
$_LANG['AddCase']           = '新建 Case';
$_LANG['NoCaseTitle']       = 'Case 标题不能为空';
$_LANG['NoCaseStep']        = 'Case 步骤不能为空';
$_LANG['NoCasePriority']    = 'Case 优先级不能为空';
$_LANG['NoCaseType']        = 'Case 类型不能为空';
$_LANG['NoCaseMethod']      = 'Case 测试方法不能为空';
$_LANG['NoDisplayOrder']    = 'Case 显示顺序不能为空';
$_LANG['IllegalDisplayOrder']    = 'Case 显示顺序必须为0-255之间的整数';
$_LANG['BadScriptedDate']   = '日期不合法或日期格式无效（例如：2008-08-08）';
$_LANG['CaseAlreadyChanged']= '该 Case 已经发生变更，请重新打开编辑。';

$_LANG['CaseStatusInfo']    = 'Case 状态';

$_LANG['CaseOpenedInfo']    = '新建';
$_LANG['CaseReviewInfo']    = '评审';

$_LANG['AddCaseButton']     = '新建 Case(N)';
$_LANG['EditCaseButton']    = '编辑(E)';
$_LANG['CopyCaseButton']    = '复制(C)';
$_LANG['RunCaseButton']     = '运行(R)';
$_LANG['RunCaseLink']       = '运行 Case';

$_LANG['CaseFiles']         = '附件';
$_LANG['CaseMainInfo']      = '主要信息';
$_LANG['CaseActionInfo']    = '操作信息';
$_LANG['CaseStepsInfo']     = '步骤';
$_LANG['CaseConditionInfo'] = 'Case 相关';
$_LANG['CaseOtherInfo']     = '其他信息';
$_LANG['CaseDefaultStepInfo'] = <<<EOT
[步骤]
1.
2.

[验证]

[备注]

EOT;

$_LANG['CaseAutomationInfo'] = '自动化脚本';
/*=====================================Setting for add, edit result====================*/
$_LANG['OpenResult']         = '新建 Result';
$_LANG['RunCaseStep']        = '运行 Case';
$_LANG['EditResult']         = '编辑 Result';
$_LANG['EditResultButton']   = '编辑(E)';
$_LANG['OpenBugButton']      = '新建 SR (B)';
$_LANG['ResultActionInfo']   = '操作信息';
$_LANG['ResultFiles']        = '附件';

$_LANG['NoResultValue']      = '执行结果不能为空';
$_LANG['NoResultSteps']      = '测试步骤不能为空';
$_LANG['NoResultBuild']      = '测试版本不能为空';
$_LANG['ResultAlreadyChanged'] = '该 Result 已经发生变更，请重新打开编辑。';
$_LANG['ResultStatusInfo']   = 'Result 状态';
$_LANG['ResultEnvInfo']      = '运行环境';
$_LANG['ResultOpenedInfo']   = '新建';
$_LANG['ResultMainInfo']     = '主要信息';
$_LANG['ResultActionInfo']   = '操作信息';
$_LANG['ResultStepsInfo']    = '步骤';
$_LANG['ResultConditionInfo'] = 'Result 相关';
$_LANG['ResultOtherInfo']    = '其他信息';


$_LANG['NotSaveMod'] = '如果您离开本页面，您所做的更改将会丢失。';

/*=====================================Setting for pagination===========================*/
$_LANG['Pagination']['FirstPage']   = '首页';
$_LANG['Pagination']['EndPage']     = '尾页';
$_LANG['Pagination']['PrePage']     = '上一页';
$_LANG['Pagination']['NextPage']    = '下一页';
$_LANG['Pagination']['TotalRecord'] = '记录总数:';
$_LANG['Pagination']['NumPerPage']  = '项每页';
$_LANG['Pagination']['PageNum']     = '页码:';
$_LANG['Pagination']['NoResult']    = '无结果！';
$_LANG['Pagination']['Result']      = '结果';


/*=====================================Setting for list caption==========================*/
$_LANG['CustomDisplay']     = '自定义显示';
$_LANG['AllFieldsTitle']    = '可选字段';
$_LANG['FieldsToShowTitle'] = '要显示的字段';
$_LANG['FieldsToShowBTN']   = '确定(O)';
$_LANG['FieldsDefaultBTN']  = '默认字段(D)';
$_LANG['ExportHtmlTable']   = '全部导出';
$_LANG['ReportForms']       = '统计报表';
$_LANG['ExportCases']       = '导出';
$_LANG['ImportCases']       = '导入';
$_LANG['OpenedBugsInLastWeek'] = '上周新建的 Bug';
$_LANG['StaleBugsForOneWeek']  = '超过一周未处理的 Bug';

/*=====================================Setting for edit my info=========================*/
$_LANG['EditMyInfoButton'] = '提交(E)';


/*=====================================Setting for install===================*/
$_LANG['InstallBugFree']      = 'BugFree安装程序';
$_LANG['InstallChoose']       = '请选择';
$_LANG['InstallNextButton']   = '下一步';
$_LANG['InstallButton']       = '开始安装';
$_LANG['UpgradeButton']       = '开始升级';
$_LANG['InstallNewBugFree']   = '安装全新的 BugFree 2';
$_LANG['UpgradeBugFreeFrom1'] = '从 BugFree 1.1 升级';
$_LANG['UpgradeBugFreeFrom2'] = '从现有的 BugFree 2 升级';
$_LANG['UpgradeBugFree']      = '升级 BugFree';
$_LANG['UpgradeNotice']       = '升级前请先备份 BugFree 1.x 的数据库、上传的附件和程序';
$_LANG['UpgradeLanguage']     = '所要升级的 BugFree 的版本语言';
$_LANG['InstallStep1']        = '1、设置存储Bug数据的数据库参数<br />BugFree自身有一个简单的用户验证表，您可以使用它来管理用户，这时您可以跳过第 2 项的设置。 如果您的应用环境中已经有了第三方的PHP应用程序，比如论坛等等， 您也可以选择使用它们的用户验证表进行验证，这时您需要对第 2 项进行配置。';
$_LANG['InstallStep2']        = '2、设置用户验证表所在的数据库';
$_LANG['InstallStep3']        = '3、BugFree邮件功能参数配置<br /> 如果你选定的发信方式为SMTP方式，需要设置smtp服务器的地址，如果smtp服务器需要验证，还需要设定用户名和密码';
$_LANG['InstallStep4']        = '4、其他配置';
$_LANG['InstallStep5']        = '5、成为管理员';
$_LANG['InstallDBHost']       = '数据库服务器地址';
$_LANG['InstallDBUser']       = '数据库用户名';
$_LANG['InstallDBPassword']   = '数据库密码';
$_LANG['InstallDBDatabase']   = 'BugFree 数据表所在的数据库名';
$_LANG['InstallTablePrefix']  = 'BugFree 数据表的前缀名称';
$_LANG['InstallCreateDB']     = '创建数据库';
$_LANG['InstallUserDBHost']   = '数据库服务器地址';
$_LANG['InstallUserDBUser']   = '数据库用户名';
$_LANG['InstallUserDBPassword'] = '数据库密码';
$_LANG['InstallUserDBDatabase'] = '用户数据表所在的数据库名';
$_LANG['InstallTableName']     = '用户验证表名';
$_LANG['InstallUserName']      = '用户名对应的字段名';
$_LANG['InstallRealName']      = '真实姓名对应的字段名';
$_LANG['InstallUserPassword']  = '密码对应的字段名';
$_LANG['InstallEmail']         = 'Email对应的字段名';
$_LANG['InstallEncryptType']   = '密码加密方式';
$_LANG["InstallFromAddress"]   = "BugFree 以哪个邮箱地址进行发信";
$_LANG["InstallFromName"]      = "BugFree 以什么称呼进行发信";
$_LANG["InstallSendMethod"]    = "BugFree 自动发信的方式";
$_LANG["InstallSmtpHost"]      = "SMTP 服务器地址";
$_LANG["InstallSmtpAuth"]      = "SMTP 服务器是否需要验证";
$_LANG["InstallSmtpUserName"]  = "SMTP 服务器用户名";
$_LANG["InstallSmtpPassword"]  = "SMTP 服务器密码";
$_LANG["InstallUploadDirectory"]   = "上传附件存放目录，此目录必须可读可写";
$_LANG["InstallMaxFileSize"]       = "上传附件最大允许尺寸，单位字节";
$_LANG["InstallAdminUserName"]     = "管理员用户名";
$_LANG["InstallAdminRealName"]     = "管理员称呼";
$_LANG["InstallAdminUserEmail"]    = "管理员邮箱";
$_LANG["InstallAdminUserPassword"] = "管理员密码";
$_LANG["SampleBugInfo"]["BugTitle"] = "Sample：欢迎使用BugFree！";
$_LANG["SampleBugInfo"]["ReproSteps"] = <<<EOT
这是一个Bug的例子。
请先访问后台管理页面[url]Admin[/url]：
1. 在用户管理页添加用户；
2. 在用户组管理页添加新的用户组,并指派添加的用户；
3. 在项目管理页添加新的项目，指定访问项目的用户组；

更多有关BugFree的使用帮助，请访问 [url]http://www.bugfree.org.cn/help[/url]。
EOT;
$_LANG["InstallSuccessed"] = <<<EOT
安装成功！<br /><br />
默认管理员帐号：admin<br />
默认管理员密码：123456<br /><br />
请点击<a href="Login.php" target="_self"><b>这里</b></a>登录。登录后，请立即修改默认密码。<br />
EOT;
$_LANG["UpgradeSuccessed"] = '升级成功，请点击<a href="Login.php" target="_self"><b>这里</b></a>登录。';
$_LANG["InstallErrorNoMysqlModule"]   = "没有安装Mysql模块！";
$_LANG["InstallErrorBugDB"]           = "BugFree 数据库验证失败: ";
$_LANG["InstallErrorPrefixNeeded"]    = "为创建新表，需要在配置文件中指定数据表前缀。<br />例：\$_CFG['DB']['TablePrefix'] = 'bf_';";
$_LANG["InstallErrorCreateBugDB"]     = "BugFree 数据库创建失败: ";
$_LANG["InstallErrorUserDB"]          = "用户数据库验证失败: ";
$_LANG["InstallErrorUserTable"]       = "用户验证表: ";
$_LANG["InstallErrorWritable"]        = "必须可读可写可执行(o=rwx)";
$_LANG["InstallErrorSmtpAuth"]        = "Smtp服务器需要验证，必须指定用户名和密码";
$_LANG["InstallErrorAdminUserInfo"]   = "请指定管理员的用户名、Email、密码";
$_LANG["InstallErrorCreateAdminUser"] = "管理员创建失败";
$_LANG["InstallErrorDBExists"]        = "数据库已经存在";
$_LANG["InstallErrorAlreadyNewest"]   = "已经是最新版本的BugFree。<br />点击<a href='Login.php' target='_self'><b>这里</b></a>登录。";
$_LANG["CreateDBSuccessed"]           = '创建数据库成功！请点击<a href="install.php" target="_self"><b>继续安装</b></a>';

/*=====================================Setting for notice===============================*/
$_LANG['NoticeBugSubject'] = '[问题跟踪系统]目前为止您需要解决的 SR 数：';
$_LANG['NoticeBugSubject2'] = '[问题跟踪系统]目前为止您需要跟踪的 SR 数：';

/*=====================================Setting for report================================*/
$_LANG['ViewReportBTN'] = '查看统计';
$_LANG['SelectAllBTN']  = '全选';
$_LANG['SelectNoneBTN'] = '全不选';
$_LANG['Others']        = '其它';
$_LANG['TotalCount']    = '总计';
$_LANG['Blank']         = '[空白]';
$_LANG['ReportTimeRange'] = array
(
    'All'       => '所有时间',
    'LastWeek'  => '上周',
    'ThisMonth' => '本月',
    'ThisYear'  => '今年',
    'Custom'    => '自定义时间',
);

$_LANG['ReportBugCount'] = 'Bug Counts';
$_LANG["BugReportType"] =  array
(
    "BugsPerProject"     => "SR 产品分布",
    "BugsPerModule"      => "SR 模块分布",
    "BugsPerStatus"      => "SR 状态分布",
    "BugsPerSeverity"    => "SR 严重程度分布",
    "BugsPerPriority"    => "SR 优先级分布",
    "BugsPerType"        => "SR 类型分布",
    "BugsPerHowFound"    => "SR 如何发现分布",
    "BugsPerOS"          => "SR 操作系统分布",
    "BugsPerBrowser"     => "SR 浏览器分布",
    "OpenedBugsPerUser"  => "SR 创建者分布",
    "OpenedBugsPerDay"   => "SR 创建日期分布（按日）",
    "OpenedBugsPerWeek"  => "SR 创建日期分布（按周）",
    "OpenedBugsPerMonth" => "SR 创建日期分布（按月）",
    "ResolvedBugsPerUser"=> "SR 解决者分布",
    "BugsPerResolution"  => "SR 解决方案分布",
    "ResolvedBugsPerDay" => "SR 解决日期分布（按日）",
    "ResolvedBugsPerWeek"=> "SR 解决日期分布（按周）",
    "ResolvedBugsPerMonth"=> "SR 解决日期分布（按月）",
    "ClosedBugsPerUser"  => "SR 关闭者分布",
    "ClosedBugsPerDay"   => "SR 关闭日期分布（按日）",
    "ClosedBugsPerWeek"  => "SR 关闭日期分布（按周）",
    "ClosedBugsPerMonth" => "SR 关闭日期分布（按月）",
    "ActivatedBugsPerDay" => "SR 激活次数日期分布（按日）",
    "ActivatedBugsPerWeek" => "SR 激活次数日期分布（按周）",
    "ActivatedBugsPerMonth" => "SR 激活次数日期分布（按月）",
    "BugLiveDays"        => "SR 处理时间分布",
    "AssignedBugsPerUser"=> "SR 被指派人分布",
    "BugHistorys"        => "SR 处理步骤分布",
);

$_LANG['ReportChangeCount'] = 'Change Counts';
$_LANG["ChangeReportType"] =  array
(
    "ChangesPerProject"     => "CR 产品分布",
    "ChangesPerModule"      => "CR 模块分布",
    "ChangesPerStatus"      => "CR 状态分布",
    "ChangesPerType"        => "CR 类型分布",
    "OpenedChangesPerUser"  => "CR 创建者分布",
    "OpenedChangesPerDay"   => "CR 创建日期分布（按日）",
    "OpenedChangesPerWeek"  => "CR 创建日期分布（按周）",
    "OpenedChangesPerMonth" => "CR 创建日期分布（按月）",
    "ResolvedChangesPerUser"=> "CR 解决者分布",
    "ChangesPerResolution"  => "CR 解决方案分布",
    "ResolvedChangesPerDay" => "CR 解决日期分布（按日）",
    "ResolvedChangesPerWeek"=> "CR 解决日期分布（按周）",
    "ResolvedChangesPerMonth"=> "CR 解决日期分布（按月）",
    "ClosedChangesPerUser"  => "CR 关闭者分布",
    "ClosedChangesPerDay"   => "CR 关闭日期分布（按日）",
    "ClosedChangesPerWeek"  => "CR 关闭日期分布（按周）",
    "ClosedChangesPerMonth" => "CR 关闭日期分布（按月）",
    "ActivatedChangesPerDay" => "CR 激活次数日期分布（按日）",
    "ActivatedChangesPerWeek" => "CR 激活次数日期分布（按周）",
    "ActivatedChangesPerMonth" => "CR 激活次数日期分布（按月）",
    "ChangeLiveDays"        => "CR 处理时间分布",
    "AssignedChangesPerUser"=> "CR 被指派人分布",
);

$_LANG['ReportReviewCount'] = 'Review Counts';
$_LANG["ReviewReportType"] =  array
(
    "ReviewsPerProject"     => "IR 产品分布",
    "ReviewsPerModule"      => "IR 模块分布",
    "ReviewsPerStatus"      => "IR 状态分布",
    "ReviewsPerType"        => "IR 类型分布",
    "OpenedReviewsPerUser"  => "IR 创建者分布",
    "OpenedReviewsPerDay"   => "IR 创建日期分布（按日）",
    "OpenedReviewsPerWeek"  => "IR 创建日期分布（按周）",
    "OpenedReviewsPerMonth" => "IR 创建日期分布（按月）",
    "ResolvedReviewsPerUser"=> "IR 解决者分布",
    "ReviewsPerResolution"  => "IR 解决方案分布",
    "ResolvedReviewsPerDay" => "IR 解决日期分布（按日）",
    "ResolvedReviewsPerWeek"=> "IR 解决日期分布（按周）",
    "ResolvedReviewsPerMonth"=> "IR 解决日期分布（按月）",
    "ClosedReviewsPerUser"  => "IR 关闭者分布",
    "ClosedReviewsPerDay"   => "IR 关闭日期分布（按日）",
    "ClosedReviewsPerWeek"  => "IR 关闭日期分布（按周）",
    "ClosedReviewsPerMonth" => "IR 关闭日期分布（按月）",
    "ActivatedReviewsPerDay" => "IR 激活次数日期分布（按日）",
    "ActivatedReviewsPerWeek" => "IR 激活次数日期分布（按周）",
    "ActivatedReviewsPerMonth" => "IR 激活次数日期分布（按月）",
    "ReviewLiveDays"        => "IR 处理时间分布",
    "AuthorReviewsPerUser"  => "IR 作者分布",
    "ModeratorReviewsPerUser"  => "IR 验证人分布",
    "RecorderReviewsPerUser"  => "IR 记录人分布",
    "InspectorReviewsPerUser"  => "IR 评审人分布",
);

$_LANG['ReportReviewCommentCount'] = 'ReviewComment Counts';
$_LANG["ReviewCommentReportType"] =  array
(
    "ReviewCommentsPerProject"     => "Comment 产品分布",
    "ReviewCommentsPerModule"      => "Comment 模块分布",
    "ReviewCommentsPerStatus"      => "Comment 状态分布",
    "ReviewCommentsPerType"        => "Comment 类型分布",
    "OpenedReviewCommentsPerUser"  => "Comment 创建者分布",
    "OpenedReviewCommentsPerDay"   => "Comment 创建日期分布（按日）",
    "OpenedReviewCommentsPerWeek"  => "Comment 创建日期分布（按周）",
    "OpenedReviewCommentsPerMonth" => "Comment 创建日期分布（按月）",
    "ResolvedReviewCommentsPerUser"=> "Comment 解决者分布",
    "ReviewCommentsPerResolution"  => "Comment 解决方案分布",
    "ResolvedReviewCommentsPerDay" => "Comment 解决日期分布（按日）",
    "ResolvedReviewCommentsPerWeek"=> "Comment 解决日期分布（按周）",
    "ResolvedReviewCommentsPerMonth"=> "Comment 解决日期分布（按月）",
    "ClosedReviewCommentsPerUser"  => "Comment 关闭者分布",
    "ClosedReviewCommentsPerDay"   => "Comment 关闭日期分布（按日）",
    "ClosedReviewCommentsPerWeek"  => "Comment 关闭日期分布（按周）",
    "ClosedReviewCommentsPerMonth" => "Comment 关闭日期分布（按月）",
    "ReviewCommentLiveDays"        => "Comment 处理时间分布",
    "OwnerReviewCommentsPerUser"  => "Comment 意见提交人分布",
);

$_LANG['ReportCaseCount'] = 'Case Counts';
$_LANG["CaseReportType"] =  array
(
    "CasesPerProject"     => "Case 项目分布",
    "CasesPerModule"      => "Case 模块分布",
    "CasesPerStatus"      => "Case 状态分布",
    "CasesPerPriority"    => "Case 优先级分布",
    "CasesPerType"        => "Case 类型分布",
    "CasesPerMethod"      => "Case 测试方法分布",
    "CasesPerPlan"        => "Case 测试计划分布",
    "OpenedCasesPerUser"  => "Case 创建者分布",
    "OpenedCasesPerDay"   => "Case 创建日期分布（按日）",
    "OpenedCasesPerWeek"  => "Case 创建日期分布（按周）",
    "OpenedCasesPerMonth" => "Case 创建日期分布（按月）",
    "CasePerScriptStatus" => "Case 脚本状态分布",
    "CasesPerScriptedBy"  => "Case 脚本编写者分布",
    "ScriptedDatePerDay"  => "Case 脚本编写时间分布（按日）",
    "ScriptedDatePerWeek" => "Case 脚本编写时间分布（按周）",
    "ScriptedDatePerMonth" => "Case 脚本编写时间分布（按月）",
);

$_LANG['ReportResultCount'] = 'Result Counts';
$_LANG["ResultReportType"] =  array
(
    "ResultsPerProject"     => "Result 项目分布",
    "ResultsPerModule"      => "Result 模块分布",
    "ResultsPerValue"       => "Result 执行结果分布",
    "ResultsPerStatus"      => "Result 执行状态分布",
    "OpenedResultsPerUser"  => "Result 创建者分布",
    "OpenedResultsPerDay"   => "Result 创建日期分布（按日）",
    "OpenedResultsPerWeek"  => "Result 创建日期分布（按周）",
    "OpenedResultsPerMonth" => "Result 创建日期分布（按月）",
    "ResultsPerOS"          => "Result 操作系统分布",
    "ResultsPerBrowser"     => "Result 浏览器分布",
);

$_CFG['ZeroTime'] = '0000-00-00 00:00:00';
$_CFG['NotForceDownloadFileType'] = array('jpg','jpeg','gif','png','bmp','html','htm');

$_CFG['FCFSWFType'] = array
(
   'Area2D'     => 'FCF_Area2D.swf',
   'Bar2D'      => 'FCF_Bar2D.swf',
   'Column2D'   => 'FCF_Column2D.swf',
   'Column3D'   => 'FCF_Column3D.swf',
   'Doughnut2D' => 'FCF_Doughnut2D.swf',
   'Line'       => 'FCF_Line.swf',
   'Pie3D'      => 'FCF_Pie3D.swf',
);

$_CFG['ReportTypeSWF'] =  array
(
//Bug ReportType Format
    "BugsPerProject"     => 'Pie3D',
    "BugsPerModule"      => 'Pie3D',
    "BugsPerStatus"      => 'Pie3D',
    "BugsPerSeverity"    => 'Pie3D',
    "BugsPerPriority"    => 'Pie3D',
    "BugsPerHowFound"    => 'Pie3D',
    "BugsPerOS"          => 'Pie3D',
    "BugsPerBrowser"     => 'Pie3D',
    "OpenedBugsPerUser"  => 'Bar2D',
    "OpenedBugsPerDay"   => 'Column2D',
    "OpenedBugsPerWeek"  => 'Column2D',
    "OpenedBugsPerMonth" => 'Column2D',
    "ResolvedBugsPerDay" => 'Line',
    "ResolvedBugsPerWeek"  => 'Line',
    "ResolvedBugsPerMonth" => 'Line',
    "ClosedBugsPerUser"   => 'Bar2D',
    "ClosedBugsPerDay"    => 'Column3D',
    "ClosedBugsPerWeek"   => 'Column3D',
    "ClosedBugsPerMonth"  => 'Column3D',
    "ActivatedBugsPerDay" => 'Column2D',
    "ActivatedBugsPerWeek" => 'Column2D',
    "ActivatedBugsPerMonth" => 'Column2D',
    "AssignedBugsPerUser" => 'Bar2D',
    "ResolvedBugsPerUser" => 'Bar2D',
    "BugsPerResolution"  => 'Bar2D',
    "BugsPerType"        => 'Bar2D',
    "BugLiveDays"        => 'Column2D',
    "BugHistorys"        => 'Column2D',
//Change ReportType Format
    "ChangesPerProject"     => 'Pie3D',
    "ChangesPerModule"      => 'Pie3D',
    "ChangesPerStatus"      => 'Pie3D',
    "OpenedChangesPerUser"  => 'Bar2D',
    "OpenedChangesPerDay"   => 'Column2D',
    "OpenedChangesPerWeek"  => 'Column2D',
    "OpenedChangesPerMonth" => 'Column2D',
    "ResolvedChangesPerDay" => 'Line',
    "ResolvedChangesPerWeek"  => 'Line',
    "ResolvedChangesPerMonth" => 'Line',
    "ClosedChangesPerUser"   => 'Bar2D',
    "ClosedChangesPerDay"    => 'Column3D',
    "ClosedChangesPerWeek"   => 'Column3D',
    "ClosedChangesPerMonth"  => 'Column3D',
    "ActivatedChangesPerDay" => 'Column2D',
    "ActivatedChangesPerWeek" => 'Column2D',
    "ActivatedChangesPerMonth" => 'Column2D',
    "AssignedChangesPerUser" => 'Bar2D',
    "ResolvedChangesPerUser" => 'Bar2D',
    "ChangesPerResolution"  => 'Bar2D',
    "ChangesPerType"        => 'Bar2D',
    "ChangeLiveDays"        => 'Column2D',
    //Review ReportType Format
    "ReviewsPerProject"     => 'Pie3D',
    "ReviewsPerModule"      => 'Pie3D',
    "ReviewsPerStatus"      => 'Pie3D',
    "OpenedReviewsPerUser"  => 'Bar2D',
    "OpenedReviewsPerDay"   => 'Column2D',
    "OpenedReviewsPerWeek"  => 'Column2D',
    "OpenedReviewsPerMonth" => 'Column2D',
    "ResolvedReviewsPerDay" => 'Line',
    "ResolvedReviewsPerWeek"  => 'Line',
    "ResolvedReviewsPerMonth" => 'Line',
    "ClosedReviewsPerUser"   => 'Bar2D',
    "ClosedReviewsPerDay"    => 'Column3D',
    "ClosedReviewsPerWeek"   => 'Column3D',
    "ClosedReviewsPerMonth"  => 'Column3D',
    "ActivatedReviewsPerDay" => 'Column2D',
    "ActivatedReviewsPerWeek" => 'Column2D',
    "ActivatedReviewsPerMonth" => 'Column2D',
    "ResolvedReviewsPerUser" => 'Bar2D',
    "ReviewsPerResolution"  => 'Bar2D',
    "ReviewsPerType"        => 'Bar2D',
    "ReviewLiveDays"        => 'Column2D',
    "AuthorReviewsPerUser"  => 'Bar2D',
    "ModeratorReviewsPerUser"  => 'Bar2D',
    "RecorderReviewsPerUser"  => 'Bar2D',
    "InspectorReviewsPerUser"  => 'Bar2D',
    //ReviewComment ReportType Format
    "ReviewCommentsPerProject"     => 'Pie3D',
    "ReviewCommentsPerModule"      => 'Pie3D',
    "ReviewCommentsPerStatus"      => 'Pie3D',
    "OpenedReviewCommentsPerUser"  => 'Bar2D',
    "OpenedReviewCommentsPerDay"   => 'Column2D',
    "OpenedReviewCommentsPerWeek"  => 'Column2D',
    "OpenedReviewCommentsPerMonth" => 'Column2D',
    "ResolvedReviewCommentsPerDay" => 'Line',
    "ResolvedReviewCommentsPerWeek"  => 'Line',
    "ResolvedReviewCommentsPerMonth" => 'Line',
    "ClosedReviewCommentsPerUser"   => 'Bar2D',
    "ClosedReviewCommentsPerDay"    => 'Column3D',
    "ClosedReviewCommentsPerWeek"   => 'Column3D',
    "ClosedReviewCommentsPerMonth"  => 'Column3D',
    "ResolvedReviewCommentsPerUser" => 'Bar2D',
    "ReviewCommentsPerType"        => 'Bar2D',
    "ReviewCommentLiveDays"        => 'Column2D',
    "OwnerReviewCommentsPerUser"  => 'Bar2D',
    //Case ReportType Format
    "CasesPerProject"    => 'Pie3D',
    "CasesPerModule"     => 'Pie3D',
    "CasesPerStatus"     => "Pie3D",
    "CasesPerPriority"   => "Pie3D",
    "CasesPerType"       => "Pie3D",
    "CasesPerMethod"     => "Pie3D",
    "CasesPerPlan"       => "Pie3D",
    "OpenedCasesPerUser" => "Bar2D",
    "OpenedCasesPerDay"  => "Column2D",
    "OpenedCasesPerWeek" => "Column2D",
    "OpenedCasesPerMonth" => "Column2D",
    "CasePerScriptStatus" => "Bar2D",
    "CasesPerScriptedBy"  => "Bar2D",
    "ScriptedDatePerDay"  => "Column3D",
    "ScriptedDatePerWeek" => "Column3D",
    "ScriptedDatePerMonth" => "Column3D",
    //Result ReportType Format
    "ResultsPerProject"    => "Pie3D",
    "ResultsPerModule"     => "Pie3D",
    "ResultsPerValue"      => "Pie3D",
    "ResultsPerStatus"     => "Pie3D",
    "OpenedResultsPerUser"  => "Bar2D",
    "OpenedResultsPerDay"   => "Column3D",
    "OpenedResultsPerWeek"  => "Column3D",
    "OpenedResultsPerMonth" => "Column3D",
    "ResultsPerOS"          => "Column2D",
    "ResultsPerBrowser"     => "Column2D",
);

$_CFG['ReportGraphOption']['Default'] = array
(
   'decimalPrecision'  => '0',
   'formatNumberScale' => '0',
   'rotateNames'       => '1',
   'basefontsize'      => '12',
   'formatNumber'      => '0',
);

$_CFG['ReportGraphOption']['Column2D'] = $_CFG['ReportGraphOption']['Default'] + array();
$_CFG['ReportGraphOption']['Column3D'] = $_CFG['ReportGraphOption']['Default'] + array();
$_CFG['ReportGraphOption']['Bar2D']    = $_CFG['ReportGraphOption']['Default'] + array();
$_CFG['ReportGraphOption']['Pie3D']    = $_CFG['ReportGraphOption']['Default'] + array
(
  'showPercentageValues'=>'0',
  'showNames'       =>'1',
  'showValues'      =>'0',
  'pieRadius'       =>"150",
  'pieYScale'       =>'60',
  'pieBorderAlpha'  =>'40',
  'pieFillAlpha'    =>'70',
  'pieSliceDepth'   =>'15',
  'chartLeftMargin' =>"0",
  'chartRightMargin'=>"0",
  'chartTopMargin'  =>"0"
);
$_CFG['ReportGraphOption']['Line'] = $_CFG['ReportGraphOption']['Default'] + array
(
  'showAlternateHGridColor' => '1',
  'AlternateHGridColor'     => 'ff5904',
  'divLineColor'            => 'ff5904',
  'divLineAlpha'            => '20',
  'alternateHGridAlpha'     => '5',
  'anchorRadius'            => '1',
);
$_CFG['ReportTypeGraphOption']['BugsPerProject'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['BugsPerModule'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['ChangesPerProject'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['ChangesPerModule'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['ReviewsPerProject'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['ReviewsPerModule'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['ReviewCommentsPerProject'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['ReviewCommentsPerModule'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['CasesPerProject'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['CasesPerModule'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['ResultsPerProject'] = array
(
 'showNames' => 0,
);
$_CFG['ReportTypeGraphOption']['ResultsPerModule'] = array
(
 'showNames' => 0,
);

/* Operators. */
$_CFG['FieldType'] = array
(
    'Project'=> 'ProjectName',
    'Number' => 'BugID,ChangeID,ReviewID,ReviewCommentID,PlanID,CaseID,ResultID,BugSeverity,BugPriority,CasePriority,DuplicateID,LinkID,DisplayOrder',
    'Date'   => 'LastEditedDate,OpenedDate,ResolvedDate,ClosedDate,ScriptedDate,AssignedDate,MeetingDate',
    'String' => 'BugTitle,OpenedBuild,ResolvedBuild,BugMachine,BugKeyword,ChangeTitle,ChangeKeyword,RootCause,Resolution,ReviewTitle,ReviewKeyword,MeetingLocation,ReviewContent,ReviewCommentTitle,CaseTitle,CaseKeyword,ResultTitle,ResultBuild,ResultMachine,ResultKeyword',
    'Group'	 => 'AssignedToDepartment,OpenedByDepartment',
    'People' => 'AssignedTo,LastEditedBy,OpenedBy,ResolvedBy,ClosedBy,ScriptedBy,Author,Moderator,ReviewCommentOwner',
    'MutilPeople' => 'MailTo,ModifiedBy,Inspectors',
    'Option' => 'BugStatus,BugSubStatus,BugType,HowFound,BugOS,BugBrowser,Resolution,ChangeType,ChangeStatus,ReviewType,ReviewStatus,ReviewConclusion,StartTime,EndTime,ReviewCommentType,ReviewCommentStatus,CaseStatus,CaseType,CaseMethod,CasePlan,ScriptStatus,MarkForDeletion,ResultValue,ResultStatus,ResultOS,ResultBrowser',
    'Path'   => 'ModulePath'
);

$_CFG['FieldTypeOperation'] = array
(
    'Project'=> '=,!=,LIKE,NOT LIKE',
    'Number' => '=,!=,>,<,>=,<=,IN',
    'Date'   => '=,!=,>,<,>=,<=',
    'String' => 'LIKE,NOT LIKE',
    'Group' => '=,!=,IN',
    'People' => '=,!=,IN',
    'MutilPeople' => '=',
    'Option' => '=,!=',
    'Path'   => 'LIKE,NOT LIKE,UNDER'
);

$_LANG['ExportCountExceed'] = '导出数量不能超过5000个, 操作失败!';
$_LANG['ImportLable'] = '请选择要导入的文件：';
$_LANG['ImportFileExceed'] = '导入文件不能超过2M, 操作失败!';

$_LANG['RunCases'] = '批量运行';
$_LANG['OpenBatchCases'] = '批量新建 Result';
$_LANG['MultipleCases'] = '(多个数值)';
$_LANG['RunCasesNotNull'] = '请至少选择一个Case';
$_LANG['NotifyEmail'] = '通知邮箱';
$_LANG['NotifyEmailPoject'] = '项目';
$_LANG['NotifyEmailModule'] = '模块';
$_LANG['NotifyEmailBugStatistics'] = '统计Bug';
$_LANG['StatisticsThisWeek'] = '最近七天新建';
$_LANG['StatisticsNotOperateOverWeek'] = '超过七天未处理';
$_LANG['StatisticsTotal'] = '历史统计';
$_LANG['RunCasesMaxValue'] = '批量运行不能超过100个Case，请缩小查询条件的范围。';
$_LANG['RunCasesConfirm'] = '批量运行将新建{param0}个 Result，是否继续？';
$_LANG['DateInvalidAlert'] = '请输入正确的日期格式。例如，2009-10-8 或 -7。';
$_LANG['WrongEmailFormat'] = '邮件格式不正确';
$_LANG['MutipleEmails'] = '多个邮箱用逗号隔开';

/* MySQL Keywords */
$_LANG['MySQL'] = array
(
    'Keywords' => array('add','all','alter','analyze','and','as','asc','asensitive','before','between','bigint','binary','blob','both','by','call','cascade','case','change','char','character','check','collate','column','condition','connection','constraint','continue','convert','create','cross','current_date','current_time','current_timestamp','current_user','cursor','database','databases','day_hour','day_microsecond','day_minute','day_second','dec','decimal','declare','default','delayed','delete','desc','describe','deterministic','distinct','distinctrow','div','double','drop','dual','each','else','elseif','enclosed','escaped','exists','exit','explain','false','fetch','float','float4','float8','for','force','foreign','from','fulltext','goto','grant','group','having','high_priority','hour_microsecond','hour_minute','hour_second','if','ignore','in','index','infile','inner','inout','insensitive','insert','int','int1','int2','int3','int4','int8','integer','interval','into','is','iterate','join','key','keys','kill','label','leading','leave','left','like','limit','linear','lines','load','localtime','localtimestamp','lock','long','longblob','longtext','loop','low_priority','match','mediumblob','mediumint','mediumtext','middleint','minute_microsecond','minute_second','mod','modifies','natural','not','no_write_to_binlog','null','numeric','on','optimize','option','optionally','or','order','out','outer','outfile','precision','primary','procedure','purge','raid0','range','read','reads','real','references','regexp','release','rename','repeat','replace','require','restrict','return','revoke','right','rlike','schema','schemas','second_microsecond','select','sensitive','separator','set','show','smallint','spatial','specific','sql','sqlexception','sqlstate','sqlwarning','sql_big_result','sql_calc_found_rows','sql_small_result','ssl','starting','straight_join','table','terminated','then','tinyblob','tinyint','tinytext','to','trailing','trigger','true','undo','union','unique','unlock','unsigned','update','usage','use','using','utc_date','utc_time','utc_timestamp','values','varbinary','varchar','varcharacter','varying','when','where','while','with','write','x509','xor','year_month','zerofill'),
    'Warning'  => '数据字段名与MySQL关键字冲突'
);

$_LANG['ImportMsg'] = array
(
    'project_not_existed' => '产品不存在',
    'module_not_existed' => '模块不存在',
    'project_no_permission' => '无权限访问该项目',
    'title_empty' => '标题不能为空'
);
?>
