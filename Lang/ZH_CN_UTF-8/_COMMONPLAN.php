<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * chinese language file.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*-BASIC SETTING FOR Plan RELATED FEATURES -*/
/* The fields of PlanInfo table. */
$_LANG['PlanFields'] = array
(
    'PlanID'       		=> 'Plan ID',
    'ProjectID'      	=> '产品ID',
    'ProjectName'    	=> '产品',
    'ModuleID'       	=> '模块ID',
    'ModulePath'     	=> '模块路径',
    'PlanTitle'    		=> '标题',
    'PlanKeyword'  		=> '关键字',
    'PlanType'     		=> '类型',
    'PlanBuild'    		=> '测试版本',
    'PlanStatus'   		=> '状态',
		'PlanStartDate'		=> '测试开始时间',
		'PlanEndDate'			=> '测试结束时间',
		'PlanContent'  		=> '测试内容',
    'PlanConclusion'	=> '测试结果',
    'PlanCaseID'			=> '测试用例',
		'PlanPassRate'		=> '测试通过率',
		'MailTo'         	=> '抄送',
    'OpenedBy'       	=> '创建人',
    'OpenedDate'     	=> '创建日期',
    'AssignedTo'     	=> '负责人',
    'AssignedDate'   	=> '分配日期',
    'ResolvedBy'     	=> '解决人',
    'ResolvedDate'   	=> '解决日期',
    'ClosedBy'       	=> '关闭人',
    'ClosedDate'     	=> '关闭日期',
    'LastEditedBy'   	=> '最后修改者',
    'LastEditedDate' 	=> '最后修改日期',
    'ModifiedBy'     	=> '曾经修改者',
);

//Change Import Related Configuration
$_LANG['PlanMustImportFields'] = array
(
    'PlanID',        // 'Plan ID',
    'ProjectName',  // '产品',
    'ModulePath',   // '模块路径',
    'PlanTitle',     // '标题',
    'PlanType',      // '类型',
    'PlanStatus',    // '状态',
);

$_LANG['PlanImportFileSuffixError'] = '导入文件格式错误,请选择XML文件！请确认导入文件包含如下字段：IR ID，产品，模块路径，标题，类型，状态。';
$_LANG['PlanImportColumnNotNull'] = '不能为空, 操作失败!';
$_LANG['PlanImportFinished'] = '导入完毕！一共导入{param0}个IR,其中{param1}个导入成功，{param2}个导入失败!({param3}个新增,{param4}个更新)';
$_LANG['PlanImportNotes'] = ' <br><div  style="text-align:left;color:red"><b>（1）导入必须包含以下字段：</b></div>
                          	 IR ID，标题、产品、模块路径、类型，状态
                          	 <br><div  style="text-align:left;color:red"><b>（2）所有导入的字段都不能为空</b></div>';

/* The fields used to query in QueryPlan.php.(Note: the field will be displayed in the order you defined here). */
$_LANG['PlanQueryField'] = array
(
    'PlanID'       		=> 'Plan ID',
    'ProjectName'    	=> '产品',
    'ModulePath'     	=> '模块路径',
    'PlanTitle'    		=> '标题',
    'PlanKeyword'  		=> '关键字',
    'PlanType'     		=> '类型',
    'PlanStatus'   		=> '状态',
		'PlanStartDate'		=> '测试开始时间',
		'PlanEndDate'			=> '测试结束时间',
		'PlanContent'  		=> '测试内容',
    'PlanConclusion'	=> '测试结果',
    'PlanCaseID'			=> '测试用例',
		'PlanPassRate'		=> '测试通过率',
		'MailTo'         	=> '抄送',
    'OpenedBy'       	=> '创建人',
    'OpenedDate'     	=> '创建日期',
    'AssignedTo'     	=> '负责人',
    'AssignedDate'   	=> '分配日期',
    'ResolvedBy'     	=> '解决人',
    'ResolvedDate'   	=> '解决日期',
    'ClosedBy'       	=> '关闭人',
    'ClosedDate'     	=> '关闭日期',
    'LastEditedBy'   	=> '最后修改者',
    'LastEditedDate' 	=> '最后修改日期',
    'ModifiedBy'     	=> '曾经修改者',
);

$_LANG["DefaultPlanQueryFields"] = array
(
    'PlanID'       		=> 'Plan ID',
    'ProjectName'    	=> '产品',
    'PlanType'     		=> '类型',
    'PlanTitle'    		=> '标题',
		'PlanStartDate'		=> '测试开始时间',
		'PlanEndDate'			=> '测试结束时间',
    'PlanStatus'   		=> '状态',
);

/* Define the types. */
$_LANG['PlanTypes'] = array
 (
 ''          => '',
 'Function'   => '功能测试',
 'UnitTest'   	=> '单元测试',
 'BVT'      	=> '版本验证测试',
 'Intergrate' 	=> '集成测试',
 'System'    => '系统测试',
 'Smoke'    	=> '冒烟测试',
 'Acceptance'	=> '验收测试',
);


/* Define the status. */
$_LANG['PlanStatus'] = array
(
    ''         => '',
    'Active'   => 'Open',
    'Resolved' => 'Performed',
    'Closed'   => 'Closed'
);
?>
