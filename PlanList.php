<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * list Plans.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');
require('Include/FuncImportOutport.php');
@ini_set('memory_limit', -1);

// the flag of using custom field
$hasCustomField = true;
$ProjectID = testGetCurrentProjectId();
if($_GET['ProjectID'] - 0 > 0)
{
    $_SESSION['PlanQueryCondition'] = "ProjectID = '{$_GET['ProjectID']}'";
}


if($_GET['ModuleID'] - 0 > 0)
{
    $_SESSION['TestCurrentModuleID'] = $_GET['ModuleID'];
    $_SESSION['PlanQueryCondition'] = "ModuleID IN ({$_GET['ChildModuleIDs']})";
}

if($_GET['ActionType']=='FromNotifyEmail')
{
      $TopModuleID = $_GET['TopModuleID'];
      $ChildModuleList = testGetProjectModuleList($_GET['ProjectID'], 'Plan');//获取所有Module，以及父子关
      $ChildModules = $ChildModuleList[$TopModuleID]['ChildIDs'];//获得当前Module的所有子Module
      if($TopModuleID>=0)
      {
         $_SESSION['PlanQueryCondition'] = str_replace("\'", "'", $_GET['Condition'])." AND ProjectID={$_GET[ProjectID]} AND ModuleID in({$ChildModules})";
      }
      else
      {
         $_SESSION['PlanQueryCondition'] = str_replace("\'", "'", $_GET['Condition'])." AND ProjectID={$_GET[ProjectID]}";
      }
      $_SESSION['TestMode'] ='Plan';
}

if($_POST['PostQuery'])
{
    $hasCustomField = false;
    $_SESSION['hasPlanCustomField'] = false;
    $QueryStr = baseGetGroupQueryStr($_POST);
    $_SESSION['PlanQueryCondition'] = $QueryStr;
    $_SESSION['PlanQueryTitle'] = '';
    $_SESSION['PlanAndOrListCondition'] = baseGetAndOrListStr($_POST);
    $_SESSION['PlanFieldListCondition'] = baseGetFieldListStr($_POST);
    $_SESSION['PlanOperatorListCondition'] = baseGetOperatorListStr($_POST);
    $_SESSION['PlanValueListCondition'] = baseGetValueListStr($_POST);
    $_SESSION['PlanLeftParenthesesListCondition'] = baseGetLeftParentheseListStr($_POST);
    $_SESSION['PlanRightParenthesesListCondition'] = baseGetRightParentheseListStr($_POST);
    $arr = array_diff($_SESSION['PlanFieldListCondition'], array_keys($_LANG['PlanQueryField']));
    if(!empty($arr))
    {
        $hasCustomField = $_SESSION['hasPlanCustomField']= true;
    }
    unset($arr);
}

if($_REQUEST['QueryID'])
{
    $hasCustomField = false;
    $_SESSION['hasPlanCustomField'] = false;
    $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='Plan'");
    $_SESSION['PlanQueryCondition'] = $QueryInfo['QueryString'];
    $_SESSION['PlanQueryTitle'] = $QueryInfo['QueryTitle'];
    $_SESSION['PlanAndOrListCondition'] = unserialize($QueryInfo['AndOrList']);
    $_SESSION['PlanFieldListCondition'] = unserialize($QueryInfo['FieldList']);
    $_SESSION['PlanOperatorListCondition'] = unserialize($QueryInfo['OperatorList']);
    $_SESSION['PlanValueListCondition'] = unserialize($QueryInfo['ValueList']);
    $_SESSION['PlanFieldsToShow'] = $QueryInfo['FieldsToShow'];
    $_SESSION['PlanLeftParenthesesListCondition'] = unserialize($QueryInfo['LeftParentheses']);
    $_SESSION['PlanRightParenthesesListCondition'] = unserialize($QueryInfo['RightParentheses']);
    $arr = array_diff($_SESSION['PlanFieldListCondition'], array_keys($_LANG['PlanQueryField']));
    if(!empty($arr))
    {
        $hasCustomField = $_SESSION['hasPlanCustomField']= true;
    }
    unset($arr);
}
else
{
   $_REQUEST['QueryID'] = '-1';
}

$WHERE = array();
$URL = array();

$WHERE[] = $_SESSION['TestUserACLSQL'];

if($_SESSION['PlanQueryCondition'] != '')
{
    $WHERE[] = $_SESSION['PlanQueryCondition'];
}

//排序GET命令处理代码
if($_GET['OrderBy'])
{
    $OrderByList = explode('|', $_GET['OrderBy']);
    $OrderByColumn = $OrderByList[0];
    $OrderByType = $OrderByList[1];
    $OrderBy = join(' ', $OrderByList);
    $URL[] = 'OrderBy=' . $_GET['OrderBy'];
    $_SESSION['PlanOrderBy']['OrderBy'] = $OrderBy;
    $_SESSION['PlanOrderBy']['OrderByColumn'] = $OrderByColumn;
    $_SESSION['PlanOrderBy']['OrderByType'] = $OrderByType;
}
else
{
    if(empty($_SESSION['PlanOrderBy']))
    {
        $_SESSION['PlanOrderBy']['OrderBy'] = ' PlanID DESC';
        $_SESSION['PlanOrderBy']['OrderByColumn'] = 'PlanID';
        $_SESSION['PlanOrderBy']['OrderByType'] = 'DESC';
    }
    $OrderBy = $_SESSION['PlanOrderBy']['OrderBy'];
    $OrderByColumn = $_SESSION['PlanOrderBy']['OrderByColumn'];
    $OrderByType = $_SESSION['PlanOrderBy']['OrderByType'];
}

if($_GET['QueryMode'])
{
    $hasCustomField = false;
    $_SESSION['hasPlanCustomField'] = false;
    $QueryModeList = explode('|', $_GET['QueryMode']);
    $QueryColumn = $QueryModeList[0];
    $QueryValue = $QueryModeList[1];
    $WHERE = array();
    $WHERE[] = $_SESSION['TestUserACLSQL'];
    $QueryCondition = "";
    if(preg_match('/date/i', $QueryColumn))
    {
        $QueryCondition =  $QueryColumn . ' ' . sysStrToDateSql($QueryValue);
    }
    else
    {
        $QueryCondition = "{$QueryColumn}='{$QueryValue}'";
    }
    $_SESSION['PlanQueryCondition'] = $QueryCondition;
    $_SESSION['PlanQueryColumn'] = $QueryColumn;
    $WHERE[] = $QueryCondition;
    $URL[] = 'QueryMode=' . sysStripSlash($_GET['QueryMode']);
    // check if is set custom field for special function
    if(!array_key_exists($QueryColumn, $_LANG['PlanQueryField']))
    {
        $hasCustomField = $_SESSION['hasPlanCustomField'] = true;
    }
}
$Url = '?' . join('&', $URL);
$WHERE[] = "IsDroped = '0'";
$Where = join(' AND ', $WHERE);

if(isset($_SESSION['hasPlanCustomField']))
{
    $hasCustomField = $_SESSION['hasPlanCustomField'];
}
$FieldsToShow = testSetCustomFields('Plan', $ProjectID, $hasCustomField);
if(!array_key_exists($OrderByColumn, $FieldsToShow))
{
    $OrderBy = 'PlanID DESC';
    $OrderByColumn = 'PlanID';
    $OrderByType = 'DESC';
}

/* Get pagination */
$Pagination = new Page('PlanInfo', '', '', '', 'WHERE ' . $Where . ' ORDER BY ' . $OrderBy, $Url, $MyDB);
$LimitNum = $Pagination->LimitNum();
$TPL->assign('PaginationDetailInfo', $Pagination->getDetailInfo());
$ColumnArray = @array_keys($FieldsToShow);
$Columns = 'PlanID,PlanStatus,' . join(',',$ColumnArray);


$OrderColumnList = $ColumnArray;
$OrderByTypeList = array();
foreach($OrderColumnList as $OrderColumn)
{
    if($OrderColumn == $OrderByColumn)
    {
        $OrderByTypeList[$OrderColumn] = $OrderTypeReverseArray[$OrderByType];
    }
    else
    {
        $OrderByTypeList[$OrderColumn] = $OrderByType;
    }
}

//PlanList的导入处理代码
if($_FILES['file']['error']==2)	//导入成功
{
      sysObFlushJs("alert('{$_LANG['ImportFileExceed']}');");
}
elseif($_FILES['file']['error']==0)
{
        if($_FILES['file']['tmp_name'])   // 如果文件存在则进行批量导入
        {
	  				$findtype=strtolower(strrchr($_FILES['file']['name'],"."));

          	if($findtype!='.xml')	//文件必须以.xml结尾
          	{
            		sysObFlushJs("alert('{$_LANG['ImportFileSuffixError']}');");
          	}
          	else 
          	{
          			//读取文件内容到FileContents
		  					$file=fopen($_FILES['file']['tmp_name'],"r");
		  					$FileContents = fread($file, filesize($_FILES['file']['tmp_name']));
		  					//Rainy_Debug($FileContents);
		  					$FileContents = preg_replace('/<Font.*>|<\/Font>/Us', '', $FileContents);
								//将文件内容解析成变量，并存储在Dom变量中	
		            $Dom =  XML_unserialize($FileContents);
    		        $DomRowArray = $Dom[Workbook][Worksheet][Table][Row];
								//解析DomRowArray 并导入至数据库中
            		ParseImportPlanXML($DomRowArray,$_LANG);
        	}
    		}
}

if($_GET['Export'] == 'HtmlTable')
{
    $Columns = '';
    $LimitNum = '';
}

$PlanListSql = dbGetListSql(dbGetPrefixTableNames('PlanInfo'), '', $Where, '', $OrderBy, $LimitNum);
if($Where != $_SESSION['OldPlanQueryWhereStr'])
{
    $_SESSION['OldPlanQueryWhereStr'] = $Where;
    if(!($_GET['QueryMode']))
    {
        $_SESSION['PlanQueryColumn'] = '';
    }
}
if(($ProjectID != null) && ($hasCustomField))
{
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="Plan"]/field');
        if($fields)
        {
            $tableName = testGetFieldTable('Plan', $ProjectID);
            $Pagination = new Page(dbGetPrefixTableNames('PlanInfo') . ' LEFT JOIN ' . $tableName .' ON ' . dbGetPrefixTableNames('PlanInfo') . '.PlanID = ' . $tableName . '.FieldID', '', '', '', 'WHERE ' . $Where . ' ORDER BY ' . $OrderBy, $Url, $MyDB, false);
            $LimitNum = $Pagination->LimitNum();
            $TPL->assign('PaginationDetailInfo', $Pagination->getDetailInfo());
            $Where .= ' AND ProjectID = ' . $ProjectID;
            $sql =  dbGetListSql(dbGetPrefixTableNames('PlanInfo') . ',' . $tableName, '', $Where, '', $OrderBy, $LimitNum);
            $PlanListSql = str_replace(dbGetPrefixTableNames('PlanInfo') . ',' . $tableName, dbGetPrefixTableNames('PlanInfo') . ' LEFT JOIN ' . $tableName .' ON ' . dbGetPrefixTableNames('PlanInfo') . '.PlanID = ' . $tableName . '.FieldID', $sql);
        }
    }
}

$PlanList = dbGetListBySql($PlanListSql);
$UserNameList = testGetOneDimUserList();
$PlanList = testSetPlanListMultiInfo($PlanList, $UserNameList);
//print_r($PlanList);exit; 

$TPL->assign('PlanList', $PlanList);

$TPL->assign('OrderByColumn', $OrderByColumn);
$TPL->assign('OrderByType', $OrderByType);
$TPL->assign('QueryMode', $_GET['QueryMode']);
$TPL->assign('QueryColumn', $_SESSION['PlanQueryColumn']);
$TPL->assign('OrderByTypeList', $OrderByTypeList);
$TPL->assign('BaseTarget', '_self');
$TPL->assign('TestMode', 'Plan');

if($_GET['Export'] == 'HtmlTable')
{
    $TPL->assign('DataList', $PlanList);
    $TPL->assign('FieldsToShow', $_LANG["PlanFields"]);
    $TPL->display('ExportList.tpl');
    exit;
}

//PlanList导出命令处理代码
if($_GET['Export'] == 'XMLFile')  //批量导出Plan
{
   $PlanExportList = dbGetList('PlanInfo','', $Where, '', $OrderBy);
   $PlanCount = sizeof($PlanExportList);


   if($PlanCount >5000 ){
     sysObFlushJs("alert('{$_LANG['ExportCountExceed']}');");
   }
   else{
       $PlanExportColumnMust = array('PlanID' , 'PlanTitle', 'ProjectName', 'ModulePath', 'ReproSteps'); //必须包含的字段

      $PlanExportColumnArray = $ColumnArray;
      foreach($PlanExportColumnMust as $Item)
      {
         if(!in_array($Item, $PlanExportColumnArray))
            $PlanExportColumnArray[] = $Item;

      }

      $URL=$_CFG['File']['UploadDirectory'] . "/Planlist.xml";
      $File = fopen($URL,"w");

      $Content = ExportXML($PlanExportList,$PlanExportColumnArray,$_LANG['PlanFields']);

      file_put_contents($URL, $Content);
      header('Content-type: text/xml; charset=utf-8');
      header('Content-Disposition: attachment; filename=Planlist.xml');
      readfile($URL);
      exit;
   }
}
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('QueryID', $_REQUEST['QueryID']);
$TPL->assign('QueryTitle', $_SESSION['PlanQueryTitle'] );
$TPL->display('PlanList.tpl');
?>