<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * view, new, edit, resolve, close, activate a bug.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

sysXajaxRegister("xProjectSetSlaveModule,xSetModuleOwner,xCreateSelectDiv,xProjectSetAssignedUser,xDeleteTestFile,xFillCustomedField");
$DisplayValue = false;

$ModuleType = 'Bug';
$BugID = $_REQUEST['BugID'];
$ActionType = $_REQUEST['ActionType'];
$Columns = '*';
$BugInfo = dbGetRow('BugInfo',$Columns,"BugID = '{$BugID}' AND {$_SESSION[TestUserACLSQL]} AND IsDroped = '0'");

if(!$BugInfo['BugID'] && $ActionType != 'OpenBug')
{
    sysErrorMsg();
}

if($BugID > 0 && $ActionType == 'OpenBug')
{
    $ProjectID = $BugInfo['ProjectID'];
    $ModuleID = $BugInfo['ModuleID'];
    unset($BugInfo['LastEditedBy']);
    unset($BugInfo['LastEditedDate']);
    unset($BugInfo['ResolvedBy']);
    unset($BugInfo['Resolution']);
    unset($BugInfo['ResolvedBuild']);
    unset($BugInfo['ResolvedDate']);
    unset($BugInfo['ClosedBy']);
    unset($BugInfo['ClosedDate']);
    unset($BugInfo['ResultID']);
}

if($ActionType == 'OpenBug')
{
    /* Create PrjectList And ModuleList */
    $ProjectID = $ProjectID != '' ? $ProjectID : $_SESSION['TestCurrentProjectID'];
    $ModuleID = $ModuleID != '' ? $ModuleID : $_SESSION['TestCurrentModuleID'];
    $ModuleInfo = dbGetRow('TestModule','*',"ModuleID = '{$ModuleID}'");

		/* Set the BugStatus Field*/
    $BugInfo['BugStatus'] = 'Active';
    $BugInfo['BugStatusName'] = $_LANG['BugStatus'][$BugInfo['BugStatus']];

    $PreBugID = 0;
    $NextBugID = 0;

    /* Fill the default repro steps */
    $DefaultBugValue = array();
    $DefaultBugValue = $_LANG['DefaultReproSteps'];
    $DefaultBugValue['ExpectResult'] = '';
    $ModuleSelected = $ModuleID;

		/* The Bug was opened by the Failed Case Result*/
    if($_GET['ResultID'] != '')
    {
        $ResultID = $_GET['ResultID'];
        $ResultInfo = dbGetRow('ResultInfo',$Columns,"ResultID = '{$ResultID}' AND {$_SESSION[TestUserACLSQL]}");
        if(!empty($ResultInfo))
        {
            $ProjectID = $ResultInfo['ProjectID'];
            $ResultModuleID = $ResultInfo['ModuleID'];
            $ModuleInfo = dbGetRow('TestModule','ModuleName',"ModuleID = '{$ResultModuleID}'");
            $ModuleSelected = $ModuleInfo['ModuleName'];
            $BugInfo['BugTitle'] = $ResultInfo['ResultTitle'];
            $BugInfo['ResultTitle'] = $ResultInfo['ResultTitle'];
            $BugInfo['ProjectID'] = $ResultInfo['ProjectID'];
            $BugInfo['BugOS'] = $ResultInfo['ResultOS'];
            $BugInfo['BugBrowser'] = $ResultInfo['ResultBrowser'];
            $BugInfo['BugMachine'] = $ResultInfo['ResultMachine'];
            $BugInfo['OpenedBuild'] = $ResultInfo['ResultBuild'];
            $BugInfo['ReproSteps'] = $ResultInfo['ResultSteps'];
            $BugInfo['ResultID'] = $ResultID;
        }
    }
}
else
{
    /* Create PrjectList And ModuleList */
    $ProjectID = $BugInfo['ProjectID'];
    $ModuleID = $BugInfo['ModuleID'];
    $ModuleSelected = $ModuleID;
    if(!empty($BugInfo['ResultID']))
    {
        $ResultInfo = dbGetRow('ResultInfo',$Columns,"ResultID = '{$BugInfo['ResultID']}' AND {$_SESSION[TestUserACLSQL]}");
        $BugInfo['ResultTitle'] = $ResultInfo['ResultTitle'];
    }

    if(empty($_SESSION['BugOrderBy']))
    {
        $_SESSION['BugOrderBy']['OrderBy'] = ' BugID DESC';
        $_SESSION['BugOrderBy']['OrderByColumn'] = 'BugID';
        $_SESSION['BugOrderBy']['OrderByType'] = 'DESC';
    }

    $WhereStr = "{$_SESSION['TestUserACLSQL']} AND {$_SESSION['BugQueryCondition']}";

   
    if('BugID' == $_SESSION['BugOrderBy']['OrderByColumn'])
    {
        $OrderByStr = "{$_SESSION['BugOrderBy']['OrderByColumn']} {$_SESSION['BugOrderBy']['OrderByType']}";
    }
    else
    {
        $OrderByStr = "{$_SESSION['BugOrderBy']['OrderByColumn']} {$_SESSION['BugOrderBy']['OrderByType']},BugID ASC";
    }

    $BugIdInfoList = dbGetList('BugInfo','BugID',$WhereStr,'',$OrderByStr,'');
    $PreNextIdArr = getPreNextId($BugIdInfoList,'BugID',$BugID);
    $PreBugID = $PreNextIdArr[0];
    $NextBugID = $PreNextIdArr[1];
       
    $UserList = testGetProjectUserNameList($BugInfo['ProjectID']);
    $GroupNameList = testGetGroupNameList();
    $BugInfo = testSetBugMultiInfo($BugInfo, $UserList, $GroupNameList);

    $BugActionList = testGetActionAndFileList('Bug', $BugInfo['BugID']);
    $LastActionID = testGetLastActionID('Bug',$BugInfo['BugID']);
    $BugInfo['ActionList'] = $BugActionList['ActionList'];
    $BugInfo['FileList'] = $BugActionList['FileList'];
		if($BugInfo['DuplicateID'] != '')
		{
    	$BugInfo['DuplicateIDList'] = getBugTitleArr($BugInfo['DuplicateID']);
    }
    if($BugInfo['LinkID'] != '')
    {
    	$BugInfo['LinkIDList'] = getBugTitleArr($BugInfo['LinkID']);
    }
    if($BugInfo['CaseID'] != '')
    {
    	$BugInfo['CaseIDList'] = getCaseTitleArr($BugInfo['CaseID']);
		}
}

if($ActionType == 'Activated' && $BugInfo['BugStatus'] == 'Active')
{
    jsGoto('Bug.php?BugID=' . $BugID);
}
if($ActionType == 'Resolved' && $BugInfo['BugStatus'] == 'Resolved')
{
    jsGoto('Bug.php?BugID=' . $BugID);
}
if($ActionType == 'Closed' && $BugInfo['BugStatus'] == 'Closed')
{
    jsGoto('Bug.php?BugID=' . $BugID);
}

if($ActionType == 'OpenBug')
{
    $BugID = -1;
}

$OnChangeStr = 'onchange="';
$OnChangeStr .= 'xajax_xProjectSetSlaveModule(this.value, \'SlaveModuleList\', \'ModuleID\', \'Bug\');';
$OnChangeStr .= 'xajax_xProjectSetAssignedUser(this.value);';
$OnChangeStr .= 'xajax_xFillCustomedField(\'CustomedFieldSet\', this.value, \'Bug\', ' . $BugID . ');';
$OnChangeStr .= '"';
$OnChangeStr .= ' class="MyInput RequiredField"';
$ProjectListSelect = testGetValidProjectSelectList('ProjectID', $ProjectID, $OnChangeStr);

$OnChangeStr = '';
if($ActionType == 'OpenBug')
{
    $OnChangeStr = 'onchange="';
    $OnChangeStr .= 'xajax_xSetModuleOwner(this.value);';
    $OnChangeStr .= '"';
}
$OnChangeStr .= ' class="MyInput RequiredField"';
$ModuleSelectList = testGetSelectModuleList($ProjectID, 'ModuleID', $ModuleSelected, $OnChangeStr, $ModuleType);

$ProjectUserList = testGetProjectUserList($ProjectID, true);
$GroupNameList	= testGetGroupNameList();
Rainy_Debug($ProjectUserList,__FUNCTION__,__LINE__,__FILE__);
Rainy_Debug($GroupNameList,__FUNCTION__,__LINE__,__FILE__);
//$ProjectUserList['Closed'] = 'Closed';

if($ActionType == 'OpenBug')
{
    $ActionTypeName = $_LANG['OpenBug'];
    $AssignedTo = $ModuleInfo['ModuleOwner'] != '' ? $ModuleInfo['ModuleOwner'] : $BugInfo['AssignedTo'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$AssignedTo, 'class="NormalSelect MyInput RequiredField"');
    $SelectAssignGroupList = htmlSelect($GroupNameList, 'AssignedToDepartment', '',$AssignedToDepartment, 'class="NormalSelect MyInput RequiredField"');
    $SelectOpenedByGroupList = htmlSelect($GroupNameList, 'OpenedByDepartment', '',$OpenedByDepartment, 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == '')
{
    $DisplayValue = true;
    $ActionTypeName = $_LANG['ViewBug'];
}
elseif($ActionType == 'Edited')
{
    $ActionTypeName = $_LANG['EditBug'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$BugInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
    $SelectAssignGroupList = htmlSelect($GroupNameList, 'AssignedToDepartment', '',$BugInfo['AssignedToDepartment'], 'class="NormalSelect MyInput RequiredField"');
    $SelectOpenedByGroupList = htmlSelect($GroupNameList, 'OpenedByDepartment', '',$BugInfo['OpenedByDepartment'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Activated')
{
    $ActionTypeName = $_LANG['ActiveBug'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$BugInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
    $SelectAssignGroupList = htmlSelect($GroupNameList, 'AssignedToDepartment', '',$BugInfo['AssignedToDepartment'], 'class="NormalSelect MyInput RequiredField"');
    $SelectOpenedByGroupList = htmlSelect($GroupNameList, 'OpenedByDepartment', '',$BugInfo['OpenedByDepartment'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Resolved')
{
    $ActionTypeName = $_LANG['ResolveBug'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$BugInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
    $SelectAssignGroupList = htmlSelect($GroupNameList, 'AssignedToDepartment', '',$BugInfo['AssignedToDepartment'], 'class="NormalSelect MyInput RequiredField"');
    $SelectOpenedByGroupList = htmlSelect($GroupNameList, 'OpenedByDepartment', '',$BugInfo['OpenedByDepartment'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Closed')
{
    $ActionTypeName = $_LANG['CloseBug'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$BugInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
    $SelectAssignGroupList = htmlSelect($GroupNameList, 'AssignedToDepartment', '',$BugInfo['AssignedToDepartment'], 'class="NormalSelect MyInput RequiredField"');
    $SelectOpenedByGroupList = htmlSelect($GroupNameList, 'OpenedByDepartment', '',$BugInfo['OpenedByDepartment'], 'class="NormalSelect MyInput RequiredField"');
}

$BugSeveritySelectList = htmlSelect($_LANG['BugSeveritys'], 'BugSeverity', '', $BugInfo['BugSeverity'], 'class="MyInput RequiredField"');
$BugPrioritySelectList = htmlSelect($_LANG['BugPriorities'], 'BugPriority','',$BugInfo['BugPriority']);
$BugTypeSelectList = htmlSelect($_LANG['BugTypes'], 'BugType', '', $BugInfo['BugType'], 'class="MyInput RequiredField"');
$BugHowFoundList = htmlSelect($_LANG['BugHowFound'], 'HowFound', '', $BugInfo['HowFound'], 'class="MyInput RequiredField"');
$BugOSSelectList = htmlSelect($_LANG['BugOS'], 'BugOS', '', $BugInfo['BugOS']);
$BugSubStatusList = htmlSelect($_LANG['BugSubStatus'], 'BugSubStatus', '', $BugInfo['BugSubStatus']);
$BugBrowserList = htmlSelect($_LANG['BugBrowser'], 'BugBrowser', '', $BugInfo['BugBrowser']);
$BugMachineList = htmlSelect($_LANG['BugMachine'], 'BugMachine', '', $BugInfo['BugMachine']);

//$OpenedBuildList = testGetBugBuildList($ProjectID);
//$OpenedBuildSelectList = htmlSelect($OpenedBuildList, 'OpenedBuild', '', $BugInfo['OpenedBuild'], '', 'OpenedBuild,OpenedBuild');
$OnChangeStr = 'onchange="';
//$OnChangeStr .= 'if(this.value==\'Duplicate\'){displayObj(\'DuplicateID\');xajax.$(\'DuplicateID\').focus();}else{xajax.$(\'DuplicateID\').value=\'\';hiddenObj(\'DuplicateID\');}';
$OnChangeStr .= '"';
$ResolutionSelectList = htmlSelect($_LANG['BugResolutions'], 'Resolution','',$BugInfo['Resolution'], 'class="MyInput RequiredField" ' . $OnChangeStr);
//$ResolvedBuildList = $OpenedBuildList;
//$ResolvedBuildSelectList = htmlSelect($ResolvedBuildList, 'ResolvedBuild', '', $BugInfo['ResolvedBuild'], '', 'OpenedBuild,OpenedBuild');

$CustomedFieldHtml = testGetCustomedFieldHtml($ProjectID, 'Bug', $BugInfo['BugID'], $DisplayValue);

if($ActionType == 'OpenBug')
{
    $TPL->assign('HeaderTitle', $_LANG['OpenBug']);
}
else
{
    $TPL->assign('HeaderTitle', 'Bug #' . $BugInfo['BugID'] . ' ' . $BugInfo['BugTitle']);
}
$ConfirmArray = array('ReplyNote'=>'ReplyNote') + $_LANG['BugFields'];
$TPL->assign('ConfirmIds', jsArray($ConfirmArray, 'Key'));

$TPL->assign('BugInfo', $BugInfo);
$TPL->assign('PreBugID', $PreBugID);
$TPL->assign('NextBugID', $NextBugID);

$TPL->assign('ProjectID', $ProjectID);
$TPL->assign('ModuleID', $ModuleID);
$TPL->assign('ProjectList', $ProjectListSelect);
$TPL->assign('ModuleList', $ModuleSelectList);
$TPL->assign('BugTypeList', $BugTypeSelectList);
$TPL->assign('BugOSList', $BugOSSelectList);
$TPL->assign('BugSeverityList', $BugSeveritySelectList);
$TPL->assign('BugPriorityList', $BugPrioritySelectList);
$TPL->assign('BugBrowserList', $BugBrowserList);
$TPL->assign('BugSubStatusList', $BugSubStatusList);
$TPL->assign('BugHowFoundList', $BugHowFoundList);
$TPL->assign('ProjectUserList', $SelectUserList);
$TPL->assign('AssignedToUserList', $SelectAssignUserList);
$TPL->assign('AssignedToDepartmentList', $SelectAssignGroupList);
$TPL->assign('OpenedByDepartmentList', $SelectOpenedByGroupList);
$TPL->assign('OpenedByUserList', $SelectOpenUserList);
$TPL->assign('ResolutionList', $ResolutionSelectList);
$TPL->assign('ActionType', $ActionType);
$TPL->assign('ActionTypeName', $ActionTypeName);
$TPL->assign('ActionRealName', $_SESSION['TestRealName']);
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('ActionDate', date('Y-m-d'));
$TPL->assign('DefaultBugValue', $DefaultBugValue);
$TPL->assign('LastActionID',$LastActionID);
$TPL->assign('BugFields', $_LANG['BugFields']);
$TPL->assign('CustomedFieldHtml', $CustomedFieldHtml);

$TPL->display('Bug.tpl');
?>
