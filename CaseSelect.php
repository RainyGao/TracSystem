<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * view, new, edit, resolve, close, activate a bug.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

sysXajaxRegister("xProjectSetSlaveModule,xSetModuleOwner,xCreateSelectDiv,xProjectSetAssignedUser,xDeleteTestFile,xFillCustomedField");
$DisplayValue = false;

$ModuleType = 'Bug';
$PlanID = $_REQUEST['PlanID'];
$ActionType = $_REQUEST['ActionType'];
$Columns = '*';
$PlanInfo = dbGetRow('PlanInfo',$Columns,"PlanID = '{$PlanID}' AND {$_SESSION[TestUserACLSQL]} AND IsDroped = '0'");

//View Plan
if(!$PlanInfo['PlanID'] && $ActionType != 'OpenPlan')
{
    sysErrorMsg();
}

//Open New Plan
if($ActionType == 'OpenPlan')
{
		//Open New Plan by Copping
		if($PlanID > 0)
		{
    	$ProjectID = $PlanInfo['ProjectID'];
    	$ModuleID = $PlanInfo['ModuleID'];
    	unset($PlanInfo['LastEditedBy']);
    	unset($PlanInfo['LastEditedDate']);
    	unset($PlanInfo['ResolvedBy']);
    	unset($PlanInfo['ResolvedDate']);
    	unset($PlanInfo['ClosedBy']);
    	unset($PlanInfo['ClosedDate']);
		}
		else
		{
    	$ProjectID = $ProjectID != '' ? $ProjectID : $_SESSION['TestCurrentProjectID'];
    	$ModuleID = $ModuleID != '' ? $ModuleID : $_SESSION['TestCurrentModuleID'];
    }
    
    //Get ModuleInfo by ModuleID
    $ModuleInfo = dbGetRow('TestModule','*',"ModuleID = '{$ModuleID}'");
		
		//Set the PlanStatus Field
    $PlanInfo['PlanStatus'] = 'Active';
    $PlanInfo['PlanStatusName'] = $_LANG['PlanStatus'][$PlanInfo['PlanStatus']];
		
		//Previous Plan ID and Next Plan ID
    $PrePlanID = 0;
    $NextPlanID = 0;
}
else
{
    //Get ProjectID and ModuleID
    $ProjectID = $PlanInfo['ProjectID'];
    $ModuleID = $PlanInfo['ModuleID'];
    $ModuleSelected = $ModuleID;

		//Set the PlanOrderBy Value
    if(empty($_SESSION['PlanOrderBy']))
    {
        $_SESSION['PlanOrderBy']['OrderBy'] = ' PlanID DESC';
        $_SESSION['PlanOrderBy']['OrderByColumn'] = 'PlanID';
        $_SESSION['PlanOrderBy']['OrderByType'] = 'DESC';
    }
		
		//Set the WhereStr
    $WhereStr = "{$_SESSION['TestUserACLSQL']} AND {$_SESSION['PlanQueryCondition']}";
		
		//Set OrderByStr
    if('PlanID' == $_SESSION['PlanOrderBy']['OrderByColumn'])
    {
        $OrderByStr = "{$_SESSION['PlanOrderBy']['OrderByColumn']} {$_SESSION['PlanOrderBy']['OrderByType']}";
    }
    else
    {
        $OrderByStr = "{$_SESSION['PlanOrderBy']['OrderByColumn']} {$_SESSION['PlanOrderBy']['OrderByType']},PlanID ASC";
    }
		
		//Get the PlanIdInfoList according to the WhereStr and OrderByStr
    $PlanIdInfoList = dbGetList('PlanInfo','PlanID',$WhereStr,'',$OrderByStr,'');
    
    //Get the Previous and Next Plan ID in the PlanIdInfoList
    $PreNextIdArr = getPreNextId($PlanIdInfoList,'PlanID',$PlanID);
    $PrePlanID = $PreNextIdArr[0];
    $NextPlanID = $PreNextIdArr[1];
		
		//Convert the PlanInfo Values
		$UserList = testGetProjectUserNameList($PlanInfo['ProjectID']);
    $PlanInfo = testSetPlanMultiInfo($PlanInfo, $UserList);
		
		//Get the ActionList on the Plan
    $PlanActionList = testGetActionAndFileList('Plan', $PlanInfo['PlanID']);
    $LastActionID = testGetLastActionID('Plan',$PlanInfo['PlanID']);
    $PlanInfo['ActionList'] = $PlanActionList['ActionList'];
    $PlanInfo['FileList'] = $PlanActionList['FileList'];
}

if($ActionType == 'Activated' && $PlanInfo['PlanStatus'] == 'Active')
{
    jsGoto('Plan.php?PlanID=' . $PlanID);
}
if($ActionType == 'Resolved' && $PlanInfo['PlanStatus'] == 'Resolved')
{
    jsGoto('Plan.php?PlanID=' . $PlanID);
}
if($ActionType == 'Closed' && $PlanInfo['PlanStatus'] == 'Closed')
{
    jsGoto('Plan.php?PlanID=' . $PlanID);
}

if($ActionType == 'OpenPlan')
{
    $PlanID = -1;
}

//Build the Html ProjectSelect List
$OnChangeStr = 'onchange="';
$OnChangeStr .= 'xajax_xProjectSetSlaveModule(this.value, \'SlaveModuleList\', \'ModuleID\', \'Bug\');';
$OnChangeStr .= 'xajax_xProjectSetAssignedUser(this.value);';
$OnChangeStr .= 'xajax_xFillCustomedField(\'CustomedFieldSet\', this.value, \'Plan\', ' . $PlanID . ');';
$OnChangeStr .= '"';
$OnChangeStr .= ' class="MyInput RequiredField"';
$ProjectListSelect = testGetValidProjectSelectList('ProjectID', $ProjectID, $OnChangeStr);

//Build Html ModuleSelect List
$OnChangeStr = '';
if($ActionType == 'OpenPlan')
{
    $OnChangeStr = 'onchange="';
    $OnChangeStr .= 'xajax_xSetModuleOwner(this.value);';
    $OnChangeStr .= '"';
}
$OnChangeStr .= ' class="MyInput RequiredField"';
$ModuleSelectList = testGetSelectModuleList($ProjectID, 'ModuleID', $ModuleSelected, $OnChangeStr, $ModuleType);

//Get the UserList on the Project
$ProjectUserList = testGetProjectUserList($ProjectID, true);

//Set the ActionTypeName and Html AssignUserList
if($ActionType == 'OpenPlan')
{
    $ActionTypeName = $_LANG['OpenPlan'];
    $AssignedTo = $ModuleInfo['ModuleOwner'] != '' ? $ModuleInfo['ModuleOwner'] : $PlanInfo['AssignedTo'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$AssignedTo, 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == '')
{
    $DisplayValue = true;
    $ActionTypeName = $_LANG['ViewPlan'];
}
elseif($ActionType == 'Edited')
{
    $ActionTypeName = $_LANG['EditPlan'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$PlanInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Activated')
{
    $ActionTypeName = $_LANG['ActivePlan'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$PlanInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Resolved')
{
    $ActionTypeName = $_LANG['ResolvePlan'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$PlanInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Closed')
{
    $ActionTypeName = $_LANG['ClosePlan'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$PlanInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
}

//Build Html SelectList for PlanType....
$PlanTypeSelectList = htmlSelect($_LANG['PlanTypes'], 'PlanType', '', $PlanInfo['PlanType'], 'class="MyInput RequiredField"');

//Set the assign the values of TPL
if($ActionType == 'OpenPlan')
{
    $TPL->assign('HeaderTitle', $_LANG['OpenPlan']);
}
else
{
    $TPL->assign('HeaderTitle', 'Plan #' . $PlanInfo['PlanID'] . ' ' . $PlanInfo['PlanTitle']);
}
$ConfirmArray = array('ReplyNote'=>'ReplyNote') + $_LANG['PlanFields'];
$TPL->assign('ConfirmIds', jsArray($ConfirmArray, 'Key'));

$TPL->assign('PlanInfo', $PlanInfo);
$TPL->assign('PrePlanID', $PrePlanID);
$TPL->assign('NextPlanID', $NextPlanID);

$TPL->assign('ProjectID', $ProjectID);
$TPL->assign('ModuleID', $ModuleID);
$TPL->assign('ProjectList', $ProjectListSelect);
$TPL->assign('ModuleList', $ModuleSelectList);
$TPL->assign('PlanTypeList', $PlanTypeSelectList);
$TPL->assign('ProjectUserList', $SelectUserList);
$TPL->assign('AssignedToUserList', $SelectAssignUserList);
$TPL->assign('OpenedByUserList', $SelectOpenUserList);
$TPL->assign('ActionType', $ActionType);
$TPL->assign('ActionTypeName', $ActionTypeName);
$TPL->assign('ActionRealName', $_SESSION['TestRealName']);
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('ActionDate', date('Y-m-d'));
$TPL->assign('DefaultPlanValue', $DefaultPlanValue);
$TPL->assign('LastActionID',$LastActionID);
$TPL->assign('PlanFields', $_LANG['PlanFields']);

$TPL->display('CaseSelect.tpl');
?>
