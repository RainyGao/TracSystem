<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * index.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*加载Init.inc.php，用于初始化BugFree系统，配置方面主要将Config.inc.php中的_CFG和_COMMON.php中的_LANG赋给CFG和Lang变量*/
/* Init BugFree system. */
require('Include/Init.inc.php');

//Get the ReviewID and ActionType From Request Variables
$ReviewID = $_REQUEST['ReviewID'];

/* Assign. */
$TPL->assign('ReviewID', $ReviewID);

$TPL->assign('CloseLeftFrame', $_COOKIE['CloseLeftFrame']);

/* Display. */
$TPL->display('ReviewIndex.tpl');
?>
