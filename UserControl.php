<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * user control.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

function xUpdateUserControl($TestMode)
{
    global $_LANG;

    $_SESSION['UpdateUserControl'] = 'UpdateUserControl';
    $LimitNum = '10';

    $UCTestUserName = mysql_real_escape_string($_SESSION['TestUserName']);

    $objResponse = new xajaxResponse();
    sleep(1); //we'll do nothing for two seconds

    $AssignedStr = '';
    $OpenedStr = '';
    $QueryStr = '';


    if($TestMode == 'Bug')
    {
        $Columns = 'BugID,BugTitle,BugStatus';
        $Where = "OpenedBy = '{$UCTestUserName}' AND BugStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $OrderBy = 'LastEditedDate DESC';
        $OpenedList = dbGetList('BugInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $OpenedList = testSetBugListMultiInfo($OpenedList);

        $Where = "AssignedTo = '{$UCTestUserName}' AND BugStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $AssignedList = dbGetList('BugInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $AssignedList = testSetBugListMultiInfo($AssignedList);
        $AssignedStr = "<table>";
        foreach($AssignedList as $Item)
        {
          $AssignedStr .= "<tr><td>{$Item[BugID]}</td><td><a href=\"Bug.php?BugID={$Item[BugID]}\" title=\"{$Item[BugTitle]}\" target=\"_blank\" class=\"LeftTabBug{$Item[BugStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $AssignedStr .= "</table>";
        $OpenedStr = "<table>";
        foreach($OpenedList as $Item)
        {
          $OpenedStr .= "<tr><td>{$Item[BugID]}</td><td><a href=\"Bug.php?BugID={$Item[BugID]}\" title=\"{$Item[BugTitle]}\" target=\"_blank\" class=\"LeftTabBug{$Item[BugStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $OpenedStr .= "</table>";
    }
    elseif($TestMode == 'Change')
    {
        $Columns = 'ChangeID,ChangeTitle,ChangeStatus';
        $Where = "OpenedBy = '{$UCTestUserName}' AND ChangeStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $OrderBy = 'LastEditedDate DESC';
        $OpenedList = dbGetList('ChangeInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $OpenedList = testSetChangeListMultiInfo($OpenedList);

        $Where = "AssignedTo = '{$UCTestUserName}' AND ChangeStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $AssignedList = dbGetList('ChangeInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $AssignedList = testSetChangeListMultiInfo($AssignedList);
        $AssignedStr = "<table>";
        foreach($AssignedList as $Item)
        {
          $AssignedStr .= "<tr><td>{$Item[ChangeID]}</td><td><a href=\"Change.php?ChangeID={$Item[ChangeID]}\" title=\"{$Item[ChangeTitle]}\" target=\"_blank\" class=\"LeftTabChange{$Item[ChangeStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $AssignedStr .= "</table>";
        $OpenedStr = "<table>";
        foreach($OpenedList as $Item)
        {
          $OpenedStr .= "<tr><td>{$Item[ChangeID]}</td><td><a href=\"Change.php?ChangeID={$Item[ChangeID]}\" title=\"{$Item[ChangeTitle]}\" target=\"_blank\" class=\"LeftTabChange{$Item[ChangeStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $OpenedStr .= "</table>";
    }
    elseif($TestMode == 'Review')
    {
        $Columns = 'ReviewID,ReviewTitle,ReviewStatus';
        $Where = "OpenedBy = '{$UCTestUserName}' AND ReviewStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $OrderBy = 'LastEditedDate DESC';
        $OpenedList = dbGetList('ReviewInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $OpenedList = testSetReviewListMultiInfo($OpenedList);

        $Where = "Author = '{$UCTestUserName}' AND ReviewStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $AssignedList = dbGetList('ReviewInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $AssignedList = testSetReviewListMultiInfo($AssignedList);
        $AssignedStr = "<table>";
        foreach($AssignedList as $Item)
        {
          $AssignedStr .= "<tr><td>{$Item[ReviewID]}</td><td><a href=\"ReviewIndex.php?ReviewID={$Item[ReviewID]}\" title=\"{$Item[ReviewTitle]}\" target=\"_blank\" class=\"LeftTabReview{$Item[ReviewStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $AssignedStr .= "</table>";
        $OpenedStr = "<table>";
        foreach($OpenedList as $Item)
        {
          $OpenedStr .= "<tr><td>{$Item[ReviewID]}</td><td><a href=\"Review.php?ReviewID={$Item[ReviewID]}\" title=\"{$Item[ReviewTitle]}\" target=\"_blank\" class=\"LeftTabReview{$Item[ReviewStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $OpenedStr .= "</table>";
    }
    elseif($TestMode == 'ReviewComment')
    {
        $Columns = 'ReviewCommentID,ReviewCommentTitle,ReviewCommentStatus';
        $Where = "OpenedBy = '{$UCTestUserName}' AND ReviewCommentStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $OrderBy = 'LastEditedDate DESC';
        $OpenedList = dbGetList('ReviewCommentInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $OpenedList = testSetReviewCommentListMultiInfo($OpenedList);

        $Where = "ReviewCommentOwner = '{$UCTestUserName}' AND ReviewCommentStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $AssignedList = dbGetList('ReviewCommentInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $AssignedList = testSetReviewCommentListMultiInfo($AssignedList);
        $AssignedStr = "<table>";
        foreach($AssignedList as $Item)
        {
          $AssignedStr .= "<tr><td>{$Item[ReviewCommentID]}</td><td><a href=\"ReviewCommentIndex.php?ReviewCommentID={$Item[ReviewCommentID]}\" title=\"{$Item[ReviewCommentTitle]}\" target=\"_blank\" class=\"LeftTabReviewComment{$Item[ReviewCommentStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $AssignedStr .= "</table>";
        $OpenedStr = "<table>";
        foreach($OpenedList as $Item)
        {
          $OpenedStr .= "<tr><td>{$Item[ReviewCommentID]}</td><td><a href=\"ReviewComment.php?ReviewCommentID={$Item[ReviewCommentID]}\" title=\"{$Item[ReviewCommentTitle]}\" target=\"_blank\" class=\"LeftTabReviewComment{$Item[ReviewCommentStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $OpenedStr .= "</table>";
    }
    elseif($TestMode == 'Plan')
    {
        $Columns = 'PlanID,PlanTitle,PlanStatus';
        $Where = "OpenedBy = '{$UCTestUserName}' AND PlanStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $OrderBy = 'LastEditedDate DESC';
        $OpenedList = dbGetList('PlanInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $OpenedList = testSetPlanListMultiInfo($OpenedList);

        $Where = "Author = '{$UCTestUserName}' AND PlanStatus <> 'Closed'";
        $Where .= " AND IsDroped = '0' ";
        $AssignedList = dbGetList('PlanInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $AssignedList = testSetPlanListMultiInfo($AssignedList);
        $AssignedStr = "<table>";
        foreach($AssignedList as $Item)
        {
          $AssignedStr .= "<tr><td>{$Item[PlanID]}</td><td><a href=\"Plan.php?PlanID={$Item[PlanID]}\" title=\"{$Item[PlanTitle]}\" target=\"_blank\" class=\"LeftTabPlan{$Item[PlanStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $AssignedStr .= "</table>";
        $OpenedStr = "<table>";
        foreach($OpenedList as $Item)
        {
          $OpenedStr .= "<tr><td>{$Item[PlanID]}</td><td><a href=\"Plan.php?PlanID={$Item[PlanID]}\" title=\"{$Item[PlanTitle]}\" target=\"_blank\" class=\"LeftTabPlan{$Item[PlanStatus]}\">{$Item[UCTitle]}</a></td></tr>";
        }
        $OpenedStr .= "</table>";
    }
    elseif($TestMode == 'Case')
    {
        $Columns = 'CaseID,CaseTitle';
        $Where = "OpenedBy = '{$UCTestUserName}'";
        $Where .= " AND IsDroped = '0' ";
        $OrderBy = 'LastEditedDate DESC';
        $OpenedList = dbGetList('CaseInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $OpenedList = testSetCaseListMultiInfo($OpenedList);

        $Where = "AssignedTo = '{$UCTestUserName}'";
        $Where .= " AND IsDroped = '0' ";
        $AssignedList = dbGetList('CaseInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $AssignedList = testSetCaseListMultiInfo($AssignedList);

        foreach($AssignedList as $Item)
        {
          $AssignedStr .= "{$Item[CaseID]}&nbsp;<a href=\"Case.php?CaseID={$Item[CaseID]}\" title=\"{$Item[CaseTitle]}\" target=\"_blank\">{$Item[UCTitle]}</a><br />";
        }

        foreach($OpenedList as $Item)
        {
          $OpenedStr .= "{$Item[CaseID]}&nbsp;<a href=\"Case.php?CaseID={$Item[CaseID]}\" title=\"{$Item[CaseTitle]}\" target=\"_blank\">{$Item[UCTitle]}</a><br />";
        }
    }
    elseif($TestMode == 'Result')
    {
        $Columns = 'ResultID,ResultTitle';
        $Where = "OpenedBy = '{$UCTestUserName}'";
        $Where .= " AND IsDroped = '0' ";
        $OrderBy = 'LastEditedDate DESC';
        $OpenedList = dbGetList('ResultInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $OpenedList = testSetResultListMultiInfo($OpenedList);

        $Where = "AssignedTo = '{$UCTestUserName}'";
        $Where .= " AND IsDroped = '0' ";
        $AssignedList = dbGetList('ResultInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
        $AssignedList = testSetResultListMultiInfo($AssignedList);

        foreach($AssignedList as $Item)
        {
          $AssignedStr .= "{$Item[ResultID]}&nbsp;<a href=\"Result.php?ResultID={$Item[ResultID]}\" title=\"{$Item[ResultTitle]}\" target=\"_blank\">{$Item[UCTitle]}</a><br />";
        }

        foreach($OpenedList as $Item)
        {
          $OpenedStr .= "{$Item[ResultID]}&nbsp;<a href=\"Result.php?ResultID={$Item[ResultID]}\" title=\"{$Item[ResultTitle]}\" target=\"_blank\">{$Item[UCTitle]}</a><br />";
        }
    }

    $Where = "UserName = '{$UCTestUserName}' AND QueryType = '{$TestMode}'";
    $OrderBy = 'QueryTitle ASC';
    $QueryList = dbGetList('TestUserQuery','', $Where, '', $OrderBy);

    foreach($QueryList as $Item)
    {
        $QueryStr .= "<div>";
        $QueryStr .= "<a href='?DelQueryID={$Item[QueryID]}' title='{$_LANG[Delete]}' onclick='return confirm(\"{$_LANG[ConfirmDelQuery]}\");' target='_self' style='color:#CC0000;text-decoration:none;font-size:11px;font-weight:bold;margin-right:0px;padding:0 5px;'><img src='Image/delete.gif'/></a>";
        $QueryStr .= "<a href='{$TestMode}List.php?QueryID={$Item[QueryID]}' title='{$Item[QueryTitle]}'>{$Item[QueryTitle]}</a>";
        $QueryStr .= "</div>";
    }

    $objResponse->addAssign('UCDiv0', 'innerHTML', $AssignedStr);
    $objResponse->addAssign('UCDiv1', 'innerHTML', $OpenedStr);
    $objResponse->addAssign('UCDiv2', 'innerHTML', $QueryStr);

    return $objResponse;
}

sysXajaxRegister("xUpdateUserControl,xSetCurrentProject,xSetCurrentTabNum");

$LimitNum = '10';

$TestMode = $_SESSION['TestMode'];
$UCTestUserName = mysql_real_escape_string($_SESSION['TestUserName']);

if($_GET['DelQueryID'] != '')
{
    dbDeleteRow('TestUserQuery', "QueryID='{$_GET[DelQueryID]}' AND UserName='{$UCTestUserName}'");
    jsGoto('UserControl.php');
    exit;
}

if($TestMode == 'Bug')
{
    $Columns = 'BugID,BugTitle,BugStatus';
    $Where = "OpenedBy = '{$UCTestUserName}' AND BugStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $OrderBy = 'LastEditedDate DESC';
    $OpenedList = dbGetList('BugInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $OpenedList = testSetBugListMultiInfo($OpenedList);

    $Where = "AssignedTo = '{$UCTestUserName}' AND BugStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $AssignedList = dbGetList('BugInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $AssignedList = testSetBugListMultiInfo($AssignedList);
}
if($TestMode == 'Change')
{
    $Columns = 'ChangeID,ChangeTitle,ChangeStatus';
    $Where = "OpenedBy = '{$UCTestUserName}' AND ChangeStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $OrderBy = 'LastEditedDate DESC';
    $OpenedList = dbGetList('ChangeInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $OpenedList = testSetChangeListMultiInfo($OpenedList);

    $Where = "AssignedTo = '{$UCTestUserName}' AND ChangeStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $AssignedList = dbGetList('ChangeInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $AssignedList = testSetChangeListMultiInfo($AssignedList);
}
if($TestMode == 'Review')
{
    $Columns = 'ReviewID,ReviewTitle,ReviewStatus';
    $Where = "OpenedBy = '{$UCTestUserName}' AND ReviewStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $OrderBy = 'LastEditedDate DESC';
    $OpenedList = dbGetList('ReviewInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $OpenedList = testSetReviewListMultiInfo($OpenedList);

    $Where = "Author = '{$UCTestUserName}' AND ReviewStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $AssignedList = dbGetList('ReviewInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $AssignedList = testSetReviewListMultiInfo($AssignedList);
}
if($TestMode == 'ReviewComment')
{
    $Columns = 'ReviewCommentID,ReviewCommentTitle,ReviewCommentStatus';
    $Where = "OpenedBy = '{$UCTestUserName}' AND ReviewCommentStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $OrderBy = 'LastEditedDate DESC';
    $OpenedList = dbGetList('ReviewCommentInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $OpenedList = testSetReviewCommentListMultiInfo($OpenedList);

    $Where = "ReviewCommentOwner = '{$UCTestUserName}' AND ReviewCommentStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $AssignedList = dbGetList('ReviewCommentInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $AssignedList = testSetReviewCommentListMultiInfo($AssignedList);
}
if($TestMode == 'Plan')
{
    $Columns = 'PlanID,PlanTitle,PlanStatus';
    $Where = "OpenedBy = '{$UCTestUserName}' AND PlanStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $OrderBy = 'LastEditedDate DESC';
    $OpenedList = dbGetList('PlanInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $OpenedList = testSetPlanListMultiInfo($OpenedList);

    $Where = "Author = '{$UCTestUserName}' AND PlanStatus <> 'Closed'";
    $Where .= " AND IsDroped = '0' ";
    $AssignedList = dbGetList('PlanInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $AssignedList = testSetPlanListMultiInfo($AssignedList);
}
elseif($TestMode == 'Case')
{
    $Columns = 'CaseID,CaseTitle';
    $Where = "OpenedBy = '{$UCTestUserName}'";
    $Where .= " AND IsDroped = '0' ";
    $OrderBy = 'LastEditedDate DESC';
    $OpenedList = dbGetList('CaseInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $OpenedList = testSetCaseListMultiInfo($OpenedList);

    $Where = "AssignedTo = '{$UCTestUserName}'";
    $Where .= " AND IsDroped = '0' ";
    $AssignedList = dbGetList('CaseInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $AssignedList = testSetCaseListMultiInfo($AssignedList);
}
elseif($TestMode == 'Result')
{
    $Columns = 'ResultID,ResultTitle';
    $Where = "OpenedBy = '{$UCTestUserName}'";
    $Where .= " AND IsDroped = '0' ";
    $OrderBy = 'LastEditedDate DESC';
    $OpenedList = dbGetList('ResultInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $OpenedList = testSetResultListMultiInfo($OpenedList);

    $Where = "AssignedTo = '{$UCTestUserName}'";
    $Where .= " AND IsDroped = '0' ";
    $AssignedList = dbGetList('ResultInfo',$Columns, $Where, '', $OrderBy, $LimitNum);
    $AssignedList = testSetResultListMultiInfo($AssignedList);
}

$Where = "UserName = '{$UCTestUserName}' AND QueryType = '{$TestMode}'";
$OrderBy = 'QueryTitle ASC';
$QueryList = dbGetList('TestUserQuery','', $Where, '', $OrderBy);
$CurrentBugTabNum = testGetCurrentTabNum(true);
$TPL->assign('CurrentBugTabNum', $CurrentBugTabNum);
$TPL->assign('AssignedList', $AssignedList);
$TPL->assign('OpenedList', $OpenedList);
$TPL->assign('QueryList', $QueryList);
$TPL->assign('TestMode', $_SESSION['TestMode']);

$TPL->display('UserControl.tpl');
?>
