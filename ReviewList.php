<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * list reviews.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');
require('Include/FuncImportOutport.php');
@ini_set('memory_limit', -1);

// the flag of using custom field
$hasCustomField = true;
$ProjectID = testGetCurrentProjectId();
if($_GET['ProjectID'] - 0 > 0)
{
    $_SESSION['ReviewQueryCondition'] = "ProjectID = '{$_GET['ProjectID']}'";
}


if($_GET['ModuleID'] - 0 > 0)
{
    $_SESSION['TestCurrentModuleID'] = $_GET['ModuleID'];
    $_SESSION['ReviewQueryCondition'] = "ModuleID IN ({$_GET['ChildModuleIDs']})";
}

if($_GET['ActionType']=='FromNotifyEmail')
{
      $TopModuleID = $_GET['TopModuleID'];
      $ChildModuleList = testGetProjectModuleList($_GET['ProjectID'], 'Bug');//获取所有Module，以及父子关
      $ChildModules = $ChildModuleList[$TopModuleID]['ChildIDs'];//获得当前Module的所有子Module
      if($TopModuleID>=0)
      {
         $_SESSION['ReviewQueryCondition'] = str_replace("\'", "'", $_GET['Condition'])." AND ProjectID={$_GET[ProjectID]} AND ModuleID in({$ChildModules})";
      }
      else
      {
         $_SESSION['ReviewQueryCondition'] = str_replace("\'", "'", $_GET['Condition'])." AND ProjectID={$_GET[ProjectID]}";
      }
      $_SESSION['TestMode'] ='Review';
}

if($_POST['PostQuery'])
{
    $hasCustomField = false;
    $_SESSION['hasReviewCustomField'] = false;
    $QueryStr = baseGetGroupQueryStr($_POST);
    $_SESSION['ReviewQueryCondition'] = $QueryStr;
    $_SESSION['ReviewQueryTitle'] = '';
    $_SESSION['ReviewAndOrListCondition'] = baseGetAndOrListStr($_POST);
    $_SESSION['ReviewFieldListCondition'] = baseGetFieldListStr($_POST);
    $_SESSION['ReviewOperatorListCondition'] = baseGetOperatorListStr($_POST);
    $_SESSION['ReviewValueListCondition'] = baseGetValueListStr($_POST);
    $_SESSION['ReviewLeftParenthesesListCondition'] = baseGetLeftParentheseListStr($_POST);
    $_SESSION['ReviewRightParenthesesListCondition'] = baseGetRightParentheseListStr($_POST);
    $arr = array_diff($_SESSION['ReviewFieldListCondition'], array_keys($_LANG['ReviewQueryField']));
    if(!empty($arr))
    {
        $hasCustomField = $_SESSION['hasReviewCustomField']= true;
    }
    unset($arr);
}

if($_REQUEST['QueryID'])
{
    $hasCustomField = false;
    $_SESSION['hasReviewCustomField'] = false;
    $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='Review'");
    $_SESSION['ReviewQueryCondition'] = $QueryInfo['QueryString'];
    $_SESSION['ReviewQueryTitle'] = $QueryInfo['QueryTitle'];
    $_SESSION['ReviewAndOrListCondition'] = unserialize($QueryInfo['AndOrList']);
    $_SESSION['ReviewFieldListCondition'] = unserialize($QueryInfo['FieldList']);
    $_SESSION['ReviewOperatorListCondition'] = unserialize($QueryInfo['OperatorList']);
    $_SESSION['ReviewValueListCondition'] = unserialize($QueryInfo['ValueList']);
    $_SESSION['ReviewFieldsToShow'] = $QueryInfo['FieldsToShow'];
    $_SESSION['ReviewLeftParenthesesListCondition'] = unserialize($QueryInfo['LeftParentheses']);
    $_SESSION['ReviewRightParenthesesListCondition'] = unserialize($QueryInfo['RightParentheses']);
    $arr = array_diff($_SESSION['ReviewFieldListCondition'], array_keys($_LANG['ReviewQueryField']));
    if(!empty($arr))
    {
        $hasCustomField = $_SESSION['hasReviewCustomField']= true;
    }
    unset($arr);
}
else
{
   $_REQUEST['QueryID'] = '-1';
}

$WHERE = array();
$URL = array();

$WHERE[] = $_SESSION['TestUserACLSQL'];

if($_SESSION['ReviewQueryCondition'] != '')
{
    $WHERE[] = $_SESSION['ReviewQueryCondition'];
}

if($_GET['OrderBy'])
{
    $OrderByList = explode('|', $_GET['OrderBy']);
    $OrderByColumn = $OrderByList[0];
    $OrderByType = $OrderByList[1];
    $OrderBy = join(' ', $OrderByList);
    $URL[] = 'OrderBy=' . $_GET['OrderBy'];
    $_SESSION['ReviewOrderBy']['OrderBy'] = $OrderBy;
    $_SESSION['ReviewOrderBy']['OrderByColumn'] = $OrderByColumn;
    $_SESSION['ReviewOrderBy']['OrderByType'] = $OrderByType;
}
else
{
    if(empty($_SESSION['ReviewOrderBy']))
    {
        $_SESSION['ReviewOrderBy']['OrderBy'] = ' ReviewID DESC';
        $_SESSION['ReviewOrderBy']['OrderByColumn'] = 'ReviewID';
        $_SESSION['ReviewOrderBy']['OrderByType'] = 'DESC';
    }
    $OrderBy = $_SESSION['ReviewOrderBy']['OrderBy'];
    $OrderByColumn = $_SESSION['ReviewOrderBy']['OrderByColumn'];
    $OrderByType = $_SESSION['ReviewOrderBy']['OrderByType'];
}

if($_GET['QueryMode'])
{
    $hasCustomField = false;
    $_SESSION['hasReviewCustomField'] = false;
    $QueryModeList = explode('|', $_GET['QueryMode']);
    $QueryColumn = $QueryModeList[0];
    $QueryValue = $QueryModeList[1];
    $WHERE = array();
    $WHERE[] = $_SESSION['TestUserACLSQL'];
    $QueryCondition = "";
    if(preg_match('/date/i', $QueryColumn))
    {
        $QueryCondition =  $QueryColumn . ' ' . sysStrToDateSql($QueryValue);
    }
    else
    {
        $QueryCondition = "{$QueryColumn}='{$QueryValue}'";
    }
    $_SESSION['ReviewQueryCondition'] = $QueryCondition;
    $_SESSION['ReviewQueryColumn'] = $QueryColumn;
    $WHERE[] = $QueryCondition;
    $URL[] = 'QueryMode=' . sysStripSlash($_GET['QueryMode']);
    // check if is set custom field for special function
    if(!array_key_exists($QueryColumn, $_LANG['ReviewQueryField']))
    {
        $hasCustomField = $_SESSION['hasReviewCustomField'] = true;
    }
}
$Url = '?' . join('&', $URL);
$WHERE[] = "IsDroped = '0'";
$Where = join(' AND ', $WHERE);

if(isset($_SESSION['hasReviewCustomField']))
{
    $hasCustomField = $_SESSION['hasReviewCustomField'];
}
$FieldsToShow = testSetCustomFields('Review', $ProjectID, $hasCustomField);
if(!array_key_exists($OrderByColumn, $FieldsToShow))
{
    $OrderBy = 'ReviewID DESC';
    $OrderByColumn = 'ReviewID';
    $OrderByType = 'DESC';
}

/* Get pagination */
$Pagination = new Page('ReviewInfo', '', '', '', 'WHERE ' . $Where . ' ORDER BY ' . $OrderBy, $Url, $MyDB);
$LimitNum = $Pagination->LimitNum();
$TPL->assign('PaginationDetailInfo', $Pagination->getDetailInfo());
$ColumnArray = @array_keys($FieldsToShow);
$Columns = 'ReviewID,ReviewStatus,' . join(',',$ColumnArray);


$OrderColumnList = $ColumnArray;
$OrderByTypeList = array();
foreach($OrderColumnList as $OrderColumn)
{
    if($OrderColumn == $OrderByColumn)
    {
        $OrderByTypeList[$OrderColumn] = $OrderTypeReverseArray[$OrderByType];
    }
    else
    {
        $OrderByTypeList[$OrderColumn] = $OrderByType;
    }
}

if($_GET['Export'] == 'HtmlTable')
{
    $Columns = '';
    $LimitNum = '';
}

$ReviewListSql = dbGetListSql(dbGetPrefixTableNames('ReviewInfo'), '', $Where, '', $OrderBy, $LimitNum);
if($Where != $_SESSION['OldReviewQueryWhereStr'])
{
    $_SESSION['OldReviewQueryWhereStr'] = $Where;
    if(!($_GET['QueryMode']))
    {
        $_SESSION['ReviewQueryColumn'] = '';
    }
}
if(($ProjectID != null) && ($hasCustomField))
{
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="Review"]/field');
        if($fields)
        {
            $tableName = testGetFieldTable('Review', $ProjectID);
            $Pagination = new Page(dbGetPrefixTableNames('ReviewInfo') . ' LEFT JOIN ' . $tableName .' ON ' . dbGetPrefixTableNames('ReviewInfo') . '.ReviewID = ' . $tableName . '.FieldID', '', '', '', 'WHERE ' . $Where . ' ORDER BY ' . $OrderBy, $Url, $MyDB, false);
            $LimitNum = $Pagination->LimitNum();
            $TPL->assign('PaginationDetailInfo', $Pagination->getDetailInfo());
            $Where .= ' AND ProjectID = ' . $ProjectID;
            $sql =  dbGetListSql(dbGetPrefixTableNames('ReviewInfo') . ',' . $tableName, '', $Where, '', $OrderBy, $LimitNum);
            $ReviewListSql = str_replace(dbGetPrefixTableNames('ReviewInfo') . ',' . $tableName, dbGetPrefixTableNames('ReviewInfo') . ' LEFT JOIN ' . $tableName .' ON ' . dbGetPrefixTableNames('ReviewInfo') . '.ReviewID = ' . $tableName . '.FieldID', $sql);
        }
    }
}

$ReviewList = dbGetListBySql($ReviewListSql);
$UserNameList = testGetOneDimUserList();
$ReviewList = testSetReviewListMultiInfo($ReviewList, $UserNameList);
$TPL->assign('ReviewList', $ReviewList);

$TPL->assign('OrderByColumn', $OrderByColumn);
$TPL->assign('OrderByType', $OrderByType);
$TPL->assign('QueryMode', $_GET['QueryMode']);
$TPL->assign('QueryColumn', $_SESSION['ReviewQueryColumn']);
$TPL->assign('OrderByTypeList', $OrderByTypeList);
$TPL->assign('BaseTarget', '_self');
$TPL->assign('TestMode', 'Review');

//ReviewList的导入处理代码
if($_FILES['file']['error']==2)	//导入成功
{
      sysObFlushJs("alert('{$_LANG['ImportFileExceed']}');");
}
elseif($_FILES['file']['error']==0)
{
        if($_FILES['file']['tmp_name'])   // 如果文件存在则进行批量导入
        {
	  				$findtype=strtolower(strrchr($_FILES['file']['name'],"."));

          	if($findtype!='.xml')	//文件必须以.xml结尾
          	{
            		sysObFlushJs("alert('{$_LANG['ImportFileSuffixError']}');");
          	}
          	else 
          	{
          			//读取文件内容到FileContents
		  					$file=fopen($_FILES['file']['tmp_name'],"r");
		  					$FileContents = fread($file, filesize($_FILES['file']['tmp_name']));
		  					//Rainy_Debug($FileContents);
		  					$FileContents = preg_replace('/<Font.*>|<\/Font>/Us', '', $FileContents);
								//将文件内容解析成变量，并存储在Dom变量中	
		            $Dom =  XML_unserialize($FileContents);
    		        $DomRowArray = $Dom[Workbook][Worksheet][Table][Row];
								//解析DomRowArray 并导入至数据库中
            		ParseImportReviewXML($DomRowArray,$_LANG);
        	}
    		}
}

if($_GET['Export'] == 'HtmlTable')
{
    $TPL->assign('DataList', $ReviewList);
    $TPL->assign('FieldsToShow', $_LANG["ReviewFields"]);
    $TPL->display('ExportList.tpl');
    exit;
}

if($_GET['Export'] == 'XMLFile')  //批量导出Review
{
   $ReviewExportList = dbGetList('ReviewInfo','', $Where, '', $OrderBy);
   $ReviewCount = sizeof($ReviewExportList);


   if($ReviewCount >5000 ){
     sysObFlushJs("alert('{$_LANG['ExportCountExceed']}');");
   }
   else{
       $ReviewExportColumnMust = array('ReviewID' , 'ReviewTitle', 'ProjectName', 'ModulePath', 'ReproSteps'); //必须包含的字段

      $ReviewExportColumnArray = $ColumnArray;
      foreach($ReviewExportColumnMust as $Item)
      {
         if(!in_array($Item, $ReviewExportColumnArray))
            $ReviewExportColumnArray[] = $Item;

      }

      $URL=$_CFG['File']['UploadDirectory'] . "/reviewlist.xml";
      $File = fopen($URL,"w");

      $Content = ExportXML($ReviewExportList,$ReviewExportColumnArray,$_LANG['ReviewFields']);

      file_put_contents($URL, $Content);
      header('Content-type: text/xml; charset=utf-8');
      header('Content-Disposition: attachment; filename=reviewlist.xml');
      readfile($URL);
      exit;
   }
}
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('QueryID', $_REQUEST['QueryID']);
$TPL->assign('QueryTitle', $_SESSION['ReviewQueryTitle'] );
$TPL->display('ReviewList.tpl');
?>