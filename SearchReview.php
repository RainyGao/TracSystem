<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * search Review form.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

$FieldName = 'Field';
$OperatorName = 'Operator';
$ValueName = 'Value';
$AndOrName = 'AndOr';
$LeftParenthesesName = 'LeftParenthesesName';
$RightParenthesesName = 'RightParenthesesName';
$FieldList = array();
$OperatorList = array();
$ValueList = array();
$AndOrList = array();
$FieldCount = $_CFG['QueryFieldNumber'];
$Attrib = 'class="FullSelect"';


$FieldListSelectItem = array(0=>'ProjectName',1=>'OpenedBy',2=>'ModulePath',3=>'AssignedTo',4=>'ReviewID',5=>'ReviewTitle');
$OperatorListSelectItem = array(0=>'=', 2=>'LIKE', 5=>'LIKE');

if(!empty($_REQUEST['reset']))
{
    $_SESSION['ReviewFieldListCondition']='';
    $_SESSION['ReviewOperatorListCondition']='';
    $_SESSION['ReviewAndOrListCondition']='';
    $_SESSION['ReviewValueListCondition']='';
    $_SESSION['ReviewQueryCondition'] = '';
    $_SESSION['ReviewQueryTitle'] = '';
    $_SESSION['ReviewFieldsToShow'] = '';
    $_SESSION['ReviewLeftParenthesesListCondition']='';
    $_SESSION['ReviewRightParenthesesListCondition']='';
}
$UserList = testGetCurrentUserNameList('PreAppend');

$ProjectID = testGetCurrentProjectId();
if($ProjectID != null)
{
    $ProjectInfo = dbGetRow('TestProject', '', "ProjectID='{$ProjectID}'");
    $FieldSet = $ProjectInfo['FieldSet'];
    if(!empty($FieldSet))
    {
        $xml = simplexml_load_string($FieldSet);
        $fields = $xml->xpath('/fieldset/fields[@type="Review"]/field');
        if($fields)
        {
            $fields = sysFieldXmlToArr($fields);
            $fieldArr = array();
            $fieldNameArr = array();
            foreach($fields as $field)
            {
                $key = $field['name'];
                if($field['status'] != 'active')
                {
                    continue;
                }
                $_LANG['ReviewQueryField']["$key"] = $field['text'];
                if($field['type'] == 'select')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'mulit')
                {
                    $fieldArr["$key"]['key'] = $fieldArr["$key"]['value'] = explode(',', $field['value']);
                }
                if($field['type'] == 'user')
                {
                    $fieldArr["$key"]['value']   = array_keys($UserList);
                    $fieldArr["$key"]['key'] = array_values($UserList);
                }
                $fieldNameArr[] = $key;
            }
            if(!empty($fieldArr))
            {
                $AutoTextValue['CustomField'] = json_encode($fieldArr);
                $AutoTextValue['CustomFieldName'] = jsArray($fieldNameArr);
            }
        }
    }
}

if($_SESSION['ReviewFieldListCondition']!='')
  $FieldListSelectItem = $_SESSION['ReviewFieldListCondition'];
if($_SESSION['ReviewOperatorListCondition']!='')
  $OperatorListSelectItem = $_SESSION['ReviewOperatorListCondition'];
if($_SESSION['ReviewAndOrListCondition']!='')
  $SelectItem = $_SESSION['ReviewAndOrListCondition'];
if($_SESSION['ReviewValueListCondition']!='')
  $ValueListSelectItem = $_SESSION['ReviewValueListCondition'];

if($_SESSION['ReviewLeftParenthesesListCondition']!='')
  $LeftParenthesesSelectItem = $_SESSION['ReviewLeftParenthesesListCondition'];
if($_SESSION['ReviewRightParenthesesListCondition']!='')
  $RightParenthesesSelectItem = $_SESSION['ReviewRightParenthesesListCondition'];

if($_GET['QueryID'])
{
   $QueryInfo = dbGetRow('TestUserQuery', '', "QueryID='{$_REQUEST[QueryID]}' AND QueryType='Review'");
   $AndOr = $QueryInfo['QueryString'];


   if($QueryInfo['FieldList']!='')
       $FieldListSelectItem = unserialize($QueryInfo['FieldList']);
   if($QueryInfo['OperatorList']!='')
       $OperatorListSelectItem = unserialize($QueryInfo['OperatorList']);
   if($QueryInfo['AndOrList']!='')
       $SelectItem =  unserialize($QueryInfo['AndOrList']);
   if($QueryInfo['ValueList']!='')
       $ValueListSelectItem = unserialize($QueryInfo['ValueList']);
   if($QueryInfo['LeftParentheses']!='')
       $LeftParenthesesSelectItem = unserialize($QueryInfo['LeftParentheses']);
   if($QueryInfo['RightParentheses']!='')
       $RightParenthesesSelectItem = unserialize($QueryInfo['RightParentheses']);
}

if($_REQUEST['UpdateQueryID'])
{
   $QueryStr = addslashes($_SESSION['ReviewQueryCondition']);
   $AndOrListCondition = serialize($_SESSION['ReviewAndOrListCondition']);
   $OperatorListCondition = serialize($_SESSION['ReviewOperatorListCondition']);
   $ValueListCondition = mysql_real_escape_string(serialize($_SESSION['ReviewValueListCondition']));
   $FieldListCondition = serialize($_SESSION['ReviewFieldListCondition']);
   $FieldsToShow = implode(",",array_keys(testSetCustomFields('Review')));
   $LeftParenthesesCondition = serialize($_SESSION['ReviewLeftParenthesesNameListCondition']);
   $RightParenthesesCondition = serialize($_SESSION['ReviewRightParenthesesNameListCondition']);

   $ProjectID = testGetCurrentProjectId();
   dbUpdateRow('TestUserQuery', 'QueryString', "'{$QueryStr}'", 'AndOrList', "'{$AndOrListCondition}'", 'OperatorList',
                    "'$OperatorListCondition'", 'ValueList',"'$ValueListCondition'", 'FieldList',"'$FieldListCondition'",
           'FieldsToShow', "'$FieldsToShow'", 'ProjectID', "'$ProjectID'",
           'LeftParentheses',"'$LeftParenthesesCondition'",
           'RightParentheses',"'$RightParenthesesCondition'",
           "QueryID={$_REQUEST['UpdateQueryID']}");
   jsGoTo('ReviewList.php',"parent.RightBottomFrame");

}

$QueryFieldCount = count($ValueListSelectItem);
if($QueryFieldCount == 0) $QueryFieldCount = 1;
$searchConditionTmp = getSearchCondition("Review");
for($I=0; $I<$QueryFieldCount; $I ++)
{
    $FieldListOnChange = ' onchange="setQueryForm('.$I.');"';
    $OperatorListOnChange = ' onchange="setQueryValue('.$I.');"';
    $FieldList[$I] = htmlSelect($_LANG['ReviewQueryField'], $FieldName . $I, $Mode, $FieldListSelectItem[$I], $Attrib.$FieldListOnChange);
    $OperatorList[$I] = htmlSelect($_LANG['Operators'], $OperatorName . $I, $Mode, $OperatorListSelectItem[$I], $Attrib.$OperatorListOnChange);
    $ValueList[$I] = '<input id="'.$ValueName.$I.'" name="' . $ValueName.$I .'" type="text" size="5" style="width:95%;"/>';
    $AndOrList[$I] = htmlSelect($_LANG['AndOr'], $AndOrName . $I, $Mode, $SelectItem[$I], $Attrib);
    $LeftParentheses[$I] = htmlSelect($_LANG['LeftParentheses'], 
            $LeftParenthesesName . $I, $Mode, $LeftParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $RightParentheses[$I] = htmlSelect($_LANG['RightParentheses'], 
            $RightParenthesesName . $I, $Mode, $RightParenthesesSelectItem[$I],
            $Attrib." onchange=\"validateParentheses()\"");
    $AddRemoveLink[$I] = '<a href="javascript:;" onclick="addSearchField('.$I.');return false;" ><img src="Image/add_search.gif"/></a>&nbsp;&nbsp;<a href="javascript:;" onclick="removeSearchField('.$I.');return false;" ><img src="Image/cancel_search.gif"/></a>';
}

$SimpleProjectList = array(''=>'')+testGetValidSimpleProjectList();

$AutoTextValue['ProjectNameText']=jsArray($SimpleProjectList);
$AutoTextValue['ProjectNameValue']=jsArray($SimpleProjectList);
$AutoTextValue['SeverityText']=jsArray($_LANG['ReviewSeveritys']);
$AutoTextValue['SeverityValue']=jsArray($_LANG['ReviewSeveritys'], 'Key');
$AutoTextValue['PriorityText']=jsArray($_LANG['ReviewPriorities']);
$AutoTextValue['PriorityValue']=jsArray($_LANG['ReviewPriorities'], 'Key');
$AutoTextValue['TypeText']=jsArray($_LANG['ReviewTypes']);
$AutoTextValue['TypeValue']=jsArray($_LANG['ReviewTypes'], 'Key');
$AutoTextValue['StatusText']=jsArray($_LANG['ReviewStatus']);
$AutoTextValue['StatusValue']=jsArray($_LANG['ReviewStatus'], 'Key');
$AutoTextValue['OSText']=jsArray($_LANG['ReviewOS']);
$AutoTextValue['OSValue']=jsArray($_LANG['ReviewOS'], 'Key');
$AutoTextValue['BrowserText']=jsArray($_LANG['ReviewBrowser']);
$AutoTextValue['BrowserValue']=jsArray($_LANG['ReviewBrowser'], 'Key');
$AutoTextValue['MachineText']=jsArray($_LANG['ReviewMachine']);
$AutoTextValue['MachineValue']=jsArray($_LANG['ReviewMachine'], 'Key');
$AutoTextValue['ResolutionText']=jsArray($_LANG['ReviewResolutions']);
$AutoTextValue['ResolutionValue']=jsArray($_LANG['ReviewResolutions'], 'Key');
$AutoTextValue['HowFoundText']=jsArray($_LANG['ReviewHowFound']);
$AutoTextValue['HowFoundValue']=jsArray($_LANG['ReviewHowFound'], 'Key');
$AutoTextValue['SubStatusText']=jsArray($_LANG['ReviewSubStatus']);
$AutoTextValue['SubStatusValue']=jsArray($_LANG['ReviewSubStatus'], 'Key');

$AutoTextValue['FieldType'] = jsArray($_CFG['FieldType']);
$AutoTextValue['FieldOperationTypeValue'] = jsArray($_CFG['FieldTypeOperation']);
$AutoTextValue['FieldOperationTypeText'] = jsArray($_LANG['FieldTypeOperationName']);


$ACUserList = array(''=>'', 'Active' => 'Active') + $UserList+array('Closed'=>'Closed');
$UserList = array(''=>'') + $UserList;
$AutoTextValue['ACUserText']=jsArray($ACUserList);
$AutoTextValue['ACUserValue']=jsArray($ACUserList, 'Key');
$AutoTextValue['UserText']=jsArray($UserList);
$AutoTextValue['UserValue']=jsArray($UserList, 'Key');

$TPL->assign('OperatorListSelectItem', $OperatorListSelectItem);
$TPL->assign('ValueListSelectItem', $ValueListSelectItem);
$TPL->assign('QueryTitle', $_SESSION['ReviewQueryTitle']);


$TPL->assign('AutoTextValue', $AutoTextValue);
$TPL->assign("FieldList", $FieldList);
$TPL->assign("OperatorList", $OperatorList);
$TPL->assign("ValueList", $ValueList);
$TPL->assign("AndOrList", $AndOrList);
$TPL->assign("FieldCount",$FieldCount);

$TPL->assign('UserLang',$_CFG['UserLang']);
$TPL->assign("QueryFieldCount",$QueryFieldCount);
$TPL->assign("LeftParentheses",$LeftParentheses);
$TPL->assign("RightParentheses",$RightParentheses);
$TPL->assign("AddRemoveLink",$AddRemoveLink);
$TPL->assign('searchConditionTmp',$searchConditionTmp);
$TPL->display('Search.tpl');
?>
