<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * view, new, edit, resolve, close, activate a bug.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

sysXajaxRegister("xProjectSetSlaveModule,xSetModuleOwner,xCreateSelectDiv,xProjectSetAssignedUser,xDeleteTestFile,xFillCustomedField");
$DisplayValue = false;

$ModuleType = 'Bug';	//Change Review will use the same module with Bug
$ChangeID = $_REQUEST['ChangeID'];
$ActionType = $_REQUEST['ActionType'];
$Columns = '*';
$ChangeInfo = dbGetRow('ChangeInfo',$Columns,"ChangeID = '{$ChangeID}' AND {$_SESSION[TestUserACLSQL]} AND IsDroped = '0'");

if(!$ChangeInfo['ChangeID'] && $ActionType != 'OpenChange')
{
    sysErrorMsg();
}

if($ChangeID > 0 && $ActionType == 'OpenChange')
{
    $ProjectID = $ChangeInfo['ProjectID'];
    $ModuleID = $ChangeInfo['ModuleID'];
    unset($ChangeInfo['LastEditedBy']);
    unset($ChangeInfo['LastEditedDate']);
    unset($ChangeInfo['ResolvedBy']);
    unset($ChangeInfo['ResolvedBuild']);
    unset($ChangeInfo['ResolvedDate']);
    unset($ChangeInfo['ClosedBy']);
    unset($ChangeInfo['ClosedDate']);
}

if($ActionType == 'OpenChange')
{
    /* Create PrjectList And ModuleList */
    $ProjectID = $ProjectID != '' ? $ProjectID : $_SESSION['TestCurrentProjectID'];
    $ModuleID = $ModuleID != '' ? $ModuleID : $_SESSION['TestCurrentModuleID'];
    $ModuleInfo = dbGetRow('TestModule','*',"ModuleID = '{$ModuleID}'");
    $ChangeInfo['ChangeStatus'] = 'Active';
    $ChangeInfo['ChangeStatusName'] = $_LANG['ChangeStatus'][$ChangeInfo['ChangeStatus']];

    $PreChangeID = 0;
    $NextChangeID = 0;

    /* Fill the default repro steps */
    $DefaultChangeValue = array();
    $DefaultChangeValue = $_LANG['DefaultReproSteps'];
    $DefaultChangeValue['ExpectResult'] = '';
    $ModuleSelected = $ModuleID;

    if($_GET['BugID'] != '')
    {
        $BugID = $_GET['BugID'];
        $BugInfo = dbGetRow('BugInfo',$Columns,"BugID = '{$BugID}' AND {$_SESSION[TestUserACLSQL]}");
        if(!empty($BugInfo))
        {
            $ProjectID = $BugInfo['ProjectID'];
            $BugModuleID = $BugInfo['ModuleID'];
            $ModuleInfo = dbGetRow('TestModule','ModuleName',"ModuleID = '{$BugModuleID}'");
            $ModuleSelected = $ModuleInfo['ModuleName'];
            $ChangeInfo['ChangeTitle'] = $BugInfo['BugTitle'];
            $ChangeInfo['ChangeType'] = ConvertBugTypeToChangeType($BugInfo['BugType']);
            $ChangeInfo['ProjectID'] = $BugInfo['ProjectID'];
            $ChangeInfo['OpenedBuild'] = $BugInfo['OpenedBuild'];
            $ChangeInfo['ResolvedBuild'] = $BugInfo['ResolvedBuild'];
            $ChangeInfo['BugID'] = $BugID;
        }
    }
}
else
{
    /* Create PrjectList And ModuleList */
    $ProjectID = $ChangeInfo['ProjectID'];
    $ModuleID = $ChangeInfo['ModuleID'];
    $ModuleSelected = $ModuleID;
 		
 		//Get the originalBug's Title
    if(!empty($ChangeInfo['BugID']))
    {
        $BugInfo = dbGetRow('BugInfo',$Columns,"BugID = '{$ChangeInfo['BugID']}' AND {$_SESSION[TestUserACLSQL]}");
        $ChangeInfo['BugTitle'] = $BugInfo['BugTitle'];
    }

    if(empty($_SESSION['ChangeOrderBy']))
    {
        $_SESSION['ChangeOrderBy']['OrderBy'] = ' ChangeID DESC';
        $_SESSION['ChangeOrderBy']['OrderByColumn'] = 'ChangeID';
        $_SESSION['ChangeOrderBy']['OrderByType'] = 'DESC';
    }

    $WhereStr = "{$_SESSION['TestUserACLSQL']} AND {$_SESSION['ChangeQueryCondition']}";

   
    if('ChangeID' == $_SESSION['ChangeOrderBy']['OrderByColumn'])
    {
        $OrderByStr = "{$_SESSION['ChangeOrderBy']['OrderByColumn']} {$_SESSION['ChangeOrderBy']['OrderByType']}";
    }
    else
    {
        $OrderByStr = "{$_SESSION['ChangeOrderBy']['OrderByColumn']} {$_SESSION['ChangeOrderBy']['OrderByType']},ChangeID ASC";
    }
    
    $ChangeIdInfoList = dbGetList('ChangeInfo','ChangeID',$WhereStr,'',$OrderByStr,'');
    Rainy_Debug($ChangeIdInfoList,__FUNCTION__,__LINE__,__FILE__); 
    
    $PreNextIdArr = getPreNextId($ChangeIdInfoList,'ChangeID',$ChangeID);
    $PreChangeID = $PreNextIdArr[0];
    $NextChangeID = $PreNextIdArr[1];
   
    $UserList = testGetProjectUserNameList($ChangeInfo['ProjectID']);
    $ChangeInfo = testSetChangeMultiInfo($ChangeInfo, $UserList);
    $ChangeActionList = testGetActionAndFileList('Change', $ChangeInfo['ChangeID']);
    $LastActionID = testGetLastActionID('Change',$ChangeInfo['ChangeID']);
    $ChangeInfo['ActionList'] = $ChangeActionList['ActionList'];
    $ChangeInfo['FileList'] = $ChangeActionList['FileList'];
}

if($ActionType == 'Activated' && $ChangeInfo['ChangeStatus'] == 'Active')
{
    jsGoto('Change.php?ChangeID=' . $ChangeID);
}
if($ActionType == 'Resolved' && $ChangeInfo['ChangeStatus'] == 'Resolved')
{
    jsGoto('Change.php?ChangeID=' . $ChangeID);
}
if($ActionType == 'Closed' && $ChangeInfo['ChangeStatus'] == 'Closed')
{
    jsGoto('Change.php?ChangeID=' . $ChangeID);
}

if($ActionType == 'OpenChange')
{
    $ChangeID = -1;
}

$OnChangeStr = 'onchange="';
$OnChangeStr .= 'xajax_xProjectSetSlaveModule(this.value, \'SlaveModuleList\', \'ModuleID\', \'Change\');';
$OnChangeStr .= 'xajax_xProjectSetAssignedUser(this.value);';
$OnChangeStr .= 'xajax_xFillCustomedField(\'CustomedFieldSet\', this.value, \'Change\', ' . $ChangeID . ');';
$OnChangeStr .= '"';
$OnChangeStr .= ' class="MyInput RequiredField"';
$ProjectListSelect = testGetValidProjectSelectList('ProjectID', $ProjectID, $OnChangeStr);

$OnChangeStr = '';
if($ActionType == 'OpenChange')
{
    $OnChangeStr = 'onchange="';
    $OnChangeStr .= 'xajax_xSetModuleOwner(this.value);';
    $OnChangeStr .= '"';
}
$OnChangeStr .= ' class="MyInput RequiredField"';
$ModuleSelectList = testGetSelectModuleList($ProjectID, 'ModuleID', $ModuleSelected, $OnChangeStr, $ModuleType);

$ProjectUserList = testGetProjectUserList($ProjectID, true);
//$ProjectUserList['Closed'] = 'Closed';

if($ActionType == 'OpenChange')
{
    $ActionTypeName = $_LANG['OpenChange'];
    $AssignedTo = $ModuleInfo['ModuleOwner'] != '' ? $ModuleInfo['ModuleOwner'] : $ChangeInfo['AssignedTo'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$AssignedTo, 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == '')
{
    $DisplayValue = true;
    $ActionTypeName = $_LANG['ViewChange'];
}
elseif($ActionType == 'Edited')
{
    $ActionTypeName = $_LANG['EditChange'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$ChangeInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Activated')
{
    $ActionTypeName = $_LANG['ActiveChange'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$ChangeInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Resolved')
{
    $ActionTypeName = $_LANG['ResolveChange'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$ChangeInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
}
elseif($ActionType == 'Closed')
{
    $ActionTypeName = $_LANG['CloseChange'];
    $SelectAssignUserList = htmlSelect($ProjectUserList, 'AssignedTo', '',$ChangeInfo['AssignedTo'], 'class="NormalSelect MyInput RequiredField"');
}

$ChangeTypeSelectList = htmlSelect($_LANG['ChangeTypes'], 'ChangeType', '', $ChangeInfo['ChangeType'], 'class="MyInput RequiredField"');

$CustomedFieldHtml = testGetCustomedFieldHtml($ProjectID, 'Change', $ChangeInfo['ChangeID'], $DisplayValue);

if($ActionType == 'OpenChange')
{
    $TPL->assign('HeaderTitle', $_LANG['OpenChange']);
}
else
{
    $TPL->assign('HeaderTitle', 'Change #' . $ChangeInfo['ChangeID'] . ' ' . $ChangeInfo['ChangeTitle']);
}
$ConfirmArray = array('ReplyNote'=>'ReplyNote') + $_LANG['ChangeFields'];
$TPL->assign('ConfirmIds', jsArray($ConfirmArray, 'Key'));

$TPL->assign('ChangeInfo', $ChangeInfo);
$TPL->assign('PreChangeID', $PreChangeID);
$TPL->assign('NextChangeID', $NextChangeID);

$TPL->assign('ProjectID', $ProjectID);
$TPL->assign('ModuleID', $ModuleID);
$TPL->assign('ProjectList', $ProjectListSelect);
$TPL->assign('ModuleList', $ModuleSelectList);
$TPL->assign('ChangeTypeList', $ChangeTypeSelectList);
$TPL->assign('ProjectUserList', $SelectUserList);
$TPL->assign('AssignedToUserList', $SelectAssignUserList);
$TPL->assign('OpenedByUserList', $SelectOpenUserList);
$TPL->assign('ActionType', $ActionType);
$TPL->assign('ActionTypeName', $ActionTypeName);
$TPL->assign('ActionRealName', $_SESSION['TestRealName']);
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('ActionDate', date('Y-m-d'));
$TPL->assign('DefaultChangeValue', $DefaultChangeValue);
$TPL->assign('LastActionID',$LastActionID);
$TPL->assign('ChangeFields', $_LANG['ChangeFields']);
$TPL->assign('CustomedFieldHtml', $CustomedFieldHtml);

$TPL->display('Change.tpl');
?>
