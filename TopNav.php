<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * top navigation.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

sysXajaxRegister("xSetTestMode,xSetCurrentProject");

$OnChangeStr  = 'onchange="';
// load search panel
$OnChangeStr .= 'parent.RightTopFrame.location=\'Search' . $_SESSION['TestMode'] . '.php?reset=1&ProjectID=\'+this.value;';
// load module list
$OnChangeStr .= 'parent.LeftTopFrame.location=\'ModuleList.php?ProjectID=\'+this.value;';
// load list
$OnChangeStr .= 'parent.RightBottomFrame.location=\'' . $_SESSION['TestMode'] . 'List.php?ProjectID=\'+this.value;';
// set current project
$OnChangeStr .= 'xajax_xSetCurrentProject(this.value);';
$OnChangeStr .= '"';

$projectId = testGetCurrentProjectId(true);
$ProjectListSelect = testGetValidProjectSelectList('TopNavProjectList', $projectId, $OnChangeStr);

$TPL->assign('TestRealName', $_SESSION['TestRealName']);
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('TestIsProjectAdmin', $_SESSION['TestIsProjectAdmin']);
$TPL->assign('TopNavProjectList', $ProjectListSelect);

$TPL->assign('TestMode', $_SESSION['TestMode']);
$TPL->display('TopNav.tpl');
?>																																																																																											