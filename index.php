<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * index.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */

/*加载Init.inc.php，用于初始化BugFree系统，配置方面主要将Config.inc.php中的_CFG和_COMMON.php中的_LANG赋给CFG和Lang变量*/
/* Init BugFree system. */
require('Include/Init.inc.php');

/*检查TestMode变量，in_arrary用来检查变量是否在数据里面*/
/* Set the URL to display in RightBottom. */
$TestMode = in_array($_GET['TestMode'], array('Bug','Change','Review','ReviewComment')) ? $_GET['TestMode'] : $_SESSION['TestMode'];
$TestMode = in_array($_GET['TestMode'], array('Bug','Change','Review','ReviewComment')) ? $TestMode : 'Bug';
$TestMode =  $TestMode == '' ? 'Bug' : $TestMode;
$_SESSION['TestMode'] = $TestMode;

/* Assign. */
$TPL->assign('TestMode', $TestMode);
$TPL->assign('CloseLeftFrame', $_COOKIE['CloseLeftFrame']);

/* Display. */
$TPL->display('Index.tpl');
?>
