<?php
require('Include/Class/XmlParse.class.php');

/** XML File Export Implementation*/
function ExportXML($CaseExportList,$CaseExportColumnArray,$FieldsArray){
	$RowCount = count($CaseExportList)+1;
	$ColumnCount = count($CaseExportColumnArray);

	$Content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                 <?mso-application progid=\"Excel.Sheet\"?>
                 <Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"
                 xmlns:o=\"urn:schemas-microsoft-com:office:office\"
                 xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
                 xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"
                 xmlns:html=\"http://www.w3.org/TR/REC-html40\">
                 <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">
                  <Created>1996-12-17T01:32:42Z</Created>
                  <LastSaved>2009-11-21T14:55:15Z</LastSaved>
                  <Version>11.9999</Version>
                 </DocumentProperties>
                 <OfficeDocumentSettings xmlns=\"urn:schemas-microsoft-com:office:office\">
                  <RemovePersonalInformation/>
                 </OfficeDocumentSettings>
                 <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">
                  <WindowHeight>4530</WindowHeight>
                  <WindowWidth>8505</WindowWidth>
                  <WindowTopX>480</WindowTopX>
                  <WindowTopY>120</WindowTopY>
                  <AcceptLabelsInFormulas/>
                  <ProtectStructure>False</ProtectStructure>
                  <ProtectWindows>False</ProtectWindows>
                 </ExcelWorkbook>
                 <Styles>
                  <Style ss:ID=\"Default\" ss:Name=\"Normal\">
                   <Alignment ss:Vertical=\"Bottom\"/>
                   <Borders/>
                   <Font ss:FontName=\"\" x:CharSet=\"134\" ss:Size=\"12\"/>
                   <Interior/>
                   <NumberFormat/>
                   <Protection/>
                  </Style>
                  <Style ss:ID=\"s21\">
                   <Alignment ss:Vertical=\"Bottom\" ss:WrapText=\"1\"/>
                  </Style>
                 </Styles>
                 <Worksheet ss:Name=\"Sheet1\">
                  <Table ss:ExpandedColumnCount=\"".$ColumnCount ."\" ss:ExpandedRowCount=\"" . $RowCount ."\" x:FullColumns=\"1\"
                   x:FullRows=\"1\" ss:DefaultColumnWidth=\"54\" ss:DefaultRowHeight=\"14.25\">";

   	$TempStr = "\n<Row>";
   	foreach($CaseExportColumnArray as $ExportItem)
   	{ 
       		$TempStr .=" \n<Cell><Data ss:Type=\"String\">" . $FieldsArray[$ExportItem] . "</Data></Cell>\n";
   	}
   	$TempStr .= "</Row>\n";
   	$arr_search = array('<','>',"'",'&', '"',"\n");
   	$arr_replace = array('&lt;','&gt;','&apos;','&amp;','&quot;', '&#10;');
        $arr_search1 = array('&amp;lt;','&amp;gt;','&amp;quot;','&amp;amp;','&amp;apos;');
   	$arr_replace1 = array('&lt;','&gt;','&quot;', '&amp;','&apos;');
 
    
   	foreach($CaseExportList as $CaseItem)
   	{
       		$TempStr .= "\n<Row>";
       		foreach($CaseExportColumnArray as $Column)
       		{         
         	   $TempStr .=" \n<Cell><Data ss:Type=\"String\">" . str_ireplace($arr_search1,$arr_replace1,str_ireplace($arr_search,$arr_replace,$CaseItem[$Column])) . "</Data></Cell> \n";
       		}
       		$TempStr .= "</Row>\n";
   	}

       
	$Content .= $TempStr;
	$Content .= "</Table>
                  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">
                   <Selected/>
                   <Panes>
                    <Pane>
                     <Number>3</Number>
                     <ActiveRow>1</ActiveRow>
                     <ActiveCol>1</ActiveCol>
                    </Pane>
                   </Panes>
                   <ProtectObjects>False</ProtectObjects>
                   <ProtectScenarios>False</ProtectScenarios>
                  </WorksheetOptions>
                 </Worksheet>
		 </Workbook>";
	return $Content;
}

/* XML File Import Implementation*/
function ParseDomRowArrary($DomRowArray,&$ImportFields,&$ImportItemList)
{
	   $ImportHeader = true;
     foreach($DomRowArray as $Key=>$Value)
     { 
     		//Parse Dom
        if(strpos($Key, 'attr')===false)
        {
	  				if($ImportHeader===true)
	  				{ 
	  						//Import Fields
             		foreach($Value['Cell'] as $CellKey => $CellValue)
             		{
                		if(strpos($CellKey, 'attr')===false)
                		{
                   			if($CellValue['Data']!==NULL)
                   			{
                      			array_push($ImportFields,$CellValue['Data']);
                   			}
                   			if($CellValue['ss:Data']!==NULL)
                      	{	
                      			array_push($ImportFields,$CellValue['ss:Data']);
                   			}
                   			$ImportHeader = false;
                		}
                }
            }
          	else
          	{ //Import Datum
            		$Temp = array();
             		foreach($Value['Cell'] as $CellKey => $CellValue)
             		{
	        				if(strpos($CellKey, 'attr')===false)
	        				{
		   							if($CellValue['Data']!==NULL)
		      						array_push($Temp,$CellValue['Data']);
		   							elseif( $CellValue['ss:Data']!==NULL)
		      						array_push($Temp,$CellValue['ss:Data']);
	         				}
	      				}
              	array_push($ImportItemList,$Temp);
           	}
        }
     }// end for
}

/************ Bug Import Functions **************************/
/*Convert Functions For XML Fields to DB Fields*/
function ConvertXMLFieldsToDBFields($XMLFields,$DefinedDBFields)
{
	$DBFields = array();
	foreach($XMLFields as $Item)
	{
			foreach($DefinedDBFields as $Key => $Value)
			{
					if($Item == $Value)
					{
						array_push($DBFields,$Key);
						break;
					}
			}
	}
	return $DBFields;
}

/*Get MustImport Fields*/
function GetMustImportFields($ObjType,$_LANG)
{	
	switch($ObjType)
	{
		case 'Bug':
			$MustImportFields = $_LANG['BugMustImportFields'];
			break;
		case 'Change':
			$MustImportFields = $_LANG['ChangeMustImportFields'];
			break;
		case 'Review':
			$MustImportFields = $_LANG['ReviewMustImportFields'];
			break;
		case 'Case':
			$MustImportFields = $_LANG['CaseMustImportFields'];
			break;
		case 'User':
			$MustImportFields = $_LANG['UserMustImportFields'];
			break;
		case 'Project':
			$MustImportFields = $_LANG['ProjectMustImportFields'];
			break;
  }
	Rainy_Debug($MustImportFields,__FUNCTION__,__LINE__,__FILE__);
	return $MustImportFields;
}

/*DefinedDBFields which was not in MustImportFields*/
function GetOptionalImportFields($ObjType,$_LANG)
{
	//Get DefinedDBFields
	switch($ObjType)
	{
		case 'Bug':
			$DefinedDBFields = $_LANG['BugFields'];
			break;
		case 'Change':
			$DefinedDBFields = $_LANG['ChangeFields'];
			break;
		case 'Review':
			$DefinedDBFields = $_LANG['ReviewFields'];
			break;
		case 'Case':
			$DefinedDBFields = $_LANG['CaseFields'];
			break;
		case 'User':
			$DefinedDBFields = $_LANG['UserFields'];
			break;
		case 'Project':
			$DefinedDBFields = $_LANG['ProjectFields'];
			break;
  }
	//Get MustImportFields
	$MustImportFields = GetMustImportFields($ObjType,$_LANG);
	
	//Get OptinalImportFileds
	$OptionalImportFields = array();
	foreach($DefinedDBFields as $Key => $Value)
	{
			if(!in_array($Key,$MustImportFields))
			{
					array_push($OptionalImportFields,$Key);
			}
	}
	Rainy_Debug($OptionalImportFields,__FUNCTION__,__LINE__,__FILE__);
	return $OptionalImportFields;	
}

/*MustImportFields Check*/
function MustImportFieldsCheck($ImportFields,$MustImportFields,$_LANG)
{
	    foreach($MustImportFields as $ColumnItem)
	    {  
	    	if(!in_array($ColumnItem,$ImportFields))
	    	{
            sysObFlushJs("alert('{$ColumnItem}{$_LANG['ImportColumnNotNull']}');");
            return false;
        }
      }	
      return true;
}

/*Get IDFields ColumnNumber*/
function GetFieldColumnNumber($FieldName,$ImportFields)
{
		$ColumnNumber = array_search($FieldName,$ImportFields);
		return 	$ColumnNumber;
}

/*Filter out the ImportFields wiht MustImportFields and OptionalImportFields*/
function FilterImportFields($ImportFields,$MustImportFields,$OptionalImportFields)
{
	$FilteredImportFields = array();
	
	//Filter the ImportFields with the MustImportFields and OptionalImportFields,
  if($ImportFields)
  {
  	//$Key是列序号,Value是列的名称
  	foreach($ImportFields as $Key => $Value)
   	{    
      if(in_array(trim($Value),$MustImportFields) || in_array(trim($Value),$OptionalImportFields))
      {
          $FilteredImportFields[$Key] = $Value;
      } 	
    }
	}
	return 	$FilteredImportFields;
}

/* Get ProjectID by ProjectName*/
function GetProjectIDByName($ProjectName)
{
		$ProjectInfo = dbGetRow('TestProject', 'ProjectID', "ProjectName='{$ProjectName}'");
		$ProjectID = $ProjectInfo['ProjectID'];
		if(empty($ProjectID))
		{
			$ProjectID = '1';
		}
		return $ProjectID;
}

/* Get ModuleID by ModulePath*/
function GetModuleIDByPath($ModulePath,$ProjectID,$ModuleType)
{
		$ModuleID = '';
    $ModuleListArray = testGetProjectModuleList($ProjectID,$ModuleType);
    foreach($ModuleListArray as $Item)
    {
         if(trim($ModulePath) == trim($Item['NamePath']))
         {
             $ModuleID = $Item['ModuleID'];
             break;
         }
    }
    if(empty($ModuleID))
		{
			$ModuleID = '0';
		}
    return $ModuleID;
}

function BuildPostEditBugInfo($ImportFields,$ImportItem,$RawBugInfo)
{
	$PostBugInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    $FieldValue = trim($ImportItem[$Key]);
    $PostBugInfo[$FieldName] = $FieldValue;
  }
	
	if(empty($PostBugInfo['ProjectID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ProjectName',$ImportFields);
			$PostBugInfo['ProjectID'] = GetProjectIDByName($ImportItem[$ColumnNumber]);
	}
	
	if(!isset($PostBugInfo['ModuleID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ModulePath',$ImportFields);
			$PostBugInfo['ModuleID'] = GetModuleIDByPath($ImportItem[$ColumnNumber],$PostBugInfo['ProjectID'],'Bug');
	}
  
  $PostBugInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  $PostBugInfo['LastActionID'] = testGetLastActionID('Bug',$RawBugInfo['BugID']);
  Rainy_Debug($PostBugInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostBugInfo;
}

function BuildPostAddBugInfo($ImportFields,$ImportItem)
{
	$PostBugInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    if($FieldName == 'BugID')
    {
    		continue;
    }
    
    $FieldValue = trim($ImportItem[$Key]);
    $PostBugInfo[$FieldName] = $FieldValue;
  }
	
	if(empty($PostBugInfo['ProjectID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ProjectName',$ImportFields);
			$PostBugInfo['ProjectID'] = GetProjectIDByName($ImportItem[$ColumnNumber]);
	}
	
	if(!isset($PostBugInfo['ModuleID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ModulePath',$ImportFields);
			$PostBugInfo['ModuleID'] = GetModuleIDByPath($ImportItem[$ColumnNumber],$PostBugInfo['ProjectID'],'Bug');
	}
  
  $PostBugInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  Rainy_Debug($PostBugInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostBugInfo;
}

/* Update BugInfo of Bug with ID:BigID*/
function UpdateBugInfo($BugID,$ImportFields,$ImportItem)
{
	//Get RawBugInfo
	$RawBugInfo = GetRawBugInfo($BugID);
	                        
	//Check And Set The Import Field Value 
	$PostBugInfo = BuildPostEditBugInfo($ImportFields,$ImportItem,$RawBugInfo);

  //Update the BugInfo                        
  $ActionMsg = testEditBug($PostBugInfo);
  unset($PostBugInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Add a new Bug In DB*/
function AddNewBug($ImportFields,$ImportItem)
{
	$PostBugInfo = BuildPostAddBugInfo($ImportFields,$ImportItem);
	
  $ActionMsg = testOpenBug($PostBugInfo);

  unset($PostBugInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Import the BugList to the DB*/
function ImportBugs($ImportItemList,$ImportFields,$ImportFieldArray,$BugIDColumn,$_LANG)
{
        $ImportTotalItem = 0;
        $ImportSuccessItem = 0;
        $ImportFailItem = 0;
        $ImportAddItem = 0;
        $ImportUpdateItem = 0;
        $ErrorMsg = '';
				$FieldCount = count($ImportFields);
				$ItemFieldCount = 0;
				
         foreach($ImportItemList as $ImportItemListKey => $ImportItemListValue)
         {     
              $ImportTotalItem = $ImportTotalItem + 1;
              
              //Confirm if ImportItem is NULL
              if(!$ImportItemListValue)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' failed:[ImportItem is empty or with empty field(s)]\n';		
              		Rainy_Debug("Import row [{$ImportTotalItem}] Failed: ImportItemListValue is NULL",__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get BugID
              $BugID = $ImportItemListValue[$BugIDColumn];
              
              //Check The ImportItem Field Count
              $ItemFieldCount = count($ImportItemListValue);
              if($FieldCount != $ItemFieldCount)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' BugID['.$BugID.'] failed:[ImportItem have empty field(s)]\n';
              		Rainy_Debug("Import row [{$ImportTotalItem}] BugID [{$BugID}] Failed: ItemFieldCount ='{$ItemFieldCount}' [$FieldCount]",__FUNCTION__,__LINE__,__FILE__);				
              		//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get BugInfo by BugID
              $BugInfo = GetRawBugInfo($BugID);
              if($BugInfo)
              {
              		//BugID存在，则更新Bug相关消息
                  if(UpdateBugInfo($BugID,$ImportFieldArray,$ImportItemListValue) == true)
                  {
                  		$ImportSuccessItem = $ImportSuccessItem + 1;          
                  		$ImportUpdateItem = $ImportUpdateItem + 1;
                  }
                  else
                  {
                  		$ErrorMsg .= 'Import row '.$ImportTotalItem.' BugID['.$BugID.'] failed:[UpdateBugInfo()]\n';
                  		Rainy_Debug("Import row [{$ImportTotalItem}] BugID [{$BugID}] Failed: UpdateBugInfo()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
                  }
              }//end edit bug
		      		else 
		      		{
		      				//BugID不存在，则新增Bug         
		 	        		if(AddNewBug($ImportFieldArray,$ImportItemListValue) == true)
		 	            {
		 	            		$ImportSuccessItem = $ImportSuccessItem + 1;          
 			            		$ImportAddItem = $ImportAddItem + 1; 
		 	            }
		 	            else
		 	            {
		 	            		$ErrorMsg .= 'Import row '.$ImportTotalItem.' BugID['.$BugID.'] failed:[AddNewBug()]\n';
		 	            		Rainy_Debug("Import row [{$ImportTotalItem}] BugID [{$BugID}] Failed: AddNewBug()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
		 	            }
              }
         }
         $ImportFailItem = $ImportTotalItem-$ImportSuccessItem;
         $FinishAlert = str_replace('{param0}',$ImportTotalItem, $_LANG['BugImportFinished']);
         $FinishAlert = str_replace('{param1}',$ImportSuccessItem, $FinishAlert);
         $FinishAlert = str_replace('{param2}',$ImportFailItem, $FinishAlert);
         $FinishAlert = str_replace('{param3}',$ImportAddItem, $FinishAlert);
         $FinishAlert = str_replace('{param4}',$ImportUpdateItem, $FinishAlert);
         if("" != $ErrorMsg)
         {
                $ErrorMsg = '\n'.$ErrorMsg;
         }
         //Rainy_Debug($ErrorMsg,__FUNCTION__,__LINE__,__FILE__);
         sysObFlushJs("alert('{$FinishAlert}{$ErrorMsg}');");
}

function ParseImportBugXML($DomRowArray,$_LANG)
{
		 	//Parse DomRowArray to ImportFields and ImportItemList
		 	$ImportXMLFields = array();
     	$ImportItemList = array();
     	ParseDomRowArrary($DomRowArray,&$ImportXMLFields,&$ImportItemList);
			Rainy_Debug($ImportXMLFields,__FUNCTION__,__LINE__,__FILE__);
			
  		//Conver XML Fields to DB Fields
      $ImportFields = ConvertXMLFieldsToDBFields($ImportXMLFields,$_LANG['BugFields']);
      Rainy_Debug($ImportFields,__FUNCTION__,__LINE__,__FILE__);
      
     	//Get the MustImportFields and Optional Import Fields
     	$MustImportFields = GetMustImportFields('Bug',$_LANG);
     	$OptionalImportFields = GetOptionalImportFields('Bug',$_LANG);
     	
			//MustImportFields Check
			$Importable = MustImportFieldsCheck($ImportFields,$MustImportFields,$_LANG);
			
			//Start to Import
      if($Importable)
      {
      	//Get BugID Field's Column Number in ImportFields
      	$BugIDColumn = GetFieldColumnNumber('BugID',$ImportFields);
      	//Filter the ImportFields with the MustImportFields and OptionalImportFields,Result was in ImportFieldArray
        $ImportFieldArray = FilterImportFields($ImportFields,$MustImportFields,$OptionalImportFields);
				Rainy_Debug($ImportFieldArray,__FUNCTION__,__LINE__,__FILE__);
				
				ImportBugs($ImportItemList,$ImportFields,$ImportFieldArray,$BugIDColumn,$_LANG);
      }
      fclose($file);

      sysObFlush(jsGoto("BugList.php"));
      exit;
}
/************ Case Import Functions **************************/
function BuildPostEditCaseInfo($ImportFields,$ImportItem,$RawCaseInfo)
{
	$PostCaseInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    $FieldValue = trim($ImportItem[$Key]);
    $PostCaseInfo[$FieldName] = $FieldValue;
  }
	
	if(empty($PostCaseInfo['ProjectID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ProjectName',$ImportFields);
			$PostCaseInfo['ProjectID'] = GetProjectIDByName($ImportItem[$ColumnNumber]);
	}
	
	if(!isset($PostCaseInfo['ModuleID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ModulePath',$ImportFields);
			$PostCaseInfo['ModuleID'] = GetModuleIDByPath($ImportItem[$ColumnNumber],$PostCaseInfo['ProjectID'],'Case');
	}
  
  $PostCaseInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  $PostCaseInfo['LastActionID'] = testGetLastActionID('Case',$RawCaseInfo['CaseID']);
  Rainy_Debug($PostCaseInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostCaseInfo;
}

function BuildPostAddCaseInfo($ImportFields,$ImportItem)
{
	$PostCaseInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    if($FieldName == 'CaseID')
    {
    		continue;
    }
    
    $FieldValue = trim($ImportItem[$Key]);
    $PostCaseInfo[$FieldName] = $FieldValue;
  }
	
	if(empty($PostCaseInfo['ProjectID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ProjectName',$ImportFields);
			$PostCaseInfo['ProjectID'] = GetProjectIDByName($ImportItem[$ColumnNumber]);
	}
	
	if(!isset($PostCaseInfo['ModuleID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ModulePath',$ImportFields);
			$PostCaseInfo['ModuleID'] = GetModuleIDByPath($ImportItem[$ColumnNumber],$PostCaseInfo['ProjectID'],'Case');
	}
  
  $PostCaseInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  Rainy_Debug($PostCaseInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostCaseInfo;
}

/* Update CaseInfo of Case with ID:BigID*/
function UpdateCaseInfo($CaseID,$ImportFields,$ImportItem)
{
	//Get RawCaseInfo
	$RawCaseInfo = GetRawCaseInfo($CaseID);
	                        
	//Check And Set The Import Field Value 
	$PostCaseInfo = BuildPostEditCaseInfo($ImportFields,$ImportItem,$RawCaseInfo);

  //Update the CaseInfo                        
  $ActionMsg = testEditCase($PostCaseInfo);
  unset($PostCaseInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Add a new Case In DB*/
function AddNewCase($ImportFields,$ImportItem)
{
	$PostCaseInfo = BuildPostAddCaseInfo($ImportFields,$ImportItem);
	
  $ActionMsg = testOpenCase($PostCaseInfo);

  unset($PostCaseInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Import the CaseList to the DB*/
function ImportCases($ImportItemList,$ImportFields,$ImportFieldArray,$CaseIDColumn,$_LANG)
{
        $ImportTotalItem = 0;
        $ImportSuccessItem = 0;
        $ImportFailItem = 0;
        $ImportAddItem = 0;
        $ImportUpdateItem = 0;
        $ErrorMsg = '';
				$FieldCount = count($ImportFields);
				$ItemFieldCount = 0;
				
         foreach($ImportItemList as $ImportItemListKey => $ImportItemListValue)
         {     
              $ImportTotalItem = $ImportTotalItem + 1;
              
              //Confirm if ImportItem is NULL
              if(!$ImportItemListValue)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' failed:[ImportItem is empty or with empty field(s)]\n';		
              		Rainy_Debug("Import row [{$ImportTotalItem}] Failed: ImportItemListValue is NULL",__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get CaseID
              $CaseID = $ImportItemListValue[$CaseIDColumn];
              
              //Check The ImportItem Field Count
              $ItemFieldCount = count($ImportItemListValue);
              if($FieldCount != $ItemFieldCount)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' CaseID['.$CaseID.'] failed:[ImportItem have empty field(s)]\n';
              		Rainy_Debug("Import row [{$ImportTotalItem}] CaseID [{$CaseID}] Failed: ItemFieldCount ='{$ItemFieldCount}' [$FieldCount]",__FUNCTION__,__LINE__,__FILE__);				
              		//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get CaseInfo by CaseID
              $CaseInfo = GetRawCaseInfo($CaseID);
              if($CaseInfo)
              {
              		//CaseID存在，则更新Case相关消息
                  if(UpdateCaseInfo($CaseID,$ImportFieldArray,$ImportItemListValue) == true)
                  {
                  		$ImportSuccessItem = $ImportSuccessItem + 1;          
                  		$ImportUpdateItem = $ImportUpdateItem + 1;
                  }
                  else
                  {
                  		$ErrorMsg .= 'Import row '.$ImportTotalItem.' CaseID['.$CaseID.'] failed:[UpdateCaseInfo()]\n';
                  		Rainy_Debug("Import row [{$ImportTotalItem}] CaseID [{$CaseID}] Failed: UpdateCaseInfo()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
                  }
              }//end edit bug
		      		else 
		      		{
		      				//CaseID不存在，则新增Case         
		 	        		if(AddNewCase($ImportFieldArray,$ImportItemListValue) == true)
		 	            {
		 	            		$ImportSuccessItem = $ImportSuccessItem + 1;          
 			            		$ImportAddItem = $ImportAddItem + 1; 
		 	            }
		 	            else
		 	            {
		 	            		$ErrorMsg .= 'Import row '.$ImportTotalItem.' CaseID['.$CaseID.'] failed:[AddNewCase()]\n';
		 	            		Rainy_Debug("Import row [{$ImportTotalItem}] CaseID [{$CaseID}] Failed: AddNewCase()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
		 	            }
              }
         }
         $ImportFailItem = $ImportTotalItem-$ImportSuccessItem;
         $FinishAlert = str_replace('{param0}',$ImportTotalItem, $_LANG['CaseImportFinished']);
         $FinishAlert = str_replace('{param1}',$ImportSuccessItem, $FinishAlert);
         $FinishAlert = str_replace('{param2}',$ImportFailItem, $FinishAlert);
         $FinishAlert = str_replace('{param3}',$ImportAddItem, $FinishAlert);
         $FinishAlert = str_replace('{param4}',$ImportUpdateItem, $FinishAlert);
         if("" != $ErrorMsg)
         {
                $ErrorMsg = '\n'.$ErrorMsg;
         }
         //Rainy_Debug($ErrorMsg,__FUNCTION__,__LINE__,__FILE__);
         sysObFlushJs("alert('{$FinishAlert}{$ErrorMsg}');");
}

function ParseImportCaseXML($DomRowArray,$_LANG)
{
		 	//Parse DomRowArray to ImportFields and ImportItemList
		 	$ImportXMLFields = array();
     	$ImportItemList = array();
     	ParseDomRowArrary($DomRowArray,&$ImportXMLFields,&$ImportItemList);
			Rainy_Debug($ImportXMLFields,__FUNCTION__,__LINE__,__FILE__);
			
  		//Conver XML Fields to DB Fields
      $ImportFields = ConvertXMLFieldsToDBFields($ImportXMLFields,$_LANG['CaseFields']);
      Rainy_Debug($ImportFields,__FUNCTION__,__LINE__,__FILE__);
      
     	//Get the MustImportFields and Optional Import Fields
     	$MustImportFields = GetMustImportFields('Case',$_LANG);
     	$OptionalImportFields = GetOptionalImportFields('Case',$_LANG);
     	
			//MustImportFields Check
			$Importable = MustImportFieldsCheck($ImportFields,$MustImportFields,$_LANG);
			
			//Start to Import
      if($Importable)
      {
      	//Get CaseID Field's Column Number in ImportFields
      	$CaseIDColumn = GetFieldColumnNumber('CaseID',$ImportFields);
      	//Filter the ImportFields with the MustImportFields and OptionalImportFields,Result was in ImportFieldArray
        $ImportFieldArray = FilterImportFields($ImportFields,$MustImportFields,$OptionalImportFields);
				Rainy_Debug($ImportFieldArray,__FUNCTION__,__LINE__,__FILE__);
				
				ImportCases($ImportItemList,$ImportFields,$ImportFieldArray,$CaseIDColumn,$_LANG);
      }
      fclose($file);

      sysObFlush(jsGoto("CaseList.php"));
      exit;
}

/************ Change Import Functions **************************/
function BuildPostEditChangeInfo($ImportFields,$ImportItem,$RawChangeInfo)
{
	$PostChangeInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    $FieldValue = trim($ImportItem[$Key]);
    $PostChangeInfo[$FieldName] = $FieldValue;
  }
	
	if(empty($PostChangeInfo['ProjectID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ProjectName',$ImportFields);
			$PostChangeInfo['ProjectID'] = GetProjectIDByName($ImportItem[$ColumnNumber]);
	}
	
	if(!isset($PostChangeInfo['ModuleID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ModulePath',$ImportFields);
			$PostChangeInfo['ModuleID'] = GetModuleIDByPath($ImportItem[$ColumnNumber],$PostChangeInfo['ProjectID'],'Change');
	}
  
  $PostChangeInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  $PostChangeInfo['LastActionID'] = testGetLastActionID('Change',$RawChangeInfo['ChangeID']);
  Rainy_Debug($PostChangeInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostChangeInfo;
}

function BuildPostAddChangeInfo($ImportFields,$ImportItem)
{
	$PostChangeInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    if($FieldName == 'ChangeID')
    {
    		continue;
    }
    
    $FieldValue = trim($ImportItem[$Key]);
    $PostChangeInfo[$FieldName] = $FieldValue;
  }
	
	if(empty($PostChangeInfo['ProjectID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ProjectName',$ImportFields);
			$PostChangeInfo['ProjectID'] = GetProjectIDByName($ImportItem[$ColumnNumber]);
	}
	
	if(!isset($PostChangeInfo['ModuleID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ModulePath',$ImportFields);
			$PostChangeInfo['ModuleID'] = GetModuleIDByPath($ImportItem[$ColumnNumber],$PostChangeInfo['ProjectID'],'Change');
	}
  
  $PostChangeInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  Rainy_Debug($PostChangeInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostChangeInfo;
}

/* Update ChangeInfo of Change with ID:BigID*/
function UpdateChangeInfo($ChangeID,$ImportFields,$ImportItem)
{
	//Get RawChangeInfo
	$RawChangeInfo = GetRawChangeInfo($ChangeID);
	                        
	//Check And Set The Import Field Value 
	$PostChangeInfo = BuildPostEditChangeInfo($ImportFields,$ImportItem,$RawChangeInfo);

  //Update the ChangeInfo                        
  $ActionMsg = testEditChange($PostChangeInfo);
  unset($PostChangeInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Add a new Change In DB*/
function AddNewChange($ImportFields,$ImportItem)
{
	$PostChangeInfo = BuildPostAddChangeInfo($ImportFields,$ImportItem);
	
  $ActionMsg = testOpenChange($PostChangeInfo);

  unset($PostChangeInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Import the ChangeList to the DB*/
function ImportChanges($ImportItemList,$ImportFields,$ImportFieldArray,$ChangeIDColumn,$_LANG)
{
        $ImportTotalItem = 0;
        $ImportSuccessItem = 0;
        $ImportFailItem = 0;
        $ImportAddItem = 0;
        $ImportUpdateItem = 0;
        $ErrorMsg = '';
				$FieldCount = count($ImportFields);
				$ItemFieldCount = 0;
				
         foreach($ImportItemList as $ImportItemListKey => $ImportItemListValue)
         {     
              $ImportTotalItem = $ImportTotalItem + 1;
              
              //Confirm if ImportItem is NULL
              if(!$ImportItemListValue)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' failed:[ImportItem is empty or with empty field(s)]\n';		
              		Rainy_Debug("Import row [{$ImportTotalItem}] Failed: ImportItemListValue is NULL",__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get ChangeID
              $ChangeID = $ImportItemListValue[$ChangeIDColumn];
              
              //Check The ImportItem Field Count
              $ItemFieldCount = count($ImportItemListValue);
              if($FieldCount != $ItemFieldCount)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' ChangeID['.$ChangeID.'] failed:[ImportItem have empty field(s)]\n';
              		Rainy_Debug("Import row [{$ImportTotalItem}] ChangeID [{$ChangeID}] Failed: ItemFieldCount ='{$ItemFieldCount}' [$FieldCount]",__FUNCTION__,__LINE__,__FILE__);				
              		//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get ChangeInfo by ChangeID
              $ChangeInfo = GetRawChangeInfo($ChangeID);
              if($ChangeInfo)
              {
              		//ChangeID存在，则更新Change相关消息
                  if(UpdateChangeInfo($ChangeID,$ImportFieldArray,$ImportItemListValue) == true)
                  {
                  		$ImportSuccessItem = $ImportSuccessItem + 1;          
                  		$ImportUpdateItem = $ImportUpdateItem + 1;
                  }
                  else
                  {
                  		$ErrorMsg .= 'Import row '.$ImportTotalItem.' ChangeID['.$ChangeID.'] failed:[UpdateChangeInfo()]\n';
                  		Rainy_Debug("Import row [{$ImportTotalItem}] ChangeID [{$ChangeID}] Failed: UpdateChangeInfo()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
                  }
              }//end edit bug
		      		else 
		      		{
		      				//ChangeID不存在，则新增Change         
		 	        		if(AddNewChange($ImportFieldArray,$ImportItemListValue) == true)
		 	            {
		 	            		$ImportSuccessItem = $ImportSuccessItem + 1;          
 			            		$ImportAddItem = $ImportAddItem + 1; 
		 	            }
		 	            else
		 	            {
		 	            		$ErrorMsg .= 'Import row '.$ImportTotalItem.' ChangeID['.$ChangeID.'] failed:[AddNewChange()]\n';
		 	            		Rainy_Debug("Import row [{$ImportTotalItem}] ChangeID [{$ChangeID}] Failed: AddNewChange()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
		 	            }
              }
         }
         $ImportFailItem = $ImportTotalItem-$ImportSuccessItem;
         $FinishAlert = str_replace('{param0}',$ImportTotalItem, $_LANG['ChangeImportFinished']);
         $FinishAlert = str_replace('{param1}',$ImportSuccessItem, $FinishAlert);
         $FinishAlert = str_replace('{param2}',$ImportFailItem, $FinishAlert);
         $FinishAlert = str_replace('{param3}',$ImportAddItem, $FinishAlert);
         $FinishAlert = str_replace('{param4}',$ImportUpdateItem, $FinishAlert);
         if("" != $ErrorMsg)
         {
                $ErrorMsg = '\n'.$ErrorMsg;
         }
         //Rainy_Debug($ErrorMsg,__FUNCTION__,__LINE__,__FILE__);
         sysObFlushJs("alert('{$FinishAlert}{$ErrorMsg}');");
}

function ParseImportChangeXML($DomRowArray,$_LANG)
{
		 	//Parse DomRowArray to ImportFields and ImportItemList
		 	$ImportXMLFields = array();
     	$ImportItemList = array();
     	ParseDomRowArrary($DomRowArray,&$ImportXMLFields,&$ImportItemList);
			Rainy_Debug($ImportXMLFields,__FUNCTION__,__LINE__,__FILE__);
			
  		//Conver XML Fields to DB Fields
      $ImportFields = ConvertXMLFieldsToDBFields($ImportXMLFields,$_LANG['ChangeFields']);
      Rainy_Debug($ImportFields,__FUNCTION__,__LINE__,__FILE__);
      
     	//Get the MustImportFields and Optional Import Fields
     	$MustImportFields = GetMustImportFields('Change',$_LANG);
     	$OptionalImportFields = GetOptionalImportFields('Change',$_LANG);
     	
			//MustImportFields Check
			$Importable = MustImportFieldsCheck($ImportFields,$MustImportFields,$_LANG);
			
			//Start to Import
      if($Importable)
      {
      	//Get ChangeID Field's Column Number in ImportFields
      	$ChangeIDColumn = GetFieldColumnNumber('ChangeID',$ImportFields);
      	//Filter the ImportFields with the MustImportFields and OptionalImportFields,Result was in ImportFieldArray
        $ImportFieldArray = FilterImportFields($ImportFields,$MustImportFields,$OptionalImportFields);
				Rainy_Debug($ImportFieldArray,__FUNCTION__,__LINE__,__FILE__);
				
				ImportChanges($ImportItemList,$ImportFields,$ImportFieldArray,$ChangeIDColumn,$_LANG);
      }
      fclose($file);

      sysObFlush(jsGoto("ChangeList.php"));
      exit;
}

/************ Review Import Functions **************************/
function BuildPostEditReviewInfo($ImportFields,$ImportItem,$RawReviewInfo)
{
	$PostReviewInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    $FieldValue = trim($ImportItem[$Key]);
    $PostReviewInfo[$FieldName] = $FieldValue;
  }
	
	if(empty($PostReviewInfo['ProjectID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ProjectName',$ImportFields);
			$PostReviewInfo['ProjectID'] = GetProjectIDByName($ImportItem[$ColumnNumber]);
	}
	
	if(!isset($PostReviewInfo['ModuleID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ModulePath',$ImportFields);
			$PostReviewInfo['ModuleID'] = GetModuleIDByPath($ImportItem[$ColumnNumber],$PostReviewInfo['ProjectID'],'Review');
	}
  
  $PostReviewInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  $PostReviewInfo['LastActionID'] = testGetLastActionID('Review',$RawReviewInfo['ReviewID']);
  Rainy_Debug($PostReviewInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostReviewInfo;
}

function BuildPostAddReviewInfo($ImportFields,$ImportItem)
{
	$PostReviewInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    if($FieldName == 'ReviewID')
    {
    		continue;
    }
    
    $FieldValue = trim($ImportItem[$Key]);
    $PostReviewInfo[$FieldName] = $FieldValue;
  }
	
	if(empty($PostReviewInfo['ProjectID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ProjectName',$ImportFields);
			$PostReviewInfo['ProjectID'] = GetProjectIDByName($ImportItem[$ColumnNumber]);
	}
	
	if(!isset($PostReviewInfo['ModuleID']))
	{
			$ColumnNumber = GetFieldColumnNumber('ModulePath',$ImportFields);
			$PostReviewInfo['ModuleID'] = GetModuleIDByPath($ImportItem[$ColumnNumber],$PostReviewInfo['ProjectID'],'Review');
	}
  
  $PostReviewInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  Rainy_Debug($PostReviewInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostReviewInfo;
}

/* Update ReviewInfo of Review with ID:BigID*/
function UpdateReviewInfo($ReviewID,$ImportFields,$ImportItem)
{
	//Get RawReviewInfo
	$RawReviewInfo = GetRawReviewInfo($ReviewID);
	                        
	//Check And Set The Import Field Value 
	$PostReviewInfo = BuildPostEditReviewInfo($ImportFields,$ImportItem,$RawReviewInfo);

  //Update the ReviewInfo                        
  $ActionMsg = testEditReview($PostReviewInfo);
  unset($PostReviewInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Add a new Review In DB*/
function AddNewReview($ImportFields,$ImportItem)
{
	$PostReviewInfo = BuildPostAddReviewInfo($ImportFields,$ImportItem);
	
  $ActionMsg = testOpenReview($PostReviewInfo);

  unset($PostReviewInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Import the ReviewList to the DB*/
function ImportReviews($ImportItemList,$ImportFields,$ImportFieldArray,$ReviewIDColumn,$_LANG)
{
        $ImportTotalItem = 0;
        $ImportSuccessItem = 0;
        $ImportFailItem = 0;
        $ImportAddItem = 0;
        $ImportUpdateItem = 0;
        $ErrorMsg = '';
				$FieldCount = count($ImportFields);
				$ItemFieldCount = 0;
				
         foreach($ImportItemList as $ImportItemListKey => $ImportItemListValue)
         {     
              $ImportTotalItem = $ImportTotalItem + 1;
              
              //Confirm if ImportItem is NULL
              if(!$ImportItemListValue)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' failed:[ImportItem is empty or with empty field(s)]\n';		
              		Rainy_Debug("Import row [{$ImportTotalItem}] Failed: ImportItemListValue is NULL",__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get ReviewID
              $ReviewID = $ImportItemListValue[$ReviewIDColumn];
              
              //Check The ImportItem Field Count
              $ItemFieldCount = count($ImportItemListValue);
              if($FieldCount != $ItemFieldCount)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' ReviewID['.$ReviewID.'] failed:[ImportItem have empty field(s)]\n';
              		Rainy_Debug("Import row [{$ImportTotalItem}] ReviewID [{$ReviewID}] Failed: ItemFieldCount ='{$ItemFieldCount}' [$FieldCount]",__FUNCTION__,__LINE__,__FILE__);				
              		//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get ReviewInfo by ReviewID
              $ReviewInfo = GetRawReviewInfo($ReviewID);
              if($ReviewInfo)
              {
              		//ReviewID存在，则更新Review相关消息
                  if(UpdateReviewInfo($ReviewID,$ImportFieldArray,$ImportItemListValue) == true)
                  {
                  		$ImportSuccessItem = $ImportSuccessItem + 1;          
                  		$ImportUpdateItem = $ImportUpdateItem + 1;
                  }
                  else
                  {
                  		$ErrorMsg .= 'Import row '.$ImportTotalItem.' ReviewID['.$ReviewID.'] failed:[UpdateReviewInfo()]\n';
                  		Rainy_Debug("Import row [{$ImportTotalItem}] ReviewID [{$ReviewID}] Failed: UpdateReviewInfo()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
                  }
              }//end edit bug
		      		else 
		      		{
		      				//ReviewID不存在，则新增Review         
		 	        		if(AddNewReview($ImportFieldArray,$ImportItemListValue) == true)
		 	            {
		 	            		$ImportSuccessItem = $ImportSuccessItem + 1;          
 			            		$ImportAddItem = $ImportAddItem + 1; 
		 	            }
		 	            else
		 	            {
		 	            		$ErrorMsg .= 'Import row '.$ImportTotalItem.' ReviewID['.$ReviewID.'] failed:[AddNewReview()]\n';
		 	            		Rainy_Debug("Import row [{$ImportTotalItem}] ReviewID [{$ReviewID}] Failed: AddNewReview()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
		 	            }
              }
         }
         $ImportFailItem = $ImportTotalItem-$ImportSuccessItem;
         $FinishAlert = str_replace('{param0}',$ImportTotalItem, $_LANG['ReviewImportFinished']);
         $FinishAlert = str_replace('{param1}',$ImportSuccessItem, $FinishAlert);
         $FinishAlert = str_replace('{param2}',$ImportFailItem, $FinishAlert);
         $FinishAlert = str_replace('{param3}',$ImportAddItem, $FinishAlert);
         $FinishAlert = str_replace('{param4}',$ImportUpdateItem, $FinishAlert);
         if("" != $ErrorMsg)
         {
                $ErrorMsg = '\n'.$ErrorMsg;
         }
         //Rainy_Debug($ErrorMsg,__FUNCTION__,__LINE__,__FILE__);
         sysObFlushJs("alert('{$FinishAlert}{$ErrorMsg}');");
}

function ParseImportReviewXML($DomRowArray,$_LANG)
{
		 	//Parse DomRowArray to ImportFields and ImportItemList
		 	$ImportXMLFields = array();
     	$ImportItemList = array();
     	ParseDomRowArrary($DomRowArray,&$ImportXMLFields,&$ImportItemList);
			Rainy_Debug($ImportXMLFields,__FUNCTION__,__LINE__,__FILE__);
			
  		//Conver XML Fields to DB Fields
      $ImportFields = ConvertXMLFieldsToDBFields($ImportXMLFields,$_LANG['ReviewFields']);
      Rainy_Debug($ImportFields,__FUNCTION__,__LINE__,__FILE__);
      
     	//Get the MustImportFields and Optional Import Fields
     	$MustImportFields = GetMustImportFields('Review',$_LANG);
     	$OptionalImportFields = GetOptionalImportFields('Review',$_LANG);
     	
			//MustImportFields Check
			$Importable = MustImportFieldsCheck($ImportFields,$MustImportFields,$_LANG);
			
			//Start to Import
      if($Importable)
      {
      	//Get ReviewID Field's Column Number in ImportFields
      	$ReviewIDColumn = GetFieldColumnNumber('ReviewID',$ImportFields);
      	//Filter the ImportFields with the MustImportFields and OptionalImportFields,Result was in ImportFieldArray
        $ImportFieldArray = FilterImportFields($ImportFields,$MustImportFields,$OptionalImportFields);
				Rainy_Debug($ImportFieldArray,__FUNCTION__,__LINE__,__FILE__);
				
				ImportReviews($ImportItemList,$ImportFields,$ImportFieldArray,$ReviewIDColumn,$_LANG);
      }
      fclose($file);

      sysObFlush(jsGoto("ReviewList.php"));
      exit;
}

/************ User Import Functions **************************/
function BuildPostEditUserInfo($ImportFields,$ImportItem,$RawUserInfo)
{
	$PostUserInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    $FieldValue = trim($ImportItem[$Key]);
    $PostUserInfo[$FieldName] = $FieldValue;
  }
  
  $PostUserInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  $PostUserInfo['LastActionID'] = testGetLastActionID('User',$RawUserInfo['UserID']);
  Rainy_Debug($PostUserInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostUserInfo;
}

function BuildPostAddUserInfo($ImportFields,$ImportItem)
{
	$PostUserInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    if($FieldName == 'UserID')
    {
    		continue;
    }
    
    $FieldValue = trim($ImportItem[$Key]);
    $PostUserInfo[$FieldName] = $FieldValue;
  }
  
  $PostUserInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  Rainy_Debug($PostUserInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostUserInfo;
}

/* Update UserInfo of User with ID:BigID*/
function UpdateUserInfo($UserID,$ImportFields,$ImportItem)
{
	//Get RawUserInfo
	$RawUserInfo = GetRawUserInfo($UserID);
	                        
	//Check And Set The Import Field Value 
	$PostUserInfo = BuildPostEditUserInfo($ImportFields,$ImportItem,$RawUserInfo);

  //Update the UserInfo                        
  $ActionMsg = testEditUser2($PostUserInfo);
  unset($PostUserInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Add a new User In DB*/
function AddNewUser($ImportFields,$ImportItem)
{
	$PostUserInfo = BuildPostAddUserInfo($ImportFields,$ImportItem);
	
  $ActionMsg = testOpenUser($PostUserInfo);

  unset($PostUserInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Import the UserList to the DB*/
function ImportUsers($ImportItemList,$ImportFields,$ImportFieldArray,$UserIDColumn,$_LANG)
{
        $ImportTotalItem = 0;
        $ImportSuccessItem = 0;
        $ImportFailItem = 0;
        $ImportAddItem = 0;
        $ImportUpdateItem = 0;
        $ErrorMsg = '';
				$FieldCount = count($ImportFields);
				$ItemFieldCount = 0;
				
         foreach($ImportItemList as $ImportItemListKey => $ImportItemListValue)
         {     
              $ImportTotalItem = $ImportTotalItem + 1;
              
              //Confirm if ImportItem is NULL
              if(!$ImportItemListValue)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' failed:[ImportItem is empty or with empty field(s)]\n';		
              		Rainy_Debug("Import row [{$ImportTotalItem}] Failed: ImportItemListValue is NULL",__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get UserID
              $UserID = $ImportItemListValue[$UserIDColumn];
              
              //Check The ImportItem Field Count
              $ItemFieldCount = count($ImportItemListValue);
              if($FieldCount != $ItemFieldCount)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' UserID['.$UserID.'] failed:[ImportItem have empty field(s)]\n';
              		Rainy_Debug("Import row [{$ImportTotalItem}] UserID [{$UserID}] Failed: ItemFieldCount ='{$ItemFieldCount}' [$FieldCount]",__FUNCTION__,__LINE__,__FILE__);				
              		//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get UserInfo by UserID
              $UserInfo = GetRawUserInfo($UserID);
              if($UserInfo)
              {
              		//UserID存在，则更新User相关消息
                  if(UpdateUserInfo($UserID,$ImportFieldArray,$ImportItemListValue) == true)
                  {
                  		$ImportSuccessItem = $ImportSuccessItem + 1;          
                  		$ImportUpdateItem = $ImportUpdateItem + 1;
                  }
                  else
                  {
                  		$ErrorMsg .= 'Import row '.$ImportTotalItem.' UserID['.$UserID.'] failed:[UpdateUserInfo()]\n';
                  		Rainy_Debug("Import row [{$ImportTotalItem}] UserID [{$UserID}] Failed: UpdateUserInfo()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
                  }
              }//end edit bug
		      		else 
		      		{
		      				//UserID不存在，则新增User         
		 	        		if(AddNewUser($ImportFieldArray,$ImportItemListValue) == true)
		 	            {
		 	            		$ImportSuccessItem = $ImportSuccessItem + 1;          
 			            		$ImportAddItem = $ImportAddItem + 1; 
		 	            }
		 	            else
		 	            {
		 	            		$ErrorMsg .= 'Import row '.$ImportTotalItem.' UserID['.$UserID.'] failed:[AddNewUser()]\n';
		 	            		Rainy_Debug("Import row [{$ImportTotalItem}] UserID [{$UserID}] Failed: AddNewUser()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
		 	            }
              }
         }
         $ImportFailItem = $ImportTotalItem-$ImportSuccessItem;
         $FinishAlert = str_replace('{param0}',$ImportTotalItem, $_LANG['UserImportFinished']);
         $FinishAlert = str_replace('{param1}',$ImportSuccessItem, $FinishAlert);
         $FinishAlert = str_replace('{param2}',$ImportFailItem, $FinishAlert);
         $FinishAlert = str_replace('{param3}',$ImportAddItem, $FinishAlert);
         $FinishAlert = str_replace('{param4}',$ImportUpdateItem, $FinishAlert);
         if("" != $ErrorMsg)
         {
                $ErrorMsg = '\n'.$ErrorMsg;
         }
         //Rainy_Debug($ErrorMsg,__FUNCTION__,__LINE__,__FILE__);
         sysObFlushJs("alert('{$FinishAlert}{$ErrorMsg}');");
}

function ParseImportUserXML($DomRowArray,$_LANG)
{
		 	//Parse DomRowArray to ImportFields and ImportItemList
		 	$ImportXMLFields = array();
     	$ImportItemList = array();
     	ParseDomRowArrary($DomRowArray,&$ImportXMLFields,&$ImportItemList);
			Rainy_Debug($ImportXMLFields,__FUNCTION__,__LINE__,__FILE__);
			
  		//Conver XML Fields to DB Fields
      $ImportFields = ConvertXMLFieldsToDBFields($ImportXMLFields,$_LANG['UserFields']);
      Rainy_Debug($ImportFields,__FUNCTION__,__LINE__,__FILE__);
      
     	//Get the MustImportFields and Optional Import Fields
     	$MustImportFields = GetMustImportFields('User',$_LANG);
     	$OptionalImportFields = GetOptionalImportFields('User',$_LANG);
     	
			//MustImportFields Check
			$Importable = MustImportFieldsCheck($ImportFields,$MustImportFields,$_LANG);
			
			//Start to Import
      if($Importable)
      {
      	//Get UserID Field's Column Number in ImportFields
      	$UserIDColumn = GetFieldColumnNumber('UserID',$ImportFields);
      	//Filter the ImportFields with the MustImportFields and OptionalImportFields,Result was in ImportFieldArray
        $ImportFieldArray = FilterImportFields($ImportFields,$MustImportFields,$OptionalImportFields);
				Rainy_Debug($ImportFieldArray,__FUNCTION__,__LINE__,__FILE__);
				
				ImportUsers($ImportItemList,$ImportFields,$ImportFieldArray,$UserIDColumn,$_LANG);
      }
      fclose($file);

      sysObFlush(jsGoto("AdminUserList.php"));
      exit;
}

/************ Project Import Functions **************************/
function BuildPostEditProjectInfo($ImportFields,$ImportItem,$RawProjectInfo)
{
	$PostProjectInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    $FieldValue = trim($ImportItem[$Key]);
    $PostProjectInfo[$FieldName] = $FieldValue;
  }
	  
  $PostProjectInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  $PostProjectInfo['LastActionID'] = testGetLastActionID('Project',$RawProjectInfo['ProjectID']);
  Rainy_Debug($PostProjectInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostProjectInfo;
}

function BuildPostAddProjectInfo($ImportFields,$ImportItem)
{
	$PostProjectInfo = array();
  foreach($ImportFields as $Key => $Value)
  {
    $FieldName = trim($Value);
    $FieldValue = trim($ImportItem[$Key]);
    $PostProjectInfo[$FieldName] = $FieldValue;
  }
  
  $PostProjectInfo['TestUserName'] = sysAddSlash($_SESSION['TestUserName']);
  Rainy_Debug($PostProjectInfo,__FUNCTION__,__LINE__,__FILE__);  
	return $PostProjectInfo;
}

/* Update ProjectInfo of Project with ID:BigID*/
function UpdateProjectInfo($ProjectID,$ImportFields,$ImportItem)
{
	//Get RawProjectInfo
	$RawProjectInfo = GetRawProjectInfo($ProjectID);
	                        
	//Check And Set The Import Field Value 
	$PostProjectInfo = BuildPostEditProjectInfo($ImportFields,$ImportItem,$RawProjectInfo);

  //Update the ProjectInfo                        
  $ActionMsg = testEditProject2($PostProjectInfo);
  unset($PostProjectInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Add a new Project In DB*/
function AddNewProject($ImportFields,$ImportItem)
{
	$PostProjectInfo = BuildPostAddProjectInfo($ImportFields,$ImportItem);
	
  $ActionMsg = testOpenProject($PostProjectInfo);

  unset($PostProjectInfo);
  
  if(!empty($ActionMsg['FailedMsg']))
  {
  	Rainy_Debug($ActionMsg['FailedMsg'],__FUNCTION__,__LINE__,__FILE__);
  	return false;
  }
  return true;
}

/*Import the ProjectList to the DB*/
function ImportProjects($ImportItemList,$ImportFields,$ImportFieldArray,$ProjectIDColumn,$_LANG)
{
        $ImportTotalItem = 0;
        $ImportSuccessItem = 0;
        $ImportFailItem = 0;
        $ImportAddItem = 0;
        $ImportUpdateItem = 0;
        $ErrorMsg = '';
				$FieldCount = count($ImportFields);
				$ItemFieldCount = 0;
				
         foreach($ImportItemList as $ImportItemListKey => $ImportItemListValue)
         {     
              $ImportTotalItem = $ImportTotalItem + 1;
              
              //Confirm if ImportItem is NULL
              if(!$ImportItemListValue)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' failed:[ImportItem is empty or with empty field(s)]\n';		
              		Rainy_Debug("Import row [{$ImportTotalItem}] Failed: ImportItemListValue is NULL",__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get ProjectID
              $ProjectID = $ImportItemListValue[$ProjectIDColumn];
              
              //Check The ImportItem Field Count
              $ItemFieldCount = count($ImportItemListValue);
              if($FieldCount != $ItemFieldCount)
              {
             			$ErrorMsg .= 'Import row '.$ImportTotalItem.' ProjectID['.$ProjectID.'] failed:[ImportItem have empty field(s)]\n';
              		Rainy_Debug("Import row [{$ImportTotalItem}] ProjectID [{$ProjectID}] Failed: ItemFieldCount ='{$ItemFieldCount}' [$FieldCount]",__FUNCTION__,__LINE__,__FILE__);				
              		//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
              		continue;
              }
              
              //Get ProjectInfo by ProjectID
              $ProjectInfo = GetRawProjectInfo($ProjectID);
              if($ProjectInfo)
              {
              		//ProjectID存在，则更新Project相关消息
                  if(UpdateProjectInfo($ProjectID,$ImportFieldArray,$ImportItemListValue) == true)
                  {
                  		$ImportSuccessItem = $ImportSuccessItem + 1;          
                  		$ImportUpdateItem = $ImportUpdateItem + 1;
                  }
                  else
                  {
                  		$ErrorMsg .= 'Import row '.$ImportTotalItem.' ProjectID['.$ProjectID.'] failed:[UpdateProjectInfo()]\n';
                  		Rainy_Debug("Import row [{$ImportTotalItem}] ProjectID [{$ProjectID}] Failed: UpdateProjectInfo()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
                  }
              }//end edit bug
		      		else 
		      		{
		      				//ProjectID不存在，则新增Project         
		 	        		if(AddNewProject($ImportFieldArray,$ImportItemListValue) == true)
		 	            {
		 	            		$ImportSuccessItem = $ImportSuccessItem + 1;          
 			            		$ImportAddItem = $ImportAddItem + 1; 
		 	            }
		 	            else
		 	            {
		 	            		$ErrorMsg .= 'Import row '.$ImportTotalItem.' ProjectID['.$ProjectID.'] failed:[AddNewProject()]\n';
		 	            		Rainy_Debug("Import row [{$ImportTotalItem}] ProjectID [{$ProjectID}] Failed: AddNewProject()",__FUNCTION__,__LINE__,__FILE__);
											//Rainy_Debug($ImportItemListValue,__FUNCTION__,__LINE__,__FILE__);
		 	            }
              }
         }
         $ImportFailItem = $ImportTotalItem-$ImportSuccessItem;
         $FinishAlert = str_replace('{param0}',$ImportTotalItem, $_LANG['ProjectImportFinished']);
         $FinishAlert = str_replace('{param1}',$ImportSuccessItem, $FinishAlert);
         $FinishAlert = str_replace('{param2}',$ImportFailItem, $FinishAlert);
         $FinishAlert = str_replace('{param3}',$ImportAddItem, $FinishAlert);
         $FinishAlert = str_replace('{param4}',$ImportUpdateItem, $FinishAlert);
         if("" != $ErrorMsg)
         {
                $ErrorMsg = '\n'.$ErrorMsg;
         }
         //Rainy_Debug($ErrorMsg,__FUNCTION__,__LINE__,__FILE__);
         sysObFlushJs("alert('{$FinishAlert}{$ErrorMsg}');");
}

function ParseImportProjectXML($DomRowArray,$_LANG)
{
		 	//Parse DomRowArray to ImportFields and ImportItemList
		 	$ImportXMLFields = array();
     	$ImportItemList = array();
     	ParseDomRowArrary($DomRowArray,&$ImportXMLFields,&$ImportItemList);
			Rainy_Debug($ImportXMLFields,__FUNCTION__,__LINE__,__FILE__);
			
  		//Conver XML Fields to DB Fields
      $ImportFields = ConvertXMLFieldsToDBFields($ImportXMLFields,$_LANG['ProjectFields']);
      Rainy_Debug($ImportFields,__FUNCTION__,__LINE__,__FILE__);
      
     	//Get the MustImportFields and Optional Import Fields
     	$MustImportFields = GetMustImportFields('Project',$_LANG);
     	$OptionalImportFields = GetOptionalImportFields('Project',$_LANG);
     	
			//MustImportFields Check
			$Importable = MustImportFieldsCheck($ImportFields,$MustImportFields,$_LANG);
			
			//Start to Import
      if($Importable)
      {
      	//Get ProjectID Field's Column Number in ImportFields
      	$ProjectIDColumn = GetFieldColumnNumber('ProjectID',$ImportFields);
      	//Filter the ImportFields with the MustImportFields and OptionalImportFields,Result was in ImportFieldArray
        $ImportFieldArray = FilterImportFields($ImportFields,$MustImportFields,$OptionalImportFields);
				Rainy_Debug($ImportFieldArray,__FUNCTION__,__LINE__,__FILE__);
				
				ImportProjects($ImportItemList,$ImportFields,$ImportFieldArray,$ProjectIDColumn,$_LANG);
      }
      fclose($file);

      sysObFlush(jsGoto("AdminProjectList.php"));
      exit;
}
?>
