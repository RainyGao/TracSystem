<?php
class Mcrypt
{
    private $key;

    public function __construct($key = 'raz')
    {
        $this->key    = $key;
    }

    public function encrypt($encrypt)
    {
        $iv        = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_DES,MCRYPT_MODE_ECB), MCRYPT_RAND); 
        $passcrypt = mcrypt_encrypt(MCRYPT_DES, $this->key, $encrypt, MCRYPT_MODE_ECB, $iv); 
        //base64编码 
        $encode = base64_encode($passcrypt); 
        return $encode;
    }

    public function decrypt($decrypt)
    {
        $decode    = base64_decode($decrypt); 
        $iv        = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_DES, MCRYPT_MODE_ECB), MCRYPT_RAND); 
        $decrypted = mcrypt_decrypt(MCRYPT_DES, $this->key, $decode, MCRYPT_MODE_ECB, $iv); 
        return $decrypted; 
    }
}
?>
