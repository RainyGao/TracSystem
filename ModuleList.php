<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * list project's modules.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

/* Local APIs*/
function GetModuleType($TestMode)
{
		$ModuleType = 'Bug';
		switch($TestMode)
		{
			case 'Case':
			case 'Result':
				$ModuleType = 'Case';
				break;
		}
		return $ModuleType;
}
$projectId = testGetCurrentProjectId();

$TestMode = baseGetTestMode();
$ModuleType = GetModuleType($TestMode);
$ModuleTree = testGetTreeModuleList($projectId, $TestMode . 'List.php?', $ModuleType);

$TPL->assign('ModuleTree', $ModuleTree);

$TPL->display('ModuleList.tpl');
?>
