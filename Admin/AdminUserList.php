<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * admin user list.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require_once("../Include/Init.inc.php");
require("../Include/FuncImportOutport.php");

baseJudgeAdminUserLogin();

if($_REQUEST['reset'])
{
   $_SESSION['SearchUser']='';
}


/* Get pagination */
$DBName = !empty($_CFG['UserDB']) ? 'MyUserDB' : 'MyDB';
global $$DBName;

$Where = "(1)";
if(isset($_GET['SearchUser']))
{
    $SSearchUser = sysStripSlash(trim($_GET['SearchUser']));
    $_SESSION['SearchUser'] =  $SSearchUser;
}

if($SSearchUser != '')
{
    $SSearchUser = mysql_real_escape_string(mysql_real_escape_string($SSearchUser));
    $Where .= " AND ( BINARY {$_CFG[UserTable][UserName]} like '%{$SSearchUser}%' ";
    $Where .= " OR BINARY {$_CFG[UserTable][RealName]} like '%{$SSearchUser}%' ";
    $Where .= " OR BINARY {$_CFG[UserTable][Email]} like '%{$SSearchUser}%' )";
}
else
{
    if($_SESSION['SearchUser'] != '')
    {
       $SSearchUser =  $_SESSION['SearchUser'];
       $SSearchUser = mysql_real_escape_string(mysql_real_escape_string($SSearchUser));
       $Where .= " AND ( BINARY {$_CFG[UserTable][UserName]} like '%{$SSearchUser}%' ";
       $Where .= " OR BINARY {$_CFG[UserTable][RealName]} like '%{$SSearchUser}%' ";
       $Where .= " OR BINARY {$_CFG[UserTable][Email]} like '%{$SSearchUser}%' )";
    }
}


if(!empty($_CFG['UserDB']))
{
    $PageWhere = "WHERE {$Where} ORDER BY {$_CFG[UserTable][UserName]} DESC";
    $OrderBy = "{$_CFG[UserTable][UserName]} DESC";
}
else
{
    //$PageWhere = "WHERE IsDroped = '0' ORDER BY UserID DESC";
    $PageWhere = "WHERE {$Where} ORDER BY UserID DESC";
    $OrderBy = "UserID DESC";
}
$OrderBy = !empty($_CFG['UserDB']) ? "{$_CFG[UserTable][UserName]} DESC" : "UserID DESC";
$Pagination = new Page($_CFG['UserTable']['TableName'], '', '', '', $PageWhere, '?SearchUser='.sysAddSlash($_SESSION['SearchUser']), $$DBName);
$LimitNum = $Pagination->LimitNum();

//UserList的导入处理代码
if($_FILES['file']['error']==2)	//导入成功
{
      sysObFlushJs("alert('{$_LANG['ImportFileExceed']}');");
}
elseif($_FILES['file']['error']==0)
{
        if($_FILES['file']['tmp_name'])   // 如果文件存在则进行批量导入
        {
	  				$findtype=strtolower(strrchr($_FILES['file']['name'],"."));

          	if($findtype!='.xml')	//文件必须以.xml结尾
          	{
            		sysObFlushJs("alert('{$_LANG['ImportFileSuffixError']}');");
          	}
          	else 
          	{
          			//读取文件内容到FileContents
		  					$file=fopen($_FILES['file']['tmp_name'],"r");
		  					$FileContents = fread($file, filesize($_FILES['file']['tmp_name']));
		  					//Rainy_Debug($FileContents);
		  					$FileContents = preg_replace('/<Font.*>|<\/Font>/Us', '', $FileContents);
								//将文件内容解析成变量，并存储在Dom变量中	
		            $Dom =  XML_unserialize($FileContents);
    		        $DomRowArray = $Dom[Workbook][Worksheet][Table][Row];
								//解析DomRowArray 并导入至数据库中
            		ParseImportUserXML($DomRowArray,$_LANG);
        	}
    		}
}

/* Get user list */
$UserList = testGetAllUserList($Where, $OrderBy, $LimitNum);
$UserNameList = testGetOneDimUserList();


foreach($UserList as $UserName => $UserInfo)
{
    $SUserName = mysql_real_escape_string(mysql_real_escape_string($UserInfo['UserName']));
    $GroupACL = dbGetList('TestGroup', '', "GroupUser LIKE '%," . $SUserName . ",%'");
    $UserGroupList = array();
    foreach($GroupACL as $Key => $GroupInfo)
    {
        $UserGroupList[$GroupInfo['GroupID']] = $GroupInfo['GroupName'];
    }
    $UserList[$UserName]['UserGroupListHTML'] = htmlSelect($UserGroupList, 'UserGroupList','', '', 'class="FullSelect"');
    $UserList[$UserName]['AuthModeName'] = $_LANG['AuthMode'][$UserInfo['AuthMode']];
}

/* Assign */
$TPL->assign('PaginationHtml', $Pagination->show('right', 'margin-right:20px'));
$TPL->assign('UserList', $UserList);
$TPL->assign('UserNameList', $UserNameList);

/* Display the template file. */
$OtherUserDB = !empty($_CFG['UserDB']) ? 1 : 0;
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('OtherUserDB', $OtherUserDB);
$TPL->assign('NavActiveUser', ' class="Active"');
$TPL->assign('SearchUser', $_SESSION['SearchUser']);

$TPL->display('Admin/UserList.tpl');
?>
