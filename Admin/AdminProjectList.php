<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * admin project list.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require_once("../Include/Init.inc.php");
require("../Include/FuncImportOutport.php");

baseJudgeAdminUserLogin();

if($_REQUEST['reset'])
{
   $_SESSION['SearchProject']='';
}

$Where = '';
if($_SESSION['TestIsAdmin'])
{
    $Where = '1';
}
elseif($_SESSION['TestIsProjectAdmin'])
{
    $Where = " ProjectManagers LIKE '%," . mysql_real_escape_string(mysql_real_escape_string($_SESSION['TestUserName'])) . ",%'";
}

if(isset($_GET['SearchProject']))
{
    $SearchProject = trim($_GET['SearchProject']);
    $_SESSION['SearchProject'] =  $SearchProject;
}

if($SearchProject != '')
{
    $Where .= " AND ( BINARY ProjectName like '%{$SearchProject}%') ";
}
else
{
    if($_SESSION['SearchProject'] != '')
    {
       $SearchProject =  $_SESSION['SearchProject'];
       $Where .= " AND ( BINARY ProjectName like '%{$SearchProject}%')";
    }
}

/* Get pagination */
$Pagination = new Page('TestProject', '', '', '', 'WHERE ' . $Where . ' ORDER BY DisplayOrder DESC, ProjectID DESC', '', $MyDB);
$LimitNum = $Pagination->LimitNum();

//ProjectList的导入处理代码
if($_FILES['file']['error']==2)	//导入成功
{
      sysObFlushJs("alert('{$_LANG['ImportFileExceed']}');");
}
elseif($_FILES['file']['error']==0)
{
        if($_FILES['file']['tmp_name'])   // 如果文件存在则进行批量导入
        {
	  				$findtype=strtolower(strrchr($_FILES['file']['name'],"."));

          	if($findtype!='.xml')	//文件必须以.xml结尾
          	{
            		sysObFlushJs("alert('{$_LANG['ImportFileSuffixError']}');");
          	}
          	else 
          	{
          			//读取文件内容到FileContents
		  					$file=fopen($_FILES['file']['tmp_name'],"r");
		  					$FileContents = fread($file, filesize($_FILES['file']['tmp_name']));
		  					//Rainy_Debug($FileContents);
		  					$FileContents = preg_replace('/<Font.*>|<\/Font>/Us', '', $FileContents);
								//将文件内容解析成变量，并存储在Dom变量中	
		            $Dom =  XML_unserialize($FileContents);
    		        $DomRowArray = $Dom[Workbook][Worksheet][Table][Row];
								//解析DomRowArray 并导入至数据库中
            		ParseImportProjectXML($DomRowArray,$_LANG);
        	}
    		}
}

/* Get project list */
$ProjectList = testGetProjectList($Where, ' DisplayOrder DESC, ProjectID DESC', $LimitNum);
$UserList = testGetUserList();

/* Get project list */
foreach($ProjectList as $ProjectID => $ProjectInfo)
{
    $GroupACL = dbGetList('TestGroup', '', "GroupID " . dbCreateIN($ProjectInfo['ProjectGroupIDs']));
    $ProjectGroupList = array();
    foreach($GroupACL as $Key => $GroupInfo)
    {
        $ProjectGroupList[$GroupInfo['GroupID']] = $GroupInfo['GroupName'];
    }
    asort($ProjectGroupList);
    $ProjectManagerNameList = array();
    foreach($ProjectInfo['ProjectManagerList'] as $UserName)
    {
        $ProjectManagerNameList[$UserName] = $UserList[$UserName]['PreAppendName'];
    }
    asort($ProjectManagerNameList);
    $ProjectList[$ProjectID]['ProjectGroupListHTML'] = htmlSelect($ProjectGroupList, 'ProjectGroupList','', '', 'class="FullSelect"');
    $ProjectList[$ProjectID]['ProjectManagerListHTML'] = htmlSelect($ProjectManagerNameList, 'ProjectManagerNameList','', '', 'class="FullSelect"');
    $ProjectList[$ProjectID]['LastEditedByName'] = $UserList[$ProjectInfo['LastEditedBy']]['RealName'];
    $ProjectList[$ProjectID]['AddedByName'] = $UserList[$ProjectInfo['AddedBy']]['RealName'];
}

/* Assign */
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('PaginationHtml', $Pagination->show('right', 'margin-right:20px'));
$TPL->assign('ProjectList', $ProjectList);
$TPL->assign('UserList', $UserList);
$TPL->assign('SearchProject', $_SESSION['SearchProject']);
/* Display the template file. */
$TPL->assign('NavActivePro', ' class="Active"');
$TPL->display('Admin/ProjectList.tpl');
?>
