<?php
/**
 * BugFree is free software under the terms of the FreeBSD License.
 *
 * view, new, edit, resolve, close, activate a review.
 *
 * @link        http://www.bugfree.org.cn
 * @package     BugFree
 */
/* Init BugFree system. */
require('Include/Init.inc.php');

/***** Local APIs ******/
function GetReviewIdInfoList($_SESSION)
{
		//Set WhereStr 
    $WhereStr = "{$_SESSION['TestUserACLSQL']} AND {$_SESSION['ReviewQueryCondition']}";
	
		//Set OrderByStr
    if(empty($_SESSION['ReviewOrderBy']))
    {
        $_SESSION['ReviewOrderBy']['OrderBy'] = ' ReviewID DESC';
        $_SESSION['ReviewOrderBy']['OrderByColumn'] = 'ReviewID';
        $_SESSION['ReviewOrderBy']['OrderByType'] = 'DESC';
    }
    
		if('ReviewID' == $_SESSION['ReviewOrderBy']['OrderByColumn'])
    {
        $OrderByStr = "{$_SESSION['ReviewOrderBy']['OrderByColumn']} {$_SESSION['ReviewOrderBy']['OrderByType']}";
    }
    else
    {
        $OrderByStr = "{$_SESSION['ReviewOrderBy']['OrderByColumn']} {$_SESSION['ReviewOrderBy']['OrderByType']},ReviewID ASC";
    }

    $ReviewIdInfoList = dbGetList('ReviewInfo','ReviewID',$WhereStr,'',$OrderByStr,'');
		return $ReviewIdInfoList;
}


function BuildHtmlAssignUserSelectList($ProjectUserList,$ReviewInfo)
{
		$SelectedUser = $ReviewInfo['AssignedTo'];
   	$UserSelectList = htmlSelect($ProjectUserList, 'AssignedTo', '',$SelectedUser, 'class="NormalSelect MyInput RequiredField"');
		return $UserSelectList;
}

function BuildHtmlAuthorSelectList($ProjectUserList,$ReviewInfo)
{
		$SelectedUser = $ReviewInfo['Author'];
   	$UserSelectList = htmlSelect($ProjectUserList, 'Author', '',$SelectedUser, 'class="NormalSelect MyInput RequiredField"');
		return $UserSelectList;
}

function BuildHtmlModeratorSelectList($ProjectUserList,$ReviewInfo)
{
		$SelectedUser = $ReviewInfo['Moderator'];
   	$UserSelectList = htmlSelect($ProjectUserList, 'Moderator', '',$SelectedUser, 'class="NormalSelect MyInput RequiredField"');
		return $UserSelectList;
}

function BuildHtmlRecorderSelectList($ProjectUserList,$ReviewInfo)
{
		$SelectedUser = $ReviewInfo['Recorder'];
   	$UserSelectList = htmlSelect($ProjectUserList, 'Recorder', '',$SelectedUser, 'class="NormalSelect MyInput RequiredField"');
		return $UserSelectList;
}

function BuildHtmlReviewTypeSelectList($ReviewTypeList,$ReviewInfo)
{
	$ReviewTypeSelectList = htmlSelect($ReviewTypeList, 'ReviewType', '', $ReviewInfo['ReviewType'], 'class="MyInput RequiredField"');
	return $ReviewTypeSelectList;
}

function BuildHtmlReviewConclusionSelectList($ReviewConclusionList,$ReviewInfo)
{
	$ReviewConclusionSelectList = htmlSelect($ReviewConclusionList, 'ReviewConclusion', '', $ReviewInfo['ReviewConclusion'], 'class="MyInput RequiredField"');
	return $ReviewConclusionSelectList;
}

function BuildHtmlStartTimeSelectList($TimeList,$ReviewInfo)
{
	$StartTimeSelectList = htmlSelect($TimeList, 'StartTime', '', $ReviewInfo['StartTime'], 'class="MyInput"');
	return $StartTimeSelectList;
}

function BuildHtmlEndTimeSelectList($TimeList,$ReviewInfo)
{
	$EndTimeSelectList = htmlSelect($TimeList, 'EndTime', '', $ReviewInfo['EndTime'], 'class="MyInput"');
	return $EndTimeSelectList;
}

sysXajaxRegister("xProjectSetSlaveModule,xSetModuleOwner,xCreateSelectDiv,xProjectSetAssignedUser,xDeleteTestFile,xFillCustomedField");
$DisplayValue = false;

//Set ModuleType
$ModuleType = 'Bug';	//Review will use the same module with Bug

//Get the ReviewID and ActionType From Request Variables
$ReviewID = $_REQUEST['ReviewID'];
$ActionType = $_REQUEST['ActionType'];

//Get ReviewInfo From DataBase
$Columns = '*';
$ReviewInfo = dbGetRow('ReviewInfo',$Columns,"ReviewID = '{$ReviewID}' AND {$_SESSION[TestUserACLSQL]} AND IsDroped = '0'");
if(!$ReviewInfo['ReviewID'] && $ActionType != 'OpenReview')
{
    sysErrorMsg();
}

//OpenReview was triggered by Copy Action 
if($ReviewID > 0 && $ActionType == 'OpenReview')
{
    $ProjectID = $ReviewInfo['ProjectID'];
    $ModuleID = $ReviewInfo['ModuleID'];
    unset($ReviewInfo['LastEditedBy']);
    unset($ReviewInfo['LastEditedDate']);
    unset($ReviewInfo['ResolvedBy']);
    unset($ReviewInfo['ResolvedBuild']);
    unset($ReviewInfo['ResolvedDate']);
    unset($ReviewInfo['ClosedBy']);
    unset($ReviewInfo['ClosedDate']);
}

if($ActionType == 'OpenReview')	//Open Review Action
{
    /* Create PrjectList And ModuleList */
    $ProjectID = $ProjectID != '' ? $ProjectID : $_SESSION['TestCurrentProjectID'];
    $ModuleID = $ModuleID != '' ? $ModuleID : $_SESSION['TestCurrentModuleID'];
    $ModuleInfo = dbGetRow('TestModule','*',"ModuleID = '{$ModuleID}'");

    $ReviewInfo['ReviewStatus'] = 'Active';
    $ReviewInfo['ReviewStatusName'] = $_LANG['ReviewStatus'][$ReviewInfo['ReviewStatus']];

    $PreReviewID = 0;
    $NextReviewID = 0;

    $ModuleSelected = $ModuleID;

    //If Review was created based on Change
    if($_GET['ChangeID'] != '')
    {
        $ChangeID = $_GET['ChangeID'];
        $ChangeInfo = dbGetRow('ChangeInfo',$Columns,"ChangeID = '{$ChangeID}' AND {$_SESSION[TestUserACLSQL]}");
        if(!empty($ChangeInfo))
        {
            $ProjectID = $ChangeInfo['ProjectID'];
            $ModuleID = $ChangeInfo['ModuleID'];
            $ModuleInfo = dbGetRow('TestModule','ModuleName',"ModuleID = '{$ModuleID}'");
            $ModuleSelected = $ModuleInfo['ModuleName'];
            $ReviewInfo['ReviewTitle'] = $ChangeInfo['ChangeTitle'];
            $ReviewInfo['ReviewType'] = $ChangeInfo['ChangeType'];
            $ReviewInfo['ProjectID'] = $ChangeInfo['ProjectID'];
            $ReviewInfo['ModuleID'] = $ChangeInfo['ModuleID'];
            $ReviewInfo['Author'] = $ChangeInfo['AssignedTo'];
            $ReviewInfo['ChangeID'] = $ChangeID;
        }
    }
}
else
{
    /* Create PrjectList And ModuleList */
    $ProjectID = $ReviewInfo['ProjectID'];
    $ModuleID = $ReviewInfo['ModuleID'];
    $ModuleSelected = $ModuleID;
    
    //Get original Change's Titlte
    if(!empty($ReviewInfo['ChangeID']))
    {
        $ChangeInfo = dbGetRow('ChangeInfo',$Columns,"ChangeID = '{$ReviewInfo['ChangeID']}' AND {$_SESSION[TestUserACLSQL]}");
        $ReviewInfo['ChangeTitle'] = $ChangeInfo['ChangeTitle'];
    }
		
		//Get ReviewIdInfoList	
    $ReviewIdInfoList = GetReviewIdInfoList($_SESSION);
    $PreNextIdArr = getPreNextId($ReviewIdInfoList,'ReviewID',$ReviewID);
    $PreReviewID = $PreNextIdArr[0];
    $NextReviewID = $PreNextIdArr[1];

		//Get the UserList of Project       
    $UserList = testGetProjectUserNameList($ReviewInfo['ProjectID']);
    
    $ReviewInfo = testSetReviewMultiInfo($ReviewInfo, $UserList);

    $ReviewActionList = testGetActionAndFileList('Review', $ReviewInfo['ReviewID']);
    $LastActionID = testGetLastActionID('Review',$ReviewInfo['ReviewID']);
    $ReviewInfo['ActionList'] = $ReviewActionList['ActionList'];
    $ReviewInfo['FileList'] = $ReviewActionList['FileList'];
}

if($ActionType == 'Activated' && $ReviewInfo['ReviewStatus'] == 'Active')
{
    jsGoto('Review.php?ReviewID=' . $ReviewID);
}
if($ActionType == 'Resolved' && $ReviewInfo['ReviewStatus'] == 'Resolved')
{
    jsGoto('Review.php?ReviewID=' . $ReviewID);
}
if($ActionType == 'Closed' && $ReviewInfo['ReviewStatus'] == 'Closed')
{
    jsGoto('Review.php?ReviewID=' . $ReviewID);
}

if($ActionType == 'OpenReview')
{
    $ReviewID = -1;
}

$OnReviewStr = 'onchange="';
$OnReviewStr .= 'xajax_xProjectSetSlaveModule(this.value, \'SlaveModuleList\', \'ModuleID\', \'Review\');';
$OnReviewStr .= 'xajax_xProjectSetAssignedUser(this.value);';
$OnReviewStr .= 'xajax_xFillCustomedField(\'CustomedFieldSet\', this.value, \'Review\', ' . $ReviewID . ');';
$OnReviewStr .= '"';
$OnReviewStr .= ' class="MyInput RequiredField"';
$ProjectListSelect = testGetValidProjectSelectList('ProjectID', $ProjectID, $OnReviewStr);

$OnReviewStr = '';
if($ActionType == 'OpenReview')
{
    $OnReviewStr = 'onchange="';
    $OnReviewStr .= 'xajax_xSetModuleOwner(this.value);';
    $OnReviewStr .= '"';
}
$OnReviewStr .= ' class="MyInput RequiredField"';
$ModuleSelectList = testGetSelectModuleList($ProjectID, 'ModuleID', $ModuleSelected, $OnReviewStr, $ModuleType);

$ProjectUserList = testGetProjectUserList($ProjectID, true);

if($ActionType == 'OpenReview')
{
    $ActionTypeName = $_LANG['OpenReview'];
    $ReviewInfo['AssignedTo'] = $ModuleInfo['ModuleOwner'] != '' ? $ModuleInfo['ModuleOwner'] : $ReviewInfo['AssignedTo'];
}
elseif($ActionType == '')
{
    $DisplayValue = true;
    $ActionTypeName = $_LANG['ViewReview'];
}
elseif($ActionType == 'Edited')
{
    $ActionTypeName = $_LANG['EditReview'];
}
elseif($ActionType == 'Activated')
{
    $ActionTypeName = $_LANG['ActiveReview'];
}
elseif($ActionType == 'Resolved')
{
    $ActionTypeName = $_LANG['ResolveReview'];
}
elseif($ActionType == 'Closed')
{
    $ActionTypeName = $_LANG['CloseReview'];
}

$SelectAssignUserList = BuildHtmlAssignUserSelectList($ProjectUserList,$ReviewInfo);
$AuthorSelectList = BuildHtmlAuthorSelectList($ProjectUserList,$ReviewInfo);
$ModeratorSelectList = BuildHtmlModeratorSelectList($ProjectUserList,$ReviewInfo);
$RecorderSelectList = BuildHtmlRecorderSelectList($ProjectUserList,$ReviewInfo);

$ReviewTypeSelectList = BuildHtmlReviewTypeSelectList($_LANG['ReviewTypes'],$ReviewInfo);
$ReviewConclusionSelectList = BuildHtmlReviewConclusionSelectList($_LANG['ReviewConclusions'],$ReviewInfo);
$StartTimeSelectList = BuildHtmlStartTimeSelectList($_LANG['TimeList'],$ReviewInfo);
$EndTimeSelectList = BuildHtmlEndTimeSelectList($_LANG['TimeList'],$ReviewInfo);


$CustomedFieldHtml = testGetCustomedFieldHtml($ProjectID, 'Review', $ReviewInfo['ReviewID'], $DisplayValue);

if($ActionType == 'OpenReview')
{
    $TPL->assign('HeaderTitle', $_LANG['OpenReview']);
}
else
{
    $TPL->assign('HeaderTitle', 'Review #' . $ReviewInfo['ReviewID'] . ' ' . $ReviewInfo['ReviewTitle']);
}
$ConfirmArray = array('ReplyNote'=>'ReplyNote') + $_LANG['ReviewFields'];
$TPL->assign('ConfirmIds', jsArray($ConfirmArray, 'Key'));

$TPL->assign('ReviewInfo', $ReviewInfo);
$TPL->assign('PreReviewID', $PreReviewID);
$TPL->assign('NextReviewID', $NextReviewID);

$TPL->assign('ProjectID', $ProjectID);
$TPL->assign('ModuleID', $ModuleID);
$TPL->assign('ProjectList', $ProjectListSelect);
$TPL->assign('ModuleList', $ModuleSelectList);
$TPL->assign('ReviewTypeList', $ReviewTypeSelectList);
$TPL->assign('ReviewConclusionList', $ReviewConclusionSelectList);
$TPL->assign('StartTimeSelectList', $StartTimeSelectList);
$TPL->assign('EndTimeSelectList', $EndTimeSelectList);
$TPL->assign('ProjectUserList', $SelectUserList);
$TPL->assign('AssignedToUserList', $SelectAssignUserList);
$TPL->assign('AuthorSelectList', $AuthorSelectList);
$TPL->assign('ModeratorSelectList', $ModeratorSelectList);
$TPL->assign('RecorderSelectList', $RecorderSelectList);
$TPL->assign('OpenedByUserList', $SelectOpenUserList);
$TPL->assign('ActionType', $ActionType);
$TPL->assign('ActionTypeName', $ActionTypeName);
$TPL->assign('ActionRealName', $_SESSION['TestRealName']);
$TPL->assign('TestIsAdmin', $_SESSION['TestIsAdmin']);
$TPL->assign('ActionDate', date('Y-m-d'));
$TPL->assign('DefaultReviewValue', $DefaultReviewValue);
$TPL->assign('LastActionID',$LastActionID);
$TPL->assign('ReviewFields', $_LANG['ReviewFields']);
$TPL->assign('CustomedFieldHtml', $CustomedFieldHtml);

$TPL->display('Review.tpl');
?>
